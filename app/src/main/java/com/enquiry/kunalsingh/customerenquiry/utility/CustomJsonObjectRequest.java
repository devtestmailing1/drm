package com.enquiry.kunalsingh.customerenquiry.utility;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * CustomJsonObjectRequest Class
 * Created by Kunal Singh
 * Date 08 January 2017
 */
public class CustomJsonObjectRequest extends JsonObjectRequest
{
    public CustomJsonObjectRequest(int method, String url, JSONObject jsonRequest, Response.Listener listener, Response.ErrorListener errorListener)
    {
        super(method, url, jsonRequest, listener, errorListener);
    }

    @Override
    public Map getHeaders() throws AuthFailureError {
        HashMap<String, String> params = new HashMap<String, String>();
       // params.put("Username", "wipro_user");
        //params.put("Password", "ieSV55Qc+eQOaYDRSha/AjzNTJE=");
        return params;
    }


}
