package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Kunal Singh on 21-05-2018.
 */
public class FetchModelGroupFamilyModelData implements Parcelable{

    @Expose
    @SerializedName("registrationNo")
    public String registrationNo;
    @Expose
    @SerializedName("modelDesc")
    public String modelDesc;
    @Expose
    @SerializedName("modelFamilyDesc")
    public String modelFamilyDesc;
    @Expose
    @SerializedName("modelGroupDesc")
    public String modelGroupDesc;
    @Expose
    @SerializedName("modelId")
    public int modelId;
    @Expose
    @SerializedName("modelGroupId")
    public int modelGroupId;
    @Expose
    @SerializedName("modelFamilyId")
    public int modelFamilyId;
    @Expose
    @SerializedName("imeiNumber")
    public String imeiNumber;
    @Expose
    @SerializedName("deviceModel")
    public String deviceModel;
    @Expose
    @SerializedName("osName")
    public String osName;
    @Expose
    @SerializedName("appVersion")
    public String appVersion;
    @Expose
    @SerializedName("mobileNumber")
    public String mobileNumber;

    protected FetchModelGroupFamilyModelData(Parcel in) {
        registrationNo = in.readString();
        modelDesc = in.readString();
        modelFamilyDesc = in.readString();
        modelGroupDesc = in.readString();
        modelId = in.readInt();
        modelGroupId = in.readInt();
        modelFamilyId = in.readInt();
        imeiNumber = in.readString();
        deviceModel = in.readString();
        osName = in.readString();
        appVersion = in.readString();
        mobileNumber = in.readString();
    }

    public static final Creator<FetchModelGroupFamilyModelData> CREATOR = new Creator<FetchModelGroupFamilyModelData>() {
        @Override
        public FetchModelGroupFamilyModelData createFromParcel(Parcel in) {
            return new FetchModelGroupFamilyModelData(in);
        }

        @Override
        public FetchModelGroupFamilyModelData[] newArray(int size) {
            return new FetchModelGroupFamilyModelData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(registrationNo);
        parcel.writeString(modelDesc);
        parcel.writeString(modelFamilyDesc);
        parcel.writeString(modelGroupDesc);
        parcel.writeInt(modelId);
        parcel.writeInt(modelGroupId);
        parcel.writeInt(modelFamilyId);
        parcel.writeString(imeiNumber);
        parcel.writeString(deviceModel);
        parcel.writeString(osName);
        parcel.writeString(appVersion);
        parcel.writeString(mobileNumber);
    }
}
