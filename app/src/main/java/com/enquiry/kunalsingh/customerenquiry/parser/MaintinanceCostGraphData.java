package com.enquiry.kunalsingh.customerenquiry.parser;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kunal Singh on 27-06-2018.
 */
public class MaintinanceCostGraphData implements Parcelable{

    @Expose
    @SerializedName("maintenanceCostList")
    public List<MaintenanceCostList> maintenanceCostList;
    @Expose
    @SerializedName("priceList")
    public List<PriceList> priceList;
    @Expose
    @SerializedName("yearList")
    public List<YearList> yearList;
    @Expose
    @SerializedName("chassisNo")
    public String chassisNo;
    @Expose
    @SerializedName("registrationNo")
    public String registrationNo;
    @Expose
    @SerializedName("dataFor")
    public String dataFor;
    @Expose
    @SerializedName("imeiNumber")
    public String imeiNumber;
    @Expose
    @SerializedName("deviceModel")
    public String deviceModel;
    @Expose
    @SerializedName("osName")
    public String osName;
    @Expose
    @SerializedName("appVersion")
    public String appVersion;
    @Expose
    @SerializedName("mobileNumber")
    public String mobileNumber;

    protected MaintinanceCostGraphData(Parcel in) {
        priceList= new ArrayList<PriceList>();
        in.readList(priceList,MaintinanceCostGraphData.PriceList.class.getClassLoader());
        yearList= new ArrayList<YearList>();
        in.readList(yearList,MaintinanceCostGraphData.YearList.class.getClassLoader());
        chassisNo = in.readString();
        registrationNo = in.readString();
        dataFor = in.readString();
        imeiNumber = in.readString();
        deviceModel = in.readString();
        osName = in.readString();
        appVersion = in.readString();
        mobileNumber = in.readString();
    }

    public static final Creator<MaintinanceCostGraphData> CREATOR = new Creator<MaintinanceCostGraphData>() {
        @Override
        public MaintinanceCostGraphData createFromParcel(Parcel in) {
            return new MaintinanceCostGraphData(in);
        }

        @Override
        public MaintinanceCostGraphData[] newArray(int size) {
            return new MaintinanceCostGraphData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(priceList);
        parcel.writeList(yearList);
        parcel.writeString(chassisNo);
        parcel.writeString(registrationNo);
        parcel.writeString(dataFor);
        parcel.writeString(imeiNumber);
        parcel.writeString(deviceModel);
        parcel.writeString(osName);
        parcel.writeString(appVersion);
        parcel.writeString(mobileNumber);
    }

    public static class MaintenanceCostList {
        @Expose
        @SerializedName("totalBillValue")
        public double totalBillValue;
        @Expose
        @SerializedName("billYear")
        public String billYear;
        @Expose
        @SerializedName("id")
        public int id;
    }

    public static class PriceList implements Parcelable{
        @Expose
        @SerializedName("price")
        public String price;

        protected PriceList(Parcel in) {
            price = in.readString();
        }

        public static final Creator<PriceList> CREATOR = new Creator<PriceList>() {
            @Override
            public PriceList createFromParcel(Parcel in) {
                return new PriceList(in);
            }

            @Override
            public PriceList[] newArray(int size) {
                return new PriceList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(price);
        }
    }

    public static class YearList {
        @Expose
        @SerializedName("year")
        public String year;
    }
}
