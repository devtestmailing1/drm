package com.enquiry.kunalsingh.customerenquiry.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.fragment.HomeFragment;
import com.enquiry.kunalsingh.customerenquiry.fragment.ShopDetailsActivity;

import java.util.ArrayList;

/**
 * Created by Kunal Singh on 15-09-2018.
 */
public class ShopListChildAdapter extends RecyclerView.Adapter<ShopListChildAdapter.MyViewHolder> {

    //private List<Movie> moviesList;

    private ShopAreaInformation eventInformation;
    private ArrayList<ShopLists> shopsArrayList;
    private Context context;
    private ItemListener mListener;

    public ShopListChildAdapter(Context context, ArrayList<ShopLists> shopsArrayList) {
        this.shopsArrayList = shopsArrayList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shop_list_child_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder,final int position) {
        ShopLists shopLists = shopsArrayList.get(position);
        holder.shop_list_shop_name.setText(shopLists.getEventName());
        if(shopLists.getShopColor().equalsIgnoreCase("BLUE")){
            holder.shop_list_shop_name.setTextColor(Color.WHITE);
            holder.cardView.setBackgroundColor(Color.BLUE);
        }
        holder.shop_list_shop_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("event name=",shopsArrayList.get(position).getEventId());
                context.startActivity(new Intent(context, ShopDetailsActivity.class).putExtra("ShopID",shopsArrayList.get(position).getEventId()));

            }
        });
    }

    @Override
    public int getItemCount() {
        return shopsArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView shop_list_shop_name;
        public CardView cardView;


        public MyViewHolder(View view) {
            super(view);
            shop_list_shop_name = (TextView) view.findViewById(R.id.shop_list_shop_name);
            cardView = (CardView)view.findViewById(R.id.card_view);

        }
    }

    public interface ItemListener {
        void onItemClick(int shopId);
    }
}

