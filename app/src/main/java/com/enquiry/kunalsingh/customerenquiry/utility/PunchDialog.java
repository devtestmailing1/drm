package com.enquiry.kunalsingh.customerenquiry.utility;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.enquiry.kunalsingh.customerenquiry.R;

/**
 * Created by Kunal Singh on 24-08-2018.
 */
public class PunchDialog extends Dialog {

    public FragmentActivity c;
    public Dialog d;
    public Button btOk,ivClose;
    public String msg,title,header;

    private int type;
    private TextView etMsgToSend;
    private boolean toExit,isCenter;

    private View.OnClickListener listener;

    public PunchDialog(FragmentActivity a, String header, String msg, String title, boolean isCenter, View.OnClickListener listener) {
        super(a);
        this.c = a;
        this.msg = msg;
        this.title=title;
        this.isCenter=isCenter;
        this.listener=listener;
        this.header=header;

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        this.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        setContentView(R.layout.dialog_location);

        TextView headers= findViewById(R.id.Heading);
        headers.setText(header);

        btOk = findViewById(R.id.ok);
        ivClose = findViewById(R.id.close);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PunchDialog.this.dismiss();
            }
        });
        btOk.setOnClickListener(listener);
        TextView txtTitle= findViewById(R.id.time_in);
        if(title!=null && !title.equalsIgnoreCase("")) {
            txtTitle.setText(title);
            txtTitle.setVisibility(View.VISIBLE);
        }
        etMsgToSend = findViewById(R.id.message);
        //etMsgToSend.setText(msg);
        etMsgToSend.setMovementMethod(new ScrollingMovementMethod());
        etMsgToSend.setText(msg);
        if(isCenter)
        {
            etMsgToSend.setGravity(Gravity.CENTER);
        }
        else
        {
            etMsgToSend.setGravity(Gravity.LEFT);
        }

    }

    public void toExit(boolean toExit)
    {
        this.toExit=toExit;
    }


    public void close()
    {
        dismiss();
    }
}
