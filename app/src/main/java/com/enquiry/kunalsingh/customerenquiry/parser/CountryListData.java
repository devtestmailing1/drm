package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CountryListData implements Parcelable {

    @Expose
    @SerializedName("countryList")
    public List<CountryListData.CountryList> countryList;

    protected CountryListData(Parcel in) {
    }

    public static final Creator<CountryListData> CREATOR = new Creator<CountryListData>() {
        @Override
        public CountryListData createFromParcel(Parcel in) {
            return new CountryListData(in);
        }

        @Override
        public CountryListData[] newArray(int size) {
            return new CountryListData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }

    public static class CountryList implements Parcelable{
        @Expose
        @SerializedName("countryName")
        public String countryName;
        @Expose
        @SerializedName("countryId")
        public int countryId;
        @Expose
        @SerializedName("selected")
        public String selected;


        protected CountryList(Parcel in) {
            countryName = in.readString();
            countryId = in.readInt();
            selected = in.readString();
        }

        public static final Creator<CountryListData.CountryList> CREATOR = new Creator<CountryList>() {
            @Override
            public CountryListData.CountryList createFromParcel(Parcel in) {
                return new CountryListData.CountryList(in);
            }

            @Override
            public CountryListData.CountryList[] newArray(int size) {
                return new CountryListData.CountryList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(countryName);
            parcel.writeInt(countryId);
            parcel.writeString(selected);
        }
    }
}