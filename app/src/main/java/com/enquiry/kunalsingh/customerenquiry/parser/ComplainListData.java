package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kunal Singh on 13-06-2018.
 */
public class ComplainListData implements Parcelable{

    @Expose
    @SerializedName("complaintModelList")
    public List<ComplaintModelList> complaintModelList;
    @Expose
    @SerializedName("dataFor")
    public String dataFor;
    @Expose
    @SerializedName("imeiNumber")
    public String imeiNumber;
    @Expose
    @SerializedName("deviceModel")
    public String deviceModel;
    @Expose
    @SerializedName("osName")
    public String osName;
    @Expose
    @SerializedName("appVersion")
    public String appVersion;
    @Expose
    @SerializedName("mobileNumber")
    public String mobileNumber;

    protected ComplainListData(Parcel in) {
        complaintModelList= new ArrayList<ComplainListData.ComplaintModelList>();
        in.readList(complaintModelList,ComplainListData.ComplaintModelList.class.getClassLoader());
        dataFor = in.readString();
        imeiNumber = in.readString();
        deviceModel = in.readString();
        osName = in.readString();
        appVersion = in.readString();
        mobileNumber = in.readString();
    }

    public static final Creator<ComplainListData> CREATOR = new Creator<ComplainListData>() {
        @Override
        public ComplainListData createFromParcel(Parcel in) {
            return new ComplainListData(in);
        }

        @Override
        public ComplainListData[] newArray(int size) {
            return new ComplainListData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(complaintModelList);
        parcel.writeString(dataFor);
        parcel.writeString(imeiNumber);
        parcel.writeString(deviceModel);
        parcel.writeString(osName);
        parcel.writeString(appVersion);
        parcel.writeString(mobileNumber);
    }

    public static class ComplaintModelList {
        @Expose
        @SerializedName("chassisNo")
        public String chassisNo;
        @Expose
        @SerializedName("registrationNumber")
        public String registrationNumber;
        @Expose
        @SerializedName("branchAddress")
        public String branchAddress;
        @Expose
        @SerializedName("dealerName")
        public String dealerName;
        @Expose
        @SerializedName("complainStatus")
        public String complainStatus;
        @Expose
        @SerializedName("complainType")
        public String complainType;
        @Expose
        @SerializedName("complainDate")
        public String complainDate;
        @Expose
        @SerializedName("complainNo")
        public String complainNo;
    }
}
