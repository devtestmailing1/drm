package com.enquiry.kunalsingh.customerenquiry.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.adapter.ComplainAdapter;
import com.enquiry.kunalsingh.customerenquiry.adapter.ShopAreas;
import com.enquiry.kunalsingh.customerenquiry.adapter.ShopAreaInformation;
import com.enquiry.kunalsingh.customerenquiry.adapter.ShopListParentAdapter;
import com.enquiry.kunalsingh.customerenquiry.adapter.ShopLists;
import com.enquiry.kunalsingh.customerenquiry.adapter.ShopListAdapter;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.parser.HomeDataParser;
import com.enquiry.kunalsingh.customerenquiry.parser.ShoapListData;
import com.enquiry.kunalsingh.customerenquiry.parser.ShopDetailsData;
import com.enquiry.kunalsingh.customerenquiry.parser.ShopListItem;
import com.enquiry.kunalsingh.customerenquiry.utility.PrefManager;
import com.enquiry.kunalsingh.customerenquiry.utility.QueryBuilder;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.enquiry.kunalsingh.customerenquiry.utility.Utils;
import com.getbase.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

/**
 * Created by Kunal Singh on 26-01-2018.
 */

public class HomeFragment extends Fragment implements HomeDataParser.onHomeListener, ComplainAdapter.ItemListener, ShopListAdapter.ItemListener {
    RecyclerView recylShopList;
    ShoapListData shoapListData;
    ComplainAdapter complainAdapter;
    View rootView;
    FloatingActionButton floatingActionButton;
    String actionName,currentLat,currentLong,selProdId,selFabId,selPartyId,selRankId;
    EditText editText;
    private ShopListAdapter shopListAdapter;
    private ShopListItem shopListItem;
    ShopListParentAdapter shop_list_parent_adapter;
    private ArrayList<ShopListItem> shopList = new ArrayList<>();


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            actionName= getArguments().getString("actionName");
            currentLat= getArguments().getString("lattitude");
            currentLong=getArguments().getString("longitude");
            selProdId=getArguments().getString("prodID");
            selFabId=getArguments().getString("fabID");
            selPartyId=getArguments().getString("partyID");
            selRankId=getArguments().getString("rankID");

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        recylShopList = rootView.findViewById(R.id.recylShopList);
        editText = rootView.findViewById(R.id.edtSearchText);
        floatingActionButton=rootView.findViewById(R.id.add_vehicle);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // startActivity(new Intent(getActivity(),AddShopActivity.class));
                startActivity(new Intent(getActivity(),AddShopsActivity.class));
            }
        });
        if(actionName!=null&&actionName.equalsIgnoreCase("Location")){
            editText.setVisibility(View.GONE);
          new HomeDataParser().executeQuery(getActivity(), QueryBuilder.getInstance().getSearchShopAPI(), QueryBuilder.getInstance().generateSearchShopQuery(getActivity(), PrefManager.getKeyAuthToken(getActivity()).trim(), "", "","","","",currentLat,currentLong), this, Settings.SEARCH_SHOP_LIST_ID, true);
        }else if(actionName!=null&&actionName.equalsIgnoreCase("Search")) {
            editText.setVisibility(View.VISIBLE);
            new HomeDataParser().executeQuery(getActivity(), QueryBuilder.getInstance().getSearchShopAPI(), QueryBuilder.getInstance().generateSearchShopQuery(getActivity(), PrefManager.getKeyAuthToken(getActivity()).trim(), "", "","","","","",""), this, Settings.VERIFY_OTP_ID, true);
        }else if(actionName!=null&&actionName.equalsIgnoreCase("Filter")){
            if(selProdId!=null){
                selProdId=selProdId;
            }else{
                selProdId="";
            }
            editText.setVisibility(View.GONE);
            new HomeDataParser().executeQuery(getActivity(), QueryBuilder.getInstance().getSearchShopAPI(), QueryBuilder.getInstance().generateSearchShopQuery(getActivity(), PrefManager.getKeyAuthToken(getActivity()).trim(), "", selFabId,selProdId,selPartyId,selRankId,"",""), this, Settings.SEARCH_SHOP_LIST_ID, true);
        }else if(actionName!=null&&actionName.equalsIgnoreCase("Home")) {
            editText.setVisibility(View.GONE);
            new HomeDataParser().executeQuery(getActivity(), QueryBuilder.getInstance().getSearchShopAPI(), QueryBuilder.getInstance().generateSearchShopQuery(getActivity(), PrefManager.getKeyAuthToken(getActivity()).trim(), "", "","","","","",""), this, Settings.SEARCH_SHOP_LIST_ID, true);
        }else{
            editText.setVisibility(View.GONE);
            //  new HomeDataParser().executeQuery(getActivity(), QueryBuilder.getInstance().verifyOTP(), QueryBuilder.getInstance().generateVerifyOTPQuery(getActivity(), PrefManager.getKeyUserMobileNumber(getActivity()).trim(), PrefManager.getKeyOtp(getActivity()).trim(), PrefManager.getKeyCityId(getActivity()).trim()), this, Settings.VERIFY_OTP_ID, true);
            new HomeDataParser().executeQuery(getActivity(), QueryBuilder.getInstance().getSearchShopAPI(), QueryBuilder.getInstance().generateSearchShopQuery(getActivity(), PrefManager.getKeyAuthToken(getActivity()).trim(), "", "","","","","",""), this, Settings.SEARCH_SHOP_LIST_ID, true);
        }
        return rootView;
    }

    @Override
    public void onSuccess(int id, String message, Object object) {
        if (object != null) {
            if(id==Settings.SEARCH_SHOP_LIST_ID) {
                shoapListData = ((ShoapListData) object);
                shopList.clear();
                ArrayList<ShopAreas> shopAreaArrayList;
                ShopAreaInformation eventInformation = new ShopAreaInformation();
                try {
                    shopAreaArrayList = new ArrayList<>();
                    for(int i=0;i<shoapListData.statelist.size();i++) {
                        for (int j =0; j < shoapListData.statelist.get(i).cityList.size(); j++) {
                            for (int k = 0; k < shoapListData.statelist.get(i).cityList.get(j).areaList.size(); k++) {
                                ShopAreas shopAreas = new ShopAreas();
                                shopAreas.setDate(shoapListData.statelist.get(i).cityList.get(j).cityDesc+" : "+ shoapListData.statelist.get(i).cityList.get(j).areaList.get(k).localityDesc);
                                ArrayList<ShopLists> shopsArrayList = new ArrayList<>();
                                for (int l = 0; l < shoapListData.statelist.get(i).cityList.get(j).areaList.get(k).shopList.size(); l++) {
                                    ShopLists shopLists = new ShopLists();
                                    shopLists.setEventId(String.valueOf(shoapListData.statelist.get(i).cityList.get(j).areaList.get(k).shopList.get(l).shopId));
                                    shopLists.setEventName(shoapListData.statelist.get(i).cityList.get(j).areaList.get(k).shopList.get(l).shopName);
                                    shopLists.setShopColor(shoapListData.statelist.get(i).cityList.get(j).areaList.get(k).shopList.get(l).colour);
                                    shopsArrayList.add(shopLists);
                                }
                                shopAreas.setEventsArrayList(shopsArrayList);
                                shopAreaArrayList.add(shopAreas);
                            }
                        }
                        eventInformation.setShopAreaList(shopAreaArrayList);
                        Log.d("message",eventInformation.toString());
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                //parent recyclerview
                shop_list_parent_adapter = new ShopListParentAdapter(eventInformation,getActivity());
                recylShopList.setHasFixedSize(true);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                recylShopList.setLayoutManager(mLayoutManager);
                recylShopList.setItemAnimator(new DefaultItemAnimator());
                recylShopList.setAdapter(shop_list_parent_adapter);

            }
        }

        if(id==Settings.VERIFY_OTP_ID) {
            if (object != null) {
                shoapListData = ((ShoapListData) object);
                shopList.clear();
                for (int i = 0; i < shoapListData.statelist.size(); i++) {
                    for (int j = 0; j < shoapListData.statelist.get(i).cityList.size(); j++) {
                        for (int k = 0; k < shoapListData.statelist.get(i).cityList.get(j).areaList.size(); k++) {
                            for (int l = 0; l < shoapListData.statelist.get(i).cityList.get(j).areaList.get(k).shopList.size(); l++) {
                                shopListItem = new ShopListItem();
                                shopListItem.setShopID(shoapListData.statelist.get(i).cityList.get(j).areaList.get(k).shopList.get(l).shopId);
                                shopListItem.setShopName(shoapListData.statelist.get(i).cityList.get(j).areaList.get(k).shopList.get(l).shopName);
                                shopListItem.setShopAddress(shoapListData.statelist.get(i).cityList.get(j).areaList.get(k).shopList.get(l).shopAddress);
                                shopListItem.setShopLocality(shoapListData.statelist.get(i).cityList.get(j).areaList.get(k).shopList.get(l).localityName);
                                shopList.add(shopListItem);
                            }
                    }
                }
            }
                shopListAdapter = new ShopListAdapter(getActivity(), shopList, this);
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                recylShopList.setLayoutManager(layoutManager);
                recylShopList.setAdapter(shopListAdapter);
                shopListAdapter.notifyDataSetChanged();
                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        filter(s.toString());
                    }
                });

        }

        }
    }



    @Override
    public void onError(String error, int id, BaseController.ErrorCode code) {
        if(error!=null){
            Utils.HitechToast(getActivity(),error);
        }
    }

    @Override
    public void onItemClick(int shopId) {
        startActivity(new Intent(getActivity(),ShopDetailsActivity.class).putExtra("ShopID",String.valueOf(shopId)));
    }

    private void filter(String text) {
        ArrayList<ShopListItem> filteredList = new ArrayList<>();
        for (ShopListItem item : shopList) {
            if (item.getShopName().toLowerCase().contains(text.toLowerCase())||item.getShopAddress().toLowerCase().contains(text.toLowerCase())) {
                filteredList.add(item);
            }
        }
        shopListAdapter.filterList(filteredList);
    }

}