package com.enquiry.kunalsingh.customerenquiry.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.adapter.ComplainAdapter;
import com.enquiry.kunalsingh.customerenquiry.adapter.FetchFeedbackAdapter;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.parser.FetchFeedbackData;
import com.enquiry.kunalsingh.customerenquiry.parser.FetchFeedbackParser;
import com.enquiry.kunalsingh.customerenquiry.parser.PunchDataParser;
import com.enquiry.kunalsingh.customerenquiry.parser.ShoapListData;
import com.enquiry.kunalsingh.customerenquiry.utility.QueryBuilder;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.getbase.floatingactionbutton.FloatingActionButton;

/**
 * Created by Kunal Singh on 27-08-2018.
 */
public class FetchFeedbackActivity extends AppCompatActivity implements FetchFeedbackParser.onFetchFeedbackListener {
    FloatingActionButton floatingActionButton;
    private String userMob,shopID;
    FetchFeedbackData fetchFeedbackData;
    FetchFeedbackAdapter fetchFeedbackAdapter;
    RecyclerView recylFeedbackList;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fetch_feedback);
        Toolbar toolbar = findViewById(R.id.header);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("Feedback Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        Intent intent = getIntent();
        userMob = intent.getStringExtra("phoneNumber");
        shopID=intent.getStringExtra("shopid");
        new FetchFeedbackParser().executeQuery(this, QueryBuilder.getInstance().getFetchFeedbackAPI(), QueryBuilder.getInstance().generateFetchFeedbackQuery(this, shopID), this, Settings.FETCH_FEEDBACK_ID, true);
        floatingActionButton=findViewById(R.id.add_Feedback);
        recylFeedbackList=findViewById(R.id.recylFeedbackList);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FetchFeedbackActivity.this,FeedbackActivity.class).putExtra("phoneNumber",userMob).putExtra("shopid",shopID));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSuccess(int id, String message, Object object) {
        if(id==Settings.FETCH_FEEDBACK_ID) {
            if (object != null) {
                fetchFeedbackData = ((FetchFeedbackData) object);
                if (fetchFeedbackData.feedbackList != null) {
                    fetchFeedbackAdapter = new FetchFeedbackAdapter(this, fetchFeedbackData.feedbackList);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                    recylFeedbackList.setLayoutManager(layoutManager);
                    recylFeedbackList.setAdapter(fetchFeedbackAdapter);
                } else {
                    recylFeedbackList.setVisibility(View.GONE);
                }
            }
        }

    }

    @Override
    public void onError(String error, int id, BaseController.ErrorCode code) {

    }
}
