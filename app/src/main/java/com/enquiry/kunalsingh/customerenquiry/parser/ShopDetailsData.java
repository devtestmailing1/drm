package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Kunal Singh on 11-08-2018.
 */
public class ShopDetailsData implements Parcelable{

    @Expose
    @SerializedName("stateList")
    public List<StateList> stateList;
    @Expose
    @SerializedName("cityList")
    public List<CityList> cityList;
    @Expose
    @SerializedName("areaList")
    public List<AreaList> areaList;
    @Expose
    @SerializedName("rankingList")
    public List<RankingList> rankingList;
    @Expose
    @SerializedName("partyTypeList")
    public List<PartyTypeList> partyTypeList;
    @Expose
    @SerializedName("productTypeList")
    public List<ProductTypeList> productTypeList;
    @Expose
    @SerializedName("fabricList")
    public List<FabricList> fabricList;
    @Expose
    @SerializedName("shopOwnerMobileNo")
    public String shopOwnerMobileNo;
    @Expose
    @SerializedName("shopOwnerName")
    public String shopOwnerName;
    @Expose
    @SerializedName("shopOwner_Id")
    public int shopOwner_Id;
    @Expose
    @SerializedName("shopAddress")
    public String shopAddress;
    @Expose
    @SerializedName("shopAddressId")
    public int shopAddressId;

    @Expose
    @SerializedName("pin")
    public String pin;
    @Expose
    @SerializedName("mohallaDesc")
    public String mohallaDesc;
    @Expose
    @SerializedName("mohallaId")
    public String mohallaId;
    @Expose
    @SerializedName("areaDesc")
    public String areaDesc;
    @Expose
    @SerializedName("areaId")
    public String areaId;
    @Expose
    @SerializedName("cityDesc")
    public String cityDesc;
    @Expose
    @SerializedName("cityId")
    public String cityId;
    @Expose
    @SerializedName("stateDesc")
    public String stateDesc;
    @Expose
    @SerializedName("stateId")
    public String stateId;
    @Expose
    @SerializedName("countryDesc")
    public String countryDesc;
    @Expose
    @SerializedName("countryId")
    public String countryId;
    @Expose
    @SerializedName("createdBy_Id")
    public String createdBy_Id;
    @Expose
    @SerializedName("isActive")
    public String isActive;
    @Expose
    @SerializedName("latitude")
    public String latitude;
    @Expose
    @SerializedName("longitude")
    public String longitude;
    @Expose
    @SerializedName("ranking")
    public String ranking;
    @Expose
    @SerializedName("ranking_Id")
    public int ranking_Id;
    @Expose
    @SerializedName("partyTypeName")
    public String partyTypeName;
    @Expose
    @SerializedName("partyType_Id")
    public String partyType_Id;
    @Expose
    @SerializedName("fabricName")
    public String fabricName;
    @Expose
    @SerializedName("febricType_Id")
    public String febricType_Id;
    @Expose
    @SerializedName("productName")
    public String productName;
    @Expose
    @SerializedName("productType_Id")
    public String productType_Id;
    @Expose
    @SerializedName("accountNumber")
    public String accountNumber;
    @Expose
    @SerializedName("accountName")
    public String accountName;
    @Expose
    @SerializedName("website")
    public String website;
    @Expose
    @SerializedName("telephone")
    public String telephone;
    @Expose
    @SerializedName("email")
    public String email;
    @Expose
    @SerializedName("purchaserMobile")
    public String purchaserMobile;
    @Expose
    @SerializedName("purchaserName")
    public String purchaserName;
    @Expose
    @SerializedName("shopName")
    public String shopName;
    @Expose
    @SerializedName("shop_Id")
    public int shop_Id;

    @Expose
    @SerializedName("imagePath1")
    public String imagePath1;

    protected ShopDetailsData(Parcel in) {
        stateList = in.createTypedArrayList(StateList.CREATOR);
        cityList = in.createTypedArrayList(CityList.CREATOR);
        areaList = in.createTypedArrayList(AreaList.CREATOR);
        rankingList = in.createTypedArrayList(RankingList.CREATOR);
        partyTypeList = in.createTypedArrayList(PartyTypeList.CREATOR);
        productTypeList = in.createTypedArrayList(ProductTypeList.CREATOR);
        fabricList = in.createTypedArrayList(FabricList.CREATOR);
        shopOwnerMobileNo = in.readString();
        shopOwnerName = in.readString();
        shopOwner_Id = in.readInt();
        shopAddress = in.readString();
        shopAddressId = in.readInt();
        pin = in.readString();
        mohallaDesc = in.readString();
        mohallaId = in.readString();
        areaDesc = in.readString();
        areaId = in.readString();
        cityDesc = in.readString();
        cityId = in.readString();
        stateDesc = in.readString();
        stateId = in.readString();
        countryDesc = in.readString();
        countryId = in.readString();
        createdBy_Id = in.readString();
        isActive = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        ranking = in.readString();
        ranking_Id = in.readInt();
        partyTypeName = in.readString();
        partyType_Id = in.readString();
        fabricName = in.readString();
        febricType_Id = in.readString();
        productName = in.readString();
        productType_Id = in.readString();
        accountNumber = in.readString();
        accountName = in.readString();
        website = in.readString();
        telephone = in.readString();
        email = in.readString();
        purchaserMobile = in.readString();
        purchaserName = in.readString();
        shopName = in.readString();
        shop_Id = in.readInt();
        imagePath1 = in.readString();
    }

    public static final Creator<ShopDetailsData> CREATOR = new Creator<ShopDetailsData>() {
        @Override
        public ShopDetailsData createFromParcel(Parcel in) {
            return new ShopDetailsData(in);
        }

        @Override
        public ShopDetailsData[] newArray(int size) {
            return new ShopDetailsData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(stateList);
        parcel.writeTypedList(cityList);
        parcel.writeTypedList(areaList);
        parcel.writeTypedList(rankingList);
        parcel.writeTypedList(partyTypeList);
        parcel.writeTypedList(productTypeList);
        parcel.writeTypedList(fabricList);
        parcel.writeString(shopOwnerMobileNo);
        parcel.writeString(shopOwnerName);
        parcel.writeInt(shopOwner_Id);
        parcel.writeString(shopAddress);
        parcel.writeInt(shopAddressId);
        parcel.writeString(pin);
        parcel.writeString(mohallaDesc);
        parcel.writeString(mohallaId);
        parcel.writeString(areaDesc);
        parcel.writeString(areaId);
        parcel.writeString(cityDesc);
        parcel.writeString(cityId);
        parcel.writeString(stateDesc);
        parcel.writeString(stateId);
        parcel.writeString(countryDesc);
        parcel.writeString(countryId);
        parcel.writeString(createdBy_Id);
        parcel.writeString(isActive);
        parcel.writeString(latitude);
        parcel.writeString(longitude);
        parcel.writeString(ranking);
        parcel.writeInt(ranking_Id);
        parcel.writeString(partyTypeName);
        parcel.writeString(partyType_Id);
        parcel.writeString(fabricName);
        parcel.writeString(febricType_Id);
        parcel.writeString(productName);
        parcel.writeString(productType_Id);
        parcel.writeString(accountNumber);
        parcel.writeString(accountName);
        parcel.writeString(website);
        parcel.writeString(telephone);
        parcel.writeString(email);
        parcel.writeString(purchaserMobile);
        parcel.writeString(purchaserName);
        parcel.writeString(shopName);
        parcel.writeInt(shop_Id);
        parcel.writeString(imagePath1);
    }

    public static class StateList implements Parcelable{
        @Expose
        @SerializedName("countryID")
        public int countryID;
        @Expose
        @SerializedName("stateDesc")
        public String stateDesc;
        @Expose
        @SerializedName("stateID")
        public int stateID;

        protected StateList(Parcel in) {
            countryID = in.readInt();
            stateDesc = in.readString();
            stateID = in.readInt();
        }

        public static final Creator<StateList> CREATOR = new Creator<StateList>() {
            @Override
            public StateList createFromParcel(Parcel in) {
                return new StateList(in);
            }

            @Override
            public StateList[] newArray(int size) {
                return new StateList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(countryID);
            parcel.writeString(stateDesc);
            parcel.writeInt(stateID);
        }
    }

    public static class CityList implements Parcelable{
        @Expose
        @SerializedName("stateId")
        public int stateId;
        @Expose
        @SerializedName("cityDesc")
        public String cityDesc;
        @Expose
        @SerializedName("cityId")
        public int cityId;

        protected CityList(Parcel in) {
            stateId = in.readInt();
            cityDesc = in.readString();
            cityId = in.readInt();
        }

        public static final Creator<CityList> CREATOR = new Creator<CityList>() {
            @Override
            public CityList createFromParcel(Parcel in) {
                return new CityList(in);
            }

            @Override
            public CityList[] newArray(int size) {
                return new CityList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(stateId);
            parcel.writeString(cityDesc);
            parcel.writeInt(cityId);
        }
    }

    public static class AreaList implements Parcelable{
        @Expose
        @SerializedName("localityName")
        public String localityName;
        @Expose
        @SerializedName("localityId")
        public int localityId;

        protected AreaList(Parcel in) {
            localityName = in.readString();
            localityId = in.readInt();
        }

        public static final Creator<AreaList> CREATOR = new Creator<AreaList>() {
            @Override
            public AreaList createFromParcel(Parcel in) {
                return new AreaList(in);
            }

            @Override
            public AreaList[] newArray(int size) {
                return new AreaList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(localityName);
            parcel.writeInt(localityId);
        }
    }

    public static class RankingList implements Parcelable{
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("rankingId")
        public int rankingId;

        protected RankingList(Parcel in) {
            name = in.readString();
            rankingId = in.readInt();
        }

        public static final Creator<RankingList> CREATOR = new Creator<RankingList>() {
            @Override
            public RankingList createFromParcel(Parcel in) {
                return new RankingList(in);
            }

            @Override
            public RankingList[] newArray(int size) {
                return new RankingList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(name);
            parcel.writeInt(rankingId);
        }
    }

    public static class PartyTypeList implements Parcelable{
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("partyTypeId")
        public String partyTypeId;

        protected PartyTypeList(Parcel in) {
            name = in.readString();
            partyTypeId = in.readString();
        }

        public static final Creator<PartyTypeList> CREATOR = new Creator<PartyTypeList>() {
            @Override
            public PartyTypeList createFromParcel(Parcel in) {
                return new PartyTypeList(in);
            }

            @Override
            public PartyTypeList[] newArray(int size) {
                return new PartyTypeList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(name);
            parcel.writeString(partyTypeId);
        }
    }

    public static class ProductTypeList implements Parcelable{
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("productTypeId")
        public String productTypeId;

        protected ProductTypeList(Parcel in) {
            name = in.readString();
            productTypeId = in.readString();
        }

        public static final Creator<ProductTypeList> CREATOR = new Creator<ProductTypeList>() {
            @Override
            public ProductTypeList createFromParcel(Parcel in) {
                return new ProductTypeList(in);
            }

            @Override
            public ProductTypeList[] newArray(int size) {
                return new ProductTypeList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(name);
            parcel.writeString(productTypeId);
        }
    }

    public static class FabricList implements Parcelable{
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("febricTypeId")
        public String febricTypeId;

        protected FabricList(Parcel in) {
            name = in.readString();
            febricTypeId = in.readString();
        }

        public static final Creator<FabricList> CREATOR = new Creator<FabricList>() {
            @Override
            public FabricList createFromParcel(Parcel in) {
                return new FabricList(in);
            }

            @Override
            public FabricList[] newArray(int size) {
                return new FabricList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(name);
            parcel.writeString(febricTypeId);
        }
    }
}