package com.enquiry.kunalsingh.customerenquiry.parser;

/**
 * Created by Kunal Singh on 22-08-2018.
 */
public class ShopListItem {
    private int shopId;
    private String shopName;
    private String shopAddress;
    private String shopLocality;
    public int getShopId() {
        return shopId;
    }

    public void setShopID(int shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getShopLocality() {
        return shopLocality;
    }

    public void setShopLocality(String shopLocality) {
        this.shopLocality = shopLocality;
    }
}
