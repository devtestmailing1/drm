package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kunal Singh on 16-04-2018.
 */
public class RegisteredUserData implements Parcelable{

    @SerializedName("securityQuestionList")
    public List<SecurityQuestionList> securityQuestionList;
    @SerializedName("countryList")
    public List<CountryList> countryList;
    @SerializedName("stateList")
    public List<StateList> stateList;
    @SerializedName("userCategoryList")
    public List<UserCategoryList> userCategoryList;
    @SerializedName("responseMsg")
    public String responseMsg;
    @SerializedName("responseCodeMsg")
    public String responseCodeMsg;
    @SerializedName("imeiNumber")
    public String imeiNumber;
    @SerializedName("deviceModel")
    public String deviceModel;
    @SerializedName("osName")
    public String osName;
    @SerializedName("appVersion")
    public String appVersion;
    @SerializedName("mobileNumber")
    public String mobileNumber;
    @SerializedName("otp")
    public String otp;

    protected RegisteredUserData(Parcel in) {
        securityQuestionList = new ArrayList<SecurityQuestionList>();
        in.readList(securityQuestionList, SecurityQuestionList.class.getClassLoader());
        countryList = new ArrayList<CountryList>();
        in.readList(countryList, CountryList.class.getClassLoader());
        stateList = new ArrayList<StateList>();
        in.readList(stateList, StateList.class.getClassLoader());
        userCategoryList = new ArrayList<UserCategoryList>();
        in.readList(userCategoryList, UserCategoryList.class.getClassLoader());
        responseMsg = in.readString();
        responseCodeMsg = in.readString();
        imeiNumber = in.readString();
        deviceModel = in.readString();
        osName = in.readString();
        appVersion = in.readString();
        mobileNumber = in.readString();
        otp=in.readString();
    }

    public static final Creator<RegisteredUserData> CREATOR = new Creator<RegisteredUserData>() {
        @Override
        public RegisteredUserData createFromParcel(Parcel in) {
            return new RegisteredUserData(in);
        }

        @Override
        public RegisteredUserData[] newArray(int size) {
            return new RegisteredUserData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(securityQuestionList);
        parcel.writeList(countryList);
        parcel.writeList(stateList);
        parcel.writeList(userCategoryList);
        parcel.writeString(responseMsg);
        parcel.writeString(responseCodeMsg);
        parcel.writeString(imeiNumber);
        parcel.writeString(deviceModel);
        parcel.writeString(osName);
        parcel.writeString(appVersion);
        parcel.writeString(mobileNumber);
        parcel.writeString(otp);
    }

    public static class SecurityQuestionList implements Parcelable{
        @SerializedName("createdBy")
        public String createdBy;
        @SerializedName("creationDate")
        public String creationDate;
        @SerializedName("isActive")
        public boolean isActive;
        @SerializedName("question")
        public String question;
        @SerializedName("appQuestionId")
        public int appQuestionId;

        protected SecurityQuestionList(Parcel in) {
            createdBy = in.readString();
            creationDate = in.readString();
            isActive = in.readByte() != 0;
            question = in.readString();
            appQuestionId = in.readInt();
        }

        public static final Creator<SecurityQuestionList> CREATOR = new Creator<SecurityQuestionList>() {
            @Override
            public SecurityQuestionList createFromParcel(Parcel in) {
                return new SecurityQuestionList(in);
            }

            @Override
            public SecurityQuestionList[] newArray(int size) {
                return new SecurityQuestionList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(createdBy);
            parcel.writeString(creationDate);
            parcel.writeByte((byte) (isActive ? 1 : 0));
            parcel.writeString(question);
            parcel.writeInt(appQuestionId);
        }
    }

    public static class CountryList implements Parcelable{
        @SerializedName("isActive")
        public boolean isActive;
        @SerializedName("countryDesc")
        public String countryDesc;
        @SerializedName("countryCode")
        public String countryCode;
        @SerializedName("countryId")
        public int countryId;

        protected CountryList(Parcel in) {
            isActive = in.readByte() != 0;
            countryDesc = in.readString();
            countryCode = in.readString();
            countryId = in.readInt();
        }

        public static final Creator<CountryList> CREATOR = new Creator<CountryList>() {
            @Override
            public CountryList createFromParcel(Parcel in) {
                return new CountryList(in);
            }

            @Override
            public CountryList[] newArray(int size) {
                return new CountryList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeByte((byte) (isActive ? 1 : 0));
            parcel.writeString(countryDesc);
            parcel.writeString(countryCode);
            parcel.writeInt(countryId);
        }
    }

    public static class StateList implements Parcelable{
        @SerializedName("isActive")
        public boolean isActive;
        @SerializedName("countryId")
        public int countryId;
        @SerializedName("stateDesc")
        public String stateDesc;
        @SerializedName("stateCode")
        public String stateCode;
        @SerializedName("stateId")
        public int stateId;

        protected StateList(Parcel in) {
            isActive = in.readByte() != 0;
            countryId = in.readInt();
            stateDesc = in.readString();
            stateCode = in.readString();
            stateId = in.readInt();
        }

        public static final Creator<StateList> CREATOR = new Creator<StateList>() {
            @Override
            public StateList createFromParcel(Parcel in) {
                return new StateList(in);
            }

            @Override
            public StateList[] newArray(int size) {
                return new StateList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeByte((byte) (isActive ? 1 : 0));
            parcel.writeInt(countryId);
            parcel.writeString(stateDesc);
            parcel.writeString(stateCode);
            parcel.writeInt(stateId);
        }
    }

    public static class UserCategoryList implements Parcelable{
        @SerializedName("userType")
        public String userType;
        @SerializedName("usertTypeId")
        public int usertTypeId;

        protected UserCategoryList(Parcel in) {
            userType = in.readString();
            usertTypeId = in.readInt();
        }

        public static final Creator<UserCategoryList> CREATOR = new Creator<UserCategoryList>() {
            @Override
            public UserCategoryList createFromParcel(Parcel in) {
                return new UserCategoryList(in);
            }

            @Override
            public UserCategoryList[] newArray(int size) {
                return new UserCategoryList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(userType);
            parcel.writeInt(usertTypeId);
        }
    }
}