package com.enquiry.kunalsingh.customerenquiry;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import com.enquiry.kunalsingh.customerenquiry.adapter.StateAdapter;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.interfaces.GetCities;
import com.enquiry.kunalsingh.customerenquiry.interfaces.StatesSelected;
import com.enquiry.kunalsingh.customerenquiry.otp.VerifyOtpScreen;
import com.enquiry.kunalsingh.customerenquiry.parser.CountryListData;
import com.enquiry.kunalsingh.customerenquiry.parser.DistrictListData;
import com.enquiry.kunalsingh.customerenquiry.parser.DistrictListParser;
import com.enquiry.kunalsingh.customerenquiry.parser.MultiStateCityData;
import com.enquiry.kunalsingh.customerenquiry.parser.PunchData;
import com.enquiry.kunalsingh.customerenquiry.parser.PunchDataParser;
import com.enquiry.kunalsingh.customerenquiry.parser.StateListData;
import com.enquiry.kunalsingh.customerenquiry.parser.StateListParser;
import com.enquiry.kunalsingh.customerenquiry.utility.PrefManager;
import com.enquiry.kunalsingh.customerenquiry.utility.QueryBuilder;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.enquiry.kunalsingh.customerenquiry.utility.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;

public class CountrySelectActivity extends AppCompatActivity implements StateListParser.onStateListListener, DistrictListParser.onLoginListener, View.OnClickListener, PunchDataParser.onPunchListener, GetCities {
    private Spinner spinnerCountry,spinnerState,spinnerCity;
    StateListData stateListData;
    DistrictListData districtListData;
    CountryListData countryListData;
    PunchData punchData;
    private static String defaultStateName;
    Map<Integer,String> map_state_values = new HashMap<Integer, String>();
    Map<Integer,String> map_dist_values = new HashMap<Integer,String>();
    Map<Integer,String> map_country_values = new HashMap<Integer,String>();
    private int selCountryId, selStateId, selDistId, selCityId, selLocalityId;
    private Button btnRegister,selectState, selectCities, selectCountry;
    private String userMob, userType, districtIDs = "" ;
    private ArrayList<String>selectedStates, selectedCountries;
    public StatesSelected statesSelected;
    private StateAdapter myAdapter;
    private int stateClickCount =0, cityClickCount=1;
    MultiStateCityData stateVO,cityVO,countryVo;
    ArrayList<MultiStateCityData> stateListVOs,cityListVOs,countryListVo;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_selection);
        if (getIntent() != null){
            userMob=getIntent().getStringExtra("userMobNumber");
            userType=getIntent().getStringExtra("customerType");
        }
        initialization();
        fillSpinnerCountry();
        executeQuery();
    }


    private void initialization() {

       // spinnerCountry=findViewById(R.id.spinerCountry);
        //  spinnerState=findViewById(R.id.spinerState);
        btnRegister=findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);
        selectState = findViewById(R.id.selectState);
        selectCities = findViewById(R.id.spinerCity);
        selectCountry = findViewById(R.id.spinerCountry);

        selectedStates = new ArrayList<>();
        selectedCountries = new ArrayList<>();
    }


    private void fillSpinnerCountry() {
        ArrayList<String> countryList = new ArrayList<String>();
        // countryList.add("Select Country");
        countryList.add("India");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, countryList);
     //   spinnerCountry.setAdapter(adapter);
    }



    private void executeQuery() {
      //  new StateListParser().executeQuery(CountrySelectActivity.this, QueryBuilder.getInstance().stateListAPI(), QueryBuilder.getInstance().generateStateListQuery(CountrySelectActivity.this), CountrySelectActivity.this, Settings.STATE_LIST_ID, true);
        new StateListParser().executeQuery(CountrySelectActivity.this, QueryBuilder.getInstance().countryListApi(), QueryBuilder.getInstance().generateCountryListQuery(CountrySelectActivity.this), CountrySelectActivity.this, Settings.COUNTRY_LIST_ID, true);
    }

    @Override
    public void onSuccess(int id, String message, Object object) {
        List<String> stateName = new ArrayList<>();
        List<String> stateID = new ArrayList<>();
        List<String> cityID = new ArrayList<>();
        List<String> selected = new ArrayList<>();
        List<String> selectedCity = new ArrayList<>();
        List<String> districtName=new ArrayList<>();
        //  stateName.add("Select State");

        List<String> countryName = new ArrayList<>();
        List<String> countryId = new ArrayList<>();
        List<String> countrySelect = new ArrayList<>();


        if (id == Settings.COUNTRY_LIST_ID){
            if (object!= null){

                selectedCountries = new ArrayList<>();

                countryListData = ((CountryListData)object);
                if (countryListData!=null){
                    for (int i = 0; i< countryListData.countryList.size(); i++){
                        countryName.add(countryListData.countryList.get(i).countryName);
                        countryId.add(String.valueOf(countryListData.countryList.get(i).countryId));
                        countrySelect.add(countryListData.countryList.get(i).selected);

                        if (countryListData.countryList.get(i).selected.equalsIgnoreCase("SELECTED")){
                            selectedCountries.add(String.valueOf(countryListData.countryList.get(i).countryId));
                        }

                        map_country_values.put(countryListData.countryList.get(i).countryId, countryListData.countryList.get(i).countryName);

                    }


                  //  PrefManager.setSelectedStates(CountrySelectActivity.this,selectedStates,PrefManager.KEY_SELECTED_STATES);

                    String countryIDS = TextUtils.join(",", selectedCountries);

                    Log.d("stateIDs>> ", countryIDS);

                    selectCountry.setText(selectedCountries.size() + " " + "Country Selected");

                   // new DistrictListParser().executeQuery(CountrySelectActivity.this, QueryBuilder.getInstance().countryListApi(), QueryBuilder.getInstance().generateCountryListQuery(CountrySelectActivity.this,countryIDS), CountrySelectActivity.this, Settings.COUNTRY_LIST_ID, true);

                    new StateListParser().executeQuery(CountrySelectActivity.this, QueryBuilder.getInstance().stateListAPI(), QueryBuilder.getInstance().generateStateListQuery(CountrySelectActivity.this, countryIDS), CountrySelectActivity.this, Settings.STATE_LIST_ID, true);

                    Log.d("lists>>>",stateID.toString() + " >>>> "+selectedCountries.toString());

                    fillingCountry(countryName, countryId, countrySelect);


                }

            }
        }

        if (id == Settings.STATE_LIST_ID) {
            if (object != null) {

                selectedStates = new ArrayList<>();

                stateListData = ((StateListData) object);
                if (stateListData != null) {
                    for (int i = 0; i < stateListData.stateList.size(); i++) {
                        stateName.add(stateListData.stateList.get(i).stateDesc);
                        selected.add(stateListData.stateList.get(i).selected);
                        stateID.add(String.valueOf(stateListData.stateList.get(i).stateId));

                        if (stateListData.stateList.get(i).selected.equalsIgnoreCase("SELECTED")){
                            selectedStates.add(String.valueOf(stateListData.stateList.get(i).stateId));
                        }

                        map_state_values.put(stateListData.stateList.get(i).stateId, stateListData.stateList.get(i).stateDesc);
                    }

                   /* fillStateSpinner(stateName, defaultStateName);
                    districtName.add("Select City");
                    fillDistrictSpinner(districtName,defaultStateName);*/


                    PrefManager.setSelectedStates(CountrySelectActivity.this,selectedStates,PrefManager.KEY_SELECTED_STATES);

                    String stateIDS = TextUtils.join(",", selectedStates);

                    Log.d("stateIDs>> ", stateIDS);

                    selectState.setText(selectedStates.size() + " " + getString(R.string.states_selected));

                    new DistrictListParser().executeQuery(CountrySelectActivity.this, QueryBuilder.getInstance().districtListApi(), QueryBuilder.getInstance().generateDistrictListQuery(CountrySelectActivity.this,stateIDS), CountrySelectActivity.this, Settings.FETCH_DISTRICT_LIST_ID, true);


                    Log.d("lists>>>",stateID.toString() + " >>>> "+selectedStates.toString());

                    fillingState(stateName, stateID, selected);

                }
            }
        }

        if (id == Settings.FETCH_DISTRICT_LIST_ID) {
            if (object != null) {
                districtListData = ((DistrictListData) object);
                if (districtListData != null) {
                    //  districtName.add("Select City");
                    // For District
                    Log.d("districtList >>", districtListData.cityList.toString());
                    for (int i = 0; i < districtListData.cityList.size(); i++) {
                        districtName.add(districtListData.cityList.get(i).cityDesc);
                        selectedCity.add(districtListData.cityList.get(i).selected);
                        cityID.add(String.valueOf(districtListData.cityList.get(i).cityId));
                        map_dist_values.put(districtListData.cityList.get(i).cityId, districtListData.cityList.get(i).cityDesc);
                    }
                    String defaultDistrict = "EAST";
                    //  fillDistrictSpinner(districtName, defaultDistrict);
                    fillingCity(districtName,cityID,selectedCity);

                }
            }
        }

        if(id==Settings.VALIDATE_MOB_ID){
            if(message!=null) {
                Utils.HitechToast(CountrySelectActivity.this, message);
                startActivity(new Intent(this, VerifyOtpScreen.class).putExtra("userMobNo", userMob).putExtra("userType", userType).putExtra("cityID", districtIDs));
            }
        }
    }





    @Override
    public void onError(String error, int id, BaseController.ErrorCode code) {
        if(error!=null){
            Utils.HitechToast(CountrySelectActivity.this, error);
        }
    }

    public void fillStateSpinner(List<String> stateNameList, String defaultState){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_dropdown_item, stateNameList);
        spinnerState.setAdapter(adapter);
        selectSpinnerValue(spinnerState,defaultState);
        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinnerState.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select State"))) {
                    for (Map.Entry<Integer, String> stateEntry : map_state_values.entrySet()) {
                        if (stateEntry.getValue().equalsIgnoreCase(value)) {
                            selStateId = stateEntry.getKey();
                            Log.v("StateId----------", "" + stateEntry.getKey());
                            new DistrictListParser().executeQuery(CountrySelectActivity.this, QueryBuilder.getInstance().districtListApi(), QueryBuilder.getInstance().generateDistrictListQuery(CountrySelectActivity.this,Integer.toString(stateEntry.getKey())), CountrySelectActivity.this, Settings.FETCH_DISTRICT_LIST_ID, true);
                        }
                    }
                }else{

                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    /********************* country handling *****************/


    private void fillingCountry(final List<String> stateNameList, final List<String> stateID, final List<String> selected){

        countryListVo = new ArrayList<>();

        for (int i = 0; i < stateNameList.size(); i++) {
            countryVo = new MultiStateCityData();
            countryVo.setTitle(stateNameList.get(i));
            countryVo.setID(stateID.get(i));
            countryVo.setSelected(false);
            countryVo.setAlreadySelected(selected.get(i));
            countryListVo.add(countryVo);
        }


        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.custom_state_list);

        Button cancel = dialog.findViewById(R.id.btnCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        Button ok = dialog.findViewById(R.id.btnOK);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedCountries = new ArrayList<>();
                selectedCountries = myAdapter.selectedStates();

                if (selectedCountries.isEmpty()){
                    Utils.HitechToast(CountrySelectActivity.this,"Please Select at least one state");
                    return;
                }

                Log.d("selected states>>>>", selectedCountries.toString());

                countryListVo = new ArrayList<>();
                for (int i = 0; i < stateNameList.size(); i++) {
                    countryVo = new MultiStateCityData();
                    countryVo.setTitle(stateNameList.get(i));
                    countryVo.setID(stateID.get(i));
                    countryVo.setSelected(false);
                    if (selectedCountries.contains(stateID.get(i))) {
                        countryVo.setAlreadySelected("SELECTED");
                        Log.d("stateID>>>>",stateID.get(i));
                    }else {
                        countryVo.setAlreadySelected("");
                    }
                    countryListVo.add(countryVo);

                }

               // PrefManager.setSelectedStates(CountrySelectActivity.this,selectedStates,PrefManager.KEY_SELECTED_STATES);

                String stateIDS = TextUtils.join(",", selectedCountries);

                Log.d("stateIDs>> ", stateIDS);

                selectCountry.setText(selectedCountries.size() + " " + "country selected");

                dialog.dismiss();

             //   new DistrictListParser().executeQuery(CountrySelectActivity.this, QueryBuilder.getInstance().districtListApi(), QueryBuilder.getInstance().generateDistrictListQuery(CountrySelectActivity.this,stateIDS), CountrySelectActivity.this, Settings.FETCH_DISTRICT_LIST_ID, true);
                new StateListParser().executeQuery(CountrySelectActivity.this, QueryBuilder.getInstance().stateListAPI(), QueryBuilder.getInstance().generateStateListQuery(CountrySelectActivity.this, stateIDS), CountrySelectActivity.this, Settings.STATE_LIST_ID, true);
            }
        });




        selectCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                myAdapter = new StateAdapter(CountrySelectActivity.this, 0,
                        countryListVo);
                ListView listView = dialog.findViewById(R.id.list_view);
                listView.setAdapter(myAdapter);
                dialog.show();
            }
        });

        dialog.setCancelable(false);

    }




    /*********************************/



    private void fillingState(final List<String> stateNameList, final List<String> stateID, final List<String> selected){

        stateListVOs = new ArrayList<>();

        for (int i = 0; i < stateNameList.size(); i++) {
            stateVO = new MultiStateCityData();
            stateVO.setTitle(stateNameList.get(i));
            stateVO.setID(stateID.get(i));
            stateVO.setSelected(false);
            stateVO.setAlreadySelected(selected.get(i));
            stateListVOs.add(stateVO);
        }


        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.custom_state_list);

        Button cancel = dialog.findViewById(R.id.btnCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        Button ok = dialog.findViewById(R.id.btnOK);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedStates = new ArrayList<>();
                selectedStates = myAdapter.selectedStates();

                if (selectedStates.isEmpty()){
                    Utils.HitechToast(CountrySelectActivity.this,"Please Select at least one state");
                    return;
                }

                Log.d("selected states>>>>", selectedStates.toString());

                stateListVOs = new ArrayList<>();
                for (int i = 0; i < stateNameList.size(); i++) {
                    stateVO = new MultiStateCityData();
                    stateVO.setTitle(stateNameList.get(i));
                    stateVO.setID(stateID.get(i));
                    stateVO.setSelected(false);
                    if (selectedStates.contains(stateID.get(i))) {
                        stateVO.setAlreadySelected("SELECTED");
                        Log.d("stateID>>>>",stateID.get(i));
                    }else {
                        stateVO.setAlreadySelected("");
                    }
                    stateListVOs.add(stateVO);

                }

                PrefManager.setSelectedStates(CountrySelectActivity.this,selectedStates,PrefManager.KEY_SELECTED_STATES);

                String stateIDS = TextUtils.join(",", selectedStates);

                Log.d("stateIDs>> ", stateIDS);

                selectState.setText(selectedStates.size() + " " + getString(R.string.states_selected));

                dialog.dismiss();

                new DistrictListParser().executeQuery(CountrySelectActivity.this, QueryBuilder.getInstance().districtListApi(), QueryBuilder.getInstance().generateDistrictListQuery(CountrySelectActivity.this,stateIDS), CountrySelectActivity.this, Settings.FETCH_DISTRICT_LIST_ID, true);
            }
        });




        selectState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                myAdapter = new StateAdapter(CountrySelectActivity.this, 0,
                        stateListVOs);
                ListView listView = dialog.findViewById(R.id.list_view);
                listView.setAdapter(myAdapter);
                dialog.show();
            }
        });

        dialog.setCancelable(false);

    }



    private void fillingCity(final List<String> stateNameList, final List<String> stateID, final List<String> selectedCity){

        Log.d("cityID>> ",stateID.toString());

         cityListVOs = new ArrayList<>();

        for (int i = 0; i < stateNameList.size(); i++) {
            cityVO = new MultiStateCityData();
            cityVO.setTitle(stateNameList.get(i));
            cityVO.setAlreadySelected(selectedCity.get(i));
            cityVO.setID(stateID.get(i));
            cityVO.setSelected(false);
            cityListVOs.add(cityVO);
        }

        /*myAdapter = new StateAdapter(CountrySelectActivity.this, 0,
                cityListVOs);*/


        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.custom_state_list);


        Button cancel = dialog.findViewById(R.id.btnCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        Button ok = dialog.findViewById(R.id.btnOK);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedStates = new ArrayList<>();
                selectedStates = myAdapter.selectedStates();

                if (selectedStates.isEmpty()){
                    Utils.HitechToast(CountrySelectActivity.this,"Please Select at least one state");
                    return;
                }

                cityListVOs = new ArrayList<>();
                for (int i = 0; i < stateNameList.size(); i++) {
                    cityVO = new MultiStateCityData();
                    cityVO.setTitle(stateNameList.get(i));
                    cityVO.setID(stateID.get(i));
                    cityVO.setSelected(false);
                    if (selectedStates.contains(stateID.get(i))) {
                        cityVO.setAlreadySelected("SELECTED");
                    }else {
                        cityVO.setAlreadySelected("");
                    }
                    cityListVOs.add(cityVO);

                }

                PrefManager.setSelectedCities(CountrySelectActivity.this,selectedStates,PrefManager.KEY_SELECTED_CITIES);
                districtIDs = TextUtils.join(",", selectedStates);

                selectCities.setText(selectedStates.size() + " " + getString(R.string.city_selected));
                dialog.dismiss();

            }
        });

        selectCities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Settings.State_OR_CITY = 1;
                Log.d("slelckdkd>",selectedStates.toString());
                if (selectedStates.isEmpty()){


                    Utils.HitechToast(CountrySelectActivity.this, "Please Select State First.");
                }else {

                    myAdapter =  new StateAdapter(CountrySelectActivity.this, 0,
                            cityListVOs);
                    ListView listView = dialog.findViewById(R.id.list_view);
                    listView.setAdapter(myAdapter);
                    dialog.show();
                }
            }
        });

        dialog.setCancelable(false);
    }




    public void fillDistrictSpinner(List<String> stateNameList, String defaultDistrict){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_dropdown_item, stateNameList);
        spinnerCity.setAdapter(adapter);
        selectSpinnerValue(spinnerCity,"Select City");
        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinnerCity.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select City"))) {
                    for (Map.Entry<Integer, String> districtEntry : map_dist_values.entrySet()) {
                        if (districtEntry.getValue().equalsIgnoreCase(value)) {
                            selDistId = districtEntry.getKey();
                            Log.v("DistrictId----------", "" + districtEntry.getKey());
                            //new CityListParser().executeQuery(RegisterScreenActivity.this, QueryBuilder.getInstance().cityListApi(), QueryBuilder.getInstance().generateCityListQuery(RegisterScreenActivity.this, registeredUserData.mobileNumber, Utils.versionNumber(RegisterScreenActivity.this), Utils.getDeviceName(), Utils.deviceToken(RegisterScreenActivity.this), Integer.toString(districtEntry.getKey())), RegisterScreenActivity.this, Settings.FETCH_CITY_LIST_ID, true);
                        }
                    }
                }else{

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void selectSpinnerValue(Spinner spinner, String myString)
    {
        int index = 0;
        for(int i = 0; i < spinner.getCount(); i++){
            if(spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                spinner.setSelection(i);
                break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnRegister:
                /*if(spinnerState.getSelectedItem().toString().equalsIgnoreCase("Select State")){
                    Utils.HitechToast(this,"Please Select State");
                }else if(spinnerCity.getSelectedItem().toString().equalsIgnoreCase("Select City")){
                    Utils.HitechToast(this,"Please Select City");
                }else {
                    PrefManager.setKeyUserState(CountrySelectActivity.this,spinnerState.getSelectedItem().toString());
                    PrefManager.setKeyUserCity(CountrySelectActivity.this,spinnerCity.getSelectedItem().toString());
                    new PunchDataParser().executeQuery(this, QueryBuilder.getInstance().getValidateMobAPI(), QueryBuilder.getInstance().generateValidateMobQuery(this, userMob, userType,String.valueOf(selDistId)), this, Settings.VALIDATE_MOB_ID, true);
                    //  startActivity(new Intent(this, VerifyOtpScreen.class).putExtra("userMobNo", userMob).putExtra("userType", userType).putExtra("cityID", String.valueOf(selDistId)));
                }*/


                if (districtIDs.isEmpty()){
                    Utils.HitechToast(CountrySelectActivity.this, "Select state and city both");
                }else {
                    Log.d("District IDs >>>>", districtIDs);
                    PrefManager.cleanPerticularPrefrence(CountrySelectActivity.this,PrefManager.KEY_SELECTED_STATES);
                    PrefManager.cleanPerticularPrefrence(CountrySelectActivity.this,PrefManager.KEY_SELECTED_CITIES);
                    new PunchDataParser().executeQuery(this, QueryBuilder.getInstance().getValidateMobAPI(), QueryBuilder.getInstance().generateValidateMobQuery(this, userMob, userType, districtIDs), this, Settings.VALIDATE_MOB_ID, true);
                }


                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    public void stateIDs(ArrayList<String> arrayList) {
        Log.d("StateIDDD>>>", arrayList.toString());
    }



}
