package com.enquiry.kunalsingh.customerenquiry.common_ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.victor.loading.rotate.RotateLoading;

/**
 * Created by Kunal Singh on 28-02-2018.
 */

public class EnquiryProgressDialog extends Dialog {

    public RotateLoading rotateLoading;
    public EnquiryProgressDialog(Context a) {
        super(a);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        setContentView(R.layout.custom_progress_dialog);
      /*  AVLoadingIndicatorView loader=(AVLoadingIndicatorView)findViewById(R.id.avi);
        loader.show();*/
        rotateLoading = (RotateLoading) findViewById(R.id.loading_spinner);
        rotateLoading.start();



    }
}

