package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kunal Singh on 23-05-2018.
 */
public class FetchModelFromModelFamilyData implements Parcelable{

    @Expose
    @SerializedName("userResponse")
    public List<UserResponse> userResponse;

    protected FetchModelFromModelFamilyData(Parcel in) {
        userResponse= new ArrayList<FetchModelFromModelFamilyData.UserResponse>();
        in.readList(userResponse,LocalityListData.UserResponse.class.getClassLoader());
    }

    public static final Creator<FetchModelFromModelFamilyData> CREATOR = new Creator<FetchModelFromModelFamilyData>() {
        @Override
        public FetchModelFromModelFamilyData createFromParcel(Parcel in) {
            return new FetchModelFromModelFamilyData(in);
        }

        @Override
        public FetchModelFromModelFamilyData[] newArray(int size) {
            return new FetchModelFromModelFamilyData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(userResponse);
    }

    public static class UserResponse implements Parcelable{
        @Expose
        @SerializedName("modifiedBy")
        public String modifiedBy;
        @Expose
        @SerializedName("modifiedDate")
        public String modifiedDate;
        @Expose
        @SerializedName("isActive")
        public boolean isActive;
        @Expose
        @SerializedName("modelDesc")
        public String modelDesc;
        @Expose
        @SerializedName("modelCode")
        public String modelCode;
        @Expose
        @SerializedName("modelId")
        public int modelId;

        protected UserResponse(Parcel in) {
            modifiedBy = in.readString();
            modifiedDate = in.readString();
            isActive = in.readByte() != 0;
            modelDesc = in.readString();
            modelCode = in.readString();
            modelId = in.readInt();
        }

        public static final Creator<UserResponse> CREATOR = new Creator<UserResponse>() {
            @Override
            public UserResponse createFromParcel(Parcel in) {
                return new UserResponse(in);
            }

            @Override
            public UserResponse[] newArray(int size) {
                return new UserResponse[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(modifiedBy);
            parcel.writeString(modifiedDate);
            parcel.writeByte((byte) (isActive ? 1 : 0));
            parcel.writeString(modelDesc);
            parcel.writeString(modelCode);
            parcel.writeInt(modelId);
        }
    }
}
