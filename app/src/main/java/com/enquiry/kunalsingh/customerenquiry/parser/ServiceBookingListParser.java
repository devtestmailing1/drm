package com.enquiry.kunalsingh.customerenquiry.parser;

import android.support.v4.app.FragmentActivity;

import com.android.volley.Request;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kunal Singh on 16-05-2018.
 */
public class ServiceBookingListParser extends BaseController {

    public ServiceBookingListParser.onServiceBookingListListener listener;
    public int id;
    public String TAG_MESSAGE = "message";

    public ServiceBookingListParser executeQuery(FragmentActivity context, String url, JSONObject json, ServiceBookingListParser.onServiceBookingListListener listener, int id, boolean toShowDialog) {
        // RequestManager.allowAllSSL();
        this.listener = listener;
        this.id = id;
        init(context, url, Request.Method.POST, json, toShowDialog);

        return null;
    }

    @Override
    public void onComplete(JSONArray response, String message) {
        switch (id) {
            case Settings.SERVICE_BOOKING_LIST_ID:
                listener.onSuccess(id, message, null);
                if (response != null) {
                    try {
                        listener.onSuccess(id, "", parserUserInfo(response.getJSONObject(0)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                break;
        }
    }

    @Override
    public void onSuccess(JSONArray response) {

    }

    @Override
    public void onFailureData(String error, ErrorCode code, JSONArray response) {

    }

    @Override
    public void onFailure(String error, ErrorCode code) {
        if (error != null) {
            listener.onError(error, id, code);
        }
    }


    public interface onServiceBookingListListener {
        public void onSuccess(int id, String message, ServiceBookingListData object);
        public void onError(String error, int id, ErrorCode code);
    }


    public ServiceBookingListData parserUserInfo(JSONObject object) {
        Gson gson = new Gson();
        ServiceBookingListData response = gson.fromJson(object.toString(), ServiceBookingListData.class);
        return response;
    }
}
