package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kunal Singh on 07-05-2018.
 */
public class RegisterVehicleData implements Parcelable{

    @Expose
    @SerializedName("displayList")
    public List<DisplayList> displayList;
    @Expose
    @SerializedName("modelGroupMstList")
    public List<ModelGroupMstList> modelGroupMstList;
    @Expose
    @SerializedName("modelFamilyMstList")
    public List<ModelFamilyMstList> modelFamilyMstList;
    @Expose
    @SerializedName("dataFor")
    public String dataFor;
    @Expose
    @SerializedName("imeiNumber")
    public String imeiNumber;
    @Expose
    @SerializedName("deviceModel")
    public String deviceModel;
    @Expose
    @SerializedName("osName")
    public String osName;
    @Expose
    @SerializedName("appVersion")
    public String appVersion;
    @Expose
    @SerializedName("mobileNumber")
    public String mobileNumber;

    protected RegisterVehicleData(Parcel in) {
        displayList= new ArrayList<DisplayList>();
        in.readList(displayList,DisplayList.class.getClassLoader());
        modelGroupMstList= new ArrayList<ModelGroupMstList>();
        in.readList(modelGroupMstList,ModelGroupMstList.class.getClassLoader());
        modelFamilyMstList= new ArrayList<ModelFamilyMstList>();
        in.readList(modelFamilyMstList,ModelFamilyMstList.class.getClassLoader());
        dataFor = in.readString();
        imeiNumber = in.readString();
        deviceModel = in.readString();
        osName = in.readString();
        appVersion = in.readString();
        mobileNumber = in.readString();
    }

    public static final Creator<RegisterVehicleData> CREATOR = new Creator<RegisterVehicleData>() {
        @Override
        public RegisterVehicleData createFromParcel(Parcel in) {
            return new RegisterVehicleData(in);
        }

        @Override
        public RegisterVehicleData[] newArray(int size) {
            return new RegisterVehicleData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(displayList);
        parcel.writeList(modelGroupMstList);
        parcel.writeList(modelFamilyMstList);
        parcel.writeString(dataFor);
        parcel.writeString(imeiNumber);
        parcel.writeString(deviceModel);
        parcel.writeString(osName);
        parcel.writeString(appVersion);
        parcel.writeString(mobileNumber);
    }

    public static class DisplayList implements Parcelable{
        @Expose
        @SerializedName("Model_Label")
        public String Model_Label;
        @Expose
        @SerializedName("Chassis_Label")
        public String Chassis_Label;
        @Expose
        @SerializedName("Chassis_Display")
        public String Chassis_Display;
        @Expose
        @SerializedName("Model_Display")
        public String Model_Display;
        @Expose
        @SerializedName("Make_Label")
        public String Make_Label;
        @Expose
        @SerializedName("Make_Display")
        public String Make_Display;
        @Expose
        @SerializedName("Registration_Label")
        public String Registration_Label;
        @Expose
        @SerializedName("ModelFamily_Label")
        public String ModelFamily_Label;
        @Expose
        @SerializedName("ModelFamily_Display")
        public String ModelFamily_Display;
        @Expose
        @SerializedName("Registration_Display")
        public String Registration_Display;

        protected DisplayList(Parcel in) {
            Model_Label = in.readString();
            Chassis_Label = in.readString();
            Chassis_Display = in.readString();
            Model_Display = in.readString();
            Make_Label = in.readString();
            Make_Display = in.readString();
            Registration_Label = in.readString();
            ModelFamily_Label = in.readString();
            ModelFamily_Display = in.readString();
            Registration_Display = in.readString();
        }

        public static final Creator<DisplayList> CREATOR = new Creator<DisplayList>() {
            @Override
            public DisplayList createFromParcel(Parcel in) {
                return new DisplayList(in);
            }

            @Override
            public DisplayList[] newArray(int size) {
                return new DisplayList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(Model_Label);
            parcel.writeString(Chassis_Label);
            parcel.writeString(Chassis_Display);
            parcel.writeString(Model_Display);
            parcel.writeString(Make_Label);
            parcel.writeString(Make_Display);
            parcel.writeString(Registration_Label);
            parcel.writeString(ModelFamily_Label);
            parcel.writeString(ModelFamily_Display);
            parcel.writeString(Registration_Display);
        }
    }

    public static class ModelGroupMstList implements Parcelable{
        @Expose
        @SerializedName("createdBy")
        public String createdBy;
        @Expose
        @SerializedName("createdDate")
        public String createdDate;
        @Expose
        @SerializedName("activeStatus")
        public boolean activeStatus;
        @Expose
        @SerializedName("batchPickFlag")
        public boolean batchPickFlag;
        @Expose
        @SerializedName("displayOrder")
        public int displayOrder;
        @Expose
        @SerializedName("modelGroupDesc")
        public String modelGroupDesc;
        @Expose
        @SerializedName("modelGroupCode")
        public String modelGroupCode;
        @Expose
        @SerializedName("productDivisionId")
        public int productDivisionId;
        @Expose
        @SerializedName("modelGroupId")
        public int modelGroupId;

        protected ModelGroupMstList(Parcel in) {
            createdBy = in.readString();
            createdDate = in.readString();
            activeStatus = in.readByte() != 0;
            batchPickFlag = in.readByte() != 0;
            displayOrder = in.readInt();
            modelGroupDesc = in.readString();
            modelGroupCode = in.readString();
            productDivisionId = in.readInt();
            modelGroupId = in.readInt();
        }

        public static final Creator<ModelGroupMstList> CREATOR = new Creator<ModelGroupMstList>() {
            @Override
            public ModelGroupMstList createFromParcel(Parcel in) {
                return new ModelGroupMstList(in);
            }

            @Override
            public ModelGroupMstList[] newArray(int size) {
                return new ModelGroupMstList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(createdBy);
            parcel.writeString(createdDate);
            parcel.writeByte((byte) (activeStatus ? 1 : 0));
            parcel.writeByte((byte) (batchPickFlag ? 1 : 0));
            parcel.writeInt(displayOrder);
            parcel.writeString(modelGroupDesc);
            parcel.writeString(modelGroupCode);
            parcel.writeInt(productDivisionId);
            parcel.writeInt(modelGroupId);
        }
    }

    public static class ModelFamilyMstList {
        @Expose
        @SerializedName("isActive")
        public boolean isActive;
        @Expose
        @SerializedName("displayOrder")
        public int displayOrder;
        @Expose
        @SerializedName("modelFamilyDesc")
        public String modelFamilyDesc;
        @Expose
        @SerializedName("modelFamilyCode")
        public String modelFamilyCode;
        @Expose
        @SerializedName("modelFamilyId")
        public int modelFamilyId;
    }
}





