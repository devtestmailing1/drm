package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kunal Singh on 30-06-2018.
 */
public class ComplainScreenDTLData implements Parcelable{

    @Expose
    @SerializedName("dealerModelList")
    public List<DealerModelList> dealerModelList;
    @Expose
    @SerializedName("vinModelList")
    public List<VinModelList> vinModelList;
    @Expose
    @SerializedName("imeiNumber")
    public String imeiNumber;
    @Expose
    @SerializedName("deviceModel")
    public String deviceModel;
    @Expose
    @SerializedName("osName")
    public String osName;
    @Expose
    @SerializedName("appVersion")
    public String appVersion;
    @Expose
    @SerializedName("mobileNumber")
    public String mobileNumber;

    protected ComplainScreenDTLData(Parcel in) {
        dealerModelList= new ArrayList<DealerModelList>();
        in.readList(dealerModelList,DealerModelList.class.getClassLoader());
        vinModelList= new ArrayList<VinModelList>();
        in.readList(vinModelList,VinModelList.class.getClassLoader());
        imeiNumber = in.readString();
        deviceModel = in.readString();
        osName = in.readString();
        appVersion = in.readString();
        mobileNumber = in.readString();
    }

    public static final Creator<ComplainScreenDTLData> CREATOR = new Creator<ComplainScreenDTLData>() {
        @Override
        public ComplainScreenDTLData createFromParcel(Parcel in) {
            return new ComplainScreenDTLData(in);
        }

        @Override
        public ComplainScreenDTLData[] newArray(int size) {
            return new ComplainScreenDTLData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(dealerModelList);
        parcel.writeList(vinModelList);
        parcel.writeString(imeiNumber);
        parcel.writeString(deviceModel);
        parcel.writeString(osName);
        parcel.writeString(appVersion);
        parcel.writeString(mobileNumber);
    }

    public static class DealerModelList implements Parcelable{
        @Expose
        @SerializedName("dealerName")
        public String dealerName;
        @Expose
        @SerializedName("branchId")
        public int branchId;

        protected DealerModelList(Parcel in) {
            dealerName = in.readString();
            branchId = in.readInt();
        }

        public static final Creator<DealerModelList> CREATOR = new Creator<DealerModelList>() {
            @Override
            public DealerModelList createFromParcel(Parcel in) {
                return new DealerModelList(in);
            }

            @Override
            public DealerModelList[] newArray(int size) {
                return new DealerModelList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(dealerName);
            parcel.writeInt(branchId);
        }
    }

    public static class VinModelList implements Parcelable{
        @Expose
        @SerializedName("registrationNo")
        public String registrationNo;

        protected VinModelList(Parcel in) {
            registrationNo = in.readString();
        }

        public static final Creator<VinModelList> CREATOR = new Creator<VinModelList>() {
            @Override
            public VinModelList createFromParcel(Parcel in) {
                return new VinModelList(in);
            }

            @Override
            public VinModelList[] newArray(int size) {
                return new VinModelList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(registrationNo);
        }
    }
}
