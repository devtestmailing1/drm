package com.enquiry.kunalsingh.customerenquiry.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.enquiry.kunalsingh.customerenquiry.CountrySelectActivity;
import com.enquiry.kunalsingh.customerenquiry.LoginScreenActivity;
import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.interfaces.GetCities;
import com.enquiry.kunalsingh.customerenquiry.interfaces.StatesSelected;
import com.enquiry.kunalsingh.customerenquiry.parser.MultiStateCityData;
import com.enquiry.kunalsingh.customerenquiry.utility.PrefManager;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;

import java.util.ArrayList;
import java.util.List;

public class StateAdapter extends ArrayAdapter<MultiStateCityData> implements View.OnClickListener {
    private Context mContext;
    private ArrayList<MultiStateCityData> listState;
    private StateAdapter myAdapter;
    private ArrayList<String>selectedCities;
    private boolean isFromView = false;
    public GetCities getCities;


    public StateAdapter(Context context, int resource, List<MultiStateCityData> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.listState = (ArrayList<MultiStateCityData>) objects;
        this.myAdapter = this;
        selectedCities = new ArrayList<>();
        Log.d("litle state >>>", this.listState.toString());

        for (int i = 0; i<this.listState.size(); i++){
            Log.d("litle state > ", this.listState.get(i).getAlreadySelected() + " >>> "+ this.listState.get(i).getTitle());
        }

    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(final int position, View convertView,
                              ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflator = LayoutInflater.from(mContext);
            convertView = layoutInflator.inflate(R.layout.custom_spinner_layout, null);
            holder = new ViewHolder();
            holder.mTextView = (TextView) convertView.findViewById(R.id.text);
            holder.mCheckBox = (CheckBox) convertView.findViewById(R.id.checkbox);
            holder.relativeLayout = convertView.findViewById(R.id.main_relative);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mTextView.setText(listState.get(position).getTitle());

        if (listState.get(position).getAlreadySelected().equalsIgnoreCase("SELECTED")){
            holder.mCheckBox.setChecked(true);
            if (holder.mCheckBox.isChecked() && !selectedCities.contains(listState.get(position).getID()) ) {
                selectedCities.add(listState.get(position).getID());
            }
        }else {
            holder.mCheckBox.setChecked(false);
            selectedCities.remove(listState.get(position).getID());

        }

        holder.mCheckBox.setTag(position);


        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.mCheckBox.isChecked()){
                    holder.mCheckBox.setChecked(false);
                }else {
                    holder.mCheckBox.setChecked(true);
                }
            }

        });

        holder.mCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.mCheckBox.isChecked()){
                    selectedCities.add(listState.get(position).getID());
                }else {
                    selectedCities.remove(listState.get(position).getID());
                }
            }
        });


        //  Log.d("listState> ",listState.get(position).getID());


        return convertView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.main_relative:
                break;

        }
    }


    private class ViewHolder {
        private TextView mTextView;
        private CheckBox mCheckBox;
        private RelativeLayout relativeLayout;
    }


    public ArrayList<String> selectedStates(){
        return selectedCities;
    }


}