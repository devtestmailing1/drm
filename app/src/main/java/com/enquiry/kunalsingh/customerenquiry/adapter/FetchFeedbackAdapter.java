package com.enquiry.kunalsingh.customerenquiry.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;


import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.parser.FetchFeedbackData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kunal Singh on 27-08-2018.
 */
public class FetchFeedbackAdapter extends RecyclerView.Adapter<FetchFeedbackAdapter.FetchFeedbackHolder>{
    private List<FetchFeedbackData.FeedbackList> list;
    private Context mContext;
    public FetchFeedbackAdapter(Context context, List<FetchFeedbackData.FeedbackList> list) {
        this.mContext = context;
        this.list = list;
    }

    @NonNull
    @Override
    public FetchFeedbackHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.custom_fetch_feddback, viewGroup, false);
        return new FetchFeedbackAdapter.FetchFeedbackHolder(view );
    }

    @Override
    public void onBindViewHolder(@NonNull FetchFeedbackHolder holder, final int position) {
        if(list.get(position).callDuration!=null) {
            holder.edtCallDuration.setText(list.get(position).callDuration);
        }else{
            holder.textinputlayout_CallDuration.setVisibility(View.GONE);
        }
        if(list.get(position).remarks!=null) {
            holder.edtRemarks.setText(list.get(position).remarks);
        }else{
            holder.textinputlayout_Remarks.setVisibility(View.GONE);
        }

        if(list.get(position).fromDate!=null) {
            holder.edtFromDate.setText(list.get(position).fromDate);
        }else{
            holder.textinputlayout_FromDate.setVisibility(View.GONE);
        }

        if(list.get(position).toDate!=null) {
            holder.edtToDate.setText(list.get(position).toDate);
        }else{
            holder.textinputlayout_Todate.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class FetchFeedbackHolder extends RecyclerView.ViewHolder{
        private TextInputLayout textinputlayout_CallDuration,textinputlayout_Remarks,textinputlayout_Todate,textinputlayout_FromDate;
        private EditText edtCallDuration,edtRemarks,edtToDate,edtFromDate;
        public FetchFeedbackHolder(View itemView) {
            super(itemView);
            textinputlayout_CallDuration= itemView.findViewById(R.id.textinputlayout_CallDuration);
            textinputlayout_Remarks= itemView.findViewById(R.id.textinputlayout_Remarks);
            textinputlayout_Todate= itemView.findViewById(R.id.textinputlayout_Todate);
            textinputlayout_FromDate= itemView.findViewById(R.id.textinputlayout_FromDate);
            edtCallDuration= itemView.findViewById(R.id.edtCallDuration);
            edtRemarks= itemView.findViewById(R.id.edtRemarks);
            edtToDate= itemView.findViewById(R.id.edtToDate);
            edtFromDate= itemView.findViewById(R.id.edtFromDate);

        }
    }
}
