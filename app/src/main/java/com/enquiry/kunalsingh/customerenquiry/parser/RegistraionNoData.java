package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kunal Singh on 06-05-2018.
 */
public class RegistraionNoData implements Parcelable{

    @Expose
    @SerializedName("vinModelList")
    public List<VinModelList> vinModelList;
    @Expose
    @SerializedName("registrationNo")
    public String registrationNo;
    @Expose
    @SerializedName("imeiNumber")
    public String imeiNumber;
    @Expose
    @SerializedName("deviceModel")
    public String deviceModel;
    @Expose
    @SerializedName("osName")
    public String osName;
    @Expose
    @SerializedName("appVersion")
    public String appVersion;
    @Expose
    @SerializedName("mobileNumber")
    public String mobileNumber;

    protected RegistraionNoData(Parcel in) {
        vinModelList = new ArrayList<VinModelList>();
        in.readList(vinModelList, VinModelList.class.getClassLoader());
        registrationNo = in.readString();
        imeiNumber = in.readString();
        deviceModel = in.readString();
        osName = in.readString();
        appVersion = in.readString();
        mobileNumber = in.readString();
    }

    public static final Creator<RegistraionNoData> CREATOR = new Creator<RegistraionNoData>() {
        @Override
        public RegistraionNoData createFromParcel(Parcel in) {
            return new RegistraionNoData(in);
        }

        @Override
        public RegistraionNoData[] newArray(int size) {
            return new RegistraionNoData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(vinModelList);
        parcel.writeString(registrationNo);
        parcel.writeString(imeiNumber);
        parcel.writeString(deviceModel);
        parcel.writeString(osName);
        parcel.writeString(appVersion);
        parcel.writeString(mobileNumber);
    }

    public static class VinModelList {
        @Expose
        @SerializedName("chassisNo")
        public String chassisNo;
        @Expose
        @SerializedName("registrationNo")
        public String registrationNo;
    }
}
