package com.enquiry.kunalsingh.customerenquiry.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.parser.ComplainParser;
import com.enquiry.kunalsingh.customerenquiry.parser.ComplainScreenDTLData;
import com.enquiry.kunalsingh.customerenquiry.utility.PrefManager;
import com.enquiry.kunalsingh.customerenquiry.utility.QueryBuilder;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.enquiry.kunalsingh.customerenquiry.utility.Utils;

import java.util.ArrayList;

/**
 * Created by Kunal Singh on 14-06-2018.
 */
public class CreateComplaint extends AppCompatActivity implements ComplainParser.onComplainListener {
    Spinner spinType, spinDealerName, spinRegNo;
    EditText edtComment;
    Button btnSubmit;
    private ComplainScreenDTLData complainScreenDTLData;
    private ArrayList<String> regNoList;
    private ArrayList<String> dealerName;
    private  ArrayList<String> type;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_complain);
        Toolbar toolbar = findViewById(R.id.header);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("Create Complain");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        regNoList=new ArrayList<>();
        dealerName=new ArrayList<>();
        type=new ArrayList<>();
        if (getIntent() != null && getIntent().getParcelableExtra("complainData") != null) {
            complainScreenDTLData = ((ComplainScreenDTLData) getIntent().getParcelableExtra("complainData"));
            for(int i=0;i<complainScreenDTLData.vinModelList.size();i++){
                regNoList.add(complainScreenDTLData.vinModelList.get(i).registrationNo);

            }

            for(int i=0;i<complainScreenDTLData.dealerModelList.size();i++){
                dealerName.add(complainScreenDTLData.dealerModelList.get(i).dealerName);

            }

            type.add("PSF");
            type.add("Generic");
        }

        spinType=findViewById(R.id.spinnerModelTypeValue);
        spinDealerName=findViewById(R.id.spinnerDealerValue);
        spinRegNo=findViewById(R.id.spinnerRegNoValue);
        edtComment=findViewById(R.id.edtCommentValue);
        btnSubmit=findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ComplainParser().executeQuery(CreateComplaint.this, QueryBuilder.getInstance().createComplainAPI(), QueryBuilder.getInstance().generateCreateComplainQuery(CreateComplaint.this, PrefManager.getKeyUserMobileNumber(CreateComplaint.this), Utils.versionNumber(CreateComplaint.this), Utils.getDeviceName(), Utils.deviceToken(CreateComplaint.this), spinRegNo.getSelectedItem().toString(),spinType.getSelectedItem().toString(),edtComment.getText().toString().trim()), CreateComplaint.this, Settings.CREATE_COMPLAIN_ID, true);
            }
        });

        ArrayAdapter<String> regNoAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, regNoList);
        spinRegNo.setAdapter(regNoAdapter);

        ArrayAdapter<String> dealerAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, dealerName);
        spinDealerName.setAdapter(dealerAdapter);

        ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, type);
        spinType.setAdapter(typeAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSuccess(int id, String message, Object object) {
        if(id==Settings.CREATE_COMPLAIN_ID){
            Utils.HitechToast(CreateComplaint.this,message);
            startActivity(new Intent(CreateComplaint.this,Complain.class));
            finishAffinity();
        }
    }

    @Override
    public void onError(String error, int id, BaseController.ErrorCode code) {
        if(error!=null){
            Utils.HitechToast(this,   error);
        }
    }
}
