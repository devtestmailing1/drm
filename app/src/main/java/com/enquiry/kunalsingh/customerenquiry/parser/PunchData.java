package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Kunal Singh on 23-08-2018.
 */
public class PunchData implements Parcelable{

    @Expose
    @SerializedName("punchDetails")
    public List<PunchDetails> punchDetails;

    protected PunchData(Parcel in) {
    }

    public static final Creator<PunchData> CREATOR = new Creator<PunchData>() {
        @Override
        public PunchData createFromParcel(Parcel in) {
            return new PunchData(in);
        }

        @Override
        public PunchData[] newArray(int size) {
            return new PunchData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }

    public static class PunchDetails implements Parcelable{
        @Expose
        @SerializedName("punchInTime")
        public String punchInTime;

        protected PunchDetails(Parcel in) {
            punchInTime = in.readString();
        }

        public static final Creator<PunchDetails> CREATOR = new Creator<PunchDetails>() {
            @Override
            public PunchDetails createFromParcel(Parcel in) {
                return new PunchDetails(in);
            }

            @Override
            public PunchDetails[] newArray(int size) {
                return new PunchDetails[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(punchInTime);
        }
    }
}
