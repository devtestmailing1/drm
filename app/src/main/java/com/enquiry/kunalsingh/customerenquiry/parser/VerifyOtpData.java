package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Kunal Singh on 19-04-2018.
 */
public class VerifyOtpData implements Parcelable {

    @Expose
    @SerializedName("token")
    public String token;

    @Expose
    @SerializedName("user")
    public User user;

    protected VerifyOtpData(Parcel in) {
        token = in.readString();
    }

    public static final Creator<VerifyOtpData> CREATOR = new Creator<VerifyOtpData>() {
        @Override
        public VerifyOtpData createFromParcel(Parcel in) {
            return new VerifyOtpData(in);
        }

        @Override
        public VerifyOtpData[] newArray(int size) {
            return new VerifyOtpData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(token);
    }





    public static class User {
        @Expose
        @SerializedName("isActive")
        public String isActive;
        @Expose
        @SerializedName("serviceEndDate")
        public String serviceEndDate;
        @Expose
        @SerializedName("city")
        public String city;
        @Expose
        @SerializedName("userType")
        public int userType;
        @Expose
        @SerializedName("mobileNumber")
        public String mobileNumber;
        @Expose
        @SerializedName("id")
        public int id;
    }
}