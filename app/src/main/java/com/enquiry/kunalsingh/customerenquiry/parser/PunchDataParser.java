package com.enquiry.kunalsingh.customerenquiry.parser;

import android.support.v4.app.FragmentActivity;

import com.android.volley.Request;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kunal Singh on 23-08-2018.
 */
public class PunchDataParser extends BaseController {

    public PunchDataParser.onPunchListener listener;
    public int id;
    public String TAG_MESSAGE = "message";

    public VerifyOtpDataParser executeQuery(FragmentActivity context, String url, JSONObject json, PunchDataParser.onPunchListener listener, int id, boolean toShowDialog) {
        // RequestManager.allowAllSSL();
        this.listener = listener;
        this.id = id;
        init(context, url, Request.Method.POST, json, toShowDialog);

        return null;
    }

    @Override
    public void onComplete(JSONArray response, String message) {
        switch (id) {
            case Settings.CHECK_PUNCH_IN_ID:
                listener.onSuccess(id, message, null);
                if (response != null) {
                    try {
                        listener.onSuccess(id, "", parserUserInfo(response.getJSONObject(0)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                break;

            case Settings.PUNCH_IN_OUT_ID:
                if (response != null) {
                    try {
                        listener.onSuccess(id, "", parserUserInfo(response.getJSONObject(0)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                break;

            case Settings.SUBMIT_FEEDBACK_ID:
                listener.onSuccess(id, message, null);
                break;
            case Settings.VALIDATE_MOB_ID:
                listener.onSuccess(id, message, null);
                break;
        }

    }

    @Override
    public void onSuccess(JSONArray response) {

    }

    @Override
    public void onFailureData(String error, ErrorCode code, JSONArray response) {

    }

    @Override
    public void onFailure(String error, ErrorCode code) {
        if (error != null) {
            listener.onError(error, id, code);
        }
    }


    public interface onPunchListener {
        public void onSuccess(int id, String message, Object object);
        public void onError(String error, int id, ErrorCode code);
    }


    public PunchData parserUserInfo(JSONObject object) {
        Gson gson = new Gson();
        PunchData response = gson.fromJson(object.toString(), PunchData.class);
        return response;
    }
}
