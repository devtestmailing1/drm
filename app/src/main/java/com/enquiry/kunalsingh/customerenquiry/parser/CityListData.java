package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kunal Singh on 22-05-2018.
 */
public class CityListData implements Parcelable{

    @Expose
    @SerializedName("userResponse")
    public List<UserResponse> userResponse;

    protected CityListData(Parcel in) {
        userResponse= new ArrayList<CityListData.UserResponse>();
        in.readList(userResponse,DistrictListData.CityList.class.getClassLoader());
    }

    public static final Creator<CityListData> CREATOR = new Creator<CityListData>() {
        @Override
        public CityListData createFromParcel(Parcel in) {
            return new CityListData(in);
        }

        @Override
        public CityListData[] newArray(int size) {
            return new CityListData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(userResponse);
    }

    public static class UserResponse implements Parcelable{
        @Expose
        @SerializedName("isActive")
        public boolean isActive;
        @Expose
        @SerializedName("districtId")
        public int districtId;
        @Expose
        @SerializedName("cityDesc")
        public String cityDesc;
        @Expose
        @SerializedName("cityCode")
        public String cityCode;
        @Expose
        @SerializedName("cityId")
        public int cityId;

        protected UserResponse(Parcel in) {
            isActive = in.readByte() != 0;
            districtId = in.readInt();
            cityDesc = in.readString();
            cityCode = in.readString();
            cityId = in.readInt();
        }

        public static final Creator<UserResponse> CREATOR = new Creator<UserResponse>() {
            @Override
            public UserResponse createFromParcel(Parcel in) {
                return new UserResponse(in);
            }

            @Override
            public UserResponse[] newArray(int size) {
                return new UserResponse[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeByte((byte) (isActive ? 1 : 0));
            parcel.writeInt(districtId);
            parcel.writeString(cityDesc);
            parcel.writeString(cityCode);
            parcel.writeInt(cityId);
        }
    }
}
