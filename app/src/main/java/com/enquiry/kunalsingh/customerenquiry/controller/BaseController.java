package com.enquiry.kunalsingh.customerenquiry.controller;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.common_ui.EnquiryProgressDialog;
import com.enquiry.kunalsingh.customerenquiry.utility.CustomJsonObjectRequest;
import com.enquiry.kunalsingh.customerenquiry.utility.Logger;
import com.enquiry.kunalsingh.customerenquiry.utility.RequestManager;
import com.enquiry.kunalsingh.customerenquiry.utility.Utils;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Base Controller Class
 * Created by Kunal Singh
 * Date 08 January 2017
 */
public abstract class BaseController implements Response.Listener<JSONObject>, Response.ErrorListener {

    EnquiryProgressDialog pDialog;
    public String TAG = "BaseController";
    public String TAG_MESSAGE_CODE = "responseCode";
    public String TAG_RESPONSE = "responseData";
    public String TAG_MESSAGE = "message";
    public Context mContext;
    public String TAG_CODE = "code";
    public ErrorCode code;
    public enum ErrorCode {NETWORK, SERVER, SESSION, NEW_USER, CHANGE_MOBILE, CHANGE_PASSWORD,OTHER}
    JsonObjectRequest request;

    public void init(Context context, String url, int type, JSONObject json, boolean isDialog) {
        RequestManager.allowAllSSL();
        if (isDialog) {
            pDialog = new EnquiryProgressDialog(context);
            pDialog.getWindow().setDimAmount(0.0f);
            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }
        this.mContext = context;
        request = new JsonObjectRequest(type, url, json, this, this);
        request.setRetryPolicy(new DefaultRetryPolicy(9000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);
    }


    @Override
    public void onResponse(JSONObject response) {
        Logger.logInfo(TAG, "onResponse :", response.toString());
        String message = null;
        if (response != null) {
            try {
                if (!response.isNull(TAG_MESSAGE_CODE)) {

                    JSONObject messageCode = response.getJSONObject(TAG_MESSAGE_CODE);

                    if (!messageCode.isNull(TAG_MESSAGE)) {
                        message = messageCode.getString(TAG_MESSAGE);
                    }
                    if (!messageCode.isNull(TAG_CODE)) {

                        if (messageCode.getString(TAG_CODE).equalsIgnoreCase("EC200")) {
                            if (!response.isNull(TAG_RESPONSE)) {
                                onComplete(response.getJSONArray(TAG_RESPONSE), message);
                            }
                            else {
                                onComplete(null, message);
                            }

                        }else if (messageCode.getString(TAG_CODE).equalsIgnoreCase("EC220")) {
                            if (!messageCode.isNull(TAG_MESSAGE)) {
                                onFailureData(messageCode.getString(TAG_MESSAGE), ErrorCode.NEW_USER,response.getJSONArray(TAG_RESPONSE));
                            }
                        }else if (messageCode.getString(TAG_CODE).equalsIgnoreCase("EC210")) {
                            if (!messageCode.isNull(TAG_MESSAGE)) {
                                onFailureData(messageCode.getString(TAG_MESSAGE), ErrorCode.CHANGE_MOBILE,response.getJSONArray(TAG_RESPONSE));
                            }
                        } else if(messageCode.getString(TAG_CODE).equalsIgnoreCase("EC204")){
                            if (!messageCode.isNull(TAG_MESSAGE)) {
                                onFailure(messageCode.getString(TAG_MESSAGE), ErrorCode.CHANGE_PASSWORD);
                            }
                        }else {
                            if (!messageCode.isNull(TAG_MESSAGE)) {
                                onFailure(messageCode.getString(TAG_MESSAGE), ErrorCode.OTHER);
                            }
                        }
                    }
                }
                //for employee refferal
                if (!response.isNull(TAG_RESPONSE)) {
                    onSuccess(response.getJSONArray(TAG_RESPONSE));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (pDialog != null && pDialog.isShowing())
            try {
                pDialog.dismiss();
            }catch (Exception e){
                e.printStackTrace();
            }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Logger.logInfo(TAG, "onErrorResponse :", error.toString());
        if (pDialog != null && pDialog.isShowing())
            pDialog.dismiss();
        if (error instanceof NetworkError) {
            if (!Utils.isNetworkAvailable(mContext))
                onFailure(mContext.getResources().getString(R.string.stringInternetError), ErrorCode.NETWORK);
            else
                onFailure(mContext.getResources().getString(R.string.stringServerError), ErrorCode.SERVER);

        } else if (error instanceof ServerError) {
            onFailure(mContext.getResources().getString(R.string.stringServerError), ErrorCode.SERVER);

        } else if (error instanceof NoConnectionError) {
            onFailure(mContext.getResources().getString(R.string.stringInternetError), ErrorCode.NETWORK);

        } else if (error instanceof TimeoutError) {
            onFailure(mContext.getResources().getString(R.string.stringServerError), ErrorCode.SERVER);

        }

        // As of f605da3 the following should work
      /*  NetworkResponse response = error.networkResponse;
        if (error instanceof ServerError && response != null) {
            try {
                String res = new String(response.data,
                        HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                // Now you can use any deserializer to make sense of data
                JSONObject obj = new JSONObject(res);
            } catch (UnsupportedEncodingException e1) {
                // Couldn't properly decode data to string
                e1.printStackTrace();
            } catch (JSONException e2) {
                // returned data is not JSONObject?
                e2.printStackTrace();
            }
        }*/
    }



    public abstract void onComplete(JSONArray response, String message);

    public abstract void onSuccess(JSONArray response);

    public abstract void onFailure(String error, ErrorCode code);

    public abstract void onFailureData(String error, ErrorCode code,JSONArray response);

    public void initWithHeader(Context context, String url, int type, JSONObject json) {

        pDialog = new EnquiryProgressDialog(context);
        // pDialog.setMessage("Loading...");
        pDialog.getWindow().setDimAmount(0.0f);
        pDialog.setCancelable(false);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.show();
        this.mContext = context;
        CustomJsonObjectRequest request = new CustomJsonObjectRequest(type, url, json, this, this);

        request.setRetryPolicy(new DefaultRetryPolicy(9000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);
    }

    public void initWithHeader(Context context, String url, int type, JSONObject json, boolean no) {
        if (no) {
            pDialog = new EnquiryProgressDialog(context);
            pDialog.getWindow().setDimAmount(0.0f);
            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);
            // pDialog.setMessage("Loading...");
            pDialog.show();
        }
        this.mContext = context;
        CustomJsonObjectRequest request = new CustomJsonObjectRequest(type, url, json, this, this);

        request.setRetryPolicy(new DefaultRetryPolicy(9000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);
    }


    public void cancel(Context context)
    {
        if(request!=null) {
            if(pDialog!=null)
                pDialog.cancel();
            request.cancel();
        }
    }


}


