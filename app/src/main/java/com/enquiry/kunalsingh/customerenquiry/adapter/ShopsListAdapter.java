package com.enquiry.kunalsingh.customerenquiry.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.parser.ShopListItem;

import java.util.ArrayList;
import java.util.HashMap;

public class ShopsListAdapter extends RecyclerView.Adapter<ShopsListAdapter.SearchShopHolder> {
    private ArrayList<HashMap<String, String>> allShopList;
    private ArrayList<HashMap<String,String>> allStates;
    private Context mContext;
    private ShopsListAdapter.ItemsListener mListener;

    public ShopsListAdapter(Context context, ArrayList<HashMap<String, String>> allShopList, ShopsListAdapter.ItemsListener itemListener,
                             ArrayList<HashMap<String,String>> allStates) {
        this.mContext = context;
        this.allShopList = allShopList;
        this.mListener = itemListener;
        this.allStates = allStates;
    }

    @NonNull
    @Override
    public ShopsListAdapter.SearchShopHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.locality_heading, viewGroup, false);
        return new ShopsListAdapter.SearchShopHolder(view,viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopsListAdapter.SearchShopHolder holder, final int position) {

        Log.d("shopname >>>", allShopList.get(position).get("shopName"));
        holder.localityHeading.setText(allShopList.get(position).get("shopName"));

    }

    @Override
    public int getItemViewType(int position) {
       /* int viewType = 1;
        if (position == 0)viewType = 0;*/
        return position;
    }

    @Override
    public int getItemCount() {
        return allShopList.size();
    }


    class SearchShopHolder extends RecyclerView.ViewHolder {
        TextView  stateHeading, localityHeading;

        SearchShopHolder(final View itemView, int Viewtype) {
            super(itemView);
         //   stateHeading = itemView.findViewById(R.id.state_heading);
            localityHeading = itemView.findViewById(R.id.locality_heading);
        }
    }

    public interface ItemsListener {
        void ItemClick(int shopId);
    }


    public void filterList(ArrayList<ShopListItem> filteredList) {
      //  list = filteredList;
        notifyDataSetChanged();
    }
}
