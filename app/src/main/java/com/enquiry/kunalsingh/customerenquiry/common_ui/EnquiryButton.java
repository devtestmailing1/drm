package com.enquiry.kunalsingh.customerenquiry.common_ui;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Kunal Singh on 28-02-2018.
 */

public class EnquiryButton extends android.support.v7.widget.AppCompatButton implements View.OnTouchListener {
    Drawable newColor;

    public EnquiryButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub
        isInEditMode();
        init();
    }

    public EnquiryButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        isInEditMode();
        init();
    }

    public EnquiryButton(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
        isInEditMode();
        init();
    }

    @Override
    public void setTypeface(Typeface tf, int style) {
        Typeface normalTypeface = Typeface.createFromAsset(getContext().getAssets(), "font/OpenSans-Regular.ttf");
        Typeface boldTypeface = Typeface.createFromAsset(getContext().getAssets(), "font/OpenSans-Bold.ttf");
        if (style == Typeface.BOLD) {
            // super.setTypeface(FontCache.getTypeface(getContext(), getResources().getString(R.string.font_bold)));
            super.setTypeface(boldTypeface);
        } else {
            super.setTypeface(normalTypeface);
        }
    }

    public void init() {
        setOnTouchListener(this);
        newColor = getBackground();
        newColor.setAlpha(170);
        getBackground().setAlpha(255);
    }

    public void turnOnImage() {
        int pL = getPaddingLeft();
        int pT = getPaddingTop();
        int pR = getPaddingRight();
        int pB = getPaddingBottom();
        TransitionDrawable trans = new TransitionDrawable(new Drawable[]{getBackground(), newColor});
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            setBackground(trans);
        } else {
            setBackgroundDrawable(trans);
        }

        trans.setCrossFadeEnabled(true);
        trans.startTransition(400);
        setPadding(pL, pT, pR, pB);
    }

   /* public void turnOffImage()
    {
        int pL = getPaddingLeft();
        int pT = getPaddingTop();
        int pR = getPaddingRight();
        int pB = getPaddingBottom();
        TransitionDrawable trans = new TransitionDrawable(new Drawable[]{(Drawable) getResources().getDrawable(R.drawable.store_action_button_on_background), (Drawable) getResources().getDrawable(R.drawable.store_action_button_background)});
        setBackground(trans);
        trans.setCrossFadeEnabled(true);
        trans.startTransition(400);
        setPadding(pL,pT,pR,pB);
    }*/

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        Log.i("----", motionEvent.getAction() + "");
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            Log.i("----", "Down");
            //getBackground().setAlpha(170);
            turnOnImage();
        } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            Log.i("----", "ACTION_UP");
            getBackground().setAlpha(255);
            // turnOffImage();
        } else if (motionEvent.getAction() == MotionEvent.ACTION_CANCEL) {
            Log.i("----", "ACTION_CANCEL");
            // setBackgroundResource(R.drawable.store_action_button_background);
            getBackground().setAlpha(255);
            //turnOffImage();
        }
        return false;
    }


}