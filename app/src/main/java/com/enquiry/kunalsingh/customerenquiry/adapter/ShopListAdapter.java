package com.enquiry.kunalsingh.customerenquiry.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.parser.ShopListItem;

import java.util.ArrayList;

/**
 * Created by Kunal Singh on 22-08-2018.
 */
public class ShopListAdapter extends RecyclerView.Adapter<ShopListAdapter.SearchShopHolder> {
    private ArrayList<ShopListItem> list;
    private Context mContext;
    private ShopListAdapter.ItemListener mListener;
    public ShopListAdapter(Context context, ArrayList<ShopListItem> list, ShopListAdapter.ItemListener itemListener) {
        this.mContext = context;
        this.list = list;
        mListener=itemListener;
    }

    @Override
    public void onBindViewHolder(@NonNull ShopListAdapter.SearchShopHolder holder, final int position) {
        holder.top_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onItemClick(list.get(position).getShopId());
            }
        });
        if(list.get(position).getShopName()!=null) {
            holder.tvShoapName.setText(list.get(position).getShopName());
        }else{
            holder.tvShoapName.setVisibility(View.GONE);
        }

        if(list.get(position).getShopAddress()!=null){
            holder.tvAddress.setText(list.get(position).getShopAddress());
        }else {
            holder.tvAddress.setVisibility(View.GONE);
        }
    }

    @NonNull
    @Override
    public ShopListAdapter.SearchShopHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.custom_shoap_list, viewGroup, false);
        return new ShopListAdapter.SearchShopHolder(view );
    }


    @Override
    public int getItemCount() {
        return list.size();
    }



    class SearchShopHolder extends RecyclerView.ViewHolder{
        TextView tvShoapName,tvAddress;
        LinearLayout top_layout;
        SearchShopHolder(final View itemView) {
            super(itemView);
            tvShoapName= itemView.findViewById(R.id.tvShoapName);
            tvAddress= itemView.findViewById(R.id.tvAddress);
            top_layout=itemView.findViewById(R.id.top_layout);
        }
    }
    public interface ItemListener {
        void onItemClick(int shopId);
    }

    public void filterList(ArrayList<ShopListItem> filteredList) {
        list = filteredList;
        notifyDataSetChanged();
    }
}
