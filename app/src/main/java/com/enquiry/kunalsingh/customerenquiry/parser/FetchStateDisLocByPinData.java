package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Kunal Singh on 23-05-2018.
 */
public class FetchStateDisLocByPinData implements Parcelable{

    @Expose
    @SerializedName("city")
    public String city;
    @Expose
    @SerializedName("cityId")
    public int cityId;
    @Expose
    @SerializedName("stateId")
    public int stateId;
    @Expose
    @SerializedName("state")
    public String state;
    @Expose
    @SerializedName("tehsil")
    public String tehsil;
    @Expose
    @SerializedName("countryDesc")
    public String countryDesc;
    @Expose
    @SerializedName("district")
    public String district;
    @Expose
    @SerializedName("districtId")
    public int districtId;
    @Expose
    @SerializedName("usrCountryId")
    public int usrCountryId;
    @Expose
    @SerializedName("localityName")
    public String localityName;
    @Expose
    @SerializedName("pinCode")
    public String pinCode;

    protected FetchStateDisLocByPinData(Parcel in) {
        city = in.readString();
        cityId = in.readInt();
        stateId = in.readInt();
        state = in.readString();
        tehsil = in.readString();
        countryDesc = in.readString();
        district = in.readString();
        districtId = in.readInt();
        usrCountryId = in.readInt();
        localityName = in.readString();
        pinCode = in.readString();
    }

    public static final Creator<FetchStateDisLocByPinData> CREATOR = new Creator<FetchStateDisLocByPinData>() {
        @Override
        public FetchStateDisLocByPinData createFromParcel(Parcel in) {
            return new FetchStateDisLocByPinData(in);
        }

        @Override
        public FetchStateDisLocByPinData[] newArray(int size) {
            return new FetchStateDisLocByPinData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(city);
        parcel.writeInt(cityId);
        parcel.writeInt(stateId);
        parcel.writeString(state);
        parcel.writeString(tehsil);
        parcel.writeString(countryDesc);
        parcel.writeString(district);
        parcel.writeInt(districtId);
        parcel.writeInt(usrCountryId);
        parcel.writeString(localityName);
        parcel.writeString(pinCode);
    }
}
