package com.enquiry.kunalsingh.customerenquiry.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.adapter.ShopListAdapter;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.parser.DashboardDetailsData;
import com.enquiry.kunalsingh.customerenquiry.parser.HomeDataParser;
import com.enquiry.kunalsingh.customerenquiry.parser.ShopListItem;
import com.enquiry.kunalsingh.customerenquiry.utility.QueryBuilder;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;

import java.util.ArrayList;

/**
 * Created by Kunal Singh on 24-09-2018.
 */
public class DashboardDetails extends AppCompatActivity implements HomeDataParser.onHomeListener, ShopListAdapter.ItemListener {
    private String keyName;
    DashboardDetailsData dashboardDetailsData;
    private ArrayList<ShopListItem> shopList = new ArrayList<>();
    private ShopListItem shopListItem;
    ShopListAdapter shopListAdapter;
    RecyclerView recylShopList;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_details);
        Toolbar toolbar = findViewById(R.id.header);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("Dashboard Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        recylShopList=findViewById(R.id.recylShopList);
        if (getIntent() != null) {
            keyName = getIntent().getStringExtra("KEYNAME");
            if(keyName.equalsIgnoreCase("TURNBLUE")){
                new HomeDataParser().executeQuery(this, QueryBuilder.getInstance().getDashboardDetailsAPI(), QueryBuilder.getInstance().generateDashboardDetailsQuery(this,"TURNBLUE"), this, Settings.DASHBOARD_DETAILS_ID, true);
            }else if(keyName.equalsIgnoreCase("PENDINGVISIT")){
                new HomeDataParser().executeQuery(this, QueryBuilder.getInstance().getDashboardDetailsAPI(), QueryBuilder.getInstance().generateDashboardDetailsQuery(this,"PENDINGVISIT"), this, Settings.DASHBOARD_DETAILS_ID, true);
            }else if(keyName.equalsIgnoreCase("CALLMADE")){
                new HomeDataParser().executeQuery(this, QueryBuilder.getInstance().getDashboardDetailsAPI(), QueryBuilder.getInstance().generateDashboardDetailsQuery(this,"CALLMADE"), this, Settings.DASHBOARD_DETAILS_ID, true);
            }else if(keyName.equalsIgnoreCase("CALLPENDING")){
                new HomeDataParser().executeQuery(this, QueryBuilder.getInstance().getDashboardDetailsAPI(), QueryBuilder.getInstance().generateDashboardDetailsQuery(this,"CALLPENDING"), this, Settings.DASHBOARD_DETAILS_ID, true);
            }

            }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSuccess(int id, String message, Object object) {
        if(id==Settings.DASHBOARD_DETAILS_ID){
            dashboardDetailsData = ((DashboardDetailsData) object);
            shopList.clear();
            for(int i=0;i<dashboardDetailsData.dashboardShopDetails.size();i++){
                shopListItem = new ShopListItem();
                shopListItem.setShopID(dashboardDetailsData.dashboardShopDetails.get(i).shopId);
                shopListItem.setShopName(dashboardDetailsData.dashboardShopDetails.get(i).shopName);
                shopListItem.setShopAddress(dashboardDetailsData.dashboardShopDetails.get(i).shopAddress);
                shopListItem.setShopLocality(dashboardDetailsData.dashboardShopDetails.get(i).localityName);
                shopList.add(shopListItem);

            }

            shopListAdapter = new ShopListAdapter(this, shopList, this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            recylShopList.setLayoutManager(layoutManager);
            recylShopList.setAdapter(shopListAdapter);
        }
    }

    @Override
    public void onError(String error, int id, BaseController.ErrorCode code) {

    }

    @Override
    public void onItemClick(int shopId) {
        startActivity(new Intent(this,ShopDetailsActivity.class).putExtra("ShopID",String.valueOf(shopId)));
    }
}
