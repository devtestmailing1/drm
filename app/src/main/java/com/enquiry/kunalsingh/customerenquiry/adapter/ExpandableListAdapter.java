package com.enquiry.kunalsingh.customerenquiry.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.enquiry.kunalsingh.customerenquiry.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Kunal Singh on 20-08-2018.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;
    ExpandableListAdapter.ItemListener mListener;

    final ArrayList<String> fabric=new ArrayList<>();
    final ArrayList<String> product=new ArrayList<>();
    final ArrayList<String> party=new ArrayList<>();
    final ArrayList<String> rank=new ArrayList<>();

    public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<String>> listChildData,ExpandableListAdapter.ItemListener itemListener) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.mListener=itemListener;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);
        final String groupName=(String)getGroup(groupPosition);

        //if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);
        //}

        final TextView txtListChild = (TextView) convertView
                .findViewById(R.id.lblListItem);
        final CheckBox chkBox=convertView.findViewById(R.id.cbSelect);

        //String selectedFromList = (lv.getItemAtPosition(position));

        txtListChild.setText(childText);

        chkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckBox checkBox = (CheckBox)view;
                if(checkBox.isChecked()) {
                    if(groupName.equalsIgnoreCase("Fabric Type")){
                        fabric.add(childText);
                    }else if(groupName.equalsIgnoreCase("Product Type")){
                        product.add(childText);
                    }else if(groupName.equalsIgnoreCase("Party Type")){
                        party.add(childText);
                    }else if(groupName.equalsIgnoreCase("Ranking")){
                        rank.add(childText);
                    }
                    //Utils.HitechToast(_context, groupName+":"+childText);
                }else{

                    if(groupName.equalsIgnoreCase("Fabric Type")){
                        for(String fabricStr : fabric)
                        {
                            if (fabricStr.equals(childText))
                            {
                                fabric.remove(childText);
                                break;
                            }
                        }
                    }else if(groupName.equalsIgnoreCase("Product Type")){
                        for(String productStr : product)
                        {
                            if (productStr.equals(childText))
                            {
                              //  Utils.HitechToast(_context,childText);
                                product.remove(childText);
                                break;
                            }
                        }
                    }else if(groupName.equalsIgnoreCase("Party Type")){
                        for(String partyStr : party)
                        {
                            if (partyStr.equals(childText))
                            {
                                party.remove(childText);
                                break;
                            }
                        }
                    }else if(groupName.equalsIgnoreCase("Ranking")){
                        for(String rankStr : rank)
                        {
                            if (rankStr.equals(childText))
                            {
                                rank.remove(childText);
                                break;
                            }
                        }
                    }

                }
                mListener.onItemClick(product,fabric,party,rank);

            }

        });

        if (product.contains(childText) || fabric.contains(childText) || party.contains(childText) || rank.contains(childText)){
            chkBox.setChecked(true);
        }else {
            chkBox.setChecked(false);
        }


        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        TextView lblListHeader = convertView.findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

      //  ExpandableListView listView = (ExpandableListView)parent;
      //  listView.expandGroup(groupPosition);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    public interface ItemListener {
        void onItemClick(ArrayList<String> product,ArrayList<String> fabric, ArrayList<String> party,ArrayList<String> rank);
    }

    public void clearValues(){
        product.clear();
        fabric.clear();
        party.clear();
        rank.clear();
    }

}
