package com.enquiry.kunalsingh.customerenquiry.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.parser.CityListData;
import com.enquiry.kunalsingh.customerenquiry.parser.CityListParser;
import com.enquiry.kunalsingh.customerenquiry.parser.DistrictListData;
import com.enquiry.kunalsingh.customerenquiry.parser.DistrictListParser;
import com.enquiry.kunalsingh.customerenquiry.parser.FetchDealerLocData;
import com.enquiry.kunalsingh.customerenquiry.parser.FetchDealerLocparser;
import com.enquiry.kunalsingh.customerenquiry.parser.FetchStateDisLocByPinData;
import com.enquiry.kunalsingh.customerenquiry.parser.FetchStateDisLocByPinParser;
import com.enquiry.kunalsingh.customerenquiry.parser.LocalityListData;
import com.enquiry.kunalsingh.customerenquiry.parser.LocalityListParser;
import com.enquiry.kunalsingh.customerenquiry.parser.StateListData;
import com.enquiry.kunalsingh.customerenquiry.parser.StateListParser;
import com.enquiry.kunalsingh.customerenquiry.utility.GetAddressIntentService;
import com.enquiry.kunalsingh.customerenquiry.utility.PrefManager;
import com.enquiry.kunalsingh.customerenquiry.utility.QueryBuilder;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.enquiry.kunalsingh.customerenquiry.utility.Utils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


/**
 * Created by Kunal Singh on 13-05-2018.
 */
public class DealerLocation extends AppCompatActivity implements StateListParser.onStateListListener, DistrictListParser.onLoginListener, CityListParser.onCityListListener, LocalityListParser.onLocalityListListener, FetchStateDisLocByPinParser.onstateDistLocListener, View.OnClickListener, FetchDealerLocparser.onDealerLocListener{
    private static String defaultStateName;
    EditText pinCodeValue;
    private Spinner spinnerCountryValue,spinnerStateValue,spinnerDistValue,spinnerCityValue,spinnerLocalityValue;
    private StateListData stateListData;
    private DistrictListData districtListData;
    private CityListData cityListData;
    private LocalityListData localityListData;
    private FetchStateDisLocByPinData fetchStateDisLocByPinData;
    private Button btnSubmit;
    private FetchDealerLocData fetchDealerLocData;
    public static int cityID, distID, stateID, loacID;
    private Map<Integer,String> map_state_values = new HashMap<Integer, String>();
    private  Map<Integer,String> map_dist_values = new HashMap<Integer,String>();
    private Map<Integer,String> map_city_values = new HashMap<Integer,String>();
    Map<String,String> map_locality_values = new HashMap();
    public Map<Integer,String> map_pin_values = new HashMap();


    private Location currentLocation;

    private LocationCallback locationCallback;

    private FusedLocationProviderClient fusedLocationClient;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 2;

    private LocationAddressResultReceiver addressResultReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer_locator);
        Toolbar toolbar = findViewById(R.id.header);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("Search GreavesCare Locator");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        addressResultReceiver = new LocationAddressResultReceiver(new Handler());
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                currentLocation = locationResult.getLocations().get(0);
                getAddress();
            };
        };
        startLocationUpdates();
        initialization();
        query();
        btnSubmit.setOnClickListener(this);
    }

    private void query() {
        //new StateListParser().executeQuery(DealerLocation.this, QueryBuilder.getInstance().stateListAPI(), QueryBuilder.getInstance().generateStateListQuery(DealerLocation.this, PrefManager.getKeyUserMobileNumber(DealerLocation.this).trim(), Utils.versionNumber(this), Utils.getDeviceName(), Utils.deviceToken(this)), DealerLocation.this, Settings.STATE_LIST_ID, true);
    }

    private void initialization() {
        btnSubmit=findViewById(R.id.btnSubmit);
        pinCodeValue=findViewById(R.id.pinCodeValue);
        spinnerCountryValue=findViewById(R.id.spinnerCountryValue);
        spinnerStateValue=findViewById(R.id.spinnerStateValue);
        spinnerCityValue=findViewById(R.id.spinnerCityValue);
        spinnerDistValue=findViewById(R.id.spinnerDistValue);
        spinnerLocalityValue=findViewById(R.id.spinnerLocalityValue);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSuccess(int id, String message, Object object) {
        populateCountry();
        List<String> stateName=new ArrayList<>();
        List<String> districtName=new ArrayList<>();
        List<String> cityName=new ArrayList<>();
        List<String> localityName=new ArrayList<>();
        stateName.add("Select State");
        if (id == Settings.STATE_LIST_ID) {
            if (object != null) {
                stateListData = ((StateListData) object);
                for(int i=0;i<stateListData.stateList.size();i++){
                    stateName.add(stateListData.stateList.get(i).stateDesc);
                    map_state_values.put(stateListData.stateList.get(i).stateId,stateListData.stateList.get(i).stateDesc);
                }

                fillStateSpinner(stateName,defaultStateName);
                districtName.clear();
                cityName.clear();
                localityName.clear();
                districtName.add("Select District");
                cityName.add("Select City");
                localityName.add("Select Locality");
                fillDistrictSpinner(districtName,defaultStateName);
                fillCitySpinner(cityName);
                fillLocalitySpinner(localityName);
                //  populateStateList();

            }
        }


        if(id==Settings.FETCH_DISTRICT_LIST_ID) {
            if (object != null) {
                districtListData = ((DistrictListData) object);
                districtName.add("Select District");
                // For District
                for (int i = 0; i < districtListData.cityList.size(); i++) {
                    districtName.add(districtListData.cityList.get(i).cityDesc);
                    map_dist_values.put(districtListData.cityList.get(i).cityId, districtListData.cityList.get(i).cityDesc);
                }
                String defaultDistrict="EAST";
                fillDistrictSpinner(districtName,defaultDistrict);
                cityName.clear();
                localityName.clear();
                cityName.add("Select City");
                localityName.add("Select Locality");
                fillCitySpinner(cityName);
                fillLocalitySpinner(localityName);
                // populateDistrictList();
            }
        }

        if(id==Settings.FETCH_CITY_LIST_ID) {
            if (object != null) {
                cityListData = ((CityListData) object);
                cityName.add("Select City");
                // For District
                for (int i = 0; i < cityListData.userResponse.size(); i++) {
                    cityName.add(cityListData.userResponse.get(i).cityDesc);
                    map_city_values.put(cityListData.userResponse.get(i).cityId, cityListData.userResponse.get(i).cityDesc);
                }
                fillCitySpinner(cityName);
                localityName.clear();
                localityName.add("Select Locality");
                fillLocalitySpinner(localityName);
                // populateCityList();
            }
        }


        if(id==Settings.FETCH_LOCALITY_LIST_ID) {
            if (object != null) {
                localityListData = ((LocalityListData) object);
                if (localityListData.userResponse != null && localityListData.userResponse.size() > 0) {
                    localityName.add("Select Locality");
                    // For District
                    for (int i = 0; i < localityListData.userResponse.size(); i++) {
                        localityName.add(localityListData.userResponse.get(i).localityName);
                        map_locality_values.put(localityListData.userResponse.get(i).pinCode, localityListData.userResponse.get(i).localityName.trim());
                        map_pin_values.put(localityListData.userResponse.get(i).pinId,localityListData.userResponse.get(i).pinCode);
                    }
                    fillLocalitySpinner(localityName);

                    // populateLocalityList();
                }
            }
        }



        if(id==Settings.FETCH_STATE_DIST_CITY_FROM_PIN_ID){
            if(object!=null){
                fetchStateDisLocByPinData=((FetchStateDisLocByPinData)object);
                // int spinnerPosition = adapter.getPosition(fetchStateDisLocByPinData.state);
                // spinnerStateValue.setSelection(spinnerPosition);
            }
        }


        if(id== Settings.FETCH_DEALER_FROM_DEALER_LOC_ID) {
            if (object != null) {
                fetchDealerLocData = ((FetchDealerLocData) object);
                if (fetchDealerLocData.dealerLocatorList != null && fetchDealerLocData.dealerLocatorList.size() > 0) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("dealerLocData", fetchDealerLocData);
                    startActivity(new Intent(DealerLocation.this, DealerLocationList.class).putExtras(bundle));
                    finish();

                }
            }
        }
    }



    @Override
    public void onError(String error, int id, BaseController.ErrorCode code) {
        if(error!=null){
            Utils.HitechToast(DealerLocation.this,error);
        }
    }





    /*todo: Populate Country List
     *   Created By Kunal Singh
     *   Date 22 May 2018
     **/
    private void populateCountry(){
        ArrayList<String> arrayListCountry=new ArrayList<>();
        arrayListCountry.add("India");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, arrayListCountry);
        spinnerCountryValue.setAdapter(adapter);
    }



    /*todo: Populate State List
     *   Created By Kunal Singh
     *   Date 22 May 2018
     **/
    private void populateStateList(){
        String[] spinnerStateArray = new String[stateListData.stateList.size()];
        final HashMap<Integer, String> spinnerStateMap = new HashMap<>();
        for (int i = 0; i < stateListData.stateList.size(); i++) {
            spinnerStateMap.put(stateListData.stateList.get(i).stateId, stateListData.stateList.get(i).stateDesc);
            spinnerStateArray[i] = stateListData.stateList.get(i).stateDesc;
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, spinnerStateArray);
        spinnerStateValue.setAdapter(adapter);

        // spinnerStateValue.setSelection(((ArrayAdapter<String>)spinnerStateValue.getAdapter()).getPosition(stateListData.defaultState)); //spinnerStateMap.get(selectState)

        spinnerStateValue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                for (Map.Entry<Integer, String> stateEntry : spinnerStateMap.entrySet()) {
                    if (stateEntry.getValue().equals(spinnerStateValue.getSelectedItem().toString())) {
                        stateID = stateEntry.getKey();
                        //  stateIDIntent= stateEntry.getKey();
                        if(spinnerStateValue.getSelectedItem().toString().equalsIgnoreCase("Please Select")) {
                            //  spinerDistrict.setAdapter(null);
                        } else {
                            new DistrictListParser().executeQuery(DealerLocation.this, QueryBuilder.getInstance().districtListApi(), QueryBuilder.getInstance().generateDistrictListQuery(DealerLocation.this,Integer.toString(stateEntry.getKey())), DealerLocation.this, Settings.FETCH_DISTRICT_LIST_ID, true);
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

















    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.btnSubmit:
                String pincode;
                if(pinCodeValue.getText().toString().equalsIgnoreCase("")){
                    pincode="";
                }else{
                    pincode=pinCodeValue.getText().toString().trim();
                }
                new FetchDealerLocparser().executeQuery(DealerLocation.this, QueryBuilder.getInstance().fetchDealerFromDealerLocAPI(), QueryBuilder.getInstance().generateFetchDealerFromDealerLocQuery(DealerLocation.this, pincode, Utils.versionNumber(this), Utils.getDeviceName(), Utils.deviceToken(this),pinCodeValue.getText().toString(),"",String.valueOf(distID),String.valueOf(cityID),String.valueOf(stateID),""), DealerLocation.this, Settings.FETCH_DEALER_FROM_DEALER_LOC_ID, true);
                break;
        }

    }


    public void fillStateSpinner(List<String> stateNameList, String defaultState){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, stateNameList);
        spinnerStateValue.setAdapter(adapter);
        selectSpinnerValue(spinnerStateValue,defaultState);
        spinnerStateValue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinnerStateValue.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select State"))) {
                    for (Map.Entry<Integer, String> stateEntry : map_state_values.entrySet()) {
                        if (stateEntry.getValue().equalsIgnoreCase(value)) {
                            stateID=stateEntry.getKey();
                            Log.v("StateId----------", "" + stateEntry.getKey());
                            new DistrictListParser().executeQuery(DealerLocation.this, QueryBuilder.getInstance().districtListApi(), QueryBuilder.getInstance().generateDistrictListQuery(DealerLocation.this,Integer.toString(stateEntry.getKey())), DealerLocation.this, Settings.FETCH_DISTRICT_LIST_ID, true);
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void selectSpinnerValue(Spinner spinner, String myString)
    {
        for(int i = 0; i < spinner.getCount(); i++){
            if(spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                spinner.setSelection(i);
                break;
            }
        }
    }



    /*todo: Populate District List on behalf of State ID
     *   Created By Kunal Singh
     *   Date 22 May 2018
     **/
    public void fillDistrictSpinner(List<String> stateNameList, String defaultDistrict){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_dropdown_item, stateNameList);
        spinnerDistValue.setAdapter(adapter);
        selectSpinnerValue(spinnerDistValue,defaultDistrict);
        spinnerDistValue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinnerDistValue.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select District"))) {
                    for (Map.Entry<Integer, String> districtEntry : map_dist_values.entrySet()) {
                        if (districtEntry.getValue().equalsIgnoreCase(value)) {
                            distID=districtEntry.getKey();
                            Log.v("DistrictId----------", "" + districtEntry.getKey());
                            new CityListParser().executeQuery(DealerLocation.this, QueryBuilder.getInstance().cityListApi(), QueryBuilder.getInstance().generateCityListQuery(DealerLocation.this, PrefManager.getKeyUserMobileNumber(DealerLocation.this), Utils.versionNumber(DealerLocation.this), Utils.getDeviceName(), Utils.deviceToken(DealerLocation.this), Integer.toString(districtEntry.getKey())), DealerLocation.this, Settings.FETCH_CITY_LIST_ID, true);
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    /*todo: Populate Locality List on behalf of District ID
     *   Created By Kunal Singh
     *   Date 22 May 2018
     **/

    public void fillLocalitySpinner(List<String> cityNameList){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, cityNameList);
        spinnerLocalityValue.setAdapter(adapter);
        selectSpinnerValue(spinnerLocalityValue,"Select Locality");

        spinnerLocalityValue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinnerLocalityValue.getSelectedItem().toString().trim();
                pinCodeValue.setText("");
                if(!(value.equalsIgnoreCase("Select Locality"))) {
                    for (Map.Entry<String, String> localityEntry : map_locality_values.entrySet()) {
                        if (localityEntry.getValue().equalsIgnoreCase(value)) {
                            loacID=Integer.parseInt(localityEntry.getKey());
                            Log.v("localityId----------", "" + localityEntry.getKey());
                            pinCodeValue.setText(localityEntry.getKey());
                            pinCodeValue.setFocusable(false);
                            // new LocalityListParser().executeQuery(RegisterScreenActivity.this, QueryBuilder.getInstance().localityListApi(), QueryBuilder.getInstance().generateLocalityListQuery(RegisterScreenActivity.this, registeredUserData.mobileNumber, Utils.versionNumber(RegisterScreenActivity.this), Utils.getDeviceName(), Utils.deviceToken(RegisterScreenActivity.this), Integer.toString(cityEntry.getKey())), RegisterScreenActivity.this, Settings.FETCH_LOCALITY_LIST_ID, true);
                        }
                    }


               /*     for (Map.Entry<String,String> e : map_locality_values.entrySet()) {
                        String key = e.getKey();
                        //Utils.HitechToast(DealerLocation.this,key);
                       pinCodeValue.setText("");
                        pinCodeValue.setText(key);
                    }*/

                }else{

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }


    /*todo: Populate City List on behalf of District ID
     *   Created By Kunal Singh
     *   Date 22 May 2018
     **/

    public void fillCitySpinner(List<String> cityNameList){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, cityNameList);
        spinnerCityValue.setAdapter(adapter);
        selectSpinnerValue(spinnerCityValue,"Select City");

        spinnerCityValue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinnerCityValue.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select City"))) {
                    for (Map.Entry<Integer, String> cityEntry : map_city_values.entrySet()) {
                        if (cityEntry.getValue().equalsIgnoreCase(value)) {
                            cityID=cityEntry.getKey();
                            Log.v("cityId----------", "" + cityEntry.getKey());
                            new LocalityListParser().executeQuery(DealerLocation.this, QueryBuilder.getInstance().localityListApi(), QueryBuilder.getInstance().generateLocalityListQuery(DealerLocation.this, PrefManager.getKeyUserMobileNumber(DealerLocation.this), Utils.versionNumber(DealerLocation.this), Utils.getDeviceName(), Utils.deviceToken(DealerLocation.this), Integer.toString(cityEntry.getKey())), DealerLocation.this, Settings.FETCH_LOCALITY_LIST_ID, true);
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




    }



















    @SuppressWarnings("MissingPermission")
    private void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
        } else {
            LocationRequest locationRequest = new LocationRequest();
            locationRequest.setInterval(2000);
            locationRequest.setFastestInterval(1000);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            fusedLocationClient.requestLocationUpdates(locationRequest,
                    locationCallback,
                    null);
        }
    }

    @SuppressWarnings("MissingPermission")
    private void getAddress() {

        if (!Geocoder.isPresent()) {
            Toast.makeText(DealerLocation.this,
                    "Can't find current address, ",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(this, GetAddressIntentService.class);
        intent.putExtra("add_receiver", addressResultReceiver);
        intent.putExtra("add_location", currentLocation);
        startService(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates();
                } else {
                    Toast.makeText(this, "Location permission not granted, " +
                                    "restart the app if you want the feature",
                            Toast.LENGTH_SHORT).show();
                }
                return;
            }

        }
    }
    private class LocationAddressResultReceiver extends ResultReceiver {
        LocationAddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            if (resultCode == 0) {
                //Last Location can be null for various reasons
                //for example the api is called first time
                //so retry till location is set
                //since intent service runs on background thread, it doesn't block main thread
                Log.d("Address", "Location null retrying");
                getAddress();
            }

            if (resultCode == 1) {
                Toast.makeText(DealerLocation.this,
                        "Address not found, " ,
                        Toast.LENGTH_SHORT).show();
            }

            String currentAdd = resultData.getString("address_result");

            showResults(currentAdd);
        }
    }

    private void showResults(String currentAdd){
       // currentAddTv.setText(currentAdd);

       /* Toast.makeText(DealerLocation.this,
                currentAdd ,
                Toast.LENGTH_SHORT).show();*/

        defaultStateName=currentAdd;
    }


    @Override
    protected void onResume() {
        super.onResume();
        startLocationUpdates();
    }

    @Override
    protected void onPause() {
        super.onPause();
        fusedLocationClient.removeLocationUpdates(locationCallback);
    }
}



