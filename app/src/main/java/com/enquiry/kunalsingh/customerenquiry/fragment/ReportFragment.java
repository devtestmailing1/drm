package com.enquiry.kunalsingh.customerenquiry.fragment;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.parser.DashboardData;
import com.enquiry.kunalsingh.customerenquiry.parser.DashboardDetailsData;
import com.enquiry.kunalsingh.customerenquiry.parser.HomeDataParser;
import com.enquiry.kunalsingh.customerenquiry.parser.ShoapListData;
import com.enquiry.kunalsingh.customerenquiry.parser.ShopDetailsData;
import com.enquiry.kunalsingh.customerenquiry.parser.ShopListItem;
import com.enquiry.kunalsingh.customerenquiry.utility.PrefManager;
import com.enquiry.kunalsingh.customerenquiry.utility.QueryBuilder;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;

import java.util.ArrayList;


/**
 * Created by Kunal Singh on 21-09-2018.
 */
public class ReportFragment extends Fragment implements HomeDataParser.onHomeListener, View.OnClickListener {
    View rootView;
    TextView tvTurnBlue,tvPendingVisit,tvCallMade,tvCallPending;
    LinearLayout lnyTurnBlue,lnyPendingVisit,lnyVisit,lnyCallPending;
    DashboardData dashboardData;

    public ReportFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_reports, container, false);
        tvTurnBlue=rootView.findViewById(R.id.tvTurnBlue);
        tvPendingVisit=rootView.findViewById(R.id.tvPendingVisit);
        tvCallMade=rootView.findViewById(R.id.tvCallMade);
        tvCallPending=rootView.findViewById(R.id.tvCallPending);
        lnyTurnBlue=rootView.findViewById(R.id.lnyTurnBlue);
        lnyPendingVisit=rootView.findViewById(R.id.lnyPendingVisit);
        lnyVisit=rootView.findViewById(R.id.lnyVisit);
        lnyCallPending=rootView.findViewById(R.id.lnyCallPending);
        lnyTurnBlue.setOnClickListener(this);
        lnyPendingVisit.setOnClickListener(this);
        lnyVisit.setOnClickListener(this);
        lnyCallPending.setOnClickListener(this);
        new HomeDataParser().executeQuery(getActivity(), QueryBuilder.getInstance().getDashboardAPI(), QueryBuilder.getInstance().generateDashboardQuery(getActivity()), this, Settings.DASHBOARD_ID, true);
        return rootView;
    }

    @Override
    public void onSuccess(int id, String message, Object object) {
        if (object != null) {
            if (id == Settings.DASHBOARD_ID) {
                dashboardData = ((DashboardData) object);
                tvCallPending.setText(String.valueOf(dashboardData.dashboardCount.get(0).callPending));
                tvPendingVisit.setText(String.valueOf(dashboardData.dashboardCount.get(0).pendingVisit));
                tvTurnBlue.setText(String.valueOf(dashboardData.dashboardCount.get(0).turnBlue));
                tvCallMade.setText(String.valueOf(dashboardData.dashboardCount.get(0).callMade));
            }

        }
    }

    @Override
    public void onError(String error, int id, BaseController.ErrorCode code) {

    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.lnyTurnBlue:
                startActivity(new Intent(getActivity(),DashboardDetails.class).putExtra("KEYNAME","TURNBLUE"));
                break;
            case R.id.lnyPendingVisit:
                startActivity(new Intent(getActivity(),DashboardDetails.class).putExtra("KEYNAME","PENDINGVISIT"));
                break;
            case R.id.lnyVisit:
                startActivity(new Intent(getActivity(),DashboardDetails.class).putExtra("KEYNAME","CALLMADE"));
                break;
            case R.id.lnyCallPending:
                startActivity(new Intent(getActivity(),DashboardDetails.class).putExtra("KEYNAME","CALLPENDING"));
                break;
        }
    }
}
