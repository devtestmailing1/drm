package com.enquiry.kunalsingh.customerenquiry.utility;

import android.content.Context;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * QueryBuilder Class for web API
 * Created by Kunal Singh
 * Date 01 January 2017
 */
public class QueryBuilder {

    public final String TAG = "QueryBuilder";
    public static QueryBuilder instance;

    public static QueryBuilder getInstance() {
        if (instance == null)
            instance = new QueryBuilder();

        return instance;
    }

    //todo: Login API

    public String login() {
        StringBuilder sb = new StringBuilder(Settings.LOGIN_API);
        Logger.logInfo(TAG, "login", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateLoginQuery(Context context, String userMobNumber, String appVesion, String deviceName, String iemeiNumber,String isAllow) {
        JSONObject jInput = new JSONObject();

        try {
            // JSONObject jInput = new JSONObject();
            jInput.put("mobileNumber", userMobNumber.trim());
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceName);
            jInput.put("imeiNumber", iemeiNumber);
            jInput.put("isAllow", isAllow);
            Logger.logInfo(TAG, "login json Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }

    //todo:  UpdateUser API for register
    public String updateUser() {
        StringBuilder sb = new StringBuilder(Settings.UPDATE_USER_API);
        Logger.logInfo(TAG, "Update User API", " " + sb.toString());
        return sb.toString();
    }



    public JSONObject generateUpdateUserQuery(Context context, String mobileNumber, String appVersion, String deviceModel, String imeiNumber,String userName, String userCategoryId,String emailID, String address,String pinCode, String countryID, String cityID,String stateID,String dob, String fatherName, String motherName) {
        JSONObject jInput = new JSONObject();
        try {

            jInput.put("mobileNumber", mobileNumber.trim());
            jInput.put("appVersion", appVersion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceModel.trim());
            jInput.put("imeiNumber", imeiNumber.trim());
            jInput.put("userName", userName.trim());
            jInput.put("userCategoryId", userCategoryId.trim());
            jInput.put("emailId", emailID.trim());
            jInput.put("address", address.trim());
            jInput.put("usrPinId", pinCode.trim());
            jInput.put("usrCountryId", countryID.trim());
            jInput.put("cityId", cityID.trim());
            jInput.put("stateId", stateID.trim());
            JSONObject joDOB = new JSONObject();
            JSONArray ja = new JSONArray();
            joDOB.put("appQuestionId", "1");
            joDOB.put("question", "DOB");
            joDOB.put("answer", dob);

            JSONObject joMother = new JSONObject();
            joMother.put("appQuestionId", "2");
            joMother.put("question", "father");
            joMother.put("answer", fatherName);

            JSONObject joFather = new JSONObject();
            joFather.put("appQuestionId", "3");
            joFather.put("question", "mother");
            joFather.put("answer", motherName);

            ja.put(joDOB);
            ja.put(joMother);
            ja.put(joFather);
            jInput.put("customerSecurityAnswerList", ja);
            Logger.logInfo(TAG, "UpdateUser json Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }


    public String verifyOTP() {
        StringBuilder sb = new StringBuilder(Settings.VERIFY_OTP_API);
        Logger.logInfo(TAG, "Verify OTP", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateVerifyOTPQuery(Context context, String userMobNumber, String otp, String cityId) {
        JSONObject jInput = new JSONObject();
        try {
            // JSONObject jInput = new JSONObject();
            jInput.put("userMob", userMobNumber.trim());
            jInput.put("otp", otp.trim());
            jInput.put("city", cityId.trim());
            Logger.logInfo(TAG, "Verify OTP json Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }



    public String searchShopList() {
        StringBuilder sb = new StringBuilder(Settings.SEARCH_SHOP_LIST);
        Logger.logInfo(TAG, "search shop list", " " + sb.toString());
        return sb.toString();
    }



    public JSONObject generateSearchShopListQuery(Context context, String tokenHeader, String shopName, String fabricIdList,
                                                  String productIdList, String partyIdList, String rankIdList ) {
        JSONObject jInput = new JSONObject();
        try {
            // JSONObject jInput = new JSONObject();
            jInput.put("tokenHeader", tokenHeader.trim());
            jInput.put("shopName", shopName.trim());
            jInput.put("fabricIdList", fabricIdList.trim());
            jInput.put("productIdList", productIdList.trim());
            jInput.put("partyIdList", partyIdList.trim());
            jInput.put("rankIdList", rankIdList.trim());
            Logger.logInfo(TAG, "Verify OTP json Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }



    public String countryListApi() {
        StringBuilder sb = new StringBuilder(Settings.FETCH_COUNTRY_LIST);
        Logger.logInfo(TAG, "District List API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateCountryListQuery(Context context) {
        JSONObject jInput = new JSONObject();

        try {
            jInput.put("mobileNo", PrefManager.read(context,"RegMobile",null));
            Logger.logInfo(TAG, "State List json Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }




    public String districtListApi() {
        StringBuilder sb = new StringBuilder(Settings.FETCH_DISTRICT_LIST_API);
        Logger.logInfo(TAG, "District List API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateDistrictListQuery(Context context, String stateID) {
        JSONObject jInput = new JSONObject();

        try {
            // JSONObject jInput = new JSONObject();
            jInput.put("stateId", stateID.trim());
            jInput.put("mobileNo", PrefManager.read(context,"RegMobile",null));
            Logger.logInfo(TAG, "State List json Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }


    public String cityListApi() {
        StringBuilder sb = new StringBuilder(Settings.FETCH_CITY_LIST_API);
        Logger.logInfo(TAG, "City List API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateCityListQuery(Context context, String userMobNumber, String appVesion, String deviceName, String iemeiNumber,String distID) {
        JSONObject jInput = new JSONObject();

        try {
            // JSONObject jInput = new JSONObject();
            jInput.put("mobileNumber", userMobNumber.trim());
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceName);
            jInput.put("imeiNumber", iemeiNumber);
            jInput.put("districtId", distID.trim());
            Logger.logInfo(TAG, "City List json Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }


    public String localityListApi() {
        StringBuilder sb = new StringBuilder(Settings.FETCH_LOCALITY_LIST_API);
        Logger.logInfo(TAG, "Locality List API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateLocalityListQuery(Context context, String userMobNumber, String appVesion, String deviceName, String iemeiNumber,String cityID) {
        JSONObject jInput = new JSONObject();

        try {
            // JSONObject jInput = new JSONObject();
            jInput.put("mobileNumber", userMobNumber.trim());
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceName);
            jInput.put("imeiNumber", iemeiNumber);
            jInput.put("cityId", cityID.trim());
            Logger.logInfo(TAG, "Locality List json Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }


//todo: fetch State Dist City from Pin Code

    public String fetchStateDistCityApi() {
        StringBuilder sb = new StringBuilder(Settings.FETCH_STATE_DIST_CITY_FROM_PIN_API);
        Logger.logInfo(TAG, "Fetch State Dist City API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateFetchStateDistCityQuery(Context context, String userMobNumber, String appVesion, String deviceName, String iemeiNumber,String pinId) {
        JSONObject jInput = new JSONObject();

        try {
            // JSONObject jInput = new JSONObject();
            jInput.put("mobileNumber", userMobNumber.trim());
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceName);
            jInput.put("imeiNumber", iemeiNumber);
            jInput.put("pinCode", pinId.trim());
            Logger.logInfo(TAG, "Fetch State Dist City json Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }




    public String validateSecurityQuestions() {
        StringBuilder sb = new StringBuilder(Settings.VALIDATE_SECURITY_QUESTION_API);
        Logger.logInfo(TAG, "Validate Security Questions OTP", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generatevalidateSecurityQuestionsQuery(Context context, String userMobNumber, String appVesion, String deviceName, String iemeiNumber,String questionID,String answer) {
        JSONObject jInput = new JSONObject();

        try {
            // JSONObject jInput = new JSONObject();
            jInput.put("mobileNumber", userMobNumber.trim());
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceName);
            jInput.put("imeiNumber", iemeiNumber);
            jInput.put("appQuestionId", questionID.trim());
            jInput.put("answer", answer.trim());
            Logger.logInfo(TAG, "Validate Security Questions Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }


    public String fetchRegNumber() {
        StringBuilder sb = new StringBuilder(Settings.FETCH_REG_NUMBER_API);
        Logger.logInfo(TAG, "Fetch Registration Number", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generatefetchRegNumberQuery(Context context, String userMobNumber, String appVesion, String deviceName, String iemeiNumber,String regNumber) {
        JSONObject jInput = new JSONObject();

        try {
            // JSONObject jInput = new JSONObject();
            jInput.put("mobileNumber", userMobNumber);
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceName);
            jInput.put("imeiNumber", iemeiNumber);
            jInput.put("registrationNo", regNumber.trim());
            Logger.logInfo(TAG, "Fetch Registration Number Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }


    public String registerVehicle() {
        StringBuilder sb = new StringBuilder(Settings.ADD_REGISTER_VEHICLE_API);
        Logger.logInfo(TAG, "Add Register Vehicle API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateRegisterVehicleQuery(Context context, String userMobNumber, String appVesion, String deviceName, String iemeiNumber,String screenName) {
        JSONObject jInput = new JSONObject();

        try {
            // JSONObject jInput = new JSONObject();
            jInput.put("mobileNumber", userMobNumber);
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceName);
            jInput.put("imeiNumber", iemeiNumber);
            jInput.put("dataFor", screenName.trim());
            Logger.logInfo(TAG, "Add Register Vehicle JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }

    public String submitRegisterVehicle() {
        StringBuilder sb = new StringBuilder(Settings.REGISTER_VEHICLE_API);
        Logger.logInfo(TAG, "Submit Register Vehicle API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateSubmitRegisterVehicleQuery(Context context, String userMobNumber, String appVesion, String deviceName, String iemeiNumber,String regNo, String chsNo, String modlFamilyId, String modlGroupId, String modleId) {
        JSONObject jInput = new JSONObject();

        try {
            // JSONObject jInput = new JSONObject();
            jInput.put("mobileNumber", userMobNumber);
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceName);
            jInput.put("imeiNumber", iemeiNumber);
            jInput.put("registrationNo", regNo.trim());
            jInput.put("chassisNo", chsNo.trim());
            jInput.put("modelFamilyId", modlFamilyId.trim());
            jInput.put("modelGroupId", modlGroupId.trim());
            jInput.put("modelId", modleId.trim());
            Logger.logInfo(TAG, "Submit Register Vehicle JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }

    public String stateListAPI() {
        StringBuilder sb = new StringBuilder(Settings.STATE_LIST_API);
        Logger.logInfo(TAG, "State List API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateStateListQuery(Context context, String countryIds) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("countryId", "1");
            jInput.put("mobileNo", PrefManager.read(context,"RegMobile",null));
            Logger.logInfo(TAG, "State List JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }



    public String serviceBookingListAPI() {
        StringBuilder sb = new StringBuilder(Settings.SERVICE_BOOKING_LIST_API);
        Logger.logInfo(TAG, "Service Booking List API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateServiceBookingListQuery(Context context, String userMobNumber, String appVesion, String deviceName, String iemeiNumber) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("mobileNumber", userMobNumber);
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceName);
            jInput.put("imeiNumber", iemeiNumber);
            Logger.logInfo(TAG, "Service Booking List JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }

    public String serviceBookingDetailsAPI() {
        StringBuilder sb = new StringBuilder(Settings.SERVICE_BOOKING_DETAILS_API);
        Logger.logInfo(TAG, "Service Booking List API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateServiceBookingDetailsQuery(Context context, String userMobNumber, String appVesion, String deviceName, String iemeiNumber) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("mobileNumber", userMobNumber);
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceName);
            jInput.put("imeiNumber", iemeiNumber);
            Logger.logInfo(TAG, "Service Booking List JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }



    public String fetchModelGroupFamilyModelByRegAPI() {
        StringBuilder sb = new StringBuilder(Settings.FETCH_MG_MF_BY_REGNO_API);
        Logger.logInfo(TAG, "Fetch Model Group Family Model By Reg List API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generatefetchModelGroupFamilyModelByRegQuery(Context context, String userMobNumber, String appVesion, String deviceName, String iemeiNumber,String regNo) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("mobileNumber", userMobNumber);
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceName);
            jInput.put("imeiNumber", iemeiNumber.trim());
            jInput.put("registrationNo",regNo.trim());
            Logger.logInfo(TAG, "Fetch Model Group Family Model By Reg List JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }




    public String fetchModelByMakeAPI() {
        StringBuilder sb = new StringBuilder(Settings.FETCH_MODEL_BY_MAKE_API);
        Logger.logInfo(TAG, "Service Booking List API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generatefetchModelByMakeQuery(Context context, String userMobNumber, String appVesion, String deviceName, String iemeiNumber, String modelFamilyId) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("mobileNumber", userMobNumber);
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceName);
            jInput.put("imeiNumber", iemeiNumber);
            jInput.put("modelFamilyId", modelFamilyId);
            Logger.logInfo(TAG, "Fetch Model By Make JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }



    public String fetchDealerFromDealerLocAPI() {
        StringBuilder sb = new StringBuilder(Settings.FETCH_DEALER_FROM_DEALER_LOC_API);
        Logger.logInfo(TAG, "Service Booking List API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateFetchDealerFromDealerLocQuery(Context context, String userMobNumber, String appVesion, String deviceName, String iemeiNumber, String pinCode,String  tehID,String distID, String cityID, String stateID, String countryID) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("mobileNumber", userMobNumber);
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceName);
            jInput.put("imeiNumber", iemeiNumber);
            jInput.put("pinCode", pinCode);
            jInput.put("usrTehsilId", tehID);
            jInput.put("districtId", distID);
            jInput.put("cityId", cityID);
            jInput.put("stateId", stateID);
            jInput.put("usrCountryId", countryID);

            Logger.logInfo(TAG, "Fetch Model By Make JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }









    public String createServiceBookingAPI() {
        StringBuilder sb = new StringBuilder(Settings.CREATE_SERVICE_BOOKING_API);
        Logger.logInfo(TAG, "Service Booking List API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateCreateServiceBookingQuery(Context context, String userMobNumber, String appVesion, String deviceName, String iemeiNumber, String custId, String registraionNumber, String chassisNumber, String apponteeName,String appointeeMob, String branchId, String modlFamilyId, String modlGroupId, String modlId, String apntSource, String serviceCatgId, String remarks, String availableTime) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("mobileNumber", userMobNumber);
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceName);
            jInput.put("imeiNumber", iemeiNumber);
            jInput.put("customerId", custId);
            jInput.put("registrationNo", registraionNumber);
            jInput.put("chassisNo", chassisNumber);
            JSONObject servcAppointment = new JSONObject();
            servcAppointment.put("registrationNo", registraionNumber);
            servcAppointment.put("chassisNo", chassisNumber);
            servcAppointment.put("appointeeName", apponteeName);
            servcAppointment.put("appointeeMobile", appointeeMob);
            servcAppointment.put("branchId", branchId);
            servcAppointment.put("modelFamilyId", modlFamilyId);
            servcAppointment.put("modelGroupId", modlGroupId);
            servcAppointment.put("modelId", modlId);
            servcAppointment.put("apntSource", apntSource);
            servcAppointment.put("serviceCategoryId", serviceCatgId);
            servcAppointment.put("remarks", remarks);
            servcAppointment.put("availabilityTime", availableTime);
            jInput.put("serviceAppointment",servcAppointment);
            Logger.logInfo(TAG, "Service Booking List JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }



    public String fetchServiceHistoryAPI() {
        StringBuilder sb = new StringBuilder(Settings.FETCH_SERVICE_HISTORY_API);
        Logger.logInfo(TAG, "Service History List API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateServiceHistoryQuery(Context context, String userMobNumber, String appVesion, String deviceName, String iemeiNumber, String dataFor,String  offset,String maxRes) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("mobileNumber", userMobNumber);
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceName);
            jInput.put("imeiNumber", iemeiNumber);
            jInput.put("dataFor", dataFor);
            jInput.put("offset", offset);
            jInput.put("maxResult", maxRes);
            Logger.logInfo(TAG, "Fetch Service History JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }


    public String fetchMaintinanceCostAPI() {
        StringBuilder sb = new StringBuilder(Settings.FETCH_MAINTINANCE_COST_API);
        Logger.logInfo(TAG, "Maintinace Cost List API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateMaintinaceCostQuery(Context context, String userMobNumber, String appVesion, String deviceName, String iemeiNumber, String status, String dataFor,String  regNo,String chasisNo) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("mobileNumber", userMobNumber.trim());
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceName);
            jInput.put("imeiNumber", iemeiNumber);
            jInput.put("status", status);
            jInput.put("dataFor", dataFor);
            jInput.put("registrationNo", regNo);
            jInput.put("chassisNo", chasisNo);
            Logger.logInfo(TAG, "Fetch Maintinance Cost JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }




    public String fetchValidateComplainAPI() {
        StringBuilder sb = new StringBuilder(Settings.FETCH_COMLAIN_VALIDATION);
        Logger.logInfo(TAG, "Validate Complain API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateValidateComplainQuery(Context context, String userMobNumber, String appVesion, String deviceName, String iemeiNumber) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("mobileNumber", userMobNumber);
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceName);
            jInput.put("imeiNumber", iemeiNumber);
            Logger.logInfo(TAG, "Validate JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }


    public String fetchComplainAPI() {
        StringBuilder sb = new StringBuilder(Settings.FETCH_COMLAIN_API);
        Logger.logInfo(TAG, "Complain List API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateComplainQuery(Context context, String userMobNumber, String appVesion, String deviceName, String iemeiNumber) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("mobileNumber", userMobNumber.trim());
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceName);
            jInput.put("imeiNumber", iemeiNumber);

            Logger.logInfo(TAG, "Complain JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }

    public String createComplainAPI() {
        StringBuilder sb = new StringBuilder(Settings.CREATE_COMLAIN_API);
        Logger.logInfo(TAG, "Create Complain API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateCreateComplainQuery(Context context, String userMobNumber, String appVesion, String deviceName, String iemeiNumber, String regNo,String  type,String remark) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("mobileNumber", userMobNumber.trim());
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceName);
            jInput.put("imeiNumber", iemeiNumber);
            jInput.put("registrationNo", regNo);
            JSONObject jsonobj = new JSONObject();
            jsonobj.put("complainType", type);
            jsonobj.put("registrationNumber", regNo);
            jsonobj.put("remarks",remark);
            jInput.put("complaintModel", jsonobj);
            Logger.logInfo(TAG, "Create Complain JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }

    public String userVehicleListAPI() {
        StringBuilder sb = new StringBuilder(Settings.USER_VEHICLE_LIST_API);
        Logger.logInfo(TAG, "User Vehicle List API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateVehicleListQuery(Context context, String userMobNumber, String appVesion, String deviceName, String iemeiNumber) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("mobileNumber", userMobNumber.trim());
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            jInput.put("deviceModel", deviceName);
            jInput.put("imeiNumber", iemeiNumber);

            Logger.logInfo(TAG, "Create Complain JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }



    public String getShopDetailsAPI() {
        StringBuilder sb = new StringBuilder(Settings.SHOP_DETAILS_API);
        Logger.logInfo(TAG, "Shop Details API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateShopDetailsQuery(Context context, String token, String shopID) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("tokenHeader", token);
            jInput.put("shopId", shopID);
            Logger.logInfo(TAG, "Shop Details JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }

    public String getInitAddShopAPI() {
        StringBuilder sb = new StringBuilder(Settings.INIT_ADD_SHP_API);
        Logger.logInfo(TAG, "Init Add Shop API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateInitAddShopQuery(Context context, String token) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("tokenHeader", token);
            Logger.logInfo(TAG, "Init Add Shop JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }


    public String getPhotoUloadAPI(String locationLevelApi) {
        StringBuilder sb = new StringBuilder(locationLevelApi);
        Logger.logInfo(TAG, "Init Photo Upload API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateUploadImageQuery(String file1, String shopId, String tokenHeader) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("filedata", file1);
            jInput.put("shopId", shopId);
            jInput.put("tokenHeader", tokenHeader);
            Logger.logInfo(TAG, "Init shop location data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }


    public String getShopLocationAPI(String locationLevelApi) {
        StringBuilder sb = new StringBuilder(locationLevelApi);
        Logger.logInfo(TAG, "Init Add Shop API", " " + sb.toString());
        return sb.toString();
    }


    public JSONObject generateShopLocationLevelQuery(String locationIDType, String idValue) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put(locationIDType, idValue);
            Logger.logInfo(TAG, "Init shop location data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }


    public String getSubmitAddShopAPI() {
        StringBuilder sb = new StringBuilder(Settings.SUBMIT_ADD_SHP_API);
        Logger.logInfo(TAG, "Submit Add Shop API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateUpdateShopQuery(Context context, String token,String shopID,String shopName, String ownerName,String ownerMob, String mohallaName, String areaName, String pinID,String purchaserName,String purchaserMob, String email, String telephone, String website, String accountName, String accountNo, String selProdID, String selFabricID, String selPartyTypeID, String rankingId, String lattitude, String longitude, String shopAddress) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("tokenHeader", token);
            jInput.put("shopId", shopID);
            jInput.put("shopName", shopName);
            jInput.put("shopOwnerName", ownerName);
            jInput.put("shopOwnerMobileNo", ownerMob);
            jInput.put("shopAddress", shopAddress);
            jInput.put("mohalla", mohallaName);
            jInput.put("area", areaName);
            jInput.put("pin", pinID);
            jInput.put("purchaserName", purchaserName);
            jInput.put("purchaserMobile", purchaserMob);
            jInput.put("email", email);
            jInput.put("telephone", telephone);
            jInput.put("website", website);
            jInput.put("accountName", accountName);
            jInput.put("accountNumber", accountNo);
            jInput.put("productTypeId", selProdID);
            jInput.put("febricTypeId", selFabricID);
            jInput.put("partyTypeId", selPartyTypeID);
            jInput.put("rankingID", rankingId);
            jInput.put("longitude", longitude);
            jInput.put("latitude", lattitude);
            Logger.logInfo(TAG, "Submit Add Shop JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }


    public JSONObject generateSubmitAddShopQuery(Context context, String token,String ownerName,String ownerMob,String shopName, String purchaserName, String purchaserMob,String email, String telephone, String website, String accountName, String accountNo,String selProdID, String selFabricID, String selPartyTypeID, String rankingId, String lattitude, String longitude, String shopAddress, String stateID, String cityID, String areaID, String mohallaID, String pinID) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("tokenHeader", token);
            jInput.put("shopOwnerName", ownerName);
            jInput.put("shopOwnerMobileNo", ownerMob);
            jInput.put("shopName", shopName);
            jInput.put("purchaserName", purchaserName);
            jInput.put("purchaserMobile", purchaserMob);
            jInput.put("email", email);
            jInput.put("telephone", telephone);
            jInput.put("website", website);
            jInput.put("accountName", accountName);
            jInput.put("accountNumber", accountNo);
            jInput.put("productTypeId", selProdID);
            jInput.put("febricTypeId", selFabricID);
            jInput.put("partyTypeId", selPartyTypeID);
            jInput.put("rankingId", rankingId);
            jInput.put("longitude", longitude);
            jInput.put("latitude", lattitude);
            jInput.put("shopAddress", shopAddress);
            jInput.put("stateId", stateID);
            jInput.put("cityId",cityID);
            jInput.put("area", areaID);
            jInput.put("mohalla", mohallaID);
            jInput.put("pin", pinID);
            Logger.logInfo(TAG, "Submit Add Shop JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }

    public JSONObject generateUpdateAddShopQuery(Context context, String token,String ownerName,String ownerMob,String shopName, String purchaserName, String purchaserMob,String email, String telephone, String website, String accountName, String accountNo,String selProdID, String selFabricID, String selPartyTypeID, String rankingId, String lattitude, String longitude, String shopAddress, String stateID, String cityID, String areaID, String mohallaID, String pinID,String shopID) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("tokenHeader", token);
            jInput.put("shopOwnerName", ownerName);
            jInput.put("shopOwnerMobileNo", ownerMob);
            jInput.put("shopName", shopName);
            jInput.put("purchaserName", purchaserName);
            jInput.put("purchaserMobile", purchaserMob);
            jInput.put("email", email);
            jInput.put("telephone", telephone);
            jInput.put("website", website);
            jInput.put("accountName", accountName);
            jInput.put("accountNumber", accountNo);
            jInput.put("productTypeId", selProdID);
            jInput.put("febricTypeId", selFabricID);
            jInput.put("partyTypeId", selPartyTypeID);
            jInput.put("rankingId", rankingId);
            jInput.put("longitude", longitude);
            jInput.put("latitude", lattitude);
            jInput.put("shopAddress", shopAddress);
            jInput.put("stateId", stateID);
            jInput.put("cityId",cityID);
            jInput.put("area", areaID);
            jInput.put("mohalla", mohallaID);
            jInput.put("pin", pinID);
            jInput.put("shopId", shopID);
            Logger.logInfo(TAG, "Update Shop JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }
    public String getSearchShopAPI() {
        StringBuilder sb = new StringBuilder(Settings.SEARCH_SHOP_LIST_API);
        Logger.logInfo(TAG, "Search Shop API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateSearchShopQuery(Context context, String token,String shopName, String fabID, String prodID, String partyID, String rankID, String lat, String lang) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("tokenHeader", token);
            jInput.put("shopName", shopName);
            jInput.put("fabricIdList", fabID);
            jInput.put("productIdList", prodID);
            jInput.put("partyIdList", partyID);
            jInput.put("rankIdList", rankID);
            jInput.put("latitude", lat);
            jInput.put("longitude", lang);
            Logger.logInfo(TAG, "Search Shop JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }

    public String getCheckPunchAPI() {
        StringBuilder sb = new StringBuilder(Settings.CHECK_PUNCH_IN_API);
        Logger.logInfo(TAG, "Check Punch IN API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateCheckPunchInAPIQuery(Context context, String token, String userId, String shopId) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("tokenHeader", token);
            jInput.put("userId", userId);
            jInput.put("shopId", shopId);
            Logger.logInfo(TAG, "Check Punch IN JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }

    public String getPunchInOutAPI() {
        StringBuilder sb = new StringBuilder(Settings.PUNCH_IN_OUT_API);
        Logger.logInfo(TAG, "Check Punch IN OUT API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generatePunchInOutAPIQuery(Context context, String token, String userId, String shopId, String lat, String lang,String btnType) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("tokenHeader", token);
            jInput.put("userId", userId);
            jInput.put("shopId", shopId);
            jInput.put("punchInLongitude", lang);
            jInput.put("punchInLatitude", lat);
            jInput.put("btnType", btnType);
            Logger.logInfo(TAG, "Punch IN Out JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }

    public String getValidateMobAPI() {
        StringBuilder sb = new StringBuilder(Settings.VALIDATE_MOB_API);
        Logger.logInfo(TAG, "Validate Mob API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateValidateMobQuery(Context context, String userMob, String userType, String cityId) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("userMob", userMob);
            jInput.put("userType", userType);
            jInput.put("cityId", cityId);
            Logger.logInfo(TAG, "Validate Mob JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }


    public String getSubmitFeedbackAPI() {
        StringBuilder sb = new StringBuilder(Settings.SUBMIT_FEEDBACK_API);
        Logger.logInfo(TAG, "Submit Feedback API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateSubmitFeedbackQuery(Context context, String callDuration, String remarks, String toDate,String fromDate, String shopID) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("tokenHeader", PrefManager.getKeyAuthToken(context));
            jInput.put("callDuration", callDuration);
            jInput.put("remarks", remarks);
            jInput.put("toDate", toDate);
            jInput.put("fromDate", fromDate);
            jInput.put("shopId", shopID);
            Logger.logInfo(TAG, "Submit Feedback JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }

    public String getFetchFeedbackAPI() {
        StringBuilder sb = new StringBuilder(Settings.FETCH_FEEDBACK_API);
        Logger.logInfo(TAG, "Fetch Feedback API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateFetchFeedbackQuery(Context context, String shopID) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("tokenHeader", PrefManager.getKeyAuthToken(context));
            jInput.put("shopId", shopID);
            Logger.logInfo(TAG, "Fetch Feedback JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }


    public String getDashboardAPI() {
        StringBuilder sb = new StringBuilder(Settings.FETCH_DASHBOARD_API);
        Logger.logInfo(TAG, "Fetch Dashboard API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateDashboardQuery(Context context) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("tokenHeader", PrefManager.getKeyAuthToken(context));
            Logger.logInfo(TAG, "Fetch Dashboard JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }

    public String getDashboardDetailsAPI() {
        StringBuilder sb = new StringBuilder(Settings.FETCH_DASHBOARD_DETAILS_API);
        Logger.logInfo(TAG, "Fetch Dashboard API", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateDashboardDetailsQuery(Context context, String keyName) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("tokenHeader", PrefManager.getKeyAuthToken(context));
            jInput.put("key",keyName);
            Logger.logInfo(TAG, "Fetch Dashboard JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }


    /********************** for location service *************************/


    public String sendLocationServiceAPI() {
        StringBuilder sb = new StringBuilder(Settings.SEND_LOCATION_SERVICE);
        Logger.logInfo(TAG, "Send Location API", " " + sb.toString());
        return sb.toString();
    }
    public JSONObject generateSendLocationServiceQuery(Context context, String latitude, String longitude) {
        JSONObject jInput = new JSONObject();
        try {
            jInput.put("tokenHeader", PrefManager.getKeyAuthToken(context));
            jInput.put("latitude",latitude);
            jInput.put("longitude",longitude);
            Logger.logInfo(TAG, "Send Location JSON Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }



    /**************************************************************************/


}













