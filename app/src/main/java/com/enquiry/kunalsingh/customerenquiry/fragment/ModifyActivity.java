package com.enquiry.kunalsingh.customerenquiry.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.enquiry.kunalsingh.customerenquiry.BuildConfig;
import com.enquiry.kunalsingh.customerenquiry.CountrySelectActivity;
import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.adapter.StateAdapter;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.drawer.MainActivity;
import com.enquiry.kunalsingh.customerenquiry.parser.DistrictListParser;
import com.enquiry.kunalsingh.customerenquiry.parser.FetchDealerLocData;
import com.enquiry.kunalsingh.customerenquiry.parser.FetchShopLocationParser;
import com.enquiry.kunalsingh.customerenquiry.parser.InitAddShopData;
import com.enquiry.kunalsingh.customerenquiry.parser.InitAddShopParser;
import com.enquiry.kunalsingh.customerenquiry.parser.LocationLevelData;
import com.enquiry.kunalsingh.customerenquiry.parser.MultiStateCityData;
import com.enquiry.kunalsingh.customerenquiry.parser.ShopDetailsData;
import com.enquiry.kunalsingh.customerenquiry.utility.PathUtils;
import com.enquiry.kunalsingh.customerenquiry.utility.PrefManager;
import com.enquiry.kunalsingh.customerenquiry.utility.QueryBuilder;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.enquiry.kunalsingh.customerenquiry.utility.Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Kunal Singh on 17-08-2018.
 */
public class ModifyActivity extends  AppCompatActivity implements InitAddShopParser.onInitShopDataListener, View.OnClickListener
        , FetchShopLocationParser.onLocationLevelDataListner{
    private Spinner spinnerLocality,spinerRanking,spinnerCountry,spinnerState,spinnerCity,spinnerMoholla;
    private EditText edtShopName,edtOwnerName,edtOwnerMob,edtPurchaserName,edtPurchaserNumber,edtEmailID,edtTelephoneNo,edtWebsite,edtAccountantName,edtAccountantNumber,edtProductType,edtProductFabric,edtPartyType,edtRanking,edtShopAdd, edtPin;
    private Button btnModifyShop, spinerProductFabric,spinerPartyType,spinerProdType;
    private TextView tvCamera;
    static String viewFileName = "";
    public static int REQUEST_IMAGE = 100;
    private ShopDetailsData shopDetailsData;
    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    String imagePath,cammeraName;
    String filePath = null, ext = "", remarks="";
    InitAddShopData initAddShopData;
    LocationLevelData locationLevelData;
    Map<Integer,String> map_country_values = new HashMap<Integer, String>();
    Map<Integer,String> map_area_values = new HashMap<Integer, String>();
    Map<Integer,String> map_state_values = new HashMap<Integer, String>();
    Map<Integer,String> map_city_values = new HashMap<Integer, String>();
    Map<Integer,String> map_moholla_values = new HashMap<Integer, String>();
    Map<Integer,String> map_prod_type_values = new HashMap<Integer, String>();
    Map<Integer,String> map_fabric_type_values = new HashMap<Integer, String>();
    Map<Integer,String> map_party_type_values = new HashMap<Integer, String>();
    Map<Integer,String> map_Ranking_No_values = new HashMap<Integer, String>();
    private int selRankingNo,selCountryID,selStateID,selCityID,selLocalityID,selMohollaID;
    private String shopID,countryDesc, stateDesc,cityDesc,areaDesc,mohallaDesc, prodDesc,fabDesc,partyDesc,rankDesc;
    private LocationManager locationManager;
    private Location location = null;
    String selProdID, selFabricID, selPartyType;
    private static final int LOCATION_MIN_UPDATE_TIME = 10;
    private static final int LOCATION_MIN_UPDATE_DISTANCE = 1000;
    ImageView drmImg;
    String encodedImage, latitude,logitude;
    Switch aSwitch;
    ArrayList<MultiStateCityData> productListVOs,febricListVOs,partyListVo;
    MultiStateCityData productVo, febricVo, partyVO;
    ArrayList<String>selectedProducts, selectedFebrics, selectedParty;
    StateAdapter productTypeAdapter;

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            locationManager.removeUpdates(locationListener);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }


        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }

    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify);
        Toolbar toolbar = findViewById(R.id.header);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("Modify Shop");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        if (getIntent() != null && getIntent().getParcelableExtra("shopDetailsData") != null) {
            shopDetailsData = ((ShopDetailsData) getIntent().getParcelableExtra("shopDetailsData"));
        }
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        initialisation();
        poulateData();
        new InitAddShopParser().executeQuery(this, QueryBuilder.getInstance().getInitAddShopAPI(), QueryBuilder.getInstance().generateInitAddShopQuery(this, PrefManager.getKeyAuthToken(this).trim()), this, Settings.INIT_ADD_SHP_ID, true);
        btnModifyShop.setOnClickListener(this);
        tvCamera.setOnClickListener(this);
    }

    private void initialisation() {
        btnModifyShop=findViewById(R.id.btnModifyShop);
        tvCamera=findViewById(R.id.tvCamera);
        spinnerCountry = findViewById(R.id.spinnerCountrySelect);
        spinnerState = findViewById(R.id.spinnerStateSelect);
        spinnerCity = findViewById(R.id.spinnerCitySelect);
        spinnerMoholla = findViewById(R.id.spinnerMohollaSelect);
        spinnerLocality=findViewById(R.id.spinerArea);
        spinerProdType=findViewById(R.id.spinerProdType);
        spinerProductFabric=findViewById(R.id.spinerProductFabric);
        spinerPartyType=findViewById(R.id.spinerPartyType);
        spinerRanking=findViewById(R.id.spinerRanking);
        edtShopName=findViewById(R.id.edtShopName);
        edtOwnerName=findViewById(R.id.edtOwnerName);
        edtOwnerMob=findViewById(R.id.edtOwnerMob);
        edtPurchaserName=findViewById(R.id.edtPurchaserName);
        edtPurchaserNumber=findViewById(R.id.edtPurchaserNumber);
        edtEmailID=findViewById(R.id.edtEmailID);
        edtTelephoneNo=findViewById(R.id.edtTelephoneNo);
        edtWebsite=findViewById(R.id.edtWebsite);
        edtAccountantName=findViewById(R.id.edtAccountantName);
        edtAccountantNumber=findViewById(R.id.edtAccountantNumber);
        edtShopAdd=findViewById(R.id.edtShopAdd);
        edtPin=findViewById(R.id.edtPin);
        drmImg = findViewById(R.id.drm_img);
        aSwitch = findViewById(R.id.aswitch);


        latitude = shopDetailsData.latitude;
        logitude = shopDetailsData.longitude;

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b){
                    latitude = shopDetailsData.latitude;
                    logitude = shopDetailsData.longitude;
                }else {
                    latitude =  String.valueOf(location.getLatitude());
                    logitude = String.valueOf(location.getLongitude());
                }
            }
        });

        spinerProdType.setText("Select Product Type");
        spinerProductFabric.setText("Select Febric");
        spinerPartyType.setText("Select Party Type");


    }

    @Override
    public void onSuccess(int id, String message, Object object) {

        if (id == Settings.INIT_ADD_SHP_ID){
            if(object!=null) {
                List<String> prodTypeName = new ArrayList<>();
                List<String> fabricTypeName = new ArrayList<>();
                List<String> partyTypeName = new ArrayList<>();
                List<String> rankingNo = new ArrayList<>();
                List<String> country =new ArrayList<>();


                List<String> productTypeID = new ArrayList<>();
                List<String> fabricTypeID = new ArrayList<>();
                List<String> partyTypeID = new ArrayList<>();

                List<String> productTypeSelected = new ArrayList<>();
                List<String> febricTypeSelected = new ArrayList<>();
                List<String> partyTypeSelected = new ArrayList<>();

                /*for (int i = 0; i<50; i++) {
                    productTypeSelected.add("SELECTED");
                    febricTypeSelected.add("SELECTED");
                    partyTypeSelected.add("SELECTED");
                }*/



               // prodTypeName.add("Select Product Type");
               // fabricTypeName.add("Select Fabric");
               // partyTypeName.add("Select Party Type");
                rankingNo.add("Select Ranking Number");
                country.add("Select Country");

                initAddShopData = ((InitAddShopData) object);

                if (initAddShopData.productTypeList != null && initAddShopData.productTypeList.size() > 0) {
                    for (int i = 0; i < initAddShopData.productTypeList.size(); i++) {
                        prodTypeName.add(initAddShopData.productTypeList.get(i).name);
                        productTypeID.add(String.valueOf(initAddShopData.productTypeList.get(i).productTypeId));
                        productTypeSelected.add(String.valueOf(initAddShopData.productTypeList.get(i).productTypeId));
                        map_prod_type_values.put(initAddShopData.productTypeList.get(i).productTypeId, initAddShopData.productTypeList.get(i).name);
                    }
                   // fillProdTypeSpinner(prodTypeName);
                    selectMultipleProductType(prodTypeName,productTypeID,productTypeSelected);
                }

                if (initAddShopData.fabricList != null && initAddShopData.fabricList.size() > 0) {
                    for (int i = 0; i < initAddShopData.fabricList.size(); i++) {
                        fabricTypeName.add(initAddShopData.fabricList.get(i).name);
                        fabricTypeID.add(String.valueOf(initAddShopData.fabricList.get(i).febricTypeId));
                        febricTypeSelected.add(String.valueOf(initAddShopData.fabricList.get(i).selected));
                        map_fabric_type_values.put(initAddShopData.fabricList.get(i).febricTypeId, initAddShopData.fabricList.get(i).name);
                    }
                    //fillFabricTypeSpinner(fabricTypeName);
                    selectMultipleFebricType(fabricTypeName,fabricTypeID,febricTypeSelected);
                }

                if (initAddShopData.partyTypeList != null && initAddShopData.partyTypeList.size() > 0) {
                    for (int i = 0; i < initAddShopData.partyTypeList.size(); i++) {
                        partyTypeName.add(initAddShopData.partyTypeList.get(i).name);
                        partyTypeID.add(String.valueOf(initAddShopData.partyTypeList.get(i).partyTypeId));
                        partyTypeSelected.add(String.valueOf(initAddShopData.partyTypeList.get(i).selected));
                        map_party_type_values.put(initAddShopData.partyTypeList.get(i).partyTypeId, initAddShopData.partyTypeList.get(i).name);
                    }
                   // fillPartyTypeSpinner(partyTypeName);

                    selectMultiplePartyType(partyTypeName,partyTypeID,partyTypeSelected);
                }

                if (initAddShopData.rankingList != null && initAddShopData.rankingList.size() > 0) {
                    for (int i = 0; i < initAddShopData.rankingList.size(); i++) {
                        rankingNo.add(initAddShopData.rankingList.get(i).name);
                        map_Ranking_No_values.put(initAddShopData.rankingList.get(i).rankingId, initAddShopData.rankingList.get(i).name);
                    }
                    fillRankingNoSpinner(rankingNo);
                }

                if (initAddShopData.countryList != null && initAddShopData.countryList.size() > 0) {
                    for (int i = 0; i < initAddShopData.countryList.size(); i++) {
                        country.add(initAddShopData.countryList.get(i).getCountryDesc());
                        map_country_values.put(initAddShopData.countryList.get(i).getCountryID(), initAddShopData.countryList.get(i).getCountryDesc());
                    }
                    fillCountrySpinner(country);
                }

                //  fetchLocationData("","",Settings.SHOP_COUNTRY_LIST, Settings.SHOP_LOCATION_COUNTRY);
            }
        }



        if (id == Settings.SHOP_LOCATION_LOCALITY ||id == Settings.SHOP_LOCATION_MOHOLLA ){

            if(object!=null) {
                locationLevelData = ((LocationLevelData) object);



                if (id == Settings.SHOP_LOCATION_LOCALITY ) {

                    List<String> area =new ArrayList<>();
                    area.add("Select Locality");

                    if (locationLevelData.getLocalityList() != null && locationLevelData.getLocalityList().size() > 0) {
                        for (int i = 0; i < locationLevelData.getLocalityList().size(); i++) {
                            area.add(locationLevelData.getLocalityList().get(i).getLocalityDesc());
                            map_area_values.put(locationLevelData.getLocalityList().get(i).getLocalityId(), locationLevelData.getLocalityList().get(i).getLocalityDesc());
                        }
                        fillLocalitySpinner(area);
                    }
                }

                if (id == Settings.SHOP_LOCATION_MOHOLLA ) {

                    List<String> moholla =new ArrayList<>();
                    moholla.add("Select Moholla");

                    if (locationLevelData.getMohollaList() != null && locationLevelData.getMohollaList().size() > 0) {
                        for (int i = 0; i < locationLevelData.getMohollaList().size(); i++) {
                            moholla.add(locationLevelData.getMohollaList().get(i).getMohallaDesc());
                            map_moholla_values.put(locationLevelData.getMohollaList().get(i).getMohallaId(), locationLevelData.getMohollaList().get(i).getMohallaDesc());
                        }
                        fillMohollaSpinner(moholla);
                    }

                }


            }

        }


        if (id == Settings.SHOP_LOCATION_STATE ||id == Settings.SHOP_LOCATION_CITY ){

            initAddShopData = ((InitAddShopData) object);

            if(object!=null) {

                if (id == Settings.SHOP_LOCATION_STATE) {

                    List<String> state = new ArrayList<>();
                    state.add("Select State");

                    if (initAddShopData.stateList != null && initAddShopData.stateList.size() > 0) {
                        for (int i = 0; i < initAddShopData.stateList.size(); i++) {
                            if (initAddShopData.stateList.get(i).getCountryID().equals(selCountryID)) {
                                state.add(initAddShopData.stateList.get(i).getStateDesc());
                                map_state_values.put(initAddShopData.stateList.get(i).getStateID(), initAddShopData.stateList.get(i).getStateDesc());
                            }
                        }
                        fillStateSpinner(state);
                    }

                }

                if (id == Settings.SHOP_LOCATION_CITY) {

                    List<String> city = new ArrayList<>();
                    city.add("Select City");

                    if (initAddShopData.cityList != null && initAddShopData.cityList.size() > 0) {
                        for (int i = 0; i < initAddShopData.cityList.size(); i++) {
                            if (initAddShopData.cityList.get(i).getStateId().equals(selStateID)) {
                                city.add(initAddShopData.cityList.get(i).getCityDesc());
                                map_city_values.put(initAddShopData.cityList.get(i).getCityId(), initAddShopData.cityList.get(i).getCityDesc());
                            }
                        }
                        fillCitySpinner(city);
                    }

                }
            }
        }



        if (id == Settings.SUBMIT_ADD_SHOP_ID) {
            Utils.HitechToast(this,message);
            if (object!=null){
                initAddShopData = ((InitAddShopData) object);
                Log.d("shopid>>>", initAddShopData.shop_Id);
                shopID = initAddShopData.shop_Id;

            }

            new InitAddShopParser().executeQuery(this, QueryBuilder.getInstance().getPhotoUloadAPI(Settings.UPLOAD_IMAGE_URL), QueryBuilder.getInstance().generateUploadImageQuery(encodedImage, shopID,PrefManager.getKeyAuthToken(this).trim()), this, Settings.IMAGE_UPLOAD, true);

            startActivity(new Intent(this, MainActivity.class));
            finishAffinity();
        }
    }

    private void fetchLocationData(String locationIDType, String locationIdValue, String locationLevelApi, int responseID ){
        new FetchShopLocationParser().executeQuery(this, QueryBuilder.getInstance().getShopLocationAPI(locationLevelApi), QueryBuilder.getInstance().generateShopLocationLevelQuery(locationIDType, locationIdValue), this, responseID, true);
    }


    private void fetchStateCity(int responseID){
        new InitAddShopParser().executeQuery(this, QueryBuilder.getInstance().getInitAddShopAPI(), QueryBuilder.getInstance().generateInitAddShopQuery(this, PrefManager.getKeyAuthToken(this).trim()), this, responseID, true);
    }



    private void fillCountryStateCitySpinners(){

    }



    @Override
    public void onError(String error, int id, BaseController.ErrorCode code) {
        if(error!=null){
            Utils.HitechToast(this,error);
        }

    }


    public void fillAreaSpinner(List<String> areaList){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, areaList);
        spinnerLocality.setAdapter(adapter);
        //  selectSpinnerValue(spinnerState,defaultState);
        spinnerLocality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinnerLocality.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select Area"))) {
                    for (Map.Entry<Integer, String> areaEntry : map_area_values.entrySet()) {
                        if (areaEntry.getValue().equalsIgnoreCase(value)) {
                          //  selProdID = areaEntry.getKey();
                            Log.v("ProdId----------", "" + areaEntry.getKey());
                        }
                    }
                }else{

                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

/*********************************** multiple product Select *******************************/

    public void selectMultipleProductType(final List<String>nameList, final List<String>idList, List<String>selectedList){

        Log.d(" list data >>>>", nameList.toString() + "\n" + idList + " \n"  + selectedList + "\n");

        productListVOs = new ArrayList<>();

        for (int i = 0; i < nameList.size(); i++) {
            productVo = new MultiStateCityData();
            productVo.setTitle(nameList.get(i));
            productVo.setID(idList.get(i));
            productVo.setSelected(false);
            productVo.setAlreadySelected(selectedList.get(i));
            productListVOs.add(productVo);
        }


        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.custom_state_list);

        Button cancel = dialog.findViewById(R.id.btnCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        Button ok = dialog.findViewById(R.id.btnOK);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedProducts = new ArrayList<>();
                selectedProducts = productTypeAdapter.selectedStates();

                if (selectedProducts.isEmpty()){
                    Utils.HitechToast(ModifyActivity.this,"Please Select at least one state");
                    return;
                }

                Log.d("selected states>>>>", selectedProducts.toString());

                productListVOs = new ArrayList<>();
                for (int i = 0; i < nameList.size(); i++) {
                    productVo = new MultiStateCityData();
                    productVo.setTitle(nameList.get(i));
                    productVo.setID(idList.get(i));
                    productVo.setSelected(false);
                    if (selectedProducts.contains(idList.get(i))) {
                        productVo.setAlreadySelected("SELECTED");
                        Log.d("stateID>>>>",idList.get(i));
                    }else {
                        productVo.setAlreadySelected("");
                    }
                    productListVOs.add(productVo);

                }

                PrefManager.setSelectedStates(ModifyActivity.this,selectedProducts,PrefManager.KEY_SELECTED_PRODUCTS);

                String stateIDS = TextUtils.join(",", selectedProducts);

                Log.d("stateIDs>> ", stateIDS);

                selProdID = stateIDS;

                spinerProdType.setText(selectedProducts.size() + " " + getString(R.string.states_selected));

                dialog.dismiss();

              //  new DistrictListParser().executeQuery(ModifyActivity.this, QueryBuilder.getInstance().districtListApi(), QueryBuilder.getInstance().generateDistrictListQuery(ModifyActivity.this,stateIDS), ModifyActivity.this, Settings.FETCH_DISTRICT_LIST_ID, true);
            }
        });




        spinerProdType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                productTypeAdapter = new StateAdapter(ModifyActivity.this, 0,
                        productListVOs);
                ListView listView = dialog.findViewById(R.id.list_view);
                listView.setAdapter(productTypeAdapter);
                dialog.show();
            }
        });

        dialog.setCancelable(false);

        int alreadySelected = 0;

        productListVOs = new ArrayList<>();
        for (int i = 0; i < nameList.size(); i++) {
            productVo = new MultiStateCityData();
            productVo.setTitle(nameList.get(i));
            productVo.setID(idList.get(i));
            productVo.setSelected(false);
            if (shopDetailsData.productType_Id.contains(idList.get(i))) {
                productVo.setAlreadySelected("SELECTED");
                Log.d("stateID>>>>",idList.get(i));
                alreadySelected++;
            }else {
                productVo.setAlreadySelected("");
            }
            productListVOs.add(productVo);
        }

        spinerProdType.setText(alreadySelected + " " + "PRODUCT TYPE SELECTED");

        Log.d("prod Id >>>> ", shopDetailsData.productType_Id);

        selProdID = shopDetailsData.productType_Id;


    }


    /**********************************************************************************/




    /************************************ multi ferbric type select ******************************/




    public void selectMultipleFebricType(final List<String>nameList, final List<String>idList, List<String>selectedList){

        Log.d(" list data >>>>", nameList.toString() + "\n" + idList + " \n"  + selectedList + "\n");

        febricListVOs = new ArrayList<>();

        for (int i = 0; i < nameList.size(); i++) {
            febricVo = new MultiStateCityData();
            febricVo.setTitle(nameList.get(i));
            febricVo.setID(idList.get(i));
            febricVo.setSelected(false);
            febricVo.setAlreadySelected(selectedList.get(i));
            febricListVOs.add(febricVo);
        }


        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.custom_state_list);

        Button cancel = dialog.findViewById(R.id.btnCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        Button ok = dialog.findViewById(R.id.btnOK);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedFebrics = new ArrayList<>();
                selectedFebrics = productTypeAdapter.selectedStates();

                if (selectedFebrics.isEmpty()){
                    Utils.HitechToast(ModifyActivity.this,"Please Select at least one state");
                    return;
                }

                Log.d("selected states>>>>", selectedFebrics.toString());

                febricListVOs = new ArrayList<>();
                for (int i = 0; i < nameList.size(); i++) {
                    febricVo = new MultiStateCityData();
                    febricVo.setTitle(nameList.get(i));
                    febricVo.setID(idList.get(i));
                    febricVo.setSelected(false);
                    if (selectedFebrics.contains(idList.get(i))) {
                        febricVo.setAlreadySelected("SELECTED");
                        Log.d("stateID>>>>",idList.get(i));
                    }else {
                        febricVo.setAlreadySelected("");
                    }
                    febricListVOs.add(febricVo);

                }

                PrefManager.setSelectedStates(ModifyActivity.this,selectedFebrics,PrefManager.KEY_SELECTED_FEBRICS);

                String stateIDS = TextUtils.join(",", selectedFebrics);

                Log.d("stateIDs>> ", stateIDS);

                selFabricID = stateIDS;

                spinerProductFabric.setText(selectedFebrics.size() + " " + getString(R.string.states_selected));

                dialog.dismiss();

                //  new DistrictListParser().executeQuery(ModifyActivity.this, QueryBuilder.getInstance().districtListApi(), QueryBuilder.getInstance().generateDistrictListQuery(ModifyActivity.this,stateIDS), ModifyActivity.this, Settings.FETCH_DISTRICT_LIST_ID, true);
            }
        });




        spinerProductFabric.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                productTypeAdapter = new StateAdapter(ModifyActivity.this, 0,
                        febricListVOs);
                ListView listView = dialog.findViewById(R.id.list_view);
                listView.setAdapter(productTypeAdapter);
                dialog.show();
            }
        });

        dialog.setCancelable(false);


        int alreadySelected = 0;

        febricListVOs = new ArrayList<>();
        for (int i = 0; i < nameList.size(); i++) {
            febricVo = new MultiStateCityData();
            febricVo.setTitle(nameList.get(i));
            febricVo.setID(idList.get(i));
            febricVo.setSelected(false);
            if (shopDetailsData.febricType_Id.contains(idList.get(i))) {
                febricVo.setAlreadySelected("SELECTED");
                alreadySelected++;
                Log.d("stateID>>>>",idList.get(i));
            }else {
                febricVo.setAlreadySelected("");
            }
            febricListVOs.add(febricVo);
        }

        selFabricID = shopDetailsData.febricType_Id;
        spinerProductFabric.setText(alreadySelected + " " +"Fabric Type Selected");
    }



    /**********************************************************************************************/




    /******************************** multiple party type select  *************************************/


    public void selectMultiplePartyType(final List<String>nameList, final List<String>idList, List<String>selectedList){

        Log.d(" list data >>>>", nameList.toString() + "\n" + idList + " \n"  + selectedList + "\n");

        partyListVo = new ArrayList<>();

        for (int i = 0; i < nameList.size(); i++) {
            partyVO = new MultiStateCityData();
            partyVO.setTitle(nameList.get(i));
            partyVO.setID(idList.get(i));
            partyVO.setSelected(false);
            partyVO.setAlreadySelected(selectedList.get(i));
            partyListVo.add(partyVO);
        }


        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.custom_state_list);

        Button cancel = dialog.findViewById(R.id.btnCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        Button ok = dialog.findViewById(R.id.btnOK);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedParty = new ArrayList<>();
                selectedParty = productTypeAdapter.selectedStates();

                if (selectedParty.isEmpty()){
                    Utils.HitechToast(ModifyActivity.this,"Please Select at least one state");
                    return;
                }

                Log.d("selected states>>>>", selectedParty.toString());

                partyListVo = new ArrayList<>();
                for (int i = 0; i < nameList.size(); i++) {
                    partyVO = new MultiStateCityData();
                    partyVO.setTitle(nameList.get(i));
                    partyVO.setID(idList.get(i));
                    partyVO.setSelected(false);
                    if (selectedParty.contains(idList.get(i))) {
                        partyVO.setAlreadySelected("SELECTED");
                        Log.d("stateID>>>>",idList.get(i));
                    }else {
                        partyVO.setAlreadySelected("");
                    }
                    partyListVo.add(partyVO);

                }

                PrefManager.setSelectedStates(ModifyActivity.this,selectedParty,PrefManager.KEY_SELECTED_STATES);

                String stateIDS = TextUtils.join(",", selectedParty);

                Log.d("stateIDs>> ", stateIDS);

                selPartyType = stateIDS;

                spinerPartyType.setText(selectedParty.size() + " " + getString(R.string.states_selected));

                dialog.dismiss();

                //  new DistrictListParser().executeQuery(ModifyActivity.this, QueryBuilder.getInstance().districtListApi(), QueryBuilder.getInstance().generateDistrictListQuery(ModifyActivity.this,stateIDS), ModifyActivity.this, Settings.FETCH_DISTRICT_LIST_ID, true);
            }
        });




        spinerPartyType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                productTypeAdapter = new StateAdapter(ModifyActivity.this, 0,
                        partyListVo);
                ListView listView = dialog.findViewById(R.id.list_view);
                listView.setAdapter(productTypeAdapter);
                dialog.show();
            }
        });

        dialog.setCancelable(false);

        int alreadySelected = 0;

        partyListVo = new ArrayList<>();
        for (int i = 0; i < nameList.size(); i++) {
            partyVO = new MultiStateCityData();
            partyVO.setTitle(nameList.get(i));
            partyVO.setID(idList.get(i));
            partyVO.setSelected(false);
            if (shopDetailsData.partyType_Id.contains(idList.get(i))) {
                partyVO.setAlreadySelected("SELECTED");
                alreadySelected++;
                Log.d("stateID>>>>",idList.get(i));
            }else {
                partyVO.setAlreadySelected("");
            }
            partyListVo.add(partyVO);

        }

        spinerPartyType.setText(alreadySelected + " " + "FABRIC TYPE SELECTED");
        selPartyType = shopDetailsData.partyType_Id;

    }


    /*****************************************************************************************************/




    /*public void fillProdTypeSpinner(List<String> prodTypeList){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, prodTypeList);
        spinerProdType.setAdapter(adapter);
        ArrayAdapter myProdAdap = (ArrayAdapter) spinerProdType.getAdapter(); //cast to an ArrayAdapter
        int spinnerPosition = myProdAdap.getPosition(prodDesc);
        spinerProdType.setSelection(spinnerPosition);

        //  selectSpinnerValue(spinnerState,defaultState);
        spinerProdType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinerProdType.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select Product Type"))) {
                    for (Map.Entry<Integer, String> prodEntry : map_prod_type_values.entrySet()) {
                        if (prodEntry.getValue().equalsIgnoreCase(value)) {
                            selProdID = prodEntry.getKey();
                            Log.v("ProdId----------", "" + prodEntry.getKey());
                        }
                    }
                }else{

                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }*/

    /*public void fillFabricTypeSpinner(List<String> fabricTypeList){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, fabricTypeList);
        spinerProductFabric.setAdapter(adapter);

        ArrayAdapter myFabAdap = (ArrayAdapter) spinerProductFabric.getAdapter(); //cast to an ArrayAdapter
        int spinnerPosition = myFabAdap.getPosition(fabDesc);
        spinerProductFabric.setSelection(spinnerPosition);

        //  selectSpinnerValue(spinnerState,defaultState);
        spinerProductFabric.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinerProductFabric.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select Fabric"))) {
                    for (Map.Entry<Integer, String> fabricEntry : map_fabric_type_values.entrySet()) {
                        if (fabricEntry.getValue().equalsIgnoreCase(value)) {
                            selFabricID = fabricEntry.getKey();
                            Log.v("FabricId----------", "" + fabricEntry.getKey());
                        }
                    }
                }else{

                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }*/

    /*public void fillPartyTypeSpinner(List<String> partTypeList){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, partTypeList);
        spinerPartyType.setAdapter(adapter);

        ArrayAdapter myPartyAdap = (ArrayAdapter) spinerPartyType.getAdapter(); //cast to an ArrayAdapter
        int spinnerPosition = myPartyAdap.getPosition(partyDesc);
        spinerPartyType.setSelection(spinnerPosition);

        spinerPartyType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinerPartyType.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select Party Type"))) {
                    for (Map.Entry<Integer, String> partyEntry : map_party_type_values.entrySet()) {
                        if (partyEntry.getValue().equalsIgnoreCase(value)) {
                            selPartyType = partyEntry.getKey();
                            Log.v("PartyId----------", "" + partyEntry.getKey());
                        }
                    }
                }else{

                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }*/


    public void fillRankingNoSpinner(List<String> rankingNumberList){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, rankingNumberList);
        spinerRanking.setAdapter(adapter);

        ArrayAdapter myRankaAdap = (ArrayAdapter) spinerRanking.getAdapter(); //cast to an ArrayAdapter
        int spinnerPosition = myRankaAdap.getPosition(rankDesc);
        spinerRanking.setSelection(spinnerPosition);

        spinerRanking.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinerRanking.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select Ranking Number"))) {
                    for (Map.Entry<Integer, String> rankingEntry : map_Ranking_No_values.entrySet()) {
                        if (rankingEntry.getValue().equalsIgnoreCase(value)) {
                            selRankingNo = rankingEntry.getKey();
                            Log.v("RankingId----------", "" + rankingEntry.getKey());
                        }
                    }
                }else{

                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }




    public void fillCountrySpinner(List<String> countryList){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, countryList);
        spinnerCountry.setAdapter(adapter);

        ArrayAdapter myCountryAdap = (ArrayAdapter) spinnerCountry.getAdapter(); //cast to an ArrayAdapter
        int spinnerPosition = myCountryAdap.getPosition(countryDesc);
        spinnerCountry.setSelection(spinnerPosition);


        spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinnerCountry.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select Country"))) {
                    for (Map.Entry<Integer, String> countryEntry : map_country_values.entrySet()) {
                        if (countryEntry.getValue().equalsIgnoreCase(value)) {
                            selCountryID = countryEntry.getKey();
                            Log.v("CountryID----------", "" + countryEntry.getKey());
                            fetchStateCity(Settings.SHOP_LOCATION_STATE);
                        }
                    }
                }else{

                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }




    public void fillStateSpinner(List<String> stateList){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, stateList);
        spinnerState.setAdapter(adapter);

        ArrayAdapter myStateAdap = (ArrayAdapter) spinnerState.getAdapter(); //cast to an ArrayAdapter
        int spinnerPosition = myStateAdap.getPosition(stateDesc);
        spinnerState.setSelection(spinnerPosition);

        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinnerState.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select State"))) {
                    for (Map.Entry<Integer, String> stateEntry : map_state_values.entrySet()) {
                        if (stateEntry.getValue().equalsIgnoreCase(value)) {
                            selStateID = stateEntry.getKey();
                            Log.v("StateID----------", "" + stateEntry.getKey());
                            fetchStateCity(Settings.SHOP_LOCATION_CITY);
                        }
                    }
                }else{

                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }



    public void fillCitySpinner(List<String> cityList){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, cityList);
        spinnerCity.setAdapter(adapter);

        ArrayAdapter myCityAdap = (ArrayAdapter) spinnerCity.getAdapter(); //cast to an ArrayAdapter
        int spinnerPosition = myCityAdap.getPosition(cityDesc);
        spinnerCity.setSelection(spinnerPosition);



        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinnerCity.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select City"))) {
                    for (Map.Entry<Integer, String> cityEntry : map_city_values.entrySet()) {
                        if (cityEntry.getValue().equalsIgnoreCase(value)) {
                            selCityID = cityEntry.getKey();
                            Log.v("CityID----------", "" + cityEntry.getKey());
                            fetchLocationData("cityId",String.valueOf(cityEntry.getKey()),Settings.SHOP_LOCALITY_LIST, Settings.SHOP_LOCATION_LOCALITY);
                        }
                    }
                }else{

                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void fillLocalitySpinner(List<String> localityList){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, localityList);
        spinnerLocality.setAdapter(adapter);

        ArrayAdapter myAreaAdap = (ArrayAdapter) spinnerLocality.getAdapter(); //cast to an ArrayAdapter
        int spinnerPosition = myAreaAdap.getPosition(areaDesc);
        spinnerLocality.setSelection(spinnerPosition);

        spinnerLocality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinnerLocality.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select Locality"))) {
                    for (Map.Entry<Integer, String> localityEntry : map_area_values.entrySet()) {
                        if (localityEntry.getValue().equalsIgnoreCase(value)) {
                            selLocalityID = localityEntry.getKey();
                            Log.v("LocalityID----------", "" + localityEntry.getKey());
                            fetchLocationData("localityId",String.valueOf(localityEntry.getKey()),Settings.SHOP_MOHOLLA_LIST, Settings.SHOP_LOCATION_MOHOLLA);
                        }
                    }
                }else{

                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }



    public void fillMohollaSpinner(List<String> mohollaList){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, mohollaList);
        spinnerMoholla.setAdapter(adapter);

        ArrayAdapter myMohallaAdap = (ArrayAdapter) spinnerMoholla.getAdapter(); //cast to an ArrayAdapter
        int spinnerPosition = myMohallaAdap.getPosition(mohallaDesc);
        spinnerMoholla.setSelection(spinnerPosition);

        spinnerMoholla.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinnerMoholla.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select Moholla"))) {
                    for (Map.Entry<Integer, String> mohollaEntry : map_moholla_values.entrySet()) {
                        if (mohollaEntry.getValue().equalsIgnoreCase(value)) {
                            selMohollaID = mohollaEntry.getKey();
                            Log.v("mohollaID----------", "" + mohollaEntry.getKey());
                            getLocationFromAddress(spinnerMoholla.getSelectedItem().toString()+ " "
                                    +spinnerLocality.getSelectedItem().toString()+ " "+spinnerCity.getSelectedItem().toString()+ " "+
                                    spinnerState.getSelectedItem().toString()+" "+spinnerCountry.getSelectedItem().toString());
                        }
                    }
                }else{

                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }



    public void getLocationFromAddress(String strAddress){
        Geocoder coder = new Geocoder(ModifyActivity.this, Locale.getDefault());
        List<Address> address;
        try {
            address = coder.getFromLocationName(strAddress,5);
            if (address==null) {
                Log.d("address >>","no address found");
            }
            String postalCode = address.get(0).getPostalCode();

            edtPin.setText(postalCode);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnModifyShop:
                if(edtShopName.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter shop name.");
             //   }else if(edtOwnerName.getText().toString().equalsIgnoreCase("")){
             //       Utils.HitechToast(this,"Please enter owner name.");
             //   }else if(edtOwnerMob.getText().toString().equalsIgnoreCase("")){
             //       Utils.HitechToast(this,"Please enter owner mob. no.");
                }else if(edtShopAdd.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter shop address.");
                } else if(spinnerCountry.getCount()==0||spinnerCountry.getSelectedItem().toString().equalsIgnoreCase("Select Country")){
                    Utils.HitechToast(this,"Please select country name");
                }else if(spinnerState.getCount()==0||spinnerState.getSelectedItem().toString().equalsIgnoreCase("Select State")){
                    Utils.HitechToast(this,"Please select state name");
                }else if(spinnerCity.getCount()==0||spinnerCity.getSelectedItem().toString().equalsIgnoreCase("Select City")){
                    Utils.HitechToast(this,"Please select city name");
                }else if(spinnerLocality.getCount()==0||spinnerLocality.getSelectedItem().toString().equalsIgnoreCase("Select Locality")){
                    Utils.HitechToast(this,"Please select locality name");
                }else if(spinnerMoholla.getCount()==0||spinnerMoholla.getSelectedItem().toString().equalsIgnoreCase("Select Moholla")){
             //       Utils.HitechToast(this,"Please select mohalla name");
             //   }else if(edtPin.getText().toString().equalsIgnoreCase("")){
             //       Utils.HitechToast(this,"Please enter pin code");
             //   } else if(edtPurchaserName.getText().toString().equalsIgnoreCase("")){
             //       Utils.HitechToast(this,"Please enter purchaser name.");
             //   }else if(edtPurchaserNumber.getText().toString().equalsIgnoreCase("")){
             //       Utils.HitechToast(this,"Please enter purchaser number.");
             //   }else if(edtEmailID.getText().toString().equalsIgnoreCase("")){
             //       Utils.HitechToast(this,"Please enter email id.");
             //   }else if(!Utils.isValidEmail(edtEmailID.getText().toString().toLowerCase().trim())) {
             //       Utils.HitechToast(this,"Please enter valid email.");
             //   }else if(edtTelephoneNo.getText().toString().equalsIgnoreCase("")){
             //       Utils.HitechToast(this,"Please enter telephone number.");
             //   }else if(edtWebsite.getText().toString().equalsIgnoreCase("")){
             //       Utils.HitechToast(this,"Please enter website.");
             //   }else if(!Utils.isValidUrl(edtWebsite.getText().toString().toLowerCase().trim())) {
             //       Utils.HitechToast(this,"Please enter valid website.");
             //   }else if(edtAccountantName.getText().toString().equalsIgnoreCase("")){
             //       Utils.HitechToast(this,"Please enter accountant name.");
             //   }else if(edtAccountantNumber.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter accountant number.");
                }else if(spinerProdType.getText().toString().equalsIgnoreCase("Select Product Type")){
                    Utils.HitechToast(this,"Please select product type");
                }else if(spinerProductFabric.getText().toString().equalsIgnoreCase("Select Fabric")){
                    Utils.HitechToast(this,"Please select fabric");
                }else if(spinerPartyType.getText().toString().equalsIgnoreCase("Select Party Type")){
                    Utils.HitechToast(this,"Please select party type");
                }else if(spinerRanking.getSelectedItem().toString().equalsIgnoreCase("Select Ranking Number")){
                    Utils.HitechToast(this,"Please select ranking number");
                }else{
                    //new InitAddShopParser().executeQuery(this, QueryBuilder.getInstance().getSubmitAddShopAPI(), QueryBuilder.getInstance().generateSubmitAddShopQuery(this, PrefManager.getKeyAuthToken(this).trim(),edtShopName.getText().toString().trim(),edtOwnerName.getText().toString().trim(),edtOwnerMob.getText().toString().trim(),spinnerMoholla.getSelectedItem().toString().trim(),spinnerLocality.getSelectedItem().toString().trim(),spinnerState.getSelectedItem().toString().trim(),String.valueOf(selCountryID),edtPin.getText().toString(),edtPurchaserName.getText().toString().trim(),edtPurchaserNumber.getText().toString().trim(),edtEmailID.getText().toString().trim(),edtTelephoneNo.getText().toString().trim(),edtWebsite.getText().toString().trim(),edtAccountantName.getText().toString().trim(),edtAccountantNumber.getText().toString().trim(),String.valueOf(selProdID),String.valueOf(selFabricID),String.valueOf(selPartyType),String.valueOf(selRankingNo),"28.6666","97.73838","Gurgaon"), this, Settings.SUBMIT_ADD_SHOP_ID, true);
                    new InitAddShopParser().executeQuery(this, QueryBuilder.getInstance().getSubmitAddShopAPI(), QueryBuilder.getInstance().generateUpdateAddShopQuery(this, PrefManager.getKeyAuthToken(this).trim(),edtOwnerName.getText().toString().trim(),edtOwnerMob.getText().toString().trim(),edtShopName.getText().toString().trim(),edtPurchaserName.getText().toString(),edtPurchaserNumber.getText().toString().trim(),edtEmailID.getText().toString().trim(),edtTelephoneNo.getText().toString().trim(),edtWebsite.getText().toString().trim(),edtAccountantName.getText().toString().trim(),edtAccountantNumber.getText().toString(),String.valueOf(selProdID),String.valueOf(selFabricID),String.valueOf(selPartyType),String.valueOf(selRankingNo),latitude,logitude,edtShopAdd.getText().toString(),String.valueOf(selStateID),String.valueOf(selCityID),String.valueOf(selLocalityID),String.valueOf(selMohollaID),edtPin.getText().toString(),shopID), this, Settings.SUBMIT_ADD_SHOP_ID, true);
                }
                break;
            case R.id.tvCamera:
                if (!hasPermissions(ModifyActivity.this, PERMISSIONS)) {
                    ActivityCompat.requestPermissions(ModifyActivity.this, PERMISSIONS, PERMISSION_ALL);
                } else {
                    // openFile();
                    uploadBreDocs();
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void uploadBreDocs(){
        final Dialog dialogBuilder = new Dialog(this);
        dialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        dialogBuilder.setContentView(R.layout.dialog_upload_doc);
        final TextView tvCamera =  dialogBuilder.findViewById(R.id.tvCamera);
        final TextView tvInteralStorage = dialogBuilder.findViewById(R.id.tvInteralStorage);
        final Button btnClose =   dialogBuilder.findViewById(R.id.close);

        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, REQUEST_IMAGE);
                dialogBuilder.dismiss();
            }
        });

        tvInteralStorage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFile();
                dialogBuilder.dismiss();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBuilder.dismiss();
            }
        });
        dialogBuilder.show();
    }

    public void openFile() {
        Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i,00);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 00) {
            if (resultCode == RESULT_OK) {
                if (data != null) {

                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };
                    Cursor cursor = getContentResolver().query(selectedImage,filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();



                    /****************** convert to base64 ************************/

                    Bitmap bm = BitmapFactory.decodeFile(picturePath);
                    Log.d("bm >>",bm.toString()+" >"+bm.getHeight());

                    int nh = (int) ( bm.getHeight() * (512.0 / bm.getWidth()) );
                    Bitmap scaled = Bitmap.createScaledBitmap(bm, 512, nh, true);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    scaled.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                    byte[] b = baos.toByteArray();

                    encodedImage = Base64.encodeToString(b, Base64.NO_WRAP);

                    tvCamera.setText("Image Attacded");


                    /****************************************************************/



                }
            }
        } else if (requestCode == REQUEST_IMAGE && resultCode == Activity.RESULT_OK) {

            Bitmap photo = (Bitmap) data.getExtras().get("data");
            File file = createDirectoryAndSaveFile(photo,"drm_image");
            imagePath = file.getAbsolutePath();

            Bitmap bm = BitmapFactory.decodeFile(imagePath);
            Log.d("bm >>",bm.toString()+" >"+bm.getHeight());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
            byte[] b = baos.toByteArray();

            encodedImage = Base64.encodeToString(b, Base64.NO_WRAP);

            tvCamera.setText("drm_image uploaded");
        }
    }




    public static String getPostalCodeByCoordinates(Context context, double lat, double lon) throws IOException {

        Geocoder mGeocoder = new Geocoder(context, Locale.getDefault());
        String zipcode=null;
        Address address=null;

        if (mGeocoder != null) {

            List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 5);

            if (addresses != null && addresses.size() > 0) {

                for (int i = 0; i < addresses.size(); i++) {
                    address = addresses.get(i);
                    if (address.getPostalCode() != null) {
                        zipcode = address.getPostalCode();
                        Log.d("TAG", "Postal code: " + address.getPostalCode());
                        break;
                    }

                }
                return zipcode;
            }
        }

        return null;
    }

    private void getLocation(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if (!isGPSEnabled && !isNetworkEnabled) {
                Toast.makeText(getApplicationContext(), getText(R.string.provider_failed), Toast.LENGTH_LONG).show();
            } else {
                location = null;
                if (isGPSEnabled) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_MIN_UPDATE_TIME, LOCATION_MIN_UPDATE_DISTANCE, locationListener);
                    location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, LOCATION_MIN_UPDATE_TIME, LOCATION_MIN_UPDATE_DISTANCE, locationListener);
                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
                if (location != null) {
                    // Utils.HitechToast(this,"Lattitude :"+location.getLatitude()+"Longitude:"+location.getLatitude());
                }
            }
        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 12);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 13);
            }
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        getLocation();
        try {
            //  edtPin.setText(getPostalCodeByCoordinates(this,location.getLatitude(),location.getLongitude()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void poulateData() {
        if (shopDetailsData.shopName!=null){
            edtShopName.setText(shopDetailsData.shopName);
        }

        if (shopDetailsData.shopOwnerName!=null){
            edtOwnerName.setText(shopDetailsData.shopOwnerName);
        }

        if (shopDetailsData.shopOwnerMobileNo!=null){
            edtOwnerMob.setText(shopDetailsData.shopOwnerMobileNo);
        }

        if (shopDetailsData.shopAddress!=null){
            edtShopAdd.setText(shopDetailsData.shopAddress);
        }

        if(shopDetailsData.purchaserName!=null){
            edtPurchaserName.setText(shopDetailsData.purchaserName);
        }

        if(shopDetailsData.purchaserMobile!=null){
            edtPurchaserNumber.setText(shopDetailsData.purchaserMobile);
        }



        if(shopDetailsData.email!=null){
            edtEmailID.setText(shopDetailsData.email);
        }

        if(shopDetailsData.telephone!=null){
            edtTelephoneNo.setText(shopDetailsData.telephone);
        }

        if(shopDetailsData.website!=null){
            edtWebsite.setText(shopDetailsData.website);
        }

        if(shopDetailsData.accountName!=null){
            edtAccountantName.setText(shopDetailsData.accountName);
        }

        if(shopDetailsData.accountNumber!=null){
            edtAccountantNumber.setText(shopDetailsData.accountNumber);
        }

        if(shopDetailsData.pin!=null){
            edtPin.setText(shopDetailsData.pin);
        }

        if(shopDetailsData.countryDesc!=null){
            countryDesc=shopDetailsData.countryDesc;
        }



        if(shopDetailsData.stateDesc!=null){
            stateDesc=shopDetailsData.stateDesc;
        }

        if(shopDetailsData.cityDesc!=null){
            cityDesc=shopDetailsData.cityDesc;
        }

        if(shopDetailsData.areaDesc!=null){
            areaDesc=shopDetailsData.areaDesc;
        }

        if(shopDetailsData.mohallaDesc!=null){
            mohallaDesc=shopDetailsData.mohallaDesc;
        }

        if(shopDetailsData.productName!=null){
            prodDesc=shopDetailsData.productName;
        }

        if(shopDetailsData.fabricName!=null){
            fabDesc=shopDetailsData.fabricName;
        }

        if(shopDetailsData.partyTypeName!=null){
            partyDesc=shopDetailsData.partyTypeName;
        }

        if(shopDetailsData.ranking!=null){
            rankDesc=shopDetailsData.ranking;
        }


        if (shopDetailsData.imagePath1!=null){
            encodedImage = shopDetailsData.imagePath1;
            byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            drmImg.setImageBitmap(decodedByte);
           // new DownloadImageTask(drmImg).execute(shopDetailsData.imagePath1);
        }
            shopID=String.valueOf(shopDetailsData.shop_Id);


    }


   /* private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }*/


    private File createDirectoryAndSaveFile(Bitmap imageToSave, String fileName) {

        File direct = new File(Environment.getExternalStorageDirectory() + "/DRM");

        if (!direct.exists()) {
            File wallpaperDirectory = new File("/sdcard/DRM/");
            wallpaperDirectory.mkdirs();
        }

        File file = new File(new File("/sdcard/DRM/"), fileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

}