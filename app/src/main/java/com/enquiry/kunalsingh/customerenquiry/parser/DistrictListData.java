package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Kunal Singh on 22-05-2018.
 */
public class DistrictListData implements Parcelable {

    @Expose
    @SerializedName("cityList")
    public List<CityList> cityList;

    protected DistrictListData(Parcel in) {
    }

    public static final Creator<DistrictListData> CREATOR = new Creator<DistrictListData>() {
        @Override
        public DistrictListData createFromParcel(Parcel in) {
            return new DistrictListData(in);
        }

        @Override
        public DistrictListData[] newArray(int size) {
            return new DistrictListData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }

    public static class CityList {
        @Expose
        @SerializedName("cityDesc")
        public String cityDesc;
        @Expose
        @SerializedName("cityId")
        public int cityId;
        @Expose
        @SerializedName("selected")
        public String selected;

    }
}
