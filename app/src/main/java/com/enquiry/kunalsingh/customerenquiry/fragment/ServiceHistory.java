package com.enquiry.kunalsingh.customerenquiry.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.adapter.ServiceHistoryAdapter;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.parser.ServiceHistoryData;
import com.enquiry.kunalsingh.customerenquiry.parser.ServiceHistotryParser;
import com.enquiry.kunalsingh.customerenquiry.utility.PrefManager;
import com.enquiry.kunalsingh.customerenquiry.utility.QueryBuilder;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.enquiry.kunalsingh.customerenquiry.utility.Utils;

import static android.view.View.VISIBLE;

/**
 * Created by Kunal Singh on 05-06-2018.
 */
public class ServiceHistory extends AppCompatActivity implements ServiceHistotryParser.onMaintinanceCostListListener {
    RecyclerView recyclerViewServiceHistoryList;
    ServiceHistoryAdapter serviceHistoryAdapter;
    ServiceHistoryData serviceHistoryData;
    private RelativeLayout noDataLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_history);
        Toolbar toolbar = findViewById(R.id.header);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("Service History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        recyclerViewServiceHistoryList=findViewById(R.id.recyclerViewServiceHistoryList);
        noDataLayout = (RelativeLayout) findViewById(R.id.no_data_rl);
        new ServiceHistotryParser().executeQuery(this, QueryBuilder.getInstance().fetchServiceHistoryAPI(), QueryBuilder.getInstance().generateServiceHistoryQuery(this, PrefManager.getKeyUserMobileNumber(this), Utils.versionNumber(this), Utils.getDeviceName(), Utils.deviceToken(this), "2018","1","20"), this, Settings.SERVICE_HISTORY_ID, true);
    }

    @Override
    public void onSuccess(int id, String message, Object object) {
        if(id==Settings.SERVICE_HISTORY_ID) {
            if (object != null) {
                serviceHistoryData = ((ServiceHistoryData) object);
                if(serviceHistoryData.serviceHistoryList!=null&&serviceHistoryData.serviceHistoryList.size()>0) {
                    recyclerViewServiceHistoryList.setVisibility(VISIBLE);
                    serviceHistoryAdapter = new ServiceHistoryAdapter(this, serviceHistoryData.serviceHistoryList);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                    recyclerViewServiceHistoryList.setLayoutManager(layoutManager);
                    recyclerViewServiceHistoryList.setAdapter(serviceHistoryAdapter);
                }else{
                    recyclerViewServiceHistoryList.setVisibility(View.GONE);
                    noDataLayout.setVisibility(VISIBLE);
                }
            }
        }
    }

    @Override
    public void onError(String error, int id, BaseController.ErrorCode code) {
        if(id==Settings.SERVICE_HISTORY_ID){
            recyclerViewServiceHistoryList.setVisibility(View.GONE);
            noDataLayout.setVisibility(VISIBLE);
            if(error!=null)
            Utils.HitechToast(this,error);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
