package com.enquiry.kunalsingh.customerenquiry;

import android.view.Menu;

import java.util.ArrayList;

/**
 * Created by Kunal Singh on 29-04-2018.
 */
public class NavMenuClass {
    Menu menu;
    ArrayList items;

    public NavMenuClass(Menu menu, ArrayList items){

        this.items = items;
        this.menu = menu;

    }

    public Menu getMenu(){
        return menu;
    }

    public ArrayList getItems(){
        return items;
    }
}
