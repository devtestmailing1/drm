package com.enquiry.kunalsingh.customerenquiry.utility;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.enquiry.kunalsingh.customerenquiry.R;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * todo: Utils Class for common method
 * Created by Kunal Singh
 * Date 22 April 2018
 */
public class Utils {
    static Toast toast1;
    public static String TAG = "Utils";
    private static final int CALL_PHONE_REQUEST_CODE = 101;
    public static void HitechToast(Context context, String msg) {
        toast1 = Toast.makeText(context, " ", Toast.LENGTH_SHORT);
        View toastView = toast1.getView();
        toastView.setBackgroundResource(R.drawable.toast_drawable);
        toast1.setText(msg);
        toast1.show();
    }

    /**
     * todo: Check Network
     * Created by Kunal Singh
     * Date 22 April 2018
     */

    public static boolean isNetworkAvailable(Context context) {
        boolean isNetworkAvailable = false;
        ConnectivityManager manager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        // For 3G check
        boolean is3g = false;
        if (manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null)
            is3g = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
                    .isConnectedOrConnecting();
        // For WiFi Check
        boolean isWifi = false;
        if (manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null)
            isWifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                    .isConnectedOrConnecting();

        if (is3g || isWifi) {
            isNetworkAvailable = true;
        }

        return isNetworkAvailable;
    }

    /**
     * todo: Check Valid Mail ID
     * Created by Kunal Singh
     * Date 22 April 2018
     */

    public static boolean isValidEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }


    /**
     * todo: Get App Version
     * Created by Kunal Singh
     * Date 22 April 2018
     */

    public static String versionNumber(Context context){
        PackageManager manager = context.getPackageManager();
        PackageInfo info = null;
        String version = "1.0";
        try {
            info = manager.getPackageInfo(
                    context.getPackageName(), 0);
            version = info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return version;
    }


    /**
     * todo: Validation for Mobile Number
     * Created by Kunal Singh
     * Date 22 April 2018
     */

    public static boolean isValidMobile(String phone)
    {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", phone))
        {
            if(phone.length()<10)
            {
                check = false;

            }
            else
            {
                check = true;

            }
        }
        else
        {
            check=false;
        }
        return check;
    }




    /**
     * todo: Date Dialog
     * Created by Kunal Singh
     * Date 22 April 2018
     */
    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    public static void dateDialog(Context context, final TextView txtViewDate) {
        Calendar cal = Calendar.getInstance();
        DatePickerDialog dateDailog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int month = monthOfYear + 1;
                txtViewDate.setText(dayOfMonth + "-" + month + "-" + year);
            }
        }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        dateDailog.getDatePicker().setMinDate(cal.getTimeInMillis());
        dateDailog.show();
    }

    /**
     * todo: Time Dialg
     * Created by Kunal Singh
     * Date 22 April 2018
     */
    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    public static void timeDialog(Context context,final TextView textViewTime) {
        Calendar cal = Calendar.getInstance();
        TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                        textViewTime.setText(selectedHour + ":" + selectedMinute);
                    }
                }, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true);
       // timePickerDialog.setTitle("Select Time");
        timePickerDialog.show();
    }


    /**
     * todo: Get IEMEI Number
     * Created by Kunal Singh
     * Date 22 April 2018
     */

    public static String deviceToken(Context context) {
        String deviceID = "";
        // Check if the READ_PHONE_STATE permission is already available.
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // READ_PHONE_STATE permission has not been granted.
            //  requestReadPhoneStatePermission(context);
        } else {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            deviceID = tm.getDeviceId();
        }
        return deviceID;
    }


    /**
     * todo: Get Device Name
     * Created by Kunal Singh
     * Date 22 April 2018
     */

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static boolean isValidUrl(String url) {
        Pattern p = Patterns.WEB_URL;
        Matcher m = p.matcher(url.toLowerCase());
        return m.matches();
    }

    public static void call(Activity context, String number) {
        Log.e("call Dialog number", number);
        try {
            int permission = ActivityCompat.checkSelfPermission(context,
                    Manifest.permission.CALL_PHONE);

            if (permission != PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "Permission to record denied");
                makeRequest(context);
            }else {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number));
                context.startActivity(intent);
            }

        } catch (ActivityNotFoundException activityException) {
            Log.e("call Dialog", "Call failed");
        }
    }

    protected static void makeRequest(Activity context) {
        ActivityCompat.requestPermissions(context,
                new String[]{Manifest.permission.CALL_PHONE},
                CALL_PHONE_REQUEST_CODE);

    }

}
