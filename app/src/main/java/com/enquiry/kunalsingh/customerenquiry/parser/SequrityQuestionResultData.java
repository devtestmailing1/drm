package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Kunal Singh on 29-06-2018.
 */
public class SequrityQuestionResultData implements Parcelable{

    @Expose
    @SerializedName("otp")
    public String otp;

    protected SequrityQuestionResultData(Parcel in) {
        otp = in.readString();
    }

    public static final Creator<SequrityQuestionResultData> CREATOR = new Creator<SequrityQuestionResultData>() {
        @Override
        public SequrityQuestionResultData createFromParcel(Parcel in) {
            return new SequrityQuestionResultData(in);
        }

        @Override
        public SequrityQuestionResultData[] newArray(int size) {
            return new SequrityQuestionResultData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(otp);
    }
}
