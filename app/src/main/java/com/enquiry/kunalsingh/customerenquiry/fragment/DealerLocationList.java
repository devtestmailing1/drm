package com.enquiry.kunalsingh.customerenquiry.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.adapter.DealerLoactionAdapter;
import com.enquiry.kunalsingh.customerenquiry.parser.FetchDealerLocData;
import com.enquiry.kunalsingh.customerenquiry.parser.RegisteredUserData;

/**
 * Created by Kunal Singh on 25-05-2018.
 */
public class DealerLocationList extends AppCompatActivity {
    private DealerLoactionAdapter dealerLoactionAdapter;
    RecyclerView recyclerViewDeliveryList;
    FetchDealerLocData fetchDealerLocData;
    Button btnSearchDealer;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer_loc_list);
        Toolbar toolbar = findViewById(R.id.header);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("Add Dealer");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        if (getIntent() != null && getIntent().getParcelableExtra("dealerLocData") != null) {
            fetchDealerLocData = ((FetchDealerLocData) getIntent().getParcelableExtra("dealerLocData"));
        }

        btnSearchDealer=findViewById(R.id.btnSearchDealer);
        recyclerViewDeliveryList=findViewById(R.id.recyclerViewDealerLocList);
        dealerLoactionAdapter = new DealerLoactionAdapter(this, fetchDealerLocData.dealerLocatorList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewDeliveryList.setLayoutManager(layoutManager);
        recyclerViewDeliveryList.setAdapter(dealerLoactionAdapter);
        btnSearchDealer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.putExtra("editTextValue", "value_here");
                    setResult(RESULT_OK, intent);
                    finish();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
