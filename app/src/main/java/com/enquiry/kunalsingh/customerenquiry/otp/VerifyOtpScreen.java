package com.enquiry.kunalsingh.customerenquiry.otp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.chaos.view.PinView;
import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.drawer.MainActivity;
import com.enquiry.kunalsingh.customerenquiry.fragment.HomeFragment;
import com.enquiry.kunalsingh.customerenquiry.parser.HomeDataParser;
import com.enquiry.kunalsingh.customerenquiry.parser.ShoapListData;
import com.enquiry.kunalsingh.customerenquiry.parser.VerifyOtpData;
import com.enquiry.kunalsingh.customerenquiry.parser.VerifyOtpDataParser;
import com.enquiry.kunalsingh.customerenquiry.utility.PrefManager;
import com.enquiry.kunalsingh.customerenquiry.utility.QueryBuilder;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.enquiry.kunalsingh.customerenquiry.utility.Utils;

/**
 * Created by Kunal Singh on 10-04-2018.
 */
public class VerifyOtpScreen extends AppCompatActivity implements VerifyOtpDataParser.onVerifyListener {
    VerifyOtpData verifyOtpData;
    String mobNumber="";
    String userMob,userType,cityId;
    String otp;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_verify_otp);
        final PinView pinView = (PinView) findViewById(R.id.pinView);
        if (getIntent() != null){
            userMob=getIntent().getStringExtra("userMobNo");
            userType=getIntent().getStringExtra("userType");
            cityId=getIntent().getStringExtra("cityID");
        }


        Button btnVerify=findViewById(R.id.btnVerify);
        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pinView.getText().toString().trim().equalsIgnoreCase("")){
                    Utils.HitechToast(VerifyOtpScreen.this,"Please enter 4 dgits OTP");
                }else {
                    otp=pinView.getText().toString().trim();
                    new VerifyOtpDataParser().executeQuery(VerifyOtpScreen.this, QueryBuilder.getInstance().verifyOTP(), QueryBuilder.getInstance().generateVerifyOTPQuery(VerifyOtpScreen.this,userMob, pinView.getText().toString(), cityId), VerifyOtpScreen.this, Settings.VERIFY_OTP_ID, true);
                }
            }
        });
    }

    @Override
    public void onSuccess(int id, String message, Object object) {
        if (object != null) {
            if(id==Settings.VERIFY_OTP_ID) {
                verifyOtpData = ((VerifyOtpData) object);
                PrefManager.setKeyOtp(this, otp);
                PrefManager.setUserMobileNumber(this, userMob);
                PrefManager.setKeyAuthToken(this, verifyOtpData.token);
                PrefManager.setKeyCityId(this, cityId);
                PrefManager.setLogin(this, true);
                startActivity(new Intent(VerifyOtpScreen.this, MainActivity.class));
            }
        }
    }

    @Override
    public void onError(String error, int id, BaseController.ErrorCode code) {
        if(error!=null){
            Utils.HitechToast(this,   error);
        }
    }
}
