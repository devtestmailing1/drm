package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kunal Singh on 22-05-2018.
 */
public class LocalityListData implements Parcelable{

    @Expose
    @SerializedName("userResponse")
    public List<UserResponse> userResponse;

    protected LocalityListData(Parcel in) {
        userResponse= new ArrayList<LocalityListData.UserResponse>();
        in.readList(userResponse,LocalityListData.UserResponse.class.getClassLoader());
    }

    public static final Creator<LocalityListData> CREATOR = new Creator<LocalityListData>() {
        @Override
        public LocalityListData createFromParcel(Parcel in) {
            return new LocalityListData(in);
        }

        @Override
        public LocalityListData[] newArray(int size) {
            return new LocalityListData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }

    public static class UserResponse implements Parcelable{
        @Expose
        @SerializedName("localityName")
        public String localityName;
        @Expose
        @SerializedName("pinId")
        public int pinId;

        @SerializedName("pinCode")
        public String pinCode;

        protected UserResponse(Parcel in) {
            localityName = in.readString();
            pinId = in.readInt();
            pinCode = in.readString();
        }

        public static final Creator<UserResponse> CREATOR = new Creator<UserResponse>() {
            @Override
            public UserResponse createFromParcel(Parcel in) {
                return new UserResponse(in);
            }

            @Override
            public UserResponse[] newArray(int size) {
                return new UserResponse[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(localityName);
            parcel.writeInt(pinId);
            parcel.writeString(pinCode);
        }
    }
}
