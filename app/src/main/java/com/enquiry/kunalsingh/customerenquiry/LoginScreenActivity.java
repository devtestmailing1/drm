package com.enquiry.kunalsingh.customerenquiry;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.otp.VerifyOtpScreen;
import com.enquiry.kunalsingh.customerenquiry.parser.LoginDataParser;
import com.enquiry.kunalsingh.customerenquiry.parser.RegisteredUserData;
import com.enquiry.kunalsingh.customerenquiry.utility.PrefManager;
import com.enquiry.kunalsingh.customerenquiry.utility.QueryBuilder;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.enquiry.kunalsingh.customerenquiry.utility.Utils;

import java.util.ArrayList;

/**
 * Created by Kunal Singh on 06 Aug 2018
 */
public class LoginScreenActivity extends AppCompatActivity{
    Button btnLogin;
    EditText edtTxtMob;
    RegisteredUserData registeredUserData;
    Spinner spinnerType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login_screen);
        edtTxtMob = findViewById(R.id.edtTxtMob);
        btnLogin = findViewById(R.id.btnLogin);
        spinnerType = findViewById(R.id.spinerType);
        ArrayList<String> securityQuestion = new ArrayList();
        securityQuestion.add("Select Type");
        securityQuestion.add("Sale");
        securityQuestion.add("Purchase");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, securityQuestion);
        spinnerType.setAdapter(adapter);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtTxtMob.getText().toString().equalsIgnoreCase("")) {
                    Utils.HitechToast(LoginScreenActivity.this, "Please enter mobile number.");
                } else if (edtTxtMob.getText().toString().trim().length() < 10) {
                    Utils.HitechToast(LoginScreenActivity.this, "Please enter valid mobile number.");
                } else if (spinnerType.getSelectedItem().toString().equalsIgnoreCase("Select Type")) {
                    Utils.HitechToast(LoginScreenActivity.this, "Please select Type.");
                } else {
                    PrefManager.write(LoginScreenActivity.this, "RegMobile",edtTxtMob.getText().toString());
                    //  startActivity(new Intent(LoginScreenActivity.this, CountrySelctionActivity.class).putExtra("userMobNumber", edtTxtMob.getText().toString()).putExtra("customerType", spinnerType.getSelectedItem().toString()));
                    startActivity(new Intent(LoginScreenActivity.this, CountrySelectActivity.class).putExtra("userMobNumber", edtTxtMob.getText().toString()).putExtra("customerType", spinnerType.getSelectedItem().toString()));
                }
            }
        });
    }




    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure, you want to close HRD app?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                PrefManager.clearAll(LoginScreenActivity.this);
                finishAffinity();
            }
        });
        builder.setNegativeButton("Cancel", null);
        builder.show();
    }


}
