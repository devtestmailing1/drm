package com.enquiry.kunalsingh.customerenquiry;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.enquiry.kunalsingh.customerenquiry.drawer.MainActivity;
import com.enquiry.kunalsingh.customerenquiry.utility.PrefManager;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;

public class SplashScreenActivity extends AppCompatActivity {
    ImageView imgLogo;
    public static int SPLASH_TIME_OUT = 3000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);


        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if(PrefManager.isLogin(SplashScreenActivity.this)){
                    startActivity(new Intent(SplashScreenActivity.this,MainActivity.class));
                    finish();
                }else {
                    startActivity(new Intent(SplashScreenActivity.this, LoginScreenActivity.class)); //LoginScreenActivity
                    // close this activity
                    finish();
                }
            }
        }, Settings.SPLASH_TIME_OUT);
    }
}