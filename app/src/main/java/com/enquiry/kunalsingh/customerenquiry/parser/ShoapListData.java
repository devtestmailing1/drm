package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kunal Singh on 06-08-2018.
 */
public class ShoapListData implements Parcelable {

    @Expose
    @SerializedName("statelist")
    public List<Statelist> statelist;

    protected ShoapListData(Parcel in) {
        statelist = new ArrayList<Statelist>();
        in.readList(statelist, ShoapListData.class.getClassLoader());
    }

    public static final Creator<ShoapListData> CREATOR = new Creator<ShoapListData>() {
        @Override
        public ShoapListData createFromParcel(Parcel in) {
            return new ShoapListData(in);
        }

        @Override
        public ShoapListData[] newArray(int size) {
            return new ShoapListData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(statelist);
    }

    public static class ShopList {
        @Expose
        @SerializedName("colour")
        public String colour;
        @Expose
        @SerializedName("localityId")
        public String localityId;
        @Expose
        @SerializedName("cityDesc")
        public String cityDesc;
        @Expose
        @SerializedName("cityId")
        public String cityId;
        @Expose
        @SerializedName("stateDesc")
        public String stateDesc;
        @Expose
        @SerializedName("stateId")
        public String stateId;
        @Expose
        @SerializedName("localityName")
        public String localityName;
        @Expose
        @SerializedName("latitude")
        public String latitude;
        @Expose
        @SerializedName("longitude")
        public String longitude;
        @Expose
        @SerializedName("shopName")
        public String shopName;
        @Expose
        @SerializedName("shopAddress")
        public String shopAddress;
        @Expose
        @SerializedName("shopId")
        public int shopId;
    }

    public static class AreaList {
        @Expose
        @SerializedName("shopList")
        public List<ShopList> shopList;
        @Expose
        @SerializedName("localityDesc")
        public String localityDesc;
        @Expose
        @SerializedName("localityId")
        public int localityId;
    }

    public static class CityList {
        @Expose
        @SerializedName("areaList")
        public List<AreaList> areaList;
        @Expose
        @SerializedName("cityDesc")
        public String cityDesc;
        @Expose
        @SerializedName("cityId")
        public int cityId;
    }

    public static class Statelist {
        @Expose
        @SerializedName("cityList")
        public List<CityList> cityList;
        @Expose
        @SerializedName("stateDesc")
        public String stateDesc;
        @Expose
        @SerializedName("stateId")
        public int stateId;
    }
}
