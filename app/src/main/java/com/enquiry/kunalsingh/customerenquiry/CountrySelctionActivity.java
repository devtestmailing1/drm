package com.enquiry.kunalsingh.customerenquiry;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Switch;

import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.otp.VerifyOtpScreen;
import com.enquiry.kunalsingh.customerenquiry.parser.DistrictListData;
import com.enquiry.kunalsingh.customerenquiry.parser.DistrictListParser;
import com.enquiry.kunalsingh.customerenquiry.parser.PunchData;
import com.enquiry.kunalsingh.customerenquiry.parser.PunchDataParser;
import com.enquiry.kunalsingh.customerenquiry.parser.StateListData;
import com.enquiry.kunalsingh.customerenquiry.parser.StateListParser;
import com.enquiry.kunalsingh.customerenquiry.parser.VerifyOtpData;
import com.enquiry.kunalsingh.customerenquiry.utility.PrefManager;
import com.enquiry.kunalsingh.customerenquiry.utility.QueryBuilder;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.enquiry.kunalsingh.customerenquiry.utility.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Kunal Singh on 04-08-2018.
 */
public class CountrySelctionActivity extends AppCompatActivity implements StateListParser.onStateListListener, DistrictListParser.onLoginListener, View.OnClickListener, PunchDataParser.onPunchListener {
    private Spinner spinnerCountry,spinnerState,spinnerCity;
    StateListData stateListData;
    DistrictListData districtListData;
    PunchData punchData;
    private static String defaultStateName;
    Map<Integer,String> map_state_values = new HashMap<Integer, String>();
    Map<Integer,String> map_dist_values = new HashMap<Integer,String>();
    private int selCountryId, selStateId, selDistId, selCityId, selLocalityId;
    private Button btnRegister;
    private String userMob, userType;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_selection);
        if (getIntent() != null){
            userMob=getIntent().getStringExtra("userMobNumber");
            userType=getIntent().getStringExtra("customerType");
        }
        initialization();
        fillSpinnerCountry();
        executeQuery();
    }


    private void initialization() {
        spinnerCountry=findViewById(R.id.spinerCountry);
       // spinnerState=findViewById(R.id.spinerState);
        spinnerCity=findViewById(R.id.spinerCity);
        btnRegister=findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);
    }


    private void fillSpinnerCountry() {
        ArrayList<String> countryList = new ArrayList<String>();
        // countryList.add("Select Country");
        countryList.add("India");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, countryList);
        spinnerCountry.setAdapter(adapter);
    }



    private void executeQuery() {
        new StateListParser().executeQuery(CountrySelctionActivity.this, QueryBuilder.getInstance().countryListApi(), QueryBuilder.getInstance().generateCountryListQuery(CountrySelctionActivity.this), CountrySelctionActivity.this, Settings.COUNTRY_LIST_ID, true);
    }

    @Override
    public void onSuccess(int id, String message, Object object) {
        List<String> stateName = new ArrayList<>();
        List<String> districtName=new ArrayList<>();
        List<String> selectedState=new ArrayList<>();
        stateName.add("Select State");
        if (id == Settings.STATE_LIST_ID) {
            if (object != null) {
                stateListData = ((StateListData) object);
                if (stateListData != null) {
                    for (int i = 0; i < stateListData.stateList.size(); i++) {
                        stateName.add(stateListData.stateList.get(i).stateDesc);
                        if(stateListData.stateList.get(i).selected!=null&&stateListData.stateList.get(i).selected.equalsIgnoreCase("SELECTED")){
                            selectedState.add(stateListData.stateList.get(i).stateDesc);
                        }
                        map_state_values.put(stateListData.stateList.get(i).stateId, stateListData.stateList.get(i).stateDesc);
                    }

                    fillStateSpinner(stateName, selectedState);
                    districtName.add("Select City");
                    fillDistrictSpinner(districtName,defaultStateName);

                }
            }
        }

        if (id == Settings.FETCH_DISTRICT_LIST_ID) {
            if (object != null) {
                districtListData = ((DistrictListData) object);
                if (districtListData != null) {
                    districtName.add("Select City");
                    // For District
                    for (int i = 0; i < districtListData.cityList.size(); i++) {
                        districtName.add(districtListData.cityList.get(i).cityDesc);
                        map_dist_values.put(districtListData.cityList.get(i).cityId, districtListData.cityList.get(i).cityDesc);
                    }
                    String defaultDistrict = "EAST";
                    fillDistrictSpinner(districtName, defaultDistrict);
                }
            }
        }

        if(id==Settings.VALIDATE_MOB_ID){
                if(message!=null) {
                    Utils.HitechToast(CountrySelctionActivity.this, message);
                    startActivity(new Intent(this, VerifyOtpScreen.class).putExtra("userMobNo", userMob).putExtra("userType", userType).putExtra("cityID", String.valueOf(selDistId)));
                }
            }
    }

    @Override
    public void onError(String error, int id, BaseController.ErrorCode code) {
      if(error!=null){
        Utils.HitechToast(CountrySelctionActivity.this, error);
    }
    }

    public void fillStateSpinner(List<String> stateNameList, List<String> defaultState){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_dropdown_item, stateNameList);
        spinnerState.setAdapter(adapter);
        //selectSpinnerValue(spinnerState,defaultState);
        for(int i=0;i<defaultState.size();i++) {
            spinnerState.setSelection(adapter.getPosition(defaultState.get(i)));
        }
        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinnerState.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select State"))) {
                    for (Map.Entry<Integer, String> stateEntry : map_state_values.entrySet()) {
                        if (stateEntry.getValue().equalsIgnoreCase(value)) {
                            selStateId = stateEntry.getKey();
                            Log.v("StateId----------", "" + stateEntry.getKey());
                            new DistrictListParser().executeQuery(CountrySelctionActivity.this, QueryBuilder.getInstance().districtListApi(), QueryBuilder.getInstance().generateDistrictListQuery(CountrySelctionActivity.this,Integer.toString(stateEntry.getKey())), CountrySelctionActivity.this, Settings.FETCH_DISTRICT_LIST_ID, true);
                        }
                    }
                }else{

                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void fillDistrictSpinner(List<String> stateNameList, String defaultDistrict){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_dropdown_item, stateNameList);
        spinnerCity.setAdapter(adapter);
        selectSpinnerValue(spinnerCity,"Select City");
        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinnerCity.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select City"))) {
                    for (Map.Entry<Integer, String> districtEntry : map_dist_values.entrySet()) {
                        if (districtEntry.getValue().equalsIgnoreCase(value)) {
                            selDistId = districtEntry.getKey();
                            Log.v("DistrictId----------", "" + districtEntry.getKey());
                            //new CityListParser().executeQuery(RegisterScreenActivity.this, QueryBuilder.getInstance().cityListApi(), QueryBuilder.getInstance().generateCityListQuery(RegisterScreenActivity.this, registeredUserData.mobileNumber, Utils.versionNumber(RegisterScreenActivity.this), Utils.getDeviceName(), Utils.deviceToken(RegisterScreenActivity.this), Integer.toString(districtEntry.getKey())), RegisterScreenActivity.this, Settings.FETCH_CITY_LIST_ID, true);
                        }
                    }
                }else{

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void selectSpinnerValue(Spinner spinner, String myString)
    {
        int index = 0;
        for(int i = 0; i < spinner.getCount(); i++){
            if(spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                spinner.setSelection(i);
                break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnRegister:
                if(spinnerState.getSelectedItem().toString().equalsIgnoreCase("Select State")){
                    Utils.HitechToast(this,"Please Select State");
                }else if(spinnerCity.getSelectedItem().toString().equalsIgnoreCase("Select City")){
                    Utils.HitechToast(this,"Please Select City");
                }else {
                    PrefManager.setKeyUserState(CountrySelctionActivity.this,spinnerState.getSelectedItem().toString());
                    PrefManager.setKeyUserCity(CountrySelctionActivity.this,spinnerCity.getSelectedItem().toString());
                    new PunchDataParser().executeQuery(this, QueryBuilder.getInstance().getValidateMobAPI(), QueryBuilder.getInstance().generateValidateMobQuery(this, userMob, userType,String.valueOf(selDistId)), this, Settings.VALIDATE_MOB_ID, true);
                    //  startActivity(new Intent(this, VerifyOtpScreen.class).putExtra("userMobNo", userMob).putExtra("userType", userType).putExtra("cityID", String.valueOf(selDistId)));
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
