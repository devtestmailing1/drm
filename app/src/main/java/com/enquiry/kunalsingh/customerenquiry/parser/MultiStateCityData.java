package com.enquiry.kunalsingh.customerenquiry.parser;

public class MultiStateCityData {

    private String title;
    private boolean selected;
    private String ID;

    private String alreadySelected;
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getAlreadySelected() {
        return alreadySelected;
    }

    public void setAlreadySelected(String alreadySelected) {
        this.alreadySelected = alreadySelected;
    }


    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }




}
