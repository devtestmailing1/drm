package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Kunal Singh on 14-05-2018.
 */
public class StateListData implements Parcelable {

    @Expose
    @SerializedName("stateList")
    public List<StateList> stateList;

    protected StateListData(Parcel in) {
    }

    public static final Creator<StateListData> CREATOR = new Creator<StateListData>() {
        @Override
        public StateListData createFromParcel(Parcel in) {
            return new StateListData(in);
        }

        @Override
        public StateListData[] newArray(int size) {
            return new StateListData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }

    public static class StateList implements Parcelable{
        @Expose
        @SerializedName("stateDesc")
        public String stateDesc;
        @Expose
        @SerializedName("stateId")
        public int stateId;
        @Expose
        @SerializedName("selected")
        public String selected;


        protected StateList(Parcel in) {
            stateDesc = in.readString();
            stateId = in.readInt();
            selected = in.readString();
        }

        public static final Creator<StateList> CREATOR = new Creator<StateList>() {
            @Override
            public StateList createFromParcel(Parcel in) {
                return new StateList(in);
            }

            @Override
            public StateList[] newArray(int size) {
                return new StateList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(stateDesc);
            parcel.writeInt(stateId);
            parcel.writeString(selected);
        }
    }
}