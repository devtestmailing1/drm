package com.enquiry.kunalsingh.customerenquiry.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.parser.ServiceHistoryData;

import java.util.List;

/**
 * Created by Kunal Singh on 06-06-2018.
 */
public class ServiceHistoryAdapter extends RecyclerView.Adapter<ServiceHistoryAdapter.ServiceHistoryHolder>{
    private List<ServiceHistoryData.ServiceHistoryList> list;
    private Activity mContext;

    public ServiceHistoryAdapter(Activity context, List<ServiceHistoryData.ServiceHistoryList> list) {
        this.mContext = context;
        this.list = list;
    }
    @NonNull
    @Override
    public ServiceHistoryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.custom_service_history, viewGroup, false);
        return new ServiceHistoryAdapter.ServiceHistoryHolder(view );
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceHistoryHolder holder, int position) {
        if(list.get(position).docDate!=null){
            holder.edtDate.setText(list.get(position).docDate);
        }

        if(list.get(position).dealer!=null){
            holder.edtDealerLocation.setText(list.get(position).dealer);
        }

        if(list.get(position).roNumber!=null){
            holder.edtJobCardNo.setText(list.get(position).roNumber);
        }

        holder.edtOdometer.setText(String.valueOf(list.get(position).odoMeter));
        holder.edtServiceType.setText(list.get(position).categoryDesc);
        holder.btnPartsValue.setText(String.valueOf(list.get(position).partBillValue));
        holder.btnLabourValue.setText(String.valueOf(list.get(position).labourBillValue));
        holder.btnTotalValue.setText(String.valueOf(list.get(position).totalBillValue));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ServiceHistoryHolder extends RecyclerView.ViewHolder{
        public EditText edtDate, edtOdometer,edtServiceType,edtJobCardNo,edtDealerLocation;
        Button btnPartsValue,btnLabourValue,btnTotalValue;
        public ServiceHistoryHolder(View itemView) {
            super(itemView);
            edtDate= itemView.findViewById(R.id.edtDate);
            edtOdometer= itemView.findViewById(R.id.edtOdometer);
            edtServiceType= itemView.findViewById(R.id.edtServiceType);
            edtJobCardNo= itemView.findViewById(R.id.edtJobCardNo);
            edtDealerLocation= itemView.findViewById(R.id.edtDealerLocation);
            btnPartsValue= itemView.findViewById(R.id.btnParts);
            btnLabourValue= itemView.findViewById(R.id.btnLabour);
            btnTotalValue= itemView.findViewById(R.id.btnTotal);

        }
    }
}
