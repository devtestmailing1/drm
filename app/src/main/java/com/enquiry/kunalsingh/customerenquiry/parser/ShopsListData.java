package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShopsListData implements Parcelable
{

    @SerializedName("shoplist")
    @Expose
    private List<Object> shoplist = null;
    public final static Parcelable.Creator<ShopsListData> CREATOR = new Creator<ShopsListData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ShopsListData createFromParcel(Parcel in) {
            return new ShopsListData(in);
        }

        public ShopsListData[] newArray(int size) {
            return (new ShopsListData[size]);
        }

    }
            ;

    protected ShopsListData(Parcel in) {
        in.readList(this.shoplist, (java.lang.Object.class.getClassLoader()));
    }

    public ShopsListData() {
    }

    public List<Object> getShoplist() {
        return shoplist;
    }

    public void setShoplist(List<Object> shoplist) {
        this.shoplist = shoplist;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(shoplist);
    }

    public int describeContents() {
        return 0;
    }

}
