package com.enquiry.kunalsingh.customerenquiry.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.CallLog;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.parser.PunchDataParser;
import com.enquiry.kunalsingh.customerenquiry.utility.PrefManager;
import com.enquiry.kunalsingh.customerenquiry.utility.QueryBuilder;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.enquiry.kunalsingh.customerenquiry.utility.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * Created by Kunal Singh on 16-08-2018.
 */
public class FeedbackActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback, View.OnClickListener, PunchDataParser.onPunchListener {
    private EditText edttoDate, edtFromDate,edtremarks, edtCallDuration;
    private Button btnSubmit;
    private Calendar calendar;
    private int year, month, day;
    private static final int REQUEST_CALL_LOG_PERMISSION = 786;
    private String userMob,shopID;
    DatePickerDialog datePickerDialog;
    int dayOfMonth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        Toolbar toolbar = findViewById(R.id.header);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("Feedback");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        btnSubmit=findViewById(R.id.btnSubmit);
        edttoDate = findViewById(R.id.edttoDate);
        edtCallDuration = findViewById(R.id.edtCallDuration);
        edtFromDate = findViewById(R.id.edtFromDate);
        edtremarks=findViewById(R.id.edtremarks);
        requestPermission();
        Intent intent = getIntent();
        userMob = intent.getStringExtra("phoneNumber");
        shopID=intent.getStringExtra("shopid");

        edttoDate.setOnClickListener(this);
        edtFromDate.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public String LastCall() {
        StringBuffer sb = new StringBuffer();
        //Cursor managedCursor = managedQuery(CallLog.Calls.CONTENT_URI, null, null, null, null);
        Cursor managedCursor = this.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, CallLog.Calls.DATE+ " DESC limit 1;" );
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        // sb.append("Call Details :");
        while (managedCursor.moveToNext()) {
            String phNumber = managedCursor.getString(number); // mobile number
            String callType = managedCursor.getString(type); // call type
            String callDate = managedCursor.getString(date); // call date
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString(duration);
            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }
            // sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- " + dir + " \nCall Date:--- " + callDayTime + " \nCall duration in sec :--- " + callDuration);
            //  sb.append("\n----------------------------------");
            sb.append("Call Duration : "+callDuration+" sec");

        }
        managedCursor.close();
        // miss_cal.setText(sb);
        //Log.e("Agil value --- ", sb.toString());
        return sb.toString();
    }




    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.READ_CALL_LOG}, REQUEST_CALL_LOG_PERMISSION);
        } else {
            edtCallDuration.setText(LastCall());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CALL_LOG_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            edtCallDuration.setText(LastCall());
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSubmit:
                if(edtCallDuration.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please Set call duration");
                }else if(edtremarks.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter remarks");
                }else if(edtFromDate.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter from date");
                }else if(edttoDate.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter to date");
                }else if(!checkDates(edtFromDate.getText().toString(), edttoDate.getText().toString())){
                    Utils.HitechToast(this,"To date should be greater than from date");
                }else {
                    new PunchDataParser().executeQuery(this, QueryBuilder.getInstance().getSubmitFeedbackAPI(), QueryBuilder.getInstance().generateSubmitFeedbackQuery(this, edtCallDuration.getText().toString(), edtremarks.getText().toString(),edttoDate.getText().toString(),edtFromDate.getText().toString(),shopID), this, Settings.SUBMIT_FEEDBACK_ID, true);
                }
                break;
            case R.id.edtFromDate:
                edtFromDate.setInputType(InputType.TYPE_NULL);
                InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                calendar = Calendar.getInstance();
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                datePickerDialog = new DatePickerDialog(FeedbackActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                edtFromDate.setText(year + "-" + (month + 1) + "-" + day);
                            }
                        }, year, month, dayOfMonth);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.setTitle("");
                datePickerDialog.show();
                break;
            case R.id.edttoDate:
                edttoDate.setInputType(InputType.TYPE_NULL);
                InputMethodManager immm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (immm != null) {
                    immm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                calendar = Calendar.getInstance();
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                datePickerDialog = new DatePickerDialog(FeedbackActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                edttoDate.setText(year + "-" + (month + 1) + "-" + day);
                            }
                        }, year, month, dayOfMonth);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.setTitle("");
                datePickerDialog.show();
                break;
            default:
                break;
        }
    }

    @Override
    public void onSuccess(int id, String message, Object object) {
        if(id==Settings.SUBMIT_FEEDBACK_ID){
            if(message!=null) {
                Utils.HitechToast(this, message);
                startActivity(new Intent(this, FetchFeedbackActivity.class).putExtra("phoneNumber",userMob).putExtra("shopid",shopID));
                finish();
            }
        }
    }

    @Override
    public void onError(String error, int id, BaseController.ErrorCode code) {
        if(error!=null) {
            Utils.HitechToast(this, error);
        }
    }


    public static boolean checkDates(String d1, String d2) {
        SimpleDateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        boolean b = false;
        try {
            b = dfDate.parse(d1).before(dfDate.parse(d2)) || dfDate.parse(d1).equals(dfDate.parse(d2));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return b;
    }
}
