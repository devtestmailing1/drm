package com.enquiry.kunalsingh.customerenquiry.parser;

import android.support.v4.app.FragmentActivity;

import com.android.volley.Request;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kunal Singh on 21-05-2018.
 */
public class FetchModelGroupFamilyModelParser extends BaseController {

    public FetchModelGroupFamilyModelParser.onFetchModelGroupFamilyModelListener listener;
    public int id;
    public String TAG_MESSAGE = "message";

    public VerifyOtpDataParser executeQuery(FragmentActivity context, String url, JSONObject json, onFetchModelGroupFamilyModelListener listener, int id, boolean toShowDialog) {
        // RequestManager.allowAllSSL();
        this.listener = listener;
        this.id = id;
        init(context, url, Request.Method.POST, json, toShowDialog);

        return null;
    }

    @Override
    public void onComplete(JSONArray response, String message) {
        switch (id) {
            case Settings.FETCH_MG_MF_BY_REGNO_ID:
                listener.onSuccess(id, message, null);
                if (response != null) {
                    try {
                        listener.onSuccess(id, "", parserUserInfo(response.getJSONObject(0)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                break;
/*
            case Settings.LOGIN_ID:
                if (response != null) {
                    try {
                        listener.onSuccess(id, "", parserUserInfo(response.getJSONObject(0)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                break;*/


        }
    }

    @Override
    public void onSuccess(JSONArray response) {

    }

    @Override
    public void onFailureData(String error, BaseController.ErrorCode code, JSONArray response) {

    }

    @Override
    public void onFailure(String error, BaseController.ErrorCode code) {
        if (error != null) {
            listener.onError(error, id, code);
        }
    }


    public interface onFetchModelGroupFamilyModelListener {
        public void onSuccess(int id, String message, Object object);
        public void onError(String error, int id, BaseController.ErrorCode code);
    }


    public FetchModelGroupFamilyModelData parserUserInfo(JSONObject object) {
        Gson gson = new Gson();
        FetchModelGroupFamilyModelData response = gson.fromJson(object.toString(), FetchModelGroupFamilyModelData.class);
        return response;
    }
}
