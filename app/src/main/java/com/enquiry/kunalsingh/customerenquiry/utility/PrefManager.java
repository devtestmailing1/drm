package com.enquiry.kunalsingh.customerenquiry.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.StringJoiner;

/**
 * PrefManager class used for save temporary data
 * Created by Kunal Singh
 * Date 01 January 2017
 */
public class PrefManager extends PreferenceActivity {

    public static final String KEY_IS_LOGIN = "islogin";
    public static final String KEY_IS_REGISTERD_VEHICLE = "isRegisterdVehicle";
    public static final String KEY_IS_LOGIN_MOBILE= "isMobile";
    public static final String KEY_IS_PARTLOCATOR = "isPartLocator";
    public static final String KEY_USER_NAME = "userName";
    public static final String KEY_USER_MOBILE_NUMBER = "userMobileNumber";
    public static final String KEY_USER_EMAIL = "userEmail";
    public static final String KEY_USER_CITY = "userCity";
    public static final String KEY_USER_STATE = "userState";
    public static final String KEY_USER_CATEGORY = "userCategory";
    public static final String KEY_ESTABLISMENT_NAME = "establismentName";
    public static final String KEY_USER_AUTH_TOKEN = "authToken";
    public static final String KEY_REGISTERED_USER = "registerdUser";
    public static final String KEY_DEALER_NAME = "dealerName";
    public static final String KEY_DEALER_ID = "dealerID";
    public static final String KEY_OTP = "otp";
    public static final String KEY_CITY_ID= "cityID";
    public static final String KEY_AUTH_TOKEN="authToken";
    public static final String KEY_SELECTED_STATES="selectedStates";
    public static final String KEY_SELECTED_CITIES="selectedCities";
    public static final String KEY_SELECTED_PRODUCTS="selectedProducts";
    public static final String KEY_SELECTED_FEBRICS="selectedFebrics";
    public static final String KEY_SELECTED_PARTY="selectedParty";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static String read(Context context, final String key, String defaultValue) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getString(key, defaultValue);
    }

    public static void write(Context context, final String key, final String value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static boolean readBoolean(Context context, final String key, final boolean defaultValue) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getBoolean(key, defaultValue);
    }


    public static void writeBoolean(Context context, final String key, final boolean value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static float readFloat(Context context, final String key, final Float defaultValue) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getFloat(key, defaultValue);
    }

    public static void writeFloat(Context context, final String key, final Float value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putFloat(key, value);
        editor.commit();
    }

    public static int readInt(Context context, final String key, final int defaultValue) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getInt(key, defaultValue);
    }

    public static void writeInt(Context context, final String key, final int value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static long readLong(Context context, final String key, final long defaultValue) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getLong(key, defaultValue);
    }

    public static void writeLong(Context context, final String key, final long value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public static void setLogin(Context context, boolean value) {
        writeBoolean(context, KEY_IS_LOGIN, value);
    }

    public static boolean isLogin(Context context) {
        return readBoolean(context, KEY_IS_LOGIN, false);
    }

    public static void setRegisterdVehicle(Context context, boolean value) {
        writeBoolean(context, KEY_IS_REGISTERD_VEHICLE, value);
    }

    public static boolean isRegisterdVehicle(Context context) {
        return readBoolean(context, KEY_IS_REGISTERD_VEHICLE, false);
    }
    public static void setRegisteredUser(Context context, String value) {
        write(context, KEY_REGISTERED_USER, value);
    }

    public static String getRegisteredUser(Context context) {
        return read(context, KEY_REGISTERED_USER, "");
    }

    public static void setLoginMobile(Context context, boolean value) {
        writeBoolean(context, KEY_IS_LOGIN_MOBILE, value);
    }

    public static boolean isLoginMobile(Context context) {
        return readBoolean(context, KEY_IS_LOGIN_MOBILE, false);
    }


    public static void setKeyIsPartlocator(Context context, boolean value) {
        writeBoolean(context, KEY_IS_PARTLOCATOR, value);
    }

    public static boolean isPartLocator(Context context) {
        return readBoolean(context, KEY_IS_PARTLOCATOR, false);
    }


    public static void setUserName(Context context, String value) {
        write(context, KEY_USER_NAME, value);
    }

    public static String getKeyUserName(Context context) {
        return read(context, KEY_USER_NAME, "");
    }


    public static void setDealerName(Context context, String value) {
        write(context, KEY_DEALER_NAME, value);
    }

    public static String getKeyDealerName(Context context) {
        return read(context, KEY_DEALER_NAME, "");
    }


    public static void setKeyDealerId(Context context, String value) {
        write(context, KEY_DEALER_ID, value);
    }

    public static String getKeyDealerId(Context context) {
        return read(context, KEY_DEALER_ID, "");
    }


    public static void setUserMobileNumber(Context context, String value) {
        write(context, KEY_USER_MOBILE_NUMBER, value);
    }

    public static String getKeyUserMobileNumber(Context context) {
        return read(context, KEY_USER_MOBILE_NUMBER, null);
    }

    public static void setKeyUserEmail(Context context, String value) {
        write(context, KEY_USER_EMAIL, value);
    }

    public static String getKeyUserEmail(Context context) {
        return read(context, KEY_USER_EMAIL, null);
    }

    public static void setKeyEstablismentName(Context context, String value) {
        write(context, KEY_ESTABLISMENT_NAME, value);
    }

    public static String getKeyEstablismentName(Context context) {
        return read(context, KEY_ESTABLISMENT_NAME, "");
    }


    public static void setKeyUserCity(Context context, String value) {
        write(context, KEY_USER_CITY, value);
    }

    public static String getKeyUserCity(Context context) {
        return read(context, KEY_USER_CITY, "");
    }


    public static void setKeyUserState(Context context, String value) {
        write(context, KEY_USER_STATE, value);
    }

    public static String getKeyUserState(Context context) {
        return read(context, KEY_USER_STATE, "");
    }

    public static void setKeyAuthToken(Context context, String value) {
        write(context, KEY_AUTH_TOKEN, value);
    }

    public static String getKeyAuthToken(Context context) {
        return read(context, KEY_AUTH_TOKEN, "");
    }


    public static void setSelectedStates(Context context, ArrayList<String> value , String key) {
        Set<String> set = new HashSet<String>();
        set.addAll(value);
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putStringSet(key, set);
        editor.commit();
    }

    public static ArrayList<String> getSelectedStates(Context context, String key) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> set = settings.getStringSet(key, null);
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.addAll(set);
        return arrayList;
    }


    public static void setSelectedCities(Context context, ArrayList<String> value, String key) {
        Set<String> set = new HashSet<String>();
        set.addAll(value);
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putStringSet(key, set);
        editor.commit();
    }

    public static ArrayList<String> getSelectedCities(Context context, String key) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> set = settings.getStringSet(key, null);
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.addAll(set);
        return arrayList;
    }

    public static void cleanPerticularPrefrence(Context context, String key){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings.edit().remove("text").apply();
    }

    public static SharedPreferences returnPrefrence(Context context){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings;
    }





    public static void setKeyCityId(Context context, String value) {
        write(context, KEY_CITY_ID, value);
    }

    public static String getKeyCityId(Context context) {
        return read(context, KEY_CITY_ID, "");
    }


    public static void setKeyUserCategory(Context context, String value) {
        write(context, KEY_USER_CATEGORY, value);
    }

    public static String getKeyUserCategory(Context context) {
        return read(context, KEY_USER_CATEGORY, "");
    }

    public static void setKeyOtp(Context context, String value) {
        write(context, KEY_OTP, value);
    }

    public static String getKeyOtp(Context context) {
        return read(context, KEY_OTP, null);
    }

    public static void clearAll(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
    }
}
