package com.enquiry.kunalsingh.customerenquiry.utility;


import android.content.Intent;

/**
 * Settings class
 * Created by Kunal Singh
 * Date 29 December 2016
 */
public class Settings {

    // todo: Splash Screen time

    public static final int SPLASH_TIME_OUT = 100;

    // todo: Main API URL

    public static final String API_SERVER =  "http://59.160.98.145:8080/HRD-APP/auth/"; //"http://125.18.53.14:8080/GREAVES-API/auth/";        // "http://59.160.98.142:8085/GREAVES-API/auth/";

   // public static final String API_SERVER = "http://192.168.16.207:8080/HRD-APP/auth/";

    //todo: Web API URL
public static final String VALIDATE_MOB_API=API_SERVER+"login";
    public static final String LOGIN_API = API_SERVER + "validateMobileNumber";
    public static final String VERIFY_OTP_API = API_SERVER + "varifyOTP";
    public static final String SHOP_DETAILS_API = API_SERVER + "getShopDetails";
    public static final String INIT_ADD_SHP_API = API_SERVER + "initAddShopDetails";
    public static final String SUBMIT_ADD_SHP_API = API_SERVER + "addUpdateShopDetails";
    public static final String CHECK_PUNCH_IN_API=API_SERVER+"hasPunchedInn";
    public static final String SEARCH_SHOP_LIST_API=API_SERVER+"searchShopList";
    public static final String PUNCH_IN_OUT_API=API_SERVER+"punchInPunchOut";
    public static final String SUBMIT_FEEDBACK_API=API_SERVER+"registerFeedback";
    public static final String FETCH_FEEDBACK_API=API_SERVER+"getFeedbackList";
    public static final String FETCH_DASHBOARD_API=API_SERVER+"dashboard";
    public static final String FETCH_DASHBOARD_DETAILS_API=API_SERVER+"dashboardDetails";
    public static final String SEARCH_SHOP_LIST=API_SERVER+"searchShopList";
    public static final String SHOP_COUNTRY_LIST=API_SERVER+"getCountryListOnNumber";
    public static final String SHOP_STATE_LIST=API_SERVER+"getStateList";
    public static final String SHOP_CITY_LIST=API_SERVER+"getCityList";
    public static final String SHOP_LOCALITY_LIST=API_SERVER+"getLocalityList";
    public static final String SHOP_MOHOLLA_LIST=API_SERVER+"getMohallaList";
    public static final String UPLOAD_IMAGE_URL=API_SERVER+"uploadMultipleFiles";







    public static final String CREATE_OFFER_API = API_SERVER + "offer/createOfferForFashionClothing";
    public static final String UPDATE_USER_API = API_SERVER + "updateUser";
    public static final String VALIDATE_SECURITY_QUESTION_API = API_SERVER + "validateSecurityQUSANS";
    public static final String FETCH_REG_NUMBER_API = API_SERVER + "fetchRegNumberFromSRVBooking";
    public static final String ADD_REGISTER_VEHICLE_API = API_SERVER + "fetchVinScreenDetail";
    public static final String  REGISTER_VEHICLE_API = API_SERVER + "saveUpdateVinDetails";
    public static final String  STATE_LIST_API = API_SERVER + "getStateList";
    public static final String  SERVICE_BOOKING_LIST_API= API_SERVER + "existingBookingList";
    public static final String  SERVICE_BOOKING_DETAILS_API= API_SERVER + "detailForServiceBooking";
    public static final String  CREATE_SERVICE_BOOKING_API= API_SERVER + "createServiceBooking";
    public static final String  FETCH_MODEL_BY_MAKE_API= API_SERVER + "fetchModelByMake";
    public static final String  FETCH_MG_MF_BY_REGNO_API= API_SERVER + "fetchRegNumberFromSRVBooking";
    public static final String  FETCH_DISTRICT_LIST_API= API_SERVER + "getCityList";
    public static final String  FETCH_CITY_LIST_API= API_SERVER + "cityList";
    public static final String  FETCH_LOCALITY_LIST_API= API_SERVER + "localityListByCityId";
    public static final String  FETCH_STATE_DIST_CITY_FROM_PIN_API= API_SERVER + "userLocalityDetailsByPinId";
    public static final String  FETCH_DEALER_FROM_DEALER_LOC_API= API_SERVER + "fetchDealerFromDealerLocatorList";
    public static final String  FETCH_SERVICE_HISTORY_API= API_SERVER + "fetchServiceHistoryList";
    public static final String  FETCH_MAINTINANCE_COST_API= API_SERVER + "fetchMaintenanceCostList";
    public static final String  FETCH_COMLAIN_API= API_SERVER + "fetchCustomerComplainList";
    public static final String  CREATE_COMLAIN_API= API_SERVER + "createCustomerComplain";
    public static final String  VALIDATE_COMLAIN_API= API_SERVER + "validateComplaintScreen";
    public static final String  USER_VEHICLE_LIST_API= API_SERVER + "fetchUserVehicleDTLList";
    public static final String  FETCH_COMLAIN_VALIDATION= API_SERVER + "fetchComplainScreenDTL";
    public static final String  FETCH_COUNTRY_LIST= API_SERVER + "getCountryListOnNumber";









    //todo: Web Services Calling ID

    public static final int LOGIN_ID = 1;
    public static final int UPDATE_USER_ID = 2;
    public static final int VERIFY_OTP_ID = 3;
    public static final int VALIDATE_SECURITY_QUESTION_ID = 4;
    public static final int FETCH_REG_NUMBER_ID = 5;
    public static final int ADD_REGISTER_VEHICLE_ID = 6;
    public static final int REGISTER_VEHICLE_ID = 7;
    public static final int STATE_LIST_ID = 8;
    public static final int SERVICE_BOOKING_LIST_ID = 9;
    public static final int SERVICE_BOOKING_DETAILS_ID = 10;
    public static final int CREATE_SERVICE_BOOKING_ID = 11;
    public static final int FETCH_MODEL_BY_MAKE_ID = 12;
    public static final int FETCH_MG_MF_BY_REGNO_ID = 13;
    public static final int FETCH_DISTRICT_LIST_ID = 14;
    public static final int FETCH_CITY_LIST_ID = 15;
    public static final int FETCH_LOCALITY_LIST_ID = 16;
    public static final int FETCH_STATE_DIST_CITY_FROM_PIN_ID = 17;
    public static final int FETCH_DEALER_FROM_DEALER_LOC_ID = 18;
    public static final int SERVICE_HISTORY_ID = 19;
    public static final int MAINTINANCE_COST_ID = 20;
    public static final int COMPLAIN_LIST_ID = 21;
    public static final int CREATE_COMPLAIN_ID = 22;
    public static final int VALIDATE_COMPLAIN_ID = 23;
    public static final int USER_VEHICLE_LIST_ID = 24;
    public static final int SHOP_DETAILS_ID= 25;
    public static final int INIT_ADD_SHP_ID= 26;
    public static final int SUBMIT_ADD_SHOP_ID= 27;
    public static final int CHECK_PUNCH_IN_ID= 28;
    public static final int PUNCH_IN_ID= 29;
    public static final int PUNCH_OUT_ID= 30;
    public static final int SEARCH_SHOP_LIST_ID= 31;
    public static final int PUNCH_IN_OUT_ID= 32;
    public static final int VALIDATE_MOB_ID=33;
    public static final int SUBMIT_FEEDBACK_ID=34;
    public static final int FETCH_FEEDBACK_ID=35;
    public static final int SHOP_LOCATION_COUNTRY=36;
    public static final int SHOP_LOCATION_STATE=37;
    public static final int SHOP_LOCATION_CITY=38;
    public static final int SHOP_LOCATION_LOCALITY=39;
    public static final int SHOP_LOCATION_MOHOLLA=40;
    public static final int DASHBOARD_ID=41;
    public static final int DASHBOARD_DETAILS_ID=42;
    public static final int IMAGE_UPLOAD = 43;
    public static final int COUNTRY_LIST_ID = 44;
   // public static final Intent ACTION_LOCATION_SOURCE_SETTINGS = ;


    public static int State_OR_CITY = 0;


    public static final String SEND_LOCATION_SERVICE=API_SERVER+"registerUserLocation";

    public static final int SEND_LOCATION_SERVICE_ID=43;

}


