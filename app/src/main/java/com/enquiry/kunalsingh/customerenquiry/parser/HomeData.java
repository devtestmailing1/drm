package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kunal Singh on 23-04-2018.
 */
public class HomeData  implements Parcelable{

    @SerializedName("announcemenyList")
    public List<AnnouncemenyList> announcemenyList;
    @SerializedName("userDashBoardMenuList")
    public List<UserDashBoardMenuList> userDashBoardMenuList;
    @SerializedName("userMenuList")
    public List<UserMenuList> userMenuList;
    @SerializedName("imeiNumber")
    public String imeiNumber;
    @SerializedName("deviceModel")
    public String deviceModel;
    @SerializedName("token")
    public String token;
    @SerializedName("city")
    public String city;
    @SerializedName("state")
    public String state;
    @SerializedName("userName")
    public String userName;
    @SerializedName("emailId")
    public String emailId;
    @SerializedName("isVinAvailable")
    public String isVinAvailable;

    protected HomeData(Parcel in) {
        announcemenyList = new ArrayList<AnnouncemenyList>();
        in.readList(announcemenyList, AnnouncemenyList.class.getClassLoader());
        userDashBoardMenuList = new ArrayList<UserDashBoardMenuList>();
        in.readList(userDashBoardMenuList, UserDashBoardMenuList.class.getClassLoader());
        userMenuList = new ArrayList<UserMenuList>();
        in.readList(userMenuList, UserMenuList.class.getClassLoader());
        imeiNumber = in.readString();
        deviceModel = in.readString();
        token = in.readString();
        city = in.readString();
        state = in.readString();
        userName=in.readString();
        emailId=in.readString();
        isVinAvailable=in.readString();
    }

    public static final Creator<HomeData> CREATOR = new Creator<HomeData>() {
        @Override
        public HomeData createFromParcel(Parcel in) {
            return new HomeData(in);
        }

        @Override
        public HomeData[] newArray(int size) {
            return new HomeData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(announcemenyList);
        parcel.writeList(userDashBoardMenuList);
        parcel.writeList(userMenuList);
        parcel.writeString(imeiNumber);
        parcel.writeString(deviceModel);
        parcel.writeString(token);
        parcel.writeString(city);
        parcel.writeString(state);
        parcel.writeString(userName);
        parcel.writeString(emailId);
        parcel.writeString(isVinAvailable);
    }

    public static class AnnouncemenyList implements Parcelable{
        @SerializedName("description")
        public String description;
        @SerializedName("title")
        public String title;
        @SerializedName("announcementId")
        public int announcementId;

        protected AnnouncemenyList(Parcel in) {
            description = in.readString();
            title = in.readString();
            announcementId = in.readInt();
        }

        public static final Creator<AnnouncemenyList> CREATOR = new Creator<AnnouncemenyList>() {
            @Override
            public AnnouncemenyList createFromParcel(Parcel in) {
                return new AnnouncemenyList(in);
            }

            @Override
            public AnnouncemenyList[] newArray(int size) {
                return new AnnouncemenyList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(description);
            parcel.writeString(title);
            parcel.writeInt(announcementId);
        }
    }

    public static class UserDashBoardMenuList implements Parcelable{
        @SerializedName("isVehicleOwned")
        public String isVehicleOwned;
        @SerializedName("isReadOnly")
        public String isReadOnly;
        @SerializedName("displayOrder")
        public int displayOrder;
        @SerializedName("menuName")
        public String menuName;
        @SerializedName("menuId")
        public int menuId;

        protected UserDashBoardMenuList(Parcel in) {
            isVehicleOwned = in.readString();
            isReadOnly = in.readString();
            displayOrder = in.readInt();
            menuName = in.readString();
            menuId = in.readInt();
        }

        public static final Creator<UserDashBoardMenuList> CREATOR = new Creator<UserDashBoardMenuList>() {
            @Override
            public UserDashBoardMenuList createFromParcel(Parcel in) {
                return new UserDashBoardMenuList(in);
            }

            @Override
            public UserDashBoardMenuList[] newArray(int size) {
                return new UserDashBoardMenuList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(isVehicleOwned);
            parcel.writeString(isReadOnly);
            parcel.writeInt(displayOrder);
            parcel.writeString(menuName);
            parcel.writeInt(menuId);
        }
    }

    public static class UserMenuList implements Parcelable{
        @SerializedName("isVehicleOwned")
        public String isVehicleOwned;
        @SerializedName("isReadOnly")
        public String isReadOnly;
        @SerializedName("displayOrder")
        public int displayOrder;
        @SerializedName("menuName")
        public String menuName;
        @SerializedName("menuId")
        public int menuId;

        protected UserMenuList(Parcel in) {
            isVehicleOwned = in.readString();
            isReadOnly = in.readString();
            displayOrder = in.readInt();
            menuName = in.readString();
            menuId = in.readInt();
        }

        public static final Creator<UserMenuList> CREATOR = new Creator<UserMenuList>() {
            @Override
            public UserMenuList createFromParcel(Parcel in) {
                return new UserMenuList(in);
            }

            @Override
            public UserMenuList[] newArray(int size) {
                return new UserMenuList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(isVehicleOwned);
            parcel.writeString(isReadOnly);
            parcel.writeInt(displayOrder);
            parcel.writeString(menuName);
            parcel.writeInt(menuId);
        }
    }
}
