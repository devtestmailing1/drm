package com.enquiry.kunalsingh.customerenquiry.interfaces;

import java.util.ArrayList;

public interface GetCities{
    public void stateIDs(ArrayList<String> arrayList);
}
