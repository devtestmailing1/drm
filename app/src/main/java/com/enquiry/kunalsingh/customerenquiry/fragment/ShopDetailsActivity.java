package com.enquiry.kunalsingh.customerenquiry.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.parser.PunchData;
import com.enquiry.kunalsingh.customerenquiry.parser.PunchDataParser;
import com.enquiry.kunalsingh.customerenquiry.parser.PunchInOutData;
import com.enquiry.kunalsingh.customerenquiry.parser.PunchInOutParser;
import com.enquiry.kunalsingh.customerenquiry.parser.ShopDetailsData;
import com.enquiry.kunalsingh.customerenquiry.parser.ShopDetailsParser;
import com.enquiry.kunalsingh.customerenquiry.utility.PrefManager;
import com.enquiry.kunalsingh.customerenquiry.utility.PunchDialog;
import com.enquiry.kunalsingh.customerenquiry.utility.QueryBuilder;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.enquiry.kunalsingh.customerenquiry.utility.Utils;
import com.getbase.floatingactionbutton.FloatingActionButton;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;


/**
 * Created by Kunal Singh on 11-08-2018.
 */
public class ShopDetailsActivity extends AppCompatActivity implements LocationResult, ShopDetailsParser.onShopDetailsListener, View.OnClickListener, PunchDataParser.onPunchListener, PunchInOutParser.onPunchInOutListener {
    String shopID;
    ShopDetailsData shopDetailsData;
    PunchData punchData;
    PunchInOutData punchInOutData;
    PunchDialog dialog = null;
    private Button btnCall, btnMap, btnPunchIn,btnPunchOut,btnFeedback;
    private TextInputLayout textinputlayout_ShopName,textinputlayout_OwnerName,textinputlayout_OwnerMob,textinputlayout_PurchaserName,textinputlayout_PurchaserNumber,textinputlayout_EmailID,textinputlayout_TelephoneNo,textinputlayout_Website,textinputlayout_AccountantName,textinputlayout_AccountantNo,textinputlayout_ProductType,textinputlayout_ProductFabric,textinputlayout_PartyType,textinputlayout_Ranking,textinputlayout_ShopAdd,textinputlayout_Mohalla,textinputlayout_Area,textinputlayout_State,textinputlayout_Country,textinputlayout_Pin,textinputlayout_Lattitude,textinputlayout_Longitude;
    private EditText edtShopName,edtOwnerName,edtOwnerMob,edtPurchaserName,edtPurchaserNumber,edtEmailID,edtTelephoneNo,edtWebsite,edtAccountantName,edtAccountantNumber,edtProductType,edtProductFabric,edtPartyType,edtRanking,edtShopAdd,edtMohalla,edtArea,edtState,edtCountry,edtPin,edtLattitude,edtLongitude;
    static String userlat="",userLang="", userAdd="";
    ImageView shopImg;
    ScrollView scrollView;



    private static final String[] INITIAL_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };

    private static final int INITIAL_REQUEST = 13;
    private MyLocation myLocation = null;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_details);
        Toolbar toolbar = findViewById(R.id.header);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("Shop Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        if (getIntent() != null) {
            shopID = getIntent().getStringExtra("ShopID");
            // Utils.HitechToast(this,shopID);
        }
        initialisation();
        myLocation = new MyLocation();
        boolean networkPresent = myLocation.getLocation(this, ShopDetailsActivity.this);
        if (!networkPresent) {
            showSettingsAlert();
        }

        new ShopDetailsParser().executeQuery(this, QueryBuilder.getInstance().getShopDetailsAPI(), QueryBuilder.getInstance().generateShopDetailsQuery(this, PrefManager.getKeyAuthToken(this).trim(), shopID), this, Settings.SHOP_DETAILS_ID, true);
        new PunchDataParser().executeQuery(this, QueryBuilder.getInstance().getCheckPunchAPI(), QueryBuilder.getInstance().generateCheckPunchInAPIQuery(this, PrefManager.getKeyAuthToken(this).trim(), "",shopID), this, Settings.CHECK_PUNCH_IN_ID, true);
    }

    private void initialisation() {
        FloatingActionButton floatingModifyShop = findViewById(R.id.FloatingModifyShop);
        textinputlayout_ShopName=findViewById(R.id.textinputlayout_ShopName);
        textinputlayout_OwnerName=findViewById(R.id.textinputlayout_OwnerName);
        textinputlayout_OwnerMob=findViewById(R.id.textinputlayout_OwnerMob);
        textinputlayout_PurchaserName=findViewById(R.id.textinputlayout_PurchaserName);
        textinputlayout_PurchaserNumber=findViewById(R.id.textinputlayout_PurchaserNumber);
        textinputlayout_EmailID=findViewById(R.id.textinputlayout_EmailID);
        textinputlayout_TelephoneNo=findViewById(R.id.textinputlayout_TelephoneNo);
        textinputlayout_Website=findViewById(R.id.textinputlayout_Website);
        textinputlayout_AccountantName=findViewById(R.id.textinputlayout_AccountantName);
        textinputlayout_AccountantNo=findViewById(R.id.textinputlayout_AccountantNo);
        textinputlayout_ProductType=findViewById(R.id.textinputlayout_ProductType);
        textinputlayout_ProductFabric=findViewById(R.id.textinputlayout_ProductFabric);
        textinputlayout_PartyType=findViewById(R.id.textinputlayout_PartyType);
        textinputlayout_Ranking=findViewById(R.id.textinputlayout_Ranking);
        textinputlayout_ShopAdd=findViewById(R.id.textinputlayout_ShopAdd);
        textinputlayout_Mohalla=findViewById(R.id.textinputlayout_Mohalla);
        textinputlayout_Area=findViewById(R.id.textinputlayout_Area);
        textinputlayout_State=findViewById(R.id.textinputlayout_State);
        textinputlayout_Country=findViewById(R.id.textinputlayout_Country);
        textinputlayout_Pin=findViewById(R.id.textinputlayout_Pin);
        textinputlayout_Lattitude=findViewById(R.id.textinputlayout_Lattitude);
        textinputlayout_Longitude=findViewById(R.id.textinputlayout_Longitude);

        edtShopName=findViewById(R.id.edtShopName);
        edtOwnerName=findViewById(R.id.edtOwnerName);
        edtOwnerMob=findViewById(R.id.edtOwnerMob);
        edtPurchaserName=findViewById(R.id.edtPurchaserName);
        edtPurchaserNumber=findViewById(R.id.edtPurchaserNumber);
        edtEmailID=findViewById(R.id.edtEmailID);
        edtTelephoneNo=findViewById(R.id.edtTelephoneNo);
        edtWebsite=findViewById(R.id.edtWebsite);
        edtAccountantName=findViewById(R.id.edtAccountantName);
        edtAccountantNumber=findViewById(R.id.edtAccountantNumber);
        edtProductType=findViewById(R.id.edtProductType);
        edtProductFabric=findViewById(R.id.edtProductFabric);
        edtPartyType=findViewById(R.id.edtPartyType);
        edtRanking=findViewById(R.id.edtRanking);
        edtShopAdd=findViewById(R.id.edtShopAdd);
        edtMohalla=findViewById(R.id.edtMohalla);
        edtArea=findViewById(R.id.edtArea);
        edtState=findViewById(R.id.edtState);
        edtCountry=findViewById(R.id.edtCountry);
        edtPin=findViewById(R.id.edtPin);
        edtLattitude=findViewById(R.id.edtLattitude);
        edtLongitude=findViewById(R.id.edtLongitude);
        shopImg = findViewById(R.id.shop_img);
        scrollView = findViewById(R.id.scroll_view);

        btnCall=findViewById(R.id.btnCall);
        btnMap=findViewById(R.id.btnMap);
        btnPunchIn=findViewById(R.id.btnPunchIn);
        btnPunchOut=findViewById(R.id.btnPunchOut);
        btnFeedback=findViewById(R.id.btnFeedback);

        btnCall.setOnClickListener(this);
        btnMap.setOnClickListener(this);
        btnPunchIn.setOnClickListener(this);
        btnPunchOut.setOnClickListener(this);
        btnFeedback.setOnClickListener(this);
        edtOwnerMob.setOnClickListener(this);
        edtPurchaserNumber.setOnClickListener(this);
        edtTelephoneNo.setOnClickListener(this);
        edtAccountantNumber.setOnClickListener(this);

        floatingModifyShop.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSuccess(int id, String message, Object object) {
        if(id==Settings.SHOP_DETAILS_ID) {
            if (object != null) {
                shopDetailsData = ((ShopDetailsData) object);
                if (shopDetailsData.shopName != null&&(!shopDetailsData.shopName.equalsIgnoreCase(""))) {
                    edtShopName.setText(shopDetailsData.shopName);
                } else {
                    textinputlayout_ShopName.setVisibility(View.GONE);
                }

                if (shopDetailsData.shopOwnerName != null&&(!shopDetailsData.shopOwnerName.equalsIgnoreCase(""))) {
                    edtOwnerName.setText(shopDetailsData.shopOwnerName);
                } else {
                    textinputlayout_OwnerName.setVisibility(View.GONE);
                }

                if (shopDetailsData.shopOwnerMobileNo != null) {
                    edtOwnerMob.setText(shopDetailsData.shopOwnerMobileNo);
                } else {
                    textinputlayout_OwnerMob.setVisibility(View.GONE);
                }

                if (shopDetailsData.purchaserName != null) {
                    edtPurchaserName.setText(shopDetailsData.purchaserName);
                } else {
                    textinputlayout_PurchaserName.setVisibility(View.GONE);
                }

                if (shopDetailsData.purchaserMobile != null) {
                    edtPurchaserNumber.setText(shopDetailsData.purchaserMobile);
                } else {
                    textinputlayout_PurchaserNumber.setVisibility(View.GONE);
                }

                if (shopDetailsData.email!= null&&(!shopDetailsData.email.equalsIgnoreCase(""))) {
                    edtEmailID.setText(shopDetailsData.email);
                } else {
                    textinputlayout_EmailID.setVisibility(View.GONE);
                }

                if (shopDetailsData.telephone != null&&(!shopDetailsData.telephone.equalsIgnoreCase(""))) {
                    edtTelephoneNo.setText(shopDetailsData.telephone);
                } else {
                    textinputlayout_TelephoneNo.setVisibility(View.GONE);
                }

                if (shopDetailsData.website != null&&(!shopDetailsData.website.equalsIgnoreCase(""))){
                    edtWebsite.setText(shopDetailsData.website);
                } else {
                    textinputlayout_Website.setVisibility(View.GONE);
                }

                if (shopDetailsData.accountName != null&&(!shopDetailsData.accountName.equalsIgnoreCase(""))) {
                    edtAccountantName.setText(shopDetailsData.accountName);
                } else {
                    textinputlayout_AccountantName.setVisibility(View.GONE);
                }

                if (shopDetailsData.accountNumber != null&&(!shopDetailsData.accountNumber.equalsIgnoreCase(""))) {
                    edtAccountantNumber.setText(shopDetailsData.accountNumber);
                } else {
                    textinputlayout_AccountantNo.setVisibility(View.GONE);
                }


                if (shopDetailsData.shopName != null&&(!shopDetailsData.shopName.equalsIgnoreCase(""))) {
                    edtShopName.setText(shopDetailsData.shopName);
                } else {
                    textinputlayout_ShopName.setVisibility(View.GONE);
                }


                if (shopDetailsData.shopAddress != null&&(!shopDetailsData.shopAddress.equalsIgnoreCase(""))){
                    edtShopAdd.setText(shopDetailsData.shopAddress);
                } else {
                    textinputlayout_ShopAdd.setVisibility(View.GONE);
                }


                if (shopDetailsData.mohallaDesc != null&&(!shopDetailsData.mohallaDesc.equalsIgnoreCase(""))) {
                    edtMohalla.setText(shopDetailsData.mohallaDesc);
                } else {
                    textinputlayout_Mohalla.setVisibility(View.GONE);
                }


                if (shopDetailsData.areaDesc != null&&(!shopDetailsData.areaDesc.equalsIgnoreCase(""))) {
                    edtArea.setText(shopDetailsData.areaDesc);
                } else {
                    textinputlayout_Area.setVisibility(View.GONE);
                }


                if (shopDetailsData.stateDesc != null&&(!shopDetailsData.stateDesc.equalsIgnoreCase(""))) {
                    edtState.setText(shopDetailsData.stateDesc);
                } else {
                    textinputlayout_State.setVisibility(View.GONE);
                }

                if (shopDetailsData.countryDesc != null&&(!shopDetailsData.countryDesc.equalsIgnoreCase(""))) {
                    edtCountry.setText(shopDetailsData.countryDesc);
                } else {
                    textinputlayout_Country.setVisibility(View.GONE);
                }


                if (shopDetailsData.pin != null&&(!shopDetailsData.pin.equalsIgnoreCase(""))) {
                    edtPin.setText(shopDetailsData.pin);
                } else {
                    textinputlayout_Pin.setVisibility(View.GONE);
                }

                if (shopDetailsData.productName != null&&(!shopDetailsData.productName.equalsIgnoreCase(""))) {
                    edtProductType.setText(shopDetailsData.productName);
                } else {
                    textinputlayout_ProductType.setVisibility(View.GONE);
                }

                if (shopDetailsData.fabricName != null&&(!shopDetailsData.fabricName.equalsIgnoreCase(""))) {
                    edtProductFabric.setText(shopDetailsData.fabricName);
                } else {
                    textinputlayout_ProductFabric.setVisibility(View.GONE);
                }

                if (shopDetailsData.partyTypeName != null&&(!shopDetailsData.partyTypeName.equalsIgnoreCase(""))) {
                    edtPartyType.setText(shopDetailsData.partyTypeName);
                } else {
                    textinputlayout_PartyType.setVisibility(View.GONE);
                }

                if (shopDetailsData.ranking != null&&(!shopDetailsData.ranking.equalsIgnoreCase(""))) {
                    edtRanking.setText(shopDetailsData.ranking);
                } else {
                    textinputlayout_Ranking.setVisibility(View.GONE);
                }

                if (shopDetailsData.latitude != null) {
                    edtLattitude.setText(shopDetailsData.latitude);
                } else {
                    textinputlayout_Lattitude.setVisibility(View.GONE);
                }


                if (shopDetailsData.longitude != null) {
                    edtLongitude.setText(shopDetailsData.longitude);
                } else {
                    textinputlayout_Longitude.setVisibility(View.GONE);
                }

                if (shopDetailsData.imagePath1!=null){
                    byte[] decodedString = Base64.decode(shopDetailsData.imagePath1, Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    shopImg.setImageBitmap(decodedByte);
                   // new DownloadImageTask(shopImg).execute(shopDetailsData.imagePath1);

                    shopImg.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (shopImg.getLayoutParams().width == 500){
                                shopImg.getLayoutParams().height=100;
                                shopImg.getLayoutParams().width = 100;
                                shopImg.requestLayout();
                            }else {
                                shopImg.getLayoutParams().height = 500;
                                shopImg.getLayoutParams().width = 500;
                                shopImg.requestLayout();
                            }

                            scrollView.post(new Runnable() {
                                @Override
                                public void run() {
                                    scrollView.fullScroll(View.FOCUS_DOWN);
                                }
                            });

                           // scrollView.smoothScrollTo(0,scrollView.getHeight());
                        }
                    });

                }else {
                    shopImg.setVisibility(View.GONE);
                }

            }
        }

        if(id==Settings.CHECK_PUNCH_IN_ID){
            if(object!=null){
                if (dialog != null) dialog.dismiss();
                punchData = ((PunchData) object);
                if(punchData.punchDetails!=null&&punchData.punchDetails.size()>0){
                    // Utils.HitechToast(this,punchData.punchDetails.get(0).punchInTime);
                    btnPunchIn.setText("IN"+"\n"+punchData.punchDetails.get(0).punchInTime);
                    btnPunchIn.setClickable(false);
                    btnPunchIn.setBackgroundColor(Color.parseColor("#d6d6d6"));
                    btnPunchIn.setTextColor(Color.parseColor("#609ddc"));

                    btnPunchOut.setClickable(true);
                    btnPunchOut.setBackgroundColor(Color.parseColor("#609ddc"));
                    btnPunchOut.setTextColor(Color.parseColor("#FFFFFF"));
                }else{
                    btnPunchOut.setClickable(false);
                    btnPunchOut.setBackgroundColor(Color.parseColor("#d6d6d6"));
                    btnPunchOut.setTextColor(Color.parseColor("#609ddc"));
                    btnPunchIn.setClickable(true);
                    btnPunchIn.setBackgroundColor(Color.parseColor("#609ddc"));
                    btnPunchIn.setTextColor(Color.parseColor("#FFFFFF"));
                }
            }else{
                btnPunchOut.setClickable(false);
                btnPunchOut.setBackgroundColor(Color.parseColor("#d6d6d6"));
                btnPunchOut.setTextColor(Color.parseColor("#609ddc"));
                btnPunchIn.setClickable(true);
                btnPunchIn.setBackgroundColor(Color.parseColor("#609ddc"));
                btnPunchIn.setTextColor(Color.parseColor("#FFFFFF"));}
        }

        if(id==Settings.PUNCH_IN_ID){
            if (dialog != null) dialog.dismiss();
            if(object!=null) {
                punchInOutData = ((PunchInOutData) object);
                if (punchInOutData.punchDetails != null && punchInOutData.punchDetails.size() > 0) {
                    // Utils.HitechToast(this,punchData.punchDetails.get(0).punchInTime);
                    btnPunchIn.setText("IN" + "\n" + punchInOutData.punchDetails.get(0).punchInTime);
                    btnPunchIn.setClickable(false);
                    btnPunchIn.setBackgroundColor(Color.parseColor("#d6d6d6"));
                    btnPunchIn.setTextColor(Color.parseColor("#609ddc"));
                    btnPunchOut.setClickable(true);
                    btnPunchOut.setBackgroundColor(Color.parseColor("#609ddc"));
                    btnPunchOut.setTextColor(Color.parseColor("#FFFFFF"));
                }else{
                    btnPunchOut.setClickable(false);
                    btnPunchOut.setBackgroundColor(Color.parseColor("#d6d6d6"));
                    btnPunchOut.setTextColor(Color.parseColor("#609ddc"));
                    btnPunchIn.setClickable(true);
                    btnPunchIn.setBackgroundColor(Color.parseColor("#609ddc"));
                    btnPunchIn.setTextColor(Color.parseColor("#FFFFFF"));
                }
            }

        }

        if(id==Settings.PUNCH_OUT_ID){
            if (dialog != null) dialog.dismiss();
            if(object!=null) {
                punchInOutData = ((PunchInOutData) object);
                if (punchInOutData.punchDetails != null && punchInOutData.punchDetails.size() > 0) {
                    // Utils.HitechToast(this,punchData.punchDetails.get(0).punchInTime);
                    btnPunchOut.setText("Out" + "\n" + punchInOutData.punchDetails.get(0).punchOutTime);
                    btnPunchIn.setClickable(false);
                    btnPunchIn.setBackgroundColor(Color.parseColor("#d6d6d6"));
                    btnPunchIn.setTextColor(Color.parseColor("#609ddc"));
                    btnPunchOut.setClickable(true);
                    btnPunchOut.setBackgroundColor(Color.parseColor("#609ddc"));
                    btnPunchOut.setTextColor(Color.parseColor("#FFFFFF"));
                }else{
                    btnPunchOut.setClickable(false);
                    btnPunchOut.setBackgroundColor(Color.parseColor("#d6d6d6"));
                    btnPunchOut.setTextColor(Color.parseColor("#609ddc"));
                    btnPunchIn.setClickable(true);
                    btnPunchIn.setBackgroundColor(Color.parseColor("#609ddc"));
                    btnPunchIn.setTextColor(Color.parseColor("#FFFFFF"));
                }
            }
        }

    }

    @Override
    public void onError(String error, int id, BaseController.ErrorCode code) {
        if(error!=null){
            Utils.HitechToast(this,error);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnPunchIn:
                punch("Punch-In");
                break;
            case R.id.btnPunchOut:
                punch("Punch-Out");
                break;
            case R.id.btnCall:
                if(!edtOwnerMob.getText().toString().equalsIgnoreCase("")){
                    Utils.call(this,edtOwnerMob.getText().toString().trim());
                }else{
                    Utils.HitechToast(this,"Owner number is not available");
                }
                break;
            case R.id.btnMap:
                startActivity(new Intent(this,TrackShop1.class).putExtra("shopLattitude",edtLattitude.getText().toString().trim()).putExtra("shopLongitude",edtLongitude.getText().toString().trim()));
                break;

            case R.id.btnFeedback:
                startActivity(new Intent(this,FetchFeedbackActivity.class).putExtra("phoneNumber",edtOwnerMob.getText().toString()).putExtra("shopid",shopID));
                break;

            case R.id.edtOwnerMob:
                if(!edtOwnerMob.getText().toString().equalsIgnoreCase("")){
                    Utils.call(this,edtOwnerMob.getText().toString().trim());
                }else{
                    Utils.HitechToast(this,"Owner number is not available");
                }
                break;

            case R.id.edtPurchaserNumber:
                if(!edtPurchaserNumber.getText().toString().equalsIgnoreCase("")){
                    Utils.call(this,edtPurchaserNumber.getText().toString().trim());
                }else{
                    Utils.HitechToast(this,"Purchaser number is not available");
                }
                break;

            case R.id.edtTelephoneNo:
                if(!edtTelephoneNo.getText().toString().equalsIgnoreCase("")){
                    Utils.call(this,edtTelephoneNo.getText().toString().trim());
                }else{
                    Utils.HitechToast(this,"Telephone number is not available");
                }
                break;

            case R.id.edtAccountantNumber:
                if(!edtAccountantNumber.getText().toString().equalsIgnoreCase("")){
                    Utils.call(this,edtAccountantNumber.getText().toString().trim());
                }else{
                    Utils.HitechToast(this,"Accountant number is not available");
                }
                break;

            case R.id.FloatingModifyShop:
                Bundle bundle = new Bundle();
                bundle.putParcelable("shopDetailsData", shopDetailsData);
                startActivity(new Intent(ShopDetailsActivity.this, ModifyActivity.class).putExtras(bundle));

                break;
        }
    }

    public void punch(final String dialogHeader) {
        dialog = new PunchDialog(this, dialogHeader, userAdd,
                null, true, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(dialogHeader.equalsIgnoreCase("Punch-In")) {
                    new PunchInOutParser().executeQuery(ShopDetailsActivity.this, QueryBuilder.getInstance().getPunchInOutAPI(), QueryBuilder.getInstance().generatePunchInOutAPIQuery(ShopDetailsActivity.this, PrefManager.getKeyAuthToken(ShopDetailsActivity.this).trim(), "", shopID, userlat, userLang, "PunchIn"), ShopDetailsActivity.this, Settings.PUNCH_IN_ID, true);
                }else{
                    new PunchInOutParser().executeQuery(ShopDetailsActivity.this, QueryBuilder.getInstance().getPunchInOutAPI(), QueryBuilder.getInstance().generatePunchInOutAPIQuery(ShopDetailsActivity.this, PrefManager.getKeyAuthToken(ShopDetailsActivity.this).trim(), "", shopID, userlat, userLang, "PunchOut"), ShopDetailsActivity.this, Settings.PUNCH_OUT_ID, true);
                }
            }
        });
        dialog.show();

    }


    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                ShopDetailsActivity.this);
        alertDialog.setTitle("SETTINGS");
        alertDialog.setMessage("Enable Location Provider! Go to settings menu?");
        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        ShopDetailsActivity.this.startActivity(intent);
                    }
                });
        /*alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });*/
        alertDialog.setCancelable(false);
        alertDialog.show();
    }


    @Override
    public void gotLocation(Location location) {
        final double latitude = location.getLatitude();
        final double longitude = location.getLongitude();
        final String result = "Latitude: " + location.getLatitude() +
                " Longitude: " + location.getLongitude();
        userlat=String.valueOf(location.getLatitude());
        userLang=String.valueOf(location.getLongitude());
        // Utils.HitechToast(getApplicationContext(),result);

        ShopDetailsActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                // tvAddress.setText(result);
                LocationAddress locationAddress = new LocationAddress();
                locationAddress.getAddressFromLocation(latitude, longitude,
                        getApplicationContext(), new GeocoderHandler());
            }
        });
    }


    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
            //  tvAddress.setText(locationAddress);
            userAdd=locationAddress;
           // Utils.HitechToast(getApplicationContext(),userAdd);
        }
    }


    /*private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }*/





}
