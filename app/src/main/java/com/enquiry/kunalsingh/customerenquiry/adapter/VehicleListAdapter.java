package com.enquiry.kunalsingh.customerenquiry.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.controller.AppController;
import com.enquiry.kunalsingh.customerenquiry.parser.VehicleListData;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Kunal Singh on 25-06-2018.
 */
public class VehicleListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<VehicleListData.UserVehicleDTLList> list;
    ImageLoader imageLoader;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    public VehicleListAdapter(Context context, List<VehicleListData.UserVehicleDTLList> list) {
        this.mContext = context;
        this.list = list;
        imageLoader = AppController.getInstance().getImageLoader();
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
    /*    RecyclerView.ViewHolder vh = null;
        if (viewType == VIEW_ITEM) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_vehicle_view_item, viewGroup, false);
            vh = new VehicleListHolder(view);

        }
        return vh;*/


        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.custom_vehicle_view_item, viewGroup, false);
        return new VehicleListAdapter.VehicleListHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder1, int position) {
        final VehicleListHolder holder = (VehicleListHolder) holder1;

        if (list.get(position).registrationNo != null) {
            holder.edtRegNo.setText(list.get(position).registrationNo);
        } else {
            holder.edtRegNo.setText("");

        }

        if (list.get(position).modelGropDesc != null) {
            holder.edtModelGroup.setText(list.get(position).modelGropDesc);
        } else {
            holder.edtModelGroup.setText("");

        }

        if (list.get(position).modelFamilyDesc != null) {
            holder.edtModelFamily.setText(list.get(position).modelFamilyDesc);
        } else {
            holder.edtModelFamily.setText("");

        }


        if (list.get(position).modelDesc != null) {
            holder.edtModel.setText(list.get(position).modelDesc);
        } else {
            holder.edtModel.setText("");

        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VehicleListHolder extends RecyclerView.ViewHolder{
        protected EditText edtRegNo, edtModelFamily, edtModelGroup,edtModel;

        public VehicleListHolder(View itemView) {
            super(itemView);
            this.edtRegNo =  itemView.findViewById(R.id.edtRegNo);
            this.edtModelFamily =  itemView.findViewById(R.id.edtModelFamily);
            this.edtModelGroup = itemView.findViewById(R.id.edtModelGroup);
            this.edtModel = itemView.findViewById(R.id.edtModel);
           // this.hide = (TextView) itemView.findViewById(R.id.hide);

        }
    }
}
