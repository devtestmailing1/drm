package com.enquiry.kunalsingh.customerenquiry.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.parser.ShoapListData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kunal Singh on 13-06-2018.
 */
public class ComplainAdapter extends RecyclerView.Adapter<ComplainAdapter.ComplainHolder>{
    private  List<ShoapListData.ShopList> list;
    private Context mContext;
    protected ItemListener mListener;
    public ComplainAdapter(Context context, List<ShoapListData.ShopList> list, ItemListener itemListener) {
        this.mContext = context;
        this.list = list;
        mListener=itemListener;
    }

    @Override
    public void onBindViewHolder(@NonNull ComplainAdapter.ComplainHolder holder, final int position) {
        holder.top_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onItemClick(list.get(position).shopId);
            }
        });
        if(list.get(position).shopAddress!=null) {
            holder.tvShoapName.setText(list.get(position).shopName);
        }else{
            holder.tvShoapName.setVisibility(View.GONE);
        }


        if(list.get(position).shopAddress!=null){
            holder.tvAddress.setText(list.get(position).shopAddress);
        }else {
            holder.tvAddress.setVisibility(View.GONE);
        }
    }

    @NonNull
    @Override
    public ComplainAdapter.ComplainHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.custom_shoap_list, viewGroup, false);
        return new ComplainAdapter.ComplainHolder(view );
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    class ComplainHolder extends RecyclerView.ViewHolder{
        TextView tvShoapName,tvAddress;
        LinearLayout top_layout;
        ComplainHolder(final View itemView) {
            super(itemView);
            tvShoapName= itemView.findViewById(R.id.tvShoapName);
            tvAddress= itemView.findViewById(R.id.tvAddress);
            top_layout=itemView.findViewById(R.id.top_layout);

        }


    }
    public interface ItemListener {
        void onItemClick(int shopId);
    }


}
