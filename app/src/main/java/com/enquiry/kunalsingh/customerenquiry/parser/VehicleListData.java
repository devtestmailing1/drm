package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kunal Singh on 25-06-2018.
 */
public class VehicleListData implements Parcelable{

    @Expose
    @SerializedName("userVehicleDTLList")
    public List<UserVehicleDTLList> userVehicleDTLList;
    @Expose
    @SerializedName("imeiNumber")
    public String imeiNumber;
    @Expose
    @SerializedName("deviceModel")
    public String deviceModel;
    @Expose
    @SerializedName("city")
    public String city;
    @Expose
    @SerializedName("state")
    public String state;
    @Expose
    @SerializedName("emailId")
    public String emailId;
    @Expose
    @SerializedName("mobileNumber")
    public String mobileNumber;
    @Expose
    @SerializedName("customerId")
    public int customerId;
    @Expose
    @SerializedName("userName")
    public String userName;

    protected VehicleListData(Parcel in) {
        userVehicleDTLList = new ArrayList<UserVehicleDTLList>();
        in.readList(userVehicleDTLList, UserVehicleDTLList.class.getClassLoader());
        imeiNumber = in.readString();
        deviceModel = in.readString();
        city = in.readString();
        state = in.readString();
        emailId = in.readString();
        mobileNumber = in.readString();
        customerId = in.readInt();
        userName = in.readString();
    }

    public static final Creator<VehicleListData> CREATOR = new Creator<VehicleListData>() {
        @Override
        public VehicleListData createFromParcel(Parcel in) {
            return new VehicleListData(in);
        }

        @Override
        public VehicleListData[] newArray(int size) {
            return new VehicleListData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(userVehicleDTLList);
        parcel.writeString(imeiNumber);
        parcel.writeString(deviceModel);
        parcel.writeString(city);
        parcel.writeString(state);
        parcel.writeString(emailId);
        parcel.writeString(mobileNumber);
        parcel.writeInt(customerId);
        parcel.writeString(userName);
    }

    public static class UserVehicleDTLList implements Parcelable{
        @Expose
        @SerializedName("createdBy")
        public String createdBy;
        @Expose
        @SerializedName("createdDate")
        public String createdDate;
        @Expose
        @SerializedName("isActive")
        public boolean isActive;
        @Expose
        @SerializedName("chassisNo")
        public String chassisNo;
        @Expose
        @SerializedName("registrationNo")
        public String registrationNo;
        @Expose
        @SerializedName("vinId")
        public int vinId;
        @Expose
        @SerializedName("modelCode")
        public String modelCode;
        @Expose
        @SerializedName("modelDesc")
        public String modelDesc;
        @Expose
        @SerializedName("modelId")
        public int modelId;
        @Expose
        @SerializedName("modelFamilyDesc")
        public String modelFamilyDesc;
        @Expose
        @SerializedName("modelFamilyCode")
        public String modelFamilyCode;
        @Expose
        @SerializedName("modelGropDesc")
        public String modelGropDesc;
        @Expose
        @SerializedName("modelGroupCode")
        public String modelGroupCode;
        @Expose
        @SerializedName("modelGroupId")
        public int modelGroupId;
        @Expose
        @SerializedName("modelFamilyId")
        public int modelFamilyId;
        @Expose
        @SerializedName("customerId")
        public int customerId;
        @Expose
        @SerializedName("appUserId")
        public int appUserId;
        @Expose
        @SerializedName("appVehicleId")
        public int appVehicleId;

        protected UserVehicleDTLList(Parcel in) {
            createdBy = in.readString();
            createdDate = in.readString();
            isActive = in.readByte() != 0;
            chassisNo = in.readString();
            registrationNo = in.readString();
            vinId = in.readInt();
            modelCode = in.readString();
            modelDesc = in.readString();
            modelId = in.readInt();
            modelFamilyDesc = in.readString();
            modelFamilyCode = in.readString();
            modelGropDesc = in.readString();
            modelGroupCode = in.readString();
            modelGroupId = in.readInt();
            modelFamilyId = in.readInt();
            customerId = in.readInt();
            appUserId = in.readInt();
            appVehicleId = in.readInt();
        }

        public static final Creator<UserVehicleDTLList> CREATOR = new Creator<UserVehicleDTLList>() {
            @Override
            public UserVehicleDTLList createFromParcel(Parcel in) {
                return new UserVehicleDTLList(in);
            }

            @Override
            public UserVehicleDTLList[] newArray(int size) {
                return new UserVehicleDTLList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(createdBy);
            parcel.writeString(createdDate);
            parcel.writeByte((byte) (isActive ? 1 : 0));
            parcel.writeString(chassisNo);
            parcel.writeString(registrationNo);
            parcel.writeInt(vinId);
            parcel.writeString(modelCode);
            parcel.writeString(modelDesc);
            parcel.writeInt(modelId);
            parcel.writeString(modelFamilyDesc);
            parcel.writeString(modelFamilyCode);
            parcel.writeString(modelGropDesc);
            parcel.writeString(modelGroupCode);
            parcel.writeInt(modelGroupId);
            parcel.writeInt(modelFamilyId);
            parcel.writeInt(customerId);
            parcel.writeInt(appUserId);
            parcel.writeInt(appVehicleId);
        }
    }
}
