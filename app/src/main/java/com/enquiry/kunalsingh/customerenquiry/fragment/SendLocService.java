package com.enquiry.kunalsingh.customerenquiry.fragment;



import android.Manifest;

import android.app.Activity;

import android.app.Service;

import android.content.Context;

import android.content.Intent;

import android.content.pm.PackageManager;

import android.location.Location;

import android.location.LocationListener;

import android.location.LocationManager;

import android.media.MediaPlayer;

import android.os.Bundle;

import android.os.IBinder;

import android.provider.Settings;

import android.support.annotation.Nullable;

import android.support.v4.app.ActivityCompat;

import android.support.v4.content.ContextCompat;

import android.util.Log;

import android.widget.Toast;

import com.enquiry.kunalsingh.customerenquiry.R;

/**

 * Created by Kunal Singh on 27-09-2018.

 */

public class SendLocService extends Service

{

    private static final String TAG = "GPSTEST";

    private LocationManager mLocationManager = null;

    private static final int LOCATION_INTERVAL = 1000;

    private static final float LOCATION_DISTANCE = 10f;

    private class LocationListener implements android.location.LocationListener

    {

        Location mLastLocation;

        public LocationListener(String provider)

        {

            Log.e(TAG, "LocationListener " + provider);

            mLastLocation = new Location(provider);

        }

        @Override

        public void onLocationChanged(Location location)

        {

            Log.e(TAG, "onLocationChanged: " + location);

            mLastLocation.set(location);

        }

        @Override

        public void onProviderDisabled(String provider)

        {

            Log.e(TAG, "onProviderDisabled: " + provider);

        }

        @Override

        public void onProviderEnabled(String provider)

        {

            Log.e(TAG, "onProviderEnabled: " + provider);

        }

        @Override

        public void onStatusChanged(String provider, int status, Bundle extras)

        {

            Log.e(TAG, "onStatusChanged: " + provider);

        }

    }

    SendLocService.LocationListener[] mLocationListeners = new SendLocService.LocationListener[] {

            new SendLocService.LocationListener(LocationManager.GPS_PROVIDER),

            new SendLocService.LocationListener(LocationManager.NETWORK_PROVIDER)

    };

    @Override

    public IBinder onBind(Intent arg0)

    {

        return null;

    }

    @Override

    public int onStartCommand(Intent intent, int flags, int startId)

    {

        Log.e(TAG, "onStartCommand");

        super.onStartCommand(intent, flags, startId);

        return START_STICKY;

    }

    @Override

    public void onCreate()

    {

        Log.e(TAG, "onCreate");

        initializeLocationManager();

        try {

            mLocationManager.requestLocationUpdates(

                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,

                    mLocationListeners[1]);

        } catch (java.lang.SecurityException ex) {

            Log.i(TAG, "fail to request location update, ignore", ex);

        } catch (IllegalArgumentException ex) {

            Log.d(TAG, "network provider does not exist, " + ex.getMessage());

        }

        try {

            mLocationManager.requestLocationUpdates(

                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,

                    mLocationListeners[0]);

        } catch (java.lang.SecurityException ex) {

            Log.i(TAG, "fail to request location update, ignore", ex);

        } catch (IllegalArgumentException ex) {

            Log.d(TAG, "gps provider does not exist " + ex.getMessage());

        }

    }

    @Override

    public void onDestroy()

    {

        Log.e(TAG, "onDestroy");

        super.onDestroy();

        if (mLocationManager != null) {

            for (int i = 0; i < mLocationListeners.length; i++) {

                try {

                    mLocationManager.removeUpdates(mLocationListeners[i]);

                } catch (Exception ex) {

                    Log.i(TAG, "fail to remove location listners, ignore", ex);

                }

            }

        }

    }

    private void initializeLocationManager() {

        Log.e(TAG, "initializeLocationManager");

        if (mLocationManager == null) {

            mLocationManager = (LocationManager)
                    getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }
}