package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Kunal Singh on 24-09-2018.
 */
public class DashboardDetailsData implements Parcelable
{

    @Expose
    @SerializedName("dashboardShopDetails")
    public List<DashboardShopDetails> dashboardShopDetails;

    protected DashboardDetailsData(Parcel in) {
    }

    public static final Creator<DashboardDetailsData> CREATOR = new Creator<DashboardDetailsData>() {
        @Override
        public DashboardDetailsData createFromParcel(Parcel in) {
            return new DashboardDetailsData(in);
        }

        @Override
        public DashboardDetailsData[] newArray(int size) {
            return new DashboardDetailsData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }

    public static class DashboardShopDetails {
        @Expose
        @SerializedName("localityName")
        public String localityName;
        @Expose
        @SerializedName("stateDesc")
        public String stateDesc;
        @Expose
        @SerializedName("shopAddress")
        public String shopAddress;
        @Expose
        @SerializedName("shopName")
        public String shopName;
        @Expose
        @SerializedName("shopId")
        public int shopId;
    }
}
