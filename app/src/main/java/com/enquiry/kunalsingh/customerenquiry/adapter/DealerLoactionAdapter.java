package com.enquiry.kunalsingh.customerenquiry.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.parser.FetchDealerLocData;
import com.enquiry.kunalsingh.customerenquiry.utility.PrefManager;
import com.enquiry.kunalsingh.customerenquiry.utility.Utils;

import java.util.List;

/**
 * Created by Kunal Singh on 25-05-2018.
 */
public class DealerLoactionAdapter extends RecyclerView.Adapter<DealerLoactionAdapter.DealerLocationHolder>{
    private List<FetchDealerLocData.DealerLocatorList> list;
    private Activity mContext;
    private static int mSelectedItem = -1;
    private static SingleClickListener sClickListener;
    public DealerLoactionAdapter(Activity context, List<FetchDealerLocData.DealerLocatorList> list) {
        this.mContext = context;
        this.list = list;
    }

    @Override
    public void onBindViewHolder(@NonNull DealerLocationHolder holder, int position) {
        holder.radioButton.setChecked(position==mSelectedItem);
        if(holder.radioButton.isChecked()){
            PrefManager.setDealerName(mContext,list.get(position).dealerName);
            PrefManager.setKeyDealerId(mContext,String.valueOf(list.get(position).branchId));
        }
        if(list.get(position).dealerName!=null){
            holder.edtDealerName.setText(list.get(position).dealerName);
        }else {
            holder.edtDealerName.setVisibility(View.GONE);
        }
        if(list.get(position).branchAddress!=null){
            holder.edtDealerAddress.setText(list.get(position).branchAddress);
        }else {
            holder.edtDealerAddress.setVisibility(View.GONE);
        }

    }

    @NonNull
    @Override
    public DealerLocationHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.custom_dealer_location, viewGroup, false);
        return new DealerLocationHolder(view );
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    class DealerLocationHolder extends RecyclerView.ViewHolder{
        public EditText edtDealerName, edtDealerAddress;
        public RadioButton radioButton;
        public DealerLocationHolder(final View itemView) {
            super(itemView);
            edtDealerName= itemView.findViewById(R.id.edtDealerName);
            edtDealerAddress= itemView.findViewById(R.id.edtDealerAddress);
            radioButton=itemView.findViewById(R.id.radioButtonDealer);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                }
            };
            itemView.setOnClickListener(clickListener);
            radioButton.setOnClickListener(clickListener);
        }


    }


    public void selectedItem() {
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(SingleClickListener clickListener) {
        sClickListener = clickListener;
    }


    public interface SingleClickListener {
        void onItemClickListener(int position, View view);
    }
}
