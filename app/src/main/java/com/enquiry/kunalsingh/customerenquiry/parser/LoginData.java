package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kunal.singh1 on 23-01-2017.
 */
public class LoginData implements Parcelable {


    @SerializedName("isRegistered")
    public String isRegistered;
    @SerializedName("otp")
    public String otp;
    @SerializedName("name")
    public String name;
    @SerializedName("userType")
    public String userType;
    @SerializedName("token")
    public String token;
    @SerializedName("status")
    public String status;
    @SerializedName("serviceType")
    public String serviceType;
    @SerializedName("message")
    public String message;

    protected LoginData(Parcel in) {
        isRegistered=in.readString();
        otp=in.readString();
        name=in.readString();
        userType=in.readString();
        token=in.readString();
        status=in.readString();
        serviceType=in.readString();
        message=in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(isRegistered);
        dest.writeString(otp);
        dest.writeString(name);
        dest.writeString(userType);
        dest.writeString(token);
        dest.writeString(status);
        dest.writeString(serviceType);
        dest.writeString(message);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LoginData> CREATOR = new Creator<LoginData>() {
        @Override
        public LoginData createFromParcel(Parcel in) {
            return new LoginData(in);
        }

        @Override
        public LoginData[] newArray(int size) {
            return new LoginData[size];
        }
    };


    }