package com.enquiry.kunalsingh.customerenquiry.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.enquiry.kunalsingh.customerenquiry.R;

/**
 * Created by Kunal Singh on 07-08-2018.
 */
public class HomeActivity extends AppCompatActivity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_home);
    }
}
