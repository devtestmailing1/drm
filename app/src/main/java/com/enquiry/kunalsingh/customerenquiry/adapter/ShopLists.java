package com.enquiry.kunalsingh.customerenquiry.adapter;

/**
 * Created by Kunal Singh on 15-09-2018.
 */
public class ShopLists {
    private String eventId;
    private String eventName;

    private String shopId;
    private String shopAddress;
    private String shopName;
    private String stateId;
    private String cityId;
    private String localityId;
    private String shopColor;

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }


    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getLocalityId() {
        return localityId;
    }

    public void setLocalityId(String localityId) {
        this.localityId = localityId;
    }


    public String getShopColor() {
        return shopColor;
    }

    public void setShopColor(String shopColor) {
        this.shopColor = shopColor;
    }
}
