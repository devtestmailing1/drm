package com.enquiry.kunalsingh.customerenquiry.parser;

import android.support.v4.app.FragmentActivity;

import com.android.volley.Request;

import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;

import com.enquiry.kunalsingh.customerenquiry.utility.Settings;

import com.google.gson.Gson;

import org.json.JSONArray;

import org.json.JSONException;

import org.json.JSONObject;

/**

 * Created by Kunal Singh on 29-9-2018.

 */

public class SendLocServiceDataParser extends BaseController {

    private onCheckURLListener listener;

    private int id;

    private String TAG_MESSAGE = "Message";

    public SendLocServiceDataParser executeQuery(FragmentActivity context, String url, JSONObject json, onCheckURLListener listener, int id, boolean toShowDialog){

        this.listener=listener;

        this.id=id;

        init(context, url, Request.Method.POST, json,toShowDialog);

        return null;

    }

    @Override

    public void onComplete(JSONArray response, String message) {

        switch (id) {

            case Settings.SEND_LOCATION_SERVICE_ID:

                if (response != null) {

                    try {

                        listener.onSuccess(id, "", parserCheckURLInfo(response.getJSONObject(0)));

                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

                }

                break;

        }

    }

    @Override

    public void onSuccess(JSONArray response) {

    }

    @Override

    public void onFailure(String error, ErrorCode code) {

        if (error != null) {

            listener.onError(error, id,code);

        }

    }

    @Override

    public void onFailureData(String error, ErrorCode code, JSONArray response) {

    }

    public interface onCheckURLListener {

        public void onSuccess(int id, String message, Object object);

        public void onError(String error, int id, ErrorCode code);

    }

    private CityListData parserCheckURLInfo(JSONObject object) {

        Gson gson = new Gson();

        CityListData response = gson.fromJson(object.toString(), CityListData.class);

        return response;

    }
}