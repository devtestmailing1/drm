package com.enquiry.kunalsingh.customerenquiry.common_ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by Kunal Singh on 28-02-2018.
 */

public class EnquiryEditText extends android.support.v7.widget.AppCompatEditText {

    public EnquiryEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setLongClickable(false);
        // TODO Auto-generated constructor stub
        isInEditMode();
    }

    public EnquiryEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setLongClickable(false);

        // TODO Auto-generated constructor stub
        isInEditMode();
    }

    public EnquiryEditText(Context context) {
        super(context);
        this.setLongClickable(false);

        // TODO Auto-generated constructor stub
        isInEditMode();
    }

    @Override
    public void setTypeface(Typeface tf, int style) {

        Typeface normalTypeface = Typeface.createFromAsset(getContext().getAssets(),"font/OpenSans-Regular.ttf");
        Typeface boldTypeface = Typeface.createFromAsset(getContext().getAssets(),"font/OpenSans-Bold.ttf");
        if (style == Typeface.BOLD) {
            // super.setTypeface(FontCache.getTypeface(getContext(), getResources().getString(R.string.font_bold)));
            super.setTypeface(boldTypeface);
        } else {
            super.setTypeface(normalTypeface);
        }
    }
}
