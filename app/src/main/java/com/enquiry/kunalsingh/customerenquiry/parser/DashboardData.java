package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Kunal Singh on 21-09-2018.
 */
public class DashboardData implements Parcelable{

    @Expose
    @SerializedName("dashboardCount")
    public List<DashboardCount> dashboardCount;

    protected DashboardData(Parcel in) {
    }

    public static final Creator<DashboardData> CREATOR = new Creator<DashboardData>() {
        @Override
        public DashboardData createFromParcel(Parcel in) {
            return new DashboardData(in);
        }

        @Override
        public DashboardData[] newArray(int size) {
            return new DashboardData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }

    public static class DashboardCount {
        @Expose
        @SerializedName("callPending")
        public int callPending;
        @Expose
        @SerializedName("callMade")
        public int callMade;
        @Expose
        @SerializedName("pendingVisit")
        public int pendingVisit;
        @Expose
        @SerializedName("turnBlue")
        public int turnBlue;
        @Expose
        @SerializedName("id")
        public int id;
    }
}
