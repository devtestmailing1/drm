package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.enquiry.kunalsingh.customerenquiry.fragment.MaintinanceCost;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kunal Singh on 08-06-2018.
 */
public class MaintinanceCostData implements Parcelable{

    @Expose
    @SerializedName("maintenanceCostList")
    public List<MaintenanceCostList> maintenanceCostList;
    @Expose
    @SerializedName("dataFor")
    public String dataFor;
    @Expose
    @SerializedName("imeiNumber")
    public String imeiNumber;
    @Expose
    @SerializedName("deviceModel")
    public String deviceModel;
    @Expose
    @SerializedName("osName")
    public String osName;
    @Expose
    @SerializedName("appVersion")
    public String appVersion;
    @Expose
    @SerializedName("mobileNumber")
    public String mobileNumber;

    protected MaintinanceCostData(Parcel in) {
        maintenanceCostList= new ArrayList<MaintenanceCostList>();
        in.readList(maintenanceCostList,MaintinanceCost.class.getClassLoader());

        dataFor = in.readString();
        imeiNumber = in.readString();
        deviceModel = in.readString();
        osName = in.readString();
        appVersion = in.readString();
        mobileNumber = in.readString();
    }

    public static final Creator<MaintinanceCostData> CREATOR = new Creator<MaintinanceCostData>() {
        @Override
        public MaintinanceCostData createFromParcel(Parcel in) {
            return new MaintinanceCostData(in);
        }

        @Override
        public MaintinanceCostData[] newArray(int size) {
            return new MaintinanceCostData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(maintenanceCostList);
        parcel.writeString(dataFor);
        parcel.writeString(imeiNumber);
        parcel.writeString(deviceModel);
        parcel.writeString(osName);
        parcel.writeString(appVersion);
        parcel.writeString(mobileNumber);
    }

    public static class MaintenanceCostList {
        @Expose
        @SerializedName("totalBillValue")
        public double totalBillValue;
        @Expose
        @SerializedName("billYear")
        public String billYear;
        @Expose
        @SerializedName("id")
        public int id;
    }
}
