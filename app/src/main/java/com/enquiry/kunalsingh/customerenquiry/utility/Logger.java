package com.enquiry.kunalsingh.customerenquiry.utility;


import android.util.Log;

/**
 * Logger class for Log Details
 * Created by Kunal Singh
 * Date 01 January 2017
 */
public class Logger {

    public static boolean printLogToConsole = true;

    /**
     * used to log an information to  LogCat.b
     * @param className : Name of the class file
     * @param methodName : Name of the method
     * @param info : Information to be print in LogCat.
     * @return Void
     */
    public static void logInfo(String className, String methodName, String info)
    {
        if(printLogToConsole)
        {
            String log="Method: "+methodName+" | ";
            log=log+"Info: "+info;
            Log.i(className,log);
        }
    }

    /**
     * used to log an error to SD-card and LogCat.
     * @param className : Name of the class file
     * @param methodName : Name of the method
     * @param errorMsg : Error Message
     * @return Void
     */
    public static void logError(String className, String methodName, String errorMsg)
    {
        String log="Class: "+className+" | ";
        log=log+"Method: "+methodName+" | ";
        log=log+"Error: "+errorMsg;
        if(printLogToConsole)
        {
            Log.e(className,log);
        }

    }



}
