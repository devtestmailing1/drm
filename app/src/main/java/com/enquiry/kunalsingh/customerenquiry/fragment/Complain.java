package com.enquiry.kunalsingh.customerenquiry.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.adapter.ComplainAdapter;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.parser.ComplainListData;
import com.enquiry.kunalsingh.customerenquiry.parser.ComplainParser;
import com.enquiry.kunalsingh.customerenquiry.parser.ComplainScreenDTLData;
import com.enquiry.kunalsingh.customerenquiry.parser.ComplainScreenDTLParser;
import com.enquiry.kunalsingh.customerenquiry.utility.PrefManager;
import com.enquiry.kunalsingh.customerenquiry.utility.QueryBuilder;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.enquiry.kunalsingh.customerenquiry.utility.Utils;
import com.getbase.floatingactionbutton.FloatingActionButton;

import static android.view.View.VISIBLE;

/**
 * Created by Kunal Singh on 13-06-2018.
 */
public class Complain extends AppCompatActivity implements ComplainParser.onComplainListener, ComplainScreenDTLParser.onComplainScreenDTLListener {
    RecyclerView recyclerViewComplain;
    ComplainAdapter complainAdapter;
    ComplainListData complainListData;
    ComplainScreenDTLData complainScreenDTLData;
    FloatingActionButton add_query;
    private RelativeLayout noDataLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complain);
        Toolbar toolbar = findViewById(R.id.header);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("Complaint");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        noDataLayout = (RelativeLayout) findViewById(R.id.no_data_rl);
        recyclerViewComplain=findViewById(R.id.recyclerViewComplain);
        add_query = (FloatingActionButton) findViewById(R.id.add_query);
        add_query.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ComplainScreenDTLParser().executeQuery(Complain.this, QueryBuilder.getInstance().fetchValidateComplainAPI(), QueryBuilder.getInstance().generateValidateComplainQuery(Complain.this, PrefManager.getKeyUserMobileNumber(Complain.this), Utils.versionNumber(Complain.this), Utils.getDeviceName(), Utils.deviceToken(Complain.this)), Complain.this, Settings.VALIDATE_COMPLAIN_ID, true);


            }
        });
        new ComplainParser().executeQuery(this, QueryBuilder.getInstance().fetchComplainAPI(), QueryBuilder.getInstance().generateComplainQuery(this, PrefManager.getKeyUserMobileNumber(this), Utils.versionNumber(this), Utils.getDeviceName(), Utils.deviceToken(this)), this, Settings.COMPLAIN_LIST_ID, true);
    }

    @Override
    public void onSuccess(int id, String message, Object object) {
        if(id==Settings.COMPLAIN_LIST_ID) {
            /*if (object != null) {
                complainListData = ((ComplainListData) object);
                if(complainListData.complaintModelList!=null) {
                    complainAdapter = new ComplainAdapter(this, complainListData.complaintModelList);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                    recyclerViewComplain.setLayoutManager(layoutManager);
                    recyclerViewComplain.setAdapter(complainAdapter);
                }else{
                    recyclerViewComplain.setVisibility(View.GONE);
                    noDataLayout.setVisibility(VISIBLE);
                }
            }*/
        }
        if(id==Settings.VALIDATE_COMPLAIN_ID){
            if (object != null) {
                complainScreenDTLData = ((ComplainScreenDTLData) object);
                if(complainScreenDTLData.vinModelList!=null&& complainScreenDTLData.vinModelList.size()>0){
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("complainData", complainScreenDTLData);
                    startActivity(new Intent(Complain.this, CreateComplaint.class).putExtras(bundle));

                }
            }

        }
    }

    @Override
    public void onError(String error, int id, BaseController.ErrorCode code) {
        if(id==Settings.VALIDATE_COMPLAIN_ID){
            if(error!=null) {
                Utils.HitechToast(this, error);
            }
        }
        if(id==Settings.COMPLAIN_LIST_ID){
            if(error!=null) {
                recyclerViewComplain.setVisibility(View.GONE);
                noDataLayout.setVisibility(VISIBLE);
                Utils.HitechToast(this, error);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
