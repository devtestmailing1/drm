package com.enquiry.kunalsingh.customerenquiry.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.parser.ServiceBookingListData;

import java.util.List;

/**
 * Created by Kunal Singh on 16-05-2018.
 */
public class ServicingBookingListAdapter extends RecyclerView.Adapter<ServicingBookingListAdapter.ServiceBookingListHolder>{
    private List<ServiceBookingListData.ExistingBookingList> list;
    private Activity mContext;


    public ServicingBookingListAdapter(Activity context, List<ServiceBookingListData.ExistingBookingList> list) {
        this.mContext = context;
        this.list = list;
    }



    @Override
    public ServiceBookingListHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.custom_service_booking_list, viewGroup, false);
        ServiceBookingListHolder viewHolder = new ServiceBookingListHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ServiceBookingListHolder holder, int position) {

        if(list.get(position).appointmentDate!=null){
            holder.edtAppontmentDate.setText(list.get(position).appointmentDate);
        }else {
            holder.edtAppontmentDate.setVisibility(View.GONE);
        }

        if(list.get(position).appStatus!=null){
            holder.txtViewAppointmentNumber.setText(list.get(position).appStatus);
        }else {
            holder.txtViewAppointmentNumber.setVisibility(View.GONE);
        }

        if(list.get(position).dealerName!=null){
            holder.edtDealerName.setText(list.get(position).dealerName);
        }else {
            holder.edtDealerName.setVisibility(View.GONE);
        }
        if(list.get(position).branchLocation!=null){
            holder.edtDealerAddress.setText(list.get(position).branchLocation);
        }else {
            holder.edtDealerAddress.setVisibility(View.GONE);
        }

        if(list.get(position).vehicleNumber!=null){
            holder.edtDealerRegNo.setText(list.get(position).vehicleNumber);
        }else {
            holder.edtDealerRegNo.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ServiceBookingListHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private LinearLayout lnyHiddenText;
        private EditText edtDealerName, edtDealerAddress,edtDealerRegNo, edtAppontmentDate;
        private TextView txtViewAppointmentNumber;
        public ServiceBookingListHolder(View itemView) {
            super(itemView);
            txtViewAppointmentNumber= itemView.findViewById(R.id.txtViewStatus);
            edtDealerName= itemView.findViewById(R.id.edtDealerName);
            edtDealerAddress= itemView.findViewById(R.id.edtDealerAddress);
            edtDealerRegNo= itemView.findViewById(R.id.edtDealerRegNo);
            edtAppontmentDate= itemView.findViewById(R.id.edtAppontmentDate);

          /*  show_info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(show_info.getText().toString().equalsIgnoreCase("Show")){
                        lnyHiddenText.setVisibility(View.VISIBLE);
                        show_info.setText("Hide");
                    } else{
                        lnyHiddenText.setVisibility(View.GONE);
                        show_info.setText("Show");
                    }
                }

            });*/
        }

        @Override
        public void onClick(View v) {
        }
    }
}
