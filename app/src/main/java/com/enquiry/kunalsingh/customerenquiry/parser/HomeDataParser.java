package com.enquiry.kunalsingh.customerenquiry.parser;

import android.support.v4.app.FragmentActivity;

import com.android.volley.Request;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kunal Singh on 23-04-2018.
 */
public class HomeDataParser extends BaseController {

    public HomeDataParser.onHomeListener listener;
    public int id;
    public String TAG_MESSAGE = "message";

    public HomeDataParser executeQuery(FragmentActivity context, String url, JSONObject json, HomeDataParser.onHomeListener listener, int id, boolean toShowDialog) {
        // RequestManager.allowAllSSL();
        this.listener = listener;
        this.id = id;
        init(context, url, Request.Method.POST, json, toShowDialog);

        return null;
    }

    @Override
    public void onComplete(JSONArray response, String message) {
        switch (id) {
            case Settings.VERIFY_OTP_ID:
               // listener.onSuccess(id, message, null);
                if (response != null) {
                    try {
                        listener.onSuccess(id, "", parserShopListInfo(response.getJSONObject(0)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                break;

            case Settings.SEARCH_SHOP_LIST_ID:
                if (response != null) {
                    try {
                        listener.onSuccess(id, "", parserShopListInfo(response.getJSONObject(0)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                break;

            case Settings.DASHBOARD_ID:
                if (response != null) {
                    try {
                        listener.onSuccess(id, "", parserDashboardData(response.getJSONObject(0)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                break;
            case Settings.DASHBOARD_DETAILS_ID:
                if (response != null) {
                    try {
                        listener.onSuccess(id, "", parserDashboardDetailsData(response.getJSONObject(0)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                break;
        }
    }

    @Override
    public void onSuccess(JSONArray response) {

    }

    @Override
    public void onFailureData(String error, ErrorCode code, JSONArray response) {

    }

    @Override
    public void onFailure(String error, ErrorCode code) {
        if (error != null) {
            listener.onError(error, id, code);
        }
    }


    public interface onHomeListener {
        public void onSuccess(int id, String message, Object object);
        public void onError(String error, int id, ErrorCode code);
    }


    public ShoapListData parserShopListInfo(JSONObject object) {
        Gson gson = new Gson();
        ShoapListData response = gson.fromJson(object.toString(), ShoapListData.class);
        return response;
    }

    public DashboardData parserDashboardData(JSONObject object) {
        Gson gson = new Gson();
        DashboardData dasboardResponse = gson.fromJson(object.toString(), DashboardData.class);
        return dasboardResponse;
    }

    public DashboardDetailsData parserDashboardDetailsData(JSONObject object) {
        Gson gson = new Gson();
        DashboardDetailsData dasboardDetailsResponse = gson.fromJson(object.toString(), DashboardDetailsData.class);
        return dasboardDetailsResponse;
    }
}
