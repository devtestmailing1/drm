package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kunal Singh on 06-06-2018.
 */
public class ServiceHistoryData implements Parcelable{

    @Expose
    @SerializedName("serviceHistoryList")
    public List<ServiceHistoryList> serviceHistoryList;
    @Expose
    @SerializedName("dataFor")
    public String dataFor;
    @Expose
    @SerializedName("imeiNumber")
    public String imeiNumber;
    @Expose
    @SerializedName("deviceModel")
    public String deviceModel;
    @Expose
    @SerializedName("osName")
    public String osName;
    @Expose
    @SerializedName("appVersion")
    public String appVersion;
    @Expose
    @SerializedName("mobileNumber")
    public String mobileNumber;

    protected ServiceHistoryData(Parcel in) {
        serviceHistoryList= new ArrayList<ServiceHistoryList>();
        in.readList(serviceHistoryList,ServiceHistoryData.ServiceHistoryList.class.getClassLoader());
        dataFor = in.readString();
        imeiNumber = in.readString();
        deviceModel = in.readString();
        osName = in.readString();
        appVersion = in.readString();
        mobileNumber = in.readString();
    }

    public static final Creator<ServiceHistoryData> CREATOR = new Creator<ServiceHistoryData>() {
        @Override
        public ServiceHistoryData createFromParcel(Parcel in) {
            return new ServiceHistoryData(in);
        }

        @Override
        public ServiceHistoryData[] newArray(int size) {
            return new ServiceHistoryData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(serviceHistoryList);
        parcel.writeString(dataFor);
        parcel.writeString(imeiNumber);
        parcel.writeString(deviceModel);
        parcel.writeString(osName);
        parcel.writeString(appVersion);
        parcel.writeString(mobileNumber);
    }

    public static class ServiceHistoryList implements Parcelable{
        @Expose
        @SerializedName("totalBillValue")
        public double totalBillValue;
        @Expose
        @SerializedName("labourBillValue")
        public double labourBillValue;
        @Expose
        @SerializedName("partBillValue")
        public double partBillValue;
        @Expose
        @SerializedName("dealer")
        public String dealer;
        @Expose
        @SerializedName("roNumber")
        public String roNumber;
        @Expose
        @SerializedName("categoryDesc")
        public String categoryDesc;
        @Expose
        @SerializedName("odoMeter")
        public int odoMeter;
        @Expose
        @SerializedName("docDate")
        public String docDate;
        @Expose
        @SerializedName("id")
        public int id;

        protected ServiceHistoryList(Parcel in) {
            totalBillValue = in.readDouble();
            labourBillValue = in.readDouble();
            partBillValue = in.readDouble();
            dealer = in.readString();
            roNumber = in.readString();
            categoryDesc = in.readString();
            odoMeter = in.readInt();
            docDate = in.readString();
            id = in.readInt();
        }

        public static final Creator<ServiceHistoryList> CREATOR = new Creator<ServiceHistoryList>() {
            @Override
            public ServiceHistoryList createFromParcel(Parcel in) {
                return new ServiceHistoryList(in);
            }

            @Override
            public ServiceHistoryList[] newArray(int size) {
                return new ServiceHistoryList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeDouble(totalBillValue);
            parcel.writeDouble(labourBillValue);
            parcel.writeDouble(partBillValue);
            parcel.writeString(dealer);
            parcel.writeString(roNumber);
            parcel.writeString(categoryDesc);
            parcel.writeInt(odoMeter);
            parcel.writeString(docDate);
            parcel.writeInt(id);
        }
    }
}
