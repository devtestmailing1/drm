package com.enquiry.kunalsingh.customerenquiry.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.parser.ShopListItem;
import com.enquiry.kunalsingh.customerenquiry.utility.Utils;

import java.util.ArrayList;

import static android.support.v7.widget.LinearLayoutManager.VERTICAL;

/**
 * Created by Kunal Singh on 15-09-2018.
 */
public class ShopListParentAdapter extends RecyclerView.Adapter<ShopListParentAdapter.MyViewHolder> implements ShopListChildAdapter.ItemListener{


    private ShopAreaInformation eventInformation;
    Context context;

    public ShopListParentAdapter(ShopAreaInformation eventInformation, Context context) {
        this.eventInformation = eventInformation;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shop_list_parent_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ShopAreas eventDates = eventInformation.getShopAreaList().get(position);
        holder.event_list_parent_date.setText(eventDates.getDate());

        LinearLayoutManager hs_linearLayout = new LinearLayoutManager(context, VERTICAL, false);
        holder.event_recycler_view_child.setLayoutManager(hs_linearLayout);
        holder.event_recycler_view_child.setHasFixedSize(true);
        ShopListChildAdapter eventListChildAdapter = new ShopListChildAdapter(context,eventInformation.getShopAreaList().get(position).getEventsArrayList());
        holder.event_recycler_view_child.setAdapter(eventListChildAdapter);

    }

    @Override
    public int getItemCount() {
        return eventInformation.getShopAreaList().size();
    }

    @Override
    public void onItemClick(int shopId) {
        Utils.HitechToast(context,String.valueOf(shopId));
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView event_list_parent_date;
        public RecyclerView event_recycler_view_child;

        public MyViewHolder(View view) {
            super(view);
            event_list_parent_date = (TextView) view.findViewById(R.id.event_list_parent_date);
            event_recycler_view_child = (RecyclerView)view.findViewById(R.id.event_recycler_view_child);
        }
    }

}