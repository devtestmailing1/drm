package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kunal Singh on 25-05-2018.
 */
public class FetchDealerLocData implements Parcelable{

    @Expose
    @SerializedName("dealerLocatorList")
    public List<DealerLocatorList> dealerLocatorList;
    @Expose
    @SerializedName("imeiNumber")
    public String imeiNumber;
    @Expose
    @SerializedName("deviceModel")
    public String deviceModel;
    @Expose
    @SerializedName("osName")
    public String osName;
    @Expose
    @SerializedName("appVersion")
    public String appVersion;
    @Expose
    @SerializedName("cityId")
    public int cityId;
    @Expose
    @SerializedName("stateId")
    public int stateId;
    @Expose
    @SerializedName("districtId")
    public int districtId;
    @Expose
    @SerializedName("pinCode")
    public String pinCode;
    @Expose
    @SerializedName("mobileNumber")
    public String mobileNumber;

    protected FetchDealerLocData(Parcel in) {
        dealerLocatorList= new ArrayList<DealerLocatorList>();
        in.readList(dealerLocatorList,DealerLocatorList.class.getClassLoader());
        imeiNumber = in.readString();
        deviceModel = in.readString();
        osName = in.readString();
        appVersion = in.readString();
        cityId = in.readInt();
        stateId = in.readInt();
        districtId = in.readInt();
        pinCode = in.readString();
        mobileNumber = in.readString();
    }

    public static final Creator<FetchDealerLocData> CREATOR = new Creator<FetchDealerLocData>() {
        @Override
        public FetchDealerLocData createFromParcel(Parcel in) {
            return new FetchDealerLocData(in);
        }

        @Override
        public FetchDealerLocData[] newArray(int size) {
            return new FetchDealerLocData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(dealerLocatorList);
        parcel.writeString(imeiNumber);
        parcel.writeString(deviceModel);
        parcel.writeString(osName);
        parcel.writeString(appVersion);
        parcel.writeInt(cityId);
        parcel.writeInt(stateId);
        parcel.writeInt(districtId);
        parcel.writeString(pinCode);
        parcel.writeString(mobileNumber);
    }

    public static class DealerLocatorList implements Parcelable{
        @Expose
        @SerializedName("cityDesc")
        public String cityDesc;
        @Expose
        @SerializedName("branchAddress")
        public String branchAddress;
        @Expose
        @SerializedName("dealerName")
        public String dealerName;
        @Expose
        @SerializedName("branchId")
        public int branchId;

        protected DealerLocatorList(Parcel in) {
            cityDesc = in.readString();
            branchAddress = in.readString();
            dealerName = in.readString();
            branchId = in.readInt();
        }

        public static final Creator<DealerLocatorList> CREATOR = new Creator<DealerLocatorList>() {
            @Override
            public DealerLocatorList createFromParcel(Parcel in) {
                return new DealerLocatorList(in);
            }

            @Override
            public DealerLocatorList[] newArray(int size) {
                return new DealerLocatorList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(cityDesc);
            parcel.writeString(branchAddress);
            parcel.writeString(dealerName);
            parcel.writeInt(branchId);
        }
    }
}
