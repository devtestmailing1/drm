package com.enquiry.kunalsingh.customerenquiry.adapter;

import java.util.ArrayList;

/**
 * Created by Kunal Singh on 15-09-2018.
 */
public class ShopAreaInformation {
    private ArrayList<ShopAreas> shopAreaList = new ArrayList<>();

    public ArrayList<ShopAreas> getShopAreaList() {
        return shopAreaList;
    }

    public void setShopAreaList(ArrayList<ShopAreas> shopAreaList) {
        this.shopAreaList = shopAreaList;
    }
}
