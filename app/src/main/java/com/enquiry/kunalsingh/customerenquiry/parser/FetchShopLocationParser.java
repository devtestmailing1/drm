package com.enquiry.kunalsingh.customerenquiry.parser;

import android.location.Location;
import android.support.v4.app.FragmentActivity;

import com.android.volley.Request;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kunal Singh on 12-08-2018.
 */
public class FetchShopLocationParser extends BaseController {

    public FetchShopLocationParser.onLocationLevelDataListner listener;
    public int id;
    public String TAG_MESSAGE = "message";

    public InitAddShopParser executeQuery(FragmentActivity context, String url, JSONObject json, FetchShopLocationParser.onLocationLevelDataListner listener, int id, boolean toShowDialog) {
        // RequestManager.allowAllSSL();
        this.listener = listener;
        this.id = id;
        init(context, url, Request.Method.POST, json, toShowDialog);

        return null;
    }

    @Override
    public void onComplete(JSONArray response, String message) {
        switch (id) {
           /* case Settings.INIT_ADD_SHP_ID:
                listener.onSuccess(id, message, null);
                if (response != null) {
                    try {
                        listener.onSuccess(id, "", parserUserInfo(response.getJSONObject(0)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                break;

            case Settings.SUBMIT_ADD_SHOP_ID:
                if (message != null)
                    listener.onSuccess(id, message, null);
                break;*/


            case Settings.SHOP_LOCATION_COUNTRY:
            case Settings.SHOP_LOCATION_STATE:
            case Settings.SHOP_LOCATION_CITY:
            case Settings.SHOP_LOCATION_LOCALITY:
            case Settings.SHOP_LOCATION_MOHOLLA:

                listener.onSuccess(id, message, null);
                if (response != null) {
                    try {
                        listener.onSuccess(id, "", parserUserInfo(response.getJSONObject(0)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                break;

        }
    }

    @Override
    public void onSuccess(JSONArray response) {

    }

    @Override
    public void onFailureData(String error, ErrorCode code, JSONArray response) {

    }

    @Override
    public void onFailure(String error, ErrorCode code) {
        if (error != null) {
            listener.onError(error, id, code);
        }
    }


    public interface onLocationLevelDataListner {
        public void onSuccess(int id, String message, Object object);
        public void onError(String error, int id, ErrorCode code);
    }


    public LocationLevelData parserUserInfo(JSONObject object) {
        Gson gson = new Gson();
        LocationLevelData response = gson.fromJson(object.toString(), LocationLevelData.class);
        return response;
    }



}
