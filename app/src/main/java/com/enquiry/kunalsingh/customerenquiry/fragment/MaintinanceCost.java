package com.enquiry.kunalsingh.customerenquiry.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.adapter.ServiceHistoryAdapter;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.parser.MaintinanceCostData;
import com.enquiry.kunalsingh.customerenquiry.parser.MaintinanceCostGraphData;
import com.enquiry.kunalsingh.customerenquiry.parser.MentinanceCostGraphParser;
import com.enquiry.kunalsingh.customerenquiry.parser.ServiceHistoryData;
import com.enquiry.kunalsingh.customerenquiry.parser.ServiceHistotryParser;
import com.enquiry.kunalsingh.customerenquiry.parser.StateListParser;
import com.enquiry.kunalsingh.customerenquiry.parser.VehicleListData;
import com.enquiry.kunalsingh.customerenquiry.parser.VehicleListParser;
import com.enquiry.kunalsingh.customerenquiry.utility.PrefManager;
import com.enquiry.kunalsingh.customerenquiry.utility.QueryBuilder;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.enquiry.kunalsingh.customerenquiry.utility.Utils;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

/**
 * Created by Kunal Singh on 08-06-2018.
 */
public class MaintinanceCost extends AppCompatActivity implements ServiceHistotryParser.onMaintinanceCostListListener, StateListParser.onStateListListener, MentinanceCostGraphParser.onLoginListener {
    ServiceHistoryAdapter maintinaceCostAdapter;
    ServiceHistoryData maintinanceCostData;
    VehicleListData vehicleListData;
    MaintinanceCostGraphData maintinanceCostGraphData;
    BarChart chart;
    Spinner spinnerRegNoValue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintinance_cost);
        Toolbar toolbar = findViewById(R.id.header);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("Maintenance Cost");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        spinnerRegNoValue=findViewById(R.id.spinnerRegNoValue);
        new VehicleListParser().executeQuery(this, QueryBuilder.getInstance().userVehicleListAPI(), QueryBuilder.getInstance().generateVehicleListQuery(this, PrefManager.getKeyUserMobileNumber(this), Utils.versionNumber(this), Utils.getDeviceName(), Utils.deviceToken(this)), this, Settings.USER_VEHICLE_LIST_ID, true);


        chart = (BarChart) findViewById(R.id.bar_chart);


    }

    @Override
    public void onSuccess(int id, String message, Object object) {
        if(id== Settings.USER_VEHICLE_LIST_ID) {
            if (object != null) {
                vehicleListData = ((VehicleListData) object);
                final ArrayList<String> regNumber=new ArrayList<>();
                regNumber.add("All");
                for(int i=0;i<vehicleListData.userVehicleDTLList.size();i++){
                    regNumber.add(vehicleListData.userVehicleDTLList.get(i).registrationNo);
                }
                ArrayAdapter<String> adapter = new ArrayAdapter(this,
                        android.R.layout.simple_spinner_dropdown_item, regNumber);
                spinnerRegNoValue.setAdapter(adapter);
                spinnerRegNoValue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        String selctedRegNo;
                        if(spinnerRegNoValue.getSelectedItem().toString().equalsIgnoreCase("All")){
                            selctedRegNo="";
                        }else{
                            selctedRegNo=spinnerRegNoValue.getSelectedItem().toString();
                        }
                        new MentinanceCostGraphParser().executeQuery(MaintinanceCost.this, QueryBuilder.getInstance().fetchMaintinanceCostAPI(), QueryBuilder.getInstance().generateMaintinaceCostQuery(MaintinanceCost.this, PrefManager.getKeyUserMobileNumber(MaintinanceCost.this), Utils.versionNumber(MaintinanceCost.this), Utils.getDeviceName(), Utils.deviceToken(MaintinanceCost.this), "","",selctedRegNo,""), MaintinanceCost.this, Settings.MAINTINANCE_COST_ID, true);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }
        }

        if(id==Settings.MAINTINANCE_COST_ID){
            if (object != null) {
                maintinanceCostGraphData = ((MaintinanceCostGraphData) object);
                ArrayList<BarEntry> barEntries=new ArrayList<>();
                for(int i=0;i<maintinanceCostGraphData.priceList.size();i++) {
                    barEntries.add(new BarEntry(Float.parseFloat(maintinanceCostGraphData.priceList.get(i).price), i));

                }
                BarDataSet barDataSet= new BarDataSet(barEntries,"Dates");

                ArrayList<String> dates=new ArrayList<>();
                for(int i=0;i<maintinanceCostGraphData.yearList.size();i++) {
                    dates.add(maintinanceCostGraphData.yearList.get(i).year);

                }
                BarData barData= new BarData(dates,barDataSet);
                chart.setData(barData);
                chart.setVisibility(View.VISIBLE);
                //chart.setTouchEnabled(true);
            }

        }

    }

    @Override
    public void onError(String error, int id, BaseController.ErrorCode code) {
        if(error!=null){
            Utils.HitechToast(this,   error);
        }
        if(id==Settings.MAINTINANCE_COST_ID){
            chart.invalidate();
            chart.setVisibility(View.GONE);
        }
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
