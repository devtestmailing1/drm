package com.enquiry.kunalsingh.customerenquiry.drawer;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.enquiry.kunalsingh.customerenquiry.LoginScreenActivity;
import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.adapter.ExpandableListAdapter;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.fragment.Complain;
import com.enquiry.kunalsingh.customerenquiry.fragment.HomeFragment;
import com.enquiry.kunalsingh.customerenquiry.fragment.ReportFragment;
import com.enquiry.kunalsingh.customerenquiry.fragment.SendLocService;
import com.enquiry.kunalsingh.customerenquiry.parser.InitAddShopData;
import com.enquiry.kunalsingh.customerenquiry.parser.InitAddShopParser;
import com.enquiry.kunalsingh.customerenquiry.parser.SendLocServiceDataParser;
import com.enquiry.kunalsingh.customerenquiry.utility.PrefManager;
import com.enquiry.kunalsingh.customerenquiry.utility.QueryBuilder;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * MainActivity For Navigation Fragment
 * Created by Kunal
 * Date 01 Aug 2018
 */
public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener, InitAddShopParser.onInitShopDataListener, ExpandableListAdapter.ItemListener {

    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    TextView tvToolbar;
    public DrawerLayout dlLeftMenu;
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    InitAddShopData initAddShopData;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    Button btnApply;
    Map<Integer,String> map_prod_values = new HashMap<Integer, String>();
    Map<Integer,String> map_fab_values = new HashMap<Integer, String>();
    Map<Integer,String> map_party_values = new HashMap<Integer, String>();
    Map<Integer,String> map_rank_values = new HashMap<Integer, String>();
    ArrayList<String> finalProd;
    ArrayList<String> finalfabric;
    ArrayList<String> finalParty;
    ArrayList<String> finalRank;
    private LocationManager locationManager;
    private Location location = null;
    private static final int LOCATION_MIN_UPDATE_TIME = 10;
    private static final int LOCATION_MIN_UPDATE_DISTANCE = 1000;
    private long interval =1000*60;

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            locationManager.removeUpdates(locationListener);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }


        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }

    };
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        tvToolbar = (TextView) mToolbar.findViewById(R.id.tvToolbar);
        mToolbar.setTitleTextColor(Color.parseColor("#000000"));
        setSupportActionBar(mToolbar);
        tvToolbar.setText(mToolbar.getTitle());
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        locationTimer();

        tvToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Bundle bundleLoc = new Bundle();
                    bundleLoc.putString("actionName", "Home");
                   Fragment fragment = new HomeFragment();
                    fragment.setArguments(bundleLoc);
                   String title = getString(R.string.title_home);
                    if (fragment != null) {
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container_body, fragment, title);
                        fragmentTransaction.commit();
                        tvToolbar.setText(title);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        dlLeftMenu = findViewById(R.id.drawer_layout);
        drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);
        // display the first navigation drawer view on app launch
        displayView(0);

        new InitAddShopParser().executeQuery(this, QueryBuilder.getInstance().getInitAddShopAPI(), QueryBuilder.getInstance().generateInitAddShopQuery(this, PrefManager.getKeyAuthToken(this).trim()), this, Settings.INIT_ADD_SHP_ID, true);
        btnApply=findViewById(R.id.btnApply);
        btnApply.setOnClickListener(new View.OnClickListener() {
            StringBuilder resultProd = new StringBuilder();
            StringBuilder resultFab = new StringBuilder();
            StringBuilder resultParty = new StringBuilder();
            StringBuilder resultRank = new StringBuilder();

            @Override
            public void onClick(View view) {

                resultProd.setLength(0);
                resultFab.setLength(0);
                resultParty.setLength(0);
                resultRank.setLength(0);

                if (finalProd!=null) {

                    for (Map.Entry<Integer, String> prodEntry : map_prod_values.entrySet()) {
                        Iterator<String> itr = finalProd.iterator();
                        while (itr.hasNext()) {
                            if (prodEntry.getValue().equalsIgnoreCase(itr.next())) {
                                resultProd.append(String.valueOf(prodEntry.getKey()));
                                resultProd.append(",");
                            }
                        }

                    }

                    for (Map.Entry<Integer, String> fabricEntry : map_fab_values.entrySet()) {
                        Iterator<String> itr = finalfabric.iterator();
                        while (itr.hasNext()) {
                            if (fabricEntry.getValue().equalsIgnoreCase(itr.next())) {
                                resultFab.append(String.valueOf(fabricEntry.getKey()));
                                resultFab.append(",");
                            }
                        }

                    }

                    for (Map.Entry<Integer, String> partyEntry : map_party_values.entrySet()) {
                        for (String aFinalParty : finalParty) {
                            if (partyEntry.getValue().equalsIgnoreCase(aFinalParty)) {
                                resultParty.append(String.valueOf(partyEntry.getKey()));
                                resultParty.append(",");
                            }
                        }

                    }

                    for (Map.Entry<Integer, String> rankEntry : map_rank_values.entrySet()) {
                        for (String aFinalParty : finalRank) {
                            if (rankEntry.getValue().equalsIgnoreCase(aFinalParty)) {
                                resultRank.append(String.valueOf(rankEntry.getKey()));
                                resultRank.append(",");
                            }
                        }
                    }
                    if (dlLeftMenu.isDrawerOpen(GravityCompat.END)) {
                        dlLeftMenu.closeDrawer(GravityCompat.END);
                    }
                    Fragment fragment;
                    String title;
                    Bundle bundleSearch = new Bundle();
                    bundleSearch.putString("actionName", "Filter");
                    bundleSearch.putString("prodID", String.valueOf(resultProd));
                    bundleSearch.putString("fabID", String.valueOf(resultFab));
                    bundleSearch.putString("partyID", String.valueOf(resultParty));
                    bundleSearch.putString("rankID", String.valueOf(resultRank));
                    fragment = new HomeFragment();
                    fragment.setArguments(bundleSearch);
                    title = getString(R.string.title_home);

                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment, title);
                    fragmentTransaction.commit();
                    tvToolbar.setText(title);

                    listAdapter.clearValues();

                }

            }
        });

    }

    private void populateExpandList() {

        // get the listview
        expListView = findViewById(R.id.lvExp);

        // preparing list data
        // prepareListData();

        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild,this);

        // setting list adapter
        expListView.setAdapter(listAdapter);

        // Listview Group click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                 /* Toast.makeText(getApplicationContext(),
                 "Group Clicked " + listDataHeader.get(groupPosition),
                  Toast.LENGTH_SHORT).show();*/

                return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                /*Toast.makeText(getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Expanded",
                        Toast.LENGTH_SHORT).show();*/
            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
               /* Toast.makeText(getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Collapsed",
                        Toast.LENGTH_SHORT).show();*/

            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
              /*  Toast.makeText(
                        getApplicationContext(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + listDataChild.get(
                                listDataHeader.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT)
                        .show();*/

                return true;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }


    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        String title = "HRD";
        switch (position) {
            case 0:
                Bundle bundle = new Bundle();
                bundle.putString("params", "My String data");
                // set MyFragment Arguments
                fragment = new HomeFragment();
                fragment.setArguments(bundle);
                title = getString(R.string.title_home);
                // startActivity(new Intent(MainActivity.this, HomeActivity.class));
                break;
            case 1:
                fragment = new ReportFragment();
                title = getString(R.string.title_Report);
                break;
            case 2:
                // startActivity(new Intent(MainActivity.this, ServiceHistory.class));

                break;

            case 3:
                PrefManager.clearAll(this);
                startActivity(new Intent(MainActivity.this, LoginScreenActivity.class));
                break;

            case 4:
                //startActivity(new Intent(MainActivity.this, MaintinanceCost.class));
                break;
            case 5:

                break;

            case 6:
                startActivity(new Intent(MainActivity.this, Complain.class));
                break;
            /*case 7:
                startActivity(new Intent(MainActivity.this, QRSearchActivity.class));
                break;

            case 8:
                startActivity(new Intent(MainActivity.this, ViewCartActivity.class));
                break;

            case 9:
                startActivity(new Intent(MainActivity.this, ViewOrderActivity.class));
                break;

            case 10:
                startActivity(new Intent(MainActivity.this, RaiseQueryActivity.class));
                break;

            case 11:
                startActivity(new Intent(MainActivity.this, ViewQueryActivity.class));
                break;

            case 12:

                startActivity(new Intent(MainActivity.this, CompanyProfileActivity.class));
                break;

            case 13:
                startActivity(new Intent(MainActivity.this, AboutGenuinePartsActivity.class));
                break;

            case 14:
                startActivity(new Intent(MainActivity.this, ContactusActivity.class));
                break;

            case 15:
                if(PrefManager.isLoginMobile(this)) {
                    startActivity(new Intent(MainActivity.this, MyProfile.class));
                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
                    builder.setTitle(R.string.title_logout);
                    builder.setMessage(R.string.logout_message);
                    builder.setPositiveButton(R.string.ok_button, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            PrefManager.clearAll(MainActivity.this);
                            Intent loginIntent = new Intent(MainActivity.this, LoginScreenActivity.class);
                            startActivity(loginIntent);
                            MainActivity.this.finish();
                        }
                    });
                    builder.setNegativeButton(R.string.cancel_button, null);
                    builder.show();
                }
                break;
            case 16:
                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
                builder.setTitle(R.string.title_logout);
                builder.setMessage(R.string.logout_message);
                builder.setPositiveButton(R.string.ok_button, new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PrefManager.clearAll(MainActivity.this);
                        Intent loginIntent = new Intent(MainActivity.this, LoginWithMobileActivity.class);
                        startActivity(loginIntent);
                        MainActivity.this.finish();
                    }
                });
                builder.setNegativeButton(R.string.cancel_button, null);
                builder.show();
                break;
*/

            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            //fragmentTransaction.setCustomAnimations(R.anim.animation, R.anim.animation2);
            fragmentTransaction.replace(R.id.container_body, fragment, title);
            fragmentTransaction.commit();
            // set the toolbar title
            // getSupportActionBar().setTitle(title);
            tvToolbar.setText(title);
        }
    }


   /* public void closeAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Close");
        builder.setMessage(R.string.close_message);
        builder.setPositiveButton(R.string.ok_button, new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                MainActivity.this.finishAffinity();
            }
        });
        builder.setNegativeButton(R.string.cancel_button, null);
        builder.show();
    }*/


    @Override
    public void onBackPressed() {

        //closeAlert();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Fragment fragment=null;
        String title;
        switch (item.getItemId()) {
            case R.id.action_search:
                Bundle bundleSearch = new Bundle();
                bundleSearch.putString("actionName", "Search");
                fragment = new HomeFragment();
                fragment.setArguments(bundleSearch);
                title = getString(R.string.title_home);

                if (fragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment, title);
                    fragmentTransaction.commit();
                    tvToolbar.setText(title);
                }


                return true;
            case R.id.action_filter:
                if (dlLeftMenu.isDrawerOpen(GravityCompat.END)) {
                    dlLeftMenu.closeDrawer(GravityCompat.END);
                } else {
                    dlLeftMenu.openDrawer(GravityCompat.END);
                }
                return true;
            case R.id.action_location:
                try {
                    Bundle bundleLoc = new Bundle();
                    bundleLoc.putString("actionName", "Location");
                    bundleLoc.putString("lattitude", String.valueOf(location.getLatitude()));
                    bundleLoc.putString("longitude", String.valueOf(location.getLongitude()));
                    fragment = new HomeFragment();
                    fragment.setArguments(bundleLoc);
                    title = getString(R.string.title_home);
                if (fragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment, title);
                    fragmentTransaction.commit();
                    tvToolbar.setText(title);
                }
                }catch (Exception e){
                    e.printStackTrace();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    private void getLocation(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if (!isGPSEnabled && !isNetworkEnabled) {
                Toast.makeText(getApplicationContext(), getText(R.string.provider_failed), Toast.LENGTH_LONG).show();
            } else {
                location = null;
                if (isGPSEnabled) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_MIN_UPDATE_TIME, LOCATION_MIN_UPDATE_DISTANCE, locationListener);
                    location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, LOCATION_MIN_UPDATE_TIME, LOCATION_MIN_UPDATE_DISTANCE, locationListener);
                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
                if (location != null) {
                    // Utils.HitechToast(this,"Lattitude :"+location.getLatitude()+"Longitude:"+location.getLatitude());
                }
            }
        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 12);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 13);
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        getLocation();
    }


    @Override
    public void onSuccess(int id, String message, Object object) {
        if (id == Settings.INIT_ADD_SHP_ID){
            if(object!=null) {
                List<String> prodTypeName = new ArrayList<>();
                List<String> fabricTypeName = new ArrayList<>();
                List<String> partyType = new ArrayList<>();
                List<String> rankingNo = new ArrayList<>();
                map_prod_values = new HashMap<>();
                map_fab_values = new HashMap<>();
                map_rank_values = new HashMap<>();
                map_party_values = new HashMap<>();
                initAddShopData = ((InitAddShopData) object);
                if (initAddShopData.productTypeList != null && initAddShopData.productTypeList.size() > 0) {
                    for (int i = 0; i < initAddShopData.productTypeList.size(); i++) {
                        prodTypeName.add(initAddShopData.productTypeList.get(i).name);
                        map_prod_values.put(initAddShopData.productTypeList.get(i).productTypeId, initAddShopData.productTypeList.get(i).name);
                    }
                }

                if (initAddShopData.fabricList != null && initAddShopData.fabricList.size() > 0) {
                    for (int i = 0; i < initAddShopData.fabricList.size(); i++) {
                        fabricTypeName.add(initAddShopData.fabricList.get(i).name);
                        map_fab_values.put(initAddShopData.fabricList.get(i).febricTypeId, initAddShopData.fabricList.get(i).name);
                    }
                }

                if (initAddShopData.partyTypeList != null && initAddShopData.partyTypeList.size() > 0) {
                    for (int i = 0; i < initAddShopData.partyTypeList.size(); i++) {
                        partyType.add(initAddShopData.partyTypeList.get(i).name);
                        map_party_values.put(initAddShopData.partyTypeList.get(i).partyTypeId, initAddShopData.partyTypeList.get(i).name);
                    }
                }

                if (initAddShopData.rankingList != null && initAddShopData.rankingList.size() > 0) {
                    for (int i = 0; i < initAddShopData.rankingList.size(); i++) {
                        rankingNo.add(initAddShopData.rankingList.get(i).name);
                        map_rank_values.put(initAddShopData.rankingList.get(i).rankingId, initAddShopData.rankingList.get(i).name);
                    }
                }

                /*For Expandable List for Filter*/
                listDataHeader = new ArrayList<String>();
                listDataChild = new HashMap<String, List<String>>();
                // Adding child data
                listDataHeader.add("Product Type");
                listDataHeader.add("Fabric Type");
                listDataHeader.add("Party Type");
                listDataHeader.add("Ranking");
                listDataChild.put(listDataHeader.get(0), prodTypeName); // Header, Child data
                listDataChild.put(listDataHeader.get(1), fabricTypeName);
                listDataChild.put(listDataHeader.get(2), partyType);
                listDataChild.put(listDataHeader.get(3), rankingNo);
                populateExpandList();

                Log.d("product >>>" , map_prod_values.toString());
                Log.d("fabric >>>", map_fab_values.toString());
                Log.d("pary > ", map_fab_values.toString());
                Log.d("ranking >", map_rank_values.toString());

            }
        }
    }

    @Override
    public void onError(String error, int id, BaseController.ErrorCode code) {

    }

    @Override
    public void onItemClick(ArrayList<String> product, ArrayList<String> fabric, ArrayList<String> party, ArrayList<String> rank) {

        finalProd=new ArrayList<>();
        finalfabric=new ArrayList<>();
        finalParty=new ArrayList<>();
        finalRank=new ArrayList<>();
        finalProd.addAll(product);
        finalfabric.addAll(fabric);
        finalParty.addAll(party);
        finalRank.addAll(rank);

        Log.d("final Prod > ", finalProd.toString());
        Log.d(" final fab > ", finalfabric.toString());
        Log.d("final party >", finalParty.toString());
        Log.d("fianl rank >", finalRank.toString());

    }


    private void locationTimer(){


        startService(new Intent(this, SendLocService.class));

      //  timerAsync.schedule(timerTaskAsync, 0, interval);

        Timer timerAsync = new Timer();

        TimerTask timerTaskAsync = new TimerTask() {

            @Override

            public void run() {

                runOnUiThread(new Runnable() {

                    @Override public void run() {

                        Log.d("repeat","after each 10 sec");

//call web service here to repeat


                        try {
                            new SendLocServiceDataParser().executeQuery(MainActivity.this, QueryBuilder.getInstance().sendLocationServiceAPI(), QueryBuilder.getInstance().generateSendLocationServiceQuery(MainActivity.this,String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude())), new SendLocServiceDataParser.onCheckURLListener() {

                                @Override

                                public void onSuccess(int id, String message, Object object) {

                                    if (object != null) {

                                    }

                                }

                                @Override

                                public void onError(String error, int id, BaseController.ErrorCode code) {

                                }

                            }, Settings.SEND_LOCATION_SERVICE_ID, true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                });

            }

        };

        timerAsync.schedule(timerTaskAsync, 0, interval);



    }


}
