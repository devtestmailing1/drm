package com.enquiry.kunalsingh.customerenquiry.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.enquiry.kunalsingh.customerenquiry.BuildConfig;
import com.enquiry.kunalsingh.customerenquiry.R;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.drawer.MainActivity;
import com.enquiry.kunalsingh.customerenquiry.parser.InitAddShopData;
import com.enquiry.kunalsingh.customerenquiry.parser.InitAddShopParser;
import com.enquiry.kunalsingh.customerenquiry.utility.PathUtils;
import com.enquiry.kunalsingh.customerenquiry.utility.PrefManager;
import com.enquiry.kunalsingh.customerenquiry.utility.QueryBuilder;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.enquiry.kunalsingh.customerenquiry.utility.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


/**
 * Created by Kunal Singh on 12-08-2018.
 */
public class AddShopActivity extends AppCompatActivity implements InitAddShopParser.onInitShopDataListener, View.OnClickListener {
    private Spinner spinnerArea,spinerProdType,spinerProductFabric,spinerPartyType,spinerRanking;
    private EditText edtShopName,edtOwnerName,edtOwnerMob,edtPurchaserName,edtPurchaserNumber,edtEmailID,edtTelephoneNo,edtWebsite,edtAccountantName,edtAccountantNumber,edtProductType,edtProductFabric,edtPartyType,edtRanking,edtShopAdd,edtPin;
    private Button btnAddShop;
    private TextView tvCamera;
    static String viewFileName = "";
    public static int REQUEST_IMAGE = 100;
    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    String imagePath,cammeraName;
    String filePath = null, ext = "", remarks="";
    InitAddShopData initAddShopData;
    Map<Integer,String> map_area_values = new HashMap<Integer, String>();
    Map<Integer,String> map_prod_type_values = new HashMap<Integer, String>();
    Map<Integer,String> map_fabric_type_values = new HashMap<Integer, String>();
    Map<Integer,String> map_party_type_values = new HashMap<Integer, String>();
    Map<Integer,String> map_Ranking_No_values = new HashMap<Integer, String>();
    private int selProdID,selFabricID,selPartyType,selRankingNo;


    private LocationManager locationManager;
    private Location location = null;
    private static final int LOCATION_MIN_UPDATE_TIME = 10;
    private static final int LOCATION_MIN_UPDATE_DISTANCE = 1000;

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            locationManager.removeUpdates(locationListener);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }


        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }

    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_shop);
        Toolbar toolbar = findViewById(R.id.header);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("Add Shop");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        initialisation();
        btnAddShop.setOnClickListener(this);
        tvCamera.setOnClickListener(this);
        new InitAddShopParser().executeQuery(this, QueryBuilder.getInstance().getInitAddShopAPI(), QueryBuilder.getInstance().generateInitAddShopQuery(this, PrefManager.getKeyAuthToken(this).trim()), this, Settings.INIT_ADD_SHP_ID, true);
    }

    private void initialisation() {
        btnAddShop=findViewById(R.id.btnAddShop);
        tvCamera=findViewById(R.id.tvCamera);
        spinnerArea=findViewById(R.id.spinerArea);
        spinerProdType=findViewById(R.id.spinerProdType);
        spinerProductFabric=findViewById(R.id.spinerProductFabric);
        spinerPartyType=findViewById(R.id.spinerPartyType);
        spinerRanking=findViewById(R.id.spinerRanking);
        edtShopName=findViewById(R.id.edtShopName);
        edtOwnerName=findViewById(R.id.edtOwnerName);
        edtOwnerMob=findViewById(R.id.edtOwnerMob);
        edtPurchaserName=findViewById(R.id.edtPurchaserName);
        edtPurchaserNumber=findViewById(R.id.edtPurchaserNumber);
        edtEmailID=findViewById(R.id.edtEmailID);
        edtTelephoneNo=findViewById(R.id.edtTelephoneNo);
        edtWebsite=findViewById(R.id.edtWebsite);
        edtAccountantName=findViewById(R.id.edtAccountantName);
        edtAccountantNumber=findViewById(R.id.edtAccountantNumber);
        edtPin=findViewById(R.id.edtPin);
        //edtState.setText(PrefManager.getKeyUserState(AddShopActivity.this));
        //edtCity.setText(PrefManager.getKeyUserCity(AddShopActivity.this));
    }

    @Override
    public void onSuccess(int id, String message, Object object) {

        if (id == Settings.INIT_ADD_SHP_ID){
            if(object!=null) {
                List<String> prodTypeName = new ArrayList<>();
                List<String> fabricTypeName = new ArrayList<>();
                List<String> partyType = new ArrayList<>();
                List<String> rankingNo = new ArrayList<>();
                List<String> area=new ArrayList<>();

                prodTypeName.add("Select Product Type");
                fabricTypeName.add("Select Fabric");
                partyType.add("Select Party Type");
                rankingNo.add("Select Ranking Number");
                area.add("Select Area");

                initAddShopData = ((InitAddShopData) object);
                if (initAddShopData.areaList != null && initAddShopData.areaList.size() > 0) {
                    for (int i = 0; i < initAddShopData.areaList.size(); i++) {
                        area.add(initAddShopData.areaList.get(i).localityName);
                        map_area_values.put(initAddShopData.areaList.get(i).localityId, initAddShopData.areaList.get(i).localityName);
                    }
                    fillAreaSpinner(area);
                }

                if (initAddShopData.productTypeList != null && initAddShopData.productTypeList.size() > 0) {
                    for (int i = 0; i < initAddShopData.productTypeList.size(); i++) {
                        prodTypeName.add(initAddShopData.productTypeList.get(i).name);
                        map_prod_type_values.put(initAddShopData.productTypeList.get(i).productTypeId, initAddShopData.productTypeList.get(i).name);
                    }
                    fillProdTypeSpinner(prodTypeName);
                }

                if (initAddShopData.fabricList != null && initAddShopData.fabricList.size() > 0) {
                    for (int i = 0; i < initAddShopData.fabricList.size(); i++) {
                        fabricTypeName.add(initAddShopData.fabricList.get(i).name);
                       // map_fabric_type_values.put(initAddShopData.fabricList.get(i).febricTypeId, initAddShopData.fabricList.get(i).name);
                    }
                    fillFabricTypeSpinner(fabricTypeName);
                }

                if (initAddShopData.partyTypeList != null && initAddShopData.partyTypeList.size() > 0) {
                    for (int i = 0; i < initAddShopData.partyTypeList.size(); i++) {
                        partyType.add(initAddShopData.partyTypeList.get(i).name);
                        map_party_type_values.put(initAddShopData.partyTypeList.get(i).partyTypeId, initAddShopData.partyTypeList.get(i).name);
                    }
                    fillPartyTypeSpinner(partyType);
                }

                if (initAddShopData.rankingList != null && initAddShopData.rankingList.size() > 0) {
                    for (int i = 0; i < initAddShopData.rankingList.size(); i++) {
                        rankingNo.add(initAddShopData.rankingList.get(i).name);
                        map_Ranking_No_values.put(initAddShopData.rankingList.get(i).rankingId, initAddShopData.rankingList.get(i).name);
                    }
                    fillRankingNoSpinner(rankingNo);
                }
            }
        }

        if (id == Settings.SUBMIT_ADD_SHOP_ID) {
            Utils.HitechToast(this,message);
            startActivity(new Intent(this, MainActivity.class));
            finishAffinity();

        }
    }




    @Override
    public void onError(String error, int id, BaseController.ErrorCode code) {
        if(error!=null){
            Utils.HitechToast(this,error);
        }

    }






    public void fillAreaSpinner(List<String> areaList){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, areaList);
        spinnerArea.setAdapter(adapter);
        //  selectSpinnerValue(spinnerState,defaultState);
        spinnerArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinnerArea.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select Area"))) {
                    for (Map.Entry<Integer, String> areaEntry : map_area_values.entrySet()) {
                        if (areaEntry.getValue().equalsIgnoreCase(value)) {
                            selProdID = areaEntry.getKey();
                            Log.v("ProdId----------", "" + areaEntry.getKey());
                        }
                    }
                }else{

                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void fillProdTypeSpinner(List<String> prodTypeList){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, prodTypeList);
        spinerProdType.setAdapter(adapter);
        //  selectSpinnerValue(spinnerState,defaultState);
        spinerProdType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinerProdType.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select Product Type"))) {
                    for (Map.Entry<Integer, String> prodEntry : map_prod_type_values.entrySet()) {
                        if (prodEntry.getValue().equalsIgnoreCase(value)) {
                            selProdID = prodEntry.getKey();
                            Log.v("ProdId----------", "" + prodEntry.getKey());
                        }
                    }
                }else{

                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void fillFabricTypeSpinner(List<String> fabricTypeList){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, fabricTypeList);
        spinerProductFabric.setAdapter(adapter);
        //  selectSpinnerValue(spinnerState,defaultState);
        spinerProductFabric.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinerProductFabric.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select Fabric"))) {
                    for (Map.Entry<Integer, String> fabricEntry : map_fabric_type_values.entrySet()) {
                        if (fabricEntry.getValue().equalsIgnoreCase(value)) {
                            selFabricID = fabricEntry.getKey();
                            Log.v("FabricId----------", "" + fabricEntry.getKey());
                        }
                    }
                }else{

                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void fillPartyTypeSpinner(List<String> partTypeList){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, partTypeList);
        spinerPartyType.setAdapter(adapter);
        spinerPartyType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinerPartyType.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select Party Type"))) {
                    for (Map.Entry<Integer, String> partyEntry : map_party_type_values.entrySet()) {
                        if (partyEntry.getValue().equalsIgnoreCase(value)) {
                            selPartyType = partyEntry.getKey();
                            Log.v("PartyId----------", "" + partyEntry.getKey());
                        }
                    }
                }else{

                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void fillRankingNoSpinner(List<String> rankingNumberList){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_dropdown_item, rankingNumberList);
        spinerRanking.setAdapter(adapter);
        spinerRanking.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinerRanking.getSelectedItem().toString();
                if(!(value.equalsIgnoreCase("Select Ranking Number"))) {
                    for (Map.Entry<Integer, String> rankingEntry : map_Ranking_No_values.entrySet()) {
                        if (rankingEntry.getValue().equalsIgnoreCase(value)) {
                            selRankingNo = rankingEntry.getKey();
                            Log.v("RankingId----------", "" + rankingEntry.getKey());
                        }
                    }
                }else{

                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnAddShop:
                if(edtShopName.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter shop name.");
                }else if(edtOwnerName.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter owner name.");
                }else if(edtOwnerMob.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter owner mob. no.");
                }else if(edtShopAdd.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter shop address.");
                }/*else if(edtMohalla.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter mohalla name");
                }else if(spinnerArea.getSelectedItem().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please select area name");
                }else if(edtState.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter state name");
                }else if(edtPin.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter pin code");
                }else if(edtCountry.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter country name");
                }*/else if(edtPurchaserName.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter purchaser name.");
                }else if(edtPurchaserNumber.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter purchaser number.");
                }else if(edtEmailID.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter email id.");
                }else if(!Utils.isValidEmail(edtEmailID.getText().toString().toLowerCase().trim())) {
                    Utils.HitechToast(this,"Please enter valid email.");
                }else if(edtTelephoneNo.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter telephone number.");
                }else if(edtWebsite.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter website.");
                }else if(!Utils.isValidUrl(edtWebsite.getText().toString().toLowerCase().trim())) {
                    Utils.HitechToast(this,"Please enter valid website.");
                }else if(edtAccountantName.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter accountant name.");
                }else if(edtAccountantNumber.getText().toString().equalsIgnoreCase("")){
                    Utils.HitechToast(this,"Please enter accountant number.");
                }else if(spinerProdType.getSelectedItem().toString().equalsIgnoreCase("Select Product Type")){
                    Utils.HitechToast(this,"Please select product type");
                }else if(spinerProductFabric.getSelectedItem().toString().equalsIgnoreCase("Select Fabric")){
                    Utils.HitechToast(this,"Please select fabric");
                }else if(spinerPartyType.getSelectedItem().toString().equalsIgnoreCase("Select Party Type")){
                    Utils.HitechToast(this,"Please select party type");
                }else if(spinerRanking.getSelectedItem().toString().equalsIgnoreCase("Select Ranking Number")){
                    Utils.HitechToast(this,"Please select ranking number");
                }else{
             //       new InitAddShopParser().executeQuery(this, QueryBuilder.getInstance().getSubmitAddShopAPI(), QueryBuilder.getInstance().generateSubmitAddShopQuery(this, PrefManager.getKeyAuthToken(this).trim(),edtShopName.getText().toString().trim(),edtOwnerName.getText().toString().trim(),edtOwnerMob.getText().toString().trim(),edtMohalla.getText().toString().trim(),spinnerArea.getSelectedItem().toString().trim(),edtState.getText().toString().trim(),edtCountry.getText().toString().trim(),edtPin.getText().toString(),edtPurchaserName.getText().toString().trim(),edtPurchaserNumber.getText().toString().trim(),edtEmailID.getText().toString().trim(),edtTelephoneNo.getText().toString().trim(),edtWebsite.getText().toString().trim(),edtAccountantName.getText().toString().trim(),edtAccountantNumber.getText().toString().trim(),String.valueOf(selProdID),String.valueOf(selFabricID),String.valueOf(selPartyType),String.valueOf(selRankingNo),"28.6666","97.73838","Gurgaon"), this, Settings.SUBMIT_ADD_SHOP_ID, true);
                }
                break;
            case R.id.tvCamera:
                if (!hasPermissions(AddShopActivity.this, PERMISSIONS)) {
                    ActivityCompat.requestPermissions(AddShopActivity.this, PERMISSIONS, PERMISSION_ALL);
                } else {
                    // openFile();
                    uploadBreDocs();
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void uploadBreDocs(){
        final Dialog dialogBuilder = new Dialog(this);
        dialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        dialogBuilder.setContentView(R.layout.dialog_upload_doc);
        final TextView tvCamera =  dialogBuilder.findViewById(R.id.tvCamera);
        final TextView tvInteralStorage = dialogBuilder.findViewById(R.id.tvInteralStorage);
        final Button btnClose =   dialogBuilder.findViewById(R.id.close);

        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(createImageFile()));
                startActivityForResult(intent, REQUEST_IMAGE);
                dialogBuilder.dismiss();
            }
        });

        tvInteralStorage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFile();
                dialogBuilder.dismiss();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBuilder.dismiss();
            }
        });
        dialogBuilder.show();
    }

    private File createImageFile() {
        String mCurrentPhotoPath="";
        File storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        //  path = path + (timeStamp + "1jpg");

        File file=null;
        try {
            file = File.createTempFile(timeStamp, ".jpg", storageDir);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (Build.VERSION.SDK_INT >= 24)
            mCurrentPhotoPath = String.valueOf(FileProvider.getUriForFile(this,
                    BuildConfig.APPLICATION_ID + ".provider", file));
        else
            mCurrentPhotoPath = String.valueOf(Uri.fromFile(file));
        cammeraName="shopImage"+timeStamp;
        return file;
    }

    public void openFile() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.setType("*/*");
        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 00);
        } catch (ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Utils.HitechToast(this, " Please install a File Manager.");
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
          if (requestCode == 00) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    Uri uri = data.getData();
                    String uriString = uri.toString();
                    File myFile = new File(uriString);
                    filePath = PathUtils.getPath(this, uri);
                    String displayName = null;
                    if (uriString.startsWith("content://")) {
                        Cursor cursor = null;
                        try {
                            cursor = getContentResolver().query(uri, null, null, null, null);
                            if (cursor != null && cursor.moveToFirst()) {
                                displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                            }
                        } finally {
                            cursor.close();
                        }
                    } else if (uriString.startsWith("file://")) {
                        displayName = myFile.getName();
                    }

                    if (displayName != null) {
                        ext = displayName.substring(displayName.lastIndexOf(".") + 1, displayName.length());
                    }
                    if (displayName.contains(".pdf") || displayName.contains(".doc") || displayName.contains(".docx") || displayName.contains(".png") || displayName.contains(".jpg")) {
                        /*uploadData = new ArrayList<UploadBreData>();
                        UploadBreData uploadBreData = new UploadBreData();
                        uploadBreData.setFileName(displayName);
                        uploadBreData.setFileSize("9");
                        uploadBreData.setFileData(filePath);
                        uploadData.add(uploadBreData);
                        new ReimbursementDataParser().executeBREAttachment(ActivitySubmitReimbursement.this,
                                QueryBuilder.getInstance().uploadBreAPI(ActivitySubmitReimbursement.this),
                                QueryBuilder.getInstance().uploadBreQuery(ActivitySubmitReimbursement.this, Integer.toString(populateSubmitData.reimburseMentID), uploadData),
                                this,
                                Settings.UPLOAD_BRE_DOC_ID,
                                true);*/
                        Utils.HitechToast(this,"Please wait...."+displayName+" is uploading.");
                    } else {
                        Utils.HitechToast(this, "Invalid file format. You can only upload pdf, doc, docx, jpg or png format only.");
                    }
                }
            }
        } else if (requestCode == REQUEST_IMAGE && resultCode == Activity.RESULT_OK) {
            try {
                FileInputStream in = new FileInputStream(createImageFile());
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 10;
                imagePath = createImageFile().getAbsolutePath();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
         /*   uploadData = new ArrayList<UploadBreData>();
            UploadBreData uploadBreData = new UploadBreData();
            uploadBreData.setFileName(cammeraName);
            uploadBreData.setFileSize("9");
            uploadBreData.setFileData(imagePath);
            uploadData.add(uploadBreData);
            new ReimbursementDataParser().executeBREAttachment(ActivitySubmitReimbursement.this,
                    QueryBuilder.getInstance().uploadBreAPI(ActivitySubmitReimbursement.this),
                    QueryBuilder.getInstance().uploadBreQuery(ActivitySubmitReimbursement.this, Integer.toString(populateSubmitData.reimburseMentID), uploadData),
                    this,
                    Settings.UPLOAD_BRE_DOC_ID,
                    true);*/
         Utils.HitechToast(this,"Please wait...."+cammeraName+" is uploading.");
        }
    }




    public static String getPostalCodeByCoordinates(Context context, double lat, double lon) throws IOException {

        Geocoder mGeocoder = new Geocoder(context, Locale.getDefault());
        String zipcode=null;
        Address address=null;

        if (mGeocoder != null) {

            List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 5);

            if (addresses != null && addresses.size() > 0) {

                for (int i = 0; i < addresses.size(); i++) {
                    address = addresses.get(i);
                    if (address.getPostalCode() != null) {
                        zipcode = address.getPostalCode();
                        Log.d("TAG", "Postal code: " + address.getPostalCode());
                        break;
                    }

                }
                return zipcode;
            }
        }

        return null;
    }

    private void getLocation(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if (!isGPSEnabled && !isNetworkEnabled) {
                Toast.makeText(getApplicationContext(), getText(R.string.provider_failed), Toast.LENGTH_LONG).show();
            } else {
                location = null;
                if (isGPSEnabled) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_MIN_UPDATE_TIME, LOCATION_MIN_UPDATE_DISTANCE, locationListener);
                    location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, LOCATION_MIN_UPDATE_TIME, LOCATION_MIN_UPDATE_DISTANCE, locationListener);
                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
                if (location != null) {
                    // Utils.HitechToast(this,"Lattitude :"+location.getLatitude()+"Longitude:"+location.getLatitude());
                }
            }
        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 12);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 13);
            }
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        getLocation();
        try {
            edtPin.setText(getPostalCodeByCoordinates(this,location.getLatitude(),location.getLongitude()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
