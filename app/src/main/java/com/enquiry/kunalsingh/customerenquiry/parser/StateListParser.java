package com.enquiry.kunalsingh.customerenquiry.parser;

import android.support.v4.app.FragmentActivity;

import com.android.volley.Request;
import com.enquiry.kunalsingh.customerenquiry.controller.BaseController;
import com.enquiry.kunalsingh.customerenquiry.utility.Settings;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kunal Singh on 15-05-2018.
 */
public class StateListParser extends BaseController {

    public StateListParser.onStateListListener listener;
    public int id;
    public String TAG_MESSAGE = "message";

    public StateListParser executeQuery(FragmentActivity context, String url, JSONObject json, StateListParser.onStateListListener listener, int id, boolean toShowDialog) {
        // RequestManager.allowAllSSL();
        this.listener = listener;
        this.id = id;
        init(context, url, Request.Method.POST, json, toShowDialog);

        return null;
    }

    @Override
    public void onComplete(JSONArray response, String message) {
        switch (id) {
            case Settings.STATE_LIST_ID:
                listener.onSuccess(id, message, null);
                if (response != null) {
                    try {
                        listener.onSuccess(id, "", parserUserInfo(response.getJSONObject(0)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                break;
            case Settings.COUNTRY_LIST_ID:
                if (response != null) {
                    try {
                        listener.onSuccess(id, "", parserUserInformation(response.getJSONObject(0)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                break;


        }
    }

    @Override
    public void onSuccess(JSONArray response) {

    }

    @Override
    public void onFailureData(String error, ErrorCode code, JSONArray response) {

    }

    @Override
    public void onFailure(String error, ErrorCode code) {
        if (error != null) {
            listener.onError(error, id, code);
        }
    }


    public interface onStateListListener {
        public void onSuccess(int id, String message, Object object);
        public void onError(String error, int id, ErrorCode code);
    }


    public CountryListData parserUserInformation(JSONObject object) {
        Gson gson = new Gson();
        CountryListData response = gson.fromJson(object.toString(), CountryListData.class);
        return response;
    }


    public StateListData parserUserInfo(JSONObject object) {
        Gson gson = new Gson();
        StateListData response = gson.fromJson(object.toString(), StateListData.class);
        return response;
    }
}
