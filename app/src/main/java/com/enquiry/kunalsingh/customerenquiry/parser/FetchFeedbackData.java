package com.enquiry.kunalsingh.customerenquiry.parser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Kunal Singh on 27-08-2018.
 */
public class FetchFeedbackData {

    @Expose
    @SerializedName("feedbackList")
    public List<FeedbackList> feedbackList;

    public static class FeedbackList {
        @Expose
        @SerializedName("toDate")
        public String toDate;
        @Expose
        @SerializedName("fromDate")
        public String fromDate;
        @Expose
        @SerializedName("remarks")
        public String remarks;
        @Expose
        @SerializedName("callDuration")
        public String callDuration;
    }
}
