package com.enquiry.kunalsingh.customerenquiry.adapter;

import java.util.ArrayList;

/**
 * Created by Kunal Singh on 15-09-2018.
 */
public class ShopAreas {
    private String date,area;
    private ArrayList<ShopLists> eventsArrayList;
    private ArrayList<ShopLists> areaArrayList;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public ArrayList<ShopLists> getEventsArrayList() {
        return eventsArrayList;
    }

    public void setEventsArrayList(ArrayList<ShopLists> eventsArrayList) {
        this.eventsArrayList = eventsArrayList;
    }

    public ArrayList<ShopLists> getAreaArrayList() {
        return areaArrayList;
    }

    public void setAreaArrayList(ArrayList<ShopLists> areaArrayList) {
        this.areaArrayList = areaArrayList;
    }
}
