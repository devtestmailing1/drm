package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kunal Singh on 16-05-2018.
 */
public class ServiceBookingListData implements Parcelable{

    @Expose
    @SerializedName("existingBookingList")
    public List<ExistingBookingList> existingBookingList;
    @Expose
    @SerializedName("imeiNumber")
    public String imeiNumber;
    @Expose
    @SerializedName("deviceModel")
    public String deviceModel;
    @Expose
    @SerializedName("osName")
    public String osName;
    @Expose
    @SerializedName("appVersion")
    public String appVersion;
    @Expose
    @SerializedName("mobileNumber")
    public String mobileNumber;

    protected ServiceBookingListData(Parcel in) {
        existingBookingList= new ArrayList<ExistingBookingList>();
        in.readList(existingBookingList,ExistingBookingList.class.getClassLoader());
        imeiNumber = in.readString();
        deviceModel = in.readString();
        osName = in.readString();
        appVersion = in.readString();
        mobileNumber = in.readString();
    }

    public static final Creator<ServiceBookingListData> CREATOR = new Creator<ServiceBookingListData>() {
        @Override
        public ServiceBookingListData createFromParcel(Parcel in) {
            return new ServiceBookingListData(in);
        }

        @Override
        public ServiceBookingListData[] newArray(int size) {
            return new ServiceBookingListData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(existingBookingList);
        parcel.writeString(imeiNumber);
        parcel.writeString(deviceModel);
        parcel.writeString(osName);
        parcel.writeString(appVersion);
        parcel.writeString(mobileNumber);
    }

    public static class ExistingBookingList implements Parcelable{
        @Expose
        @SerializedName("appStatus")
        public String appStatus;
        @Expose
        @SerializedName("branch_id")
        public int branch_id;
        @Expose
        @SerializedName("appointmentDate")
        public String appointmentDate;
        @Expose
        @SerializedName("appointmentNumber")
        public String appointmentNumber;
        @Expose
        @SerializedName("vehicleNumber")
        public String vehicleNumber;
        @Expose
        @SerializedName("branchLocation")
        public String branchLocation;
        @Expose
        @SerializedName("dealerName")
        public String dealerName;

        protected ExistingBookingList(Parcel in) {
            branch_id = in.readInt();
            appointmentDate = in.readString();
            appointmentNumber = in.readString();
            vehicleNumber = in.readString();
            branchLocation = in.readString();
            dealerName = in.readString();
        }

        public static final Creator<ExistingBookingList> CREATOR = new Creator<ExistingBookingList>() {
            @Override
            public ExistingBookingList createFromParcel(Parcel in) {
                return new ExistingBookingList(in);
            }

            @Override
            public ExistingBookingList[] newArray(int size) {
                return new ExistingBookingList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(appStatus);
            parcel.writeInt(branch_id);
            parcel.writeString(appointmentDate);
            parcel.writeString(appointmentNumber);
            parcel.writeString(vehicleNumber);
            parcel.writeString(branchLocation);
            parcel.writeString(dealerName);
        }
    }
}
