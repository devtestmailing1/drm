package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LocationLevelData implements Parcelable {


    @SerializedName("countryList")
    @Expose
    private List<CountryList> countryList = null;
    @SerializedName("stateList")
    @Expose
    private List<StateList> stateList = null;
    @SerializedName("cityList")
    @Expose
    private List<CityList> cityList = null;
    @SerializedName("localityList")
    @Expose
    private List<LocalityList> localityList = null;
    @SerializedName("mohallaList")
    @Expose
    private List<MohallaList> mohallaList = null;
    public final static Parcelable.Creator<LocationLevelData> CREATOR = new Creator<LocationLevelData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public LocationLevelData createFromParcel(Parcel in) {
            return new LocationLevelData(in);
        }

        public LocationLevelData[] newArray(int size) {
            return (new LocationLevelData[size]);
        }

    };

    protected LocationLevelData(Parcel in) {
        in.readList(this.countryList, (CountryList.class.getClassLoader()));
        in.readList(this.stateList, (StateList.class.getClassLoader()));
        in.readList(this.cityList, (CityList.class.getClassLoader()));
        in.readList(this.localityList, (LocalityList.class.getClassLoader()));
        in.readList(this.mohallaList, (MohallaList.class.getClassLoader()));
    }

    public LocationLevelData() {
    }

    public List<CountryList> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<CountryList> countryList) {
        this.countryList = countryList;
    }

    public List<StateList> getStateList() {
        return stateList;
    }

    public void setStateList(List<StateList> stateList) {
        this.stateList = stateList;
    }

    public List<CityList> getCityList() {
        return cityList;
    }

    public void setCityList(List<CityList> cityList) {
        this.cityList = cityList;
    }


    public List<LocalityList> getLocalityList() {
        return localityList;
    }

    public void setLocalityList(List<LocalityList> localityList) {
        this.localityList = localityList;
    }


    public List<MohallaList> getMohollaList() {
        return mohallaList;
    }

    public void setMohallaList(List<MohallaList> mohallaList) {
        this.mohallaList = mohallaList;
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(countryList);
        dest.writeList(stateList);
        dest.writeList(cityList);
        dest.writeList(localityList);
        dest.writeList(mohallaList);
    }

    public int describeContents() {
        return 0;
    }


    public static class CountryList implements Parcelable {

        @SerializedName("countryId")
        @Expose
        private Integer countryId;
        @SerializedName("countryName")
        @Expose
        private String countryName;
        public final static Parcelable.Creator<CountryList> CREATOR = new Creator<CountryList>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public CountryList createFromParcel(Parcel in) {
                return new CountryList(in);
            }

            public CountryList[] newArray(int size) {
                return (new CountryList[size]);
            }

        };

        protected CountryList(Parcel in) {
            this.countryId = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.countryName = ((String) in.readValue((String.class.getClassLoader())));
        }

        public CountryList() {
        }

        public Integer getCountryId() {
            return countryId;
        }

        public void setCountryId(Integer countryId) {
            this.countryId = countryId;
        }

        public String getCountryName() {
            return countryName;
        }

        public void setCountryName(String countryName) {
            this.countryName = countryName;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(countryId);
            dest.writeValue(countryName);
        }

        public int describeContents() {
            return 0;
        }

    }


    public static class StateList implements Parcelable {

        @SerializedName("stateId")
        @Expose
        private Integer stateId;
        @SerializedName("stateDesc")
        @Expose
        private String stateDesc;
        public final static Parcelable.Creator<StateList> CREATOR = new Creator<StateList>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public StateList createFromParcel(Parcel in) {
                return new StateList(in);
            }

            public StateList[] newArray(int size) {
                return (new StateList[size]);
            }

        };

        protected StateList(Parcel in) {
            this.stateId = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.stateDesc = ((String) in.readValue((String.class.getClassLoader())));
        }

        public StateList() {
        }

        public Integer getStateId() {
            return stateId;
        }

        public void setStateId(Integer stateId) {
            this.stateId = stateId;
        }

        public String getStateDesc() {
            return stateDesc;
        }

        public void setStateDesc(String stateDesc) {
            this.stateDesc = stateDesc;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(stateId);
            dest.writeValue(stateDesc);
        }

        public int describeContents() {
            return 0;
        }

    }




    public static class CityList implements Parcelable
    {

        @SerializedName("cityId")
        @Expose
        private Integer cityId;
        @SerializedName("cityDesc")
        @Expose
        private String cityDesc;
        public final static Parcelable.Creator<CityList> CREATOR = new Creator<CityList>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public CityList createFromParcel(Parcel in) {
                return new CityList(in);
            }

            public CityList[] newArray(int size) {
                return (new CityList[size]);
            }

        }
                ;

        protected CityList(Parcel in) {
            this.cityId = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.cityDesc = ((String) in.readValue((String.class.getClassLoader())));
        }

        public CityList() {
        }

        public Integer getCityId() {
            return cityId;
        }

        public void setCityId(Integer cityId) {
            this.cityId = cityId;
        }

        public String getCityDesc() {
            return cityDesc;
        }

        public void setCityDesc(String cityDesc) {
            this.cityDesc = cityDesc;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(cityId);
            dest.writeValue(cityDesc);
        }

        public int describeContents() {
            return 0;
        }

    }



    public static class LocalityList implements Parcelable
    {

        @SerializedName("localityId")
        @Expose
        private Integer localityId;
        @SerializedName("localityDesc")
        @Expose
        private String localityDesc;
        public final static Parcelable.Creator<LocalityList> CREATOR = new Creator<LocalityList>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public LocalityList createFromParcel(Parcel in) {
                return new LocalityList(in);
            }

            public LocalityList[] newArray(int size) {
                return (new LocalityList[size]);
            }

        }
                ;

        protected LocalityList(Parcel in) {
            this.localityId = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.localityDesc = ((String) in.readValue((String.class.getClassLoader())));
        }

        public LocalityList() {
        }

        public Integer getLocalityId() {
            return localityId;
        }

        public void setLocalityId(Integer localityId) {
            this.localityId = localityId;
        }

        public String getLocalityDesc() {
            return localityDesc;
        }

        public void setLocalityDesc(String localityDesc) {
            this.localityDesc = localityDesc;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(localityId);
            dest.writeValue(localityDesc);
        }

        public int describeContents() {
            return 0;
        }

    }



    public static class MohallaList implements Parcelable
    {

        @SerializedName("mohallaId")
        @Expose
        private Integer mohallaId;
        @SerializedName("mohallaDesc")
        @Expose
        private String mohallaDesc;
        public final static Parcelable.Creator<MohallaList> CREATOR = new Creator<MohallaList>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public MohallaList createFromParcel(Parcel in) {
                return new MohallaList(in);
            }

            public MohallaList[] newArray(int size) {
                return (new MohallaList[size]);
            }

        }
                ;

        protected MohallaList(Parcel in) {
            this.mohallaId = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.mohallaDesc = ((String) in.readValue((String.class.getClassLoader())));
        }

        public MohallaList() {
        }

        public Integer getMohallaId() {
            return mohallaId;
        }

        public void setMohallaId(Integer mohallaId) {
            this.mohallaId = mohallaId;
        }

        public String getMohallaDesc() {
            return mohallaDesc;
        }

        public void setMohallaDesc(String mohallaDesc) {
            this.mohallaDesc = mohallaDesc;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(mohallaId);
            dest.writeValue(mohallaDesc);
        }

        public int describeContents() {
            return 0;
        }

    }



}
