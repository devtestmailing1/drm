package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Kunal Singh on 24-08-2018.
 */
public class PunchInOutData implements Parcelable{

    @Expose
    @SerializedName("punchDetails")
    public List<PunchDetails> punchDetails;

    protected PunchInOutData(Parcel in) {
    }

    public static final Creator<PunchInOutData> CREATOR = new Creator<PunchInOutData>() {
        @Override
        public PunchInOutData createFromParcel(Parcel in) {
            return new PunchInOutData(in);
        }

        @Override
        public PunchInOutData[] newArray(int size) {
            return new PunchInOutData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }

    public static class PunchDetails implements Parcelable{
        @Expose
        @SerializedName("punchOutTime")
        public String punchOutTime;
        @Expose
        @SerializedName("punchInTime")
        public String punchInTime;

        protected PunchDetails(Parcel in) {
            punchOutTime = in.readString();
            punchInTime = in.readString();
        }

        public static final Creator<PunchDetails> CREATOR = new Creator<PunchDetails>() {
            @Override
            public PunchDetails createFromParcel(Parcel in) {
                return new PunchDetails(in);
            }

            @Override
            public PunchDetails[] newArray(int size) {
                return new PunchDetails[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(punchOutTime);
            parcel.writeString(punchInTime);
        }
    }
}
