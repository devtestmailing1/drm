package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Kunal Singh on 12-08-2018.
 */
public class InitAddShopData implements Parcelable {

    @Expose
    @SerializedName("rankingList")
    public List<RankingList> rankingList;
    @Expose
    @SerializedName("partyTypeList")
    public List<PartyTypeList> partyTypeList;
    @Expose
    @SerializedName("productTypeList")
    public List<ProductTypeList> productTypeList;
    @Expose
    @SerializedName("fabricList")
    public List<FabricList> fabricList;
    @Expose
    @SerializedName("areaList")
    public List<AreaList> areaList;
    @Expose
    @SerializedName("stateList")
    public List<StateList> stateList;
    @Expose
    @SerializedName("cityList")
    public List<CityList> cityList;
    @Expose
    @SerializedName("countryList")
    public List<CountryList> countryList;
    @Expose
    @SerializedName("shop_Id")
    public String shop_Id;


    protected InitAddShopData(Parcel in) {
        shop_Id = in.readString();
    }

    public static final Creator<InitAddShopData> CREATOR = new Creator<InitAddShopData>() {
        @Override
        public InitAddShopData createFromParcel(Parcel in) {
            return new InitAddShopData(in);
        }

        @Override
        public InitAddShopData[] newArray(int size) {
            return new InitAddShopData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(shop_Id);
    }

    public static class RankingList implements Parcelable {
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("rankingId")
        public int rankingId;

        protected RankingList(Parcel in) {
            name = in.readString();
            rankingId = in.readInt();
        }

        public static final Creator<RankingList> CREATOR = new Creator<RankingList>() {
            @Override
            public RankingList createFromParcel(Parcel in) {
                return new RankingList(in);
            }

            @Override
            public RankingList[] newArray(int size) {
                return new RankingList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(name);
            parcel.writeInt(rankingId);
        }
    }

    public static class PartyTypeList implements Parcelable {
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("selected")
        public String selected;
        @Expose
        @SerializedName("partyTypeId")
        public int partyTypeId;

        protected PartyTypeList(Parcel in) {
            name = in.readString();
            partyTypeId = in.readInt();
            selected = in.readString();
        }

        public static final Creator<PartyTypeList> CREATOR = new Creator<PartyTypeList>() {
            @Override
            public PartyTypeList createFromParcel(Parcel in) {
                return new PartyTypeList(in);
            }

            @Override
            public PartyTypeList[] newArray(int size) {
                return new PartyTypeList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(name);
            parcel.writeInt(partyTypeId);
            parcel.writeString(selected);
        }
    }

    public static class ProductTypeList {
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("selected")
        public String selected;
        @Expose
        @SerializedName("productTypeId")
        public int productTypeId;
    }

    public static class FabricList {
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("selected")
        public String selected;
        @Expose
        @SerializedName("fabricTypeId")
        public int febricTypeId;
    }

    public static class AreaList {
        @Expose
        @SerializedName("localityId")
        public int localityId;
        @Expose
        @SerializedName("localityName")
        public String localityName;
    }


    public static class StateList implements Parcelable {

        @SerializedName("stateID")
        @Expose
        private Integer stateID;
        @SerializedName("stateDesc")
        @Expose
        private String stateDesc;
        @SerializedName("countryID")
        @Expose
        private Integer countryID;
        public final static Parcelable.Creator<StateList> CREATOR = new Creator<StateList>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public StateList createFromParcel(Parcel in) {
                return new StateList(in);
            }

            public StateList[] newArray(int size) {
                return (new StateList[size]);
            }

        };

        protected StateList(Parcel in) {
            this.stateID = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.stateDesc = ((String) in.readValue((String.class.getClassLoader())));
            this.countryID = ((Integer) in.readValue((Integer.class.getClassLoader())));
        }


        public StateList() {
        }

        public Integer getStateID() {
            return stateID;
        }

        public void setStateID(Integer stateID) {
            this.stateID = stateID;
        }

        public String getStateDesc() {
            return stateDesc;
        }

        public void setStateDesc(String stateDesc) {
            this.stateDesc = stateDesc;
        }

        public Integer getCountryID() {
            return countryID;
        }

        public void setCountryID(Integer countryID) {
            this.countryID = countryID;
        }


        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(stateID);
            dest.writeValue(stateDesc);
            dest.writeValue(countryID);
        }

        public int describeContents() {
            return 0;
        }

    }


    public static class CityList implements Parcelable {

        @SerializedName("cityId")
        @Expose
        private Integer cityId;
        @SerializedName("cityDesc")
        @Expose
        private String cityDesc;
        @SerializedName("stateId")
        @Expose
        private Integer stateId;
        public final static Parcelable.Creator<CityList> CREATOR = new Creator<CityList>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public CityList createFromParcel(Parcel in) {
                return new CityList(in);
            }

            public CityList[] newArray(int size) {
                return (new CityList[size]);
            }

        };

        protected CityList(Parcel in) {
            this.cityId = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.cityDesc = ((String) in.readValue((String.class.getClassLoader())));
            this.stateId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        }


        public CityList() {
        }

        public Integer getCityId() {
            return cityId;
        }

        public void setCityId(Integer cityId) {
            this.cityId = cityId;
        }

        public String getCityDesc() {
            return cityDesc;
        }

        public void setCityDesc(String cityDesc) {
            this.cityDesc = cityDesc;
        }

        public Integer getStateId() {
            return stateId;
        }

        public void setStateId(Integer stateId) {
            this.cityId = stateId;
        }



        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(cityId);
            dest.writeValue(cityDesc);
        }

        public int describeContents() {
            return 0;
        }

    }


    public static class CountryList implements Parcelable
    {

        @SerializedName("countryID")
        @Expose
        private Integer countryID;
        @SerializedName("countryDesc")
        @Expose
        private String countryDesc;
        public final static Parcelable.Creator<CountryList> CREATOR = new Creator<CountryList>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public CountryList createFromParcel(Parcel in) {
                return new CountryList(in);
            }

            public CountryList[] newArray(int size) {
                return (new CountryList[size]);
            }

        }
                ;

        protected CountryList(Parcel in) {
            this.countryID = ((Integer) in.readValue((Integer.class.getClassLoader())));
            this.countryDesc = ((String) in.readValue((String.class.getClassLoader())));
        }

        public CountryList() {
        }

        public Integer getCountryID() {
            return countryID;
        }

        public void setCountryID(Integer countryID) {
            this.countryID = countryID;
        }

        public String getCountryDesc() {
            return countryDesc;
        }

        public void setCountryDesc(String countryDesc) {
            this.countryDesc = countryDesc;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(countryID);
            dest.writeValue(countryDesc);
        }

        public int describeContents() {
            return 0;
        }

    }


}
