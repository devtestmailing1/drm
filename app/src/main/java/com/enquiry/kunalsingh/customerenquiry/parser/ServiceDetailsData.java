package com.enquiry.kunalsingh.customerenquiry.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Kunal Singh on 21-05-2018.
 */
public class ServiceDetailsData implements Parcelable{

    @Expose
    @SerializedName("categoryList")
    public List<CategoryList> categoryList;

    @Expose
    @SerializedName("appoinmentAvailabilityList")
    public List<AppoinmentAvailabilityList> appoinmentAvailabilityList;
    @Expose
    @SerializedName("jsonObjectList")
    public List<JsonObjectList> jsonObjectList;
    @Expose
    @SerializedName("actionMap")
    public ActionMap actionMap;
    @Expose
    @SerializedName("vinModelList")
    public List<VinModelList> vinModelList;
    @Expose
    @SerializedName("imeiNumber")
    public String imeiNumber;
    @Expose
    @SerializedName("deviceModel")
    public String deviceModel;
    @Expose
    @SerializedName("osName")
    public String osName;
    @Expose
    @SerializedName("appVersion")
    public String appVersion;
    @Expose
    @SerializedName("mobileNumber")
    public String mobileNumber;
    @Expose
    @SerializedName("customerId")
    public int customerId;
    @Expose
    @SerializedName("userName")
    public String userName;

    protected ServiceDetailsData(Parcel in) {
        categoryList = in.createTypedArrayList(CategoryList.CREATOR);
        appoinmentAvailabilityList = in.createTypedArrayList(AppoinmentAvailabilityList.CREATOR);
        jsonObjectList = in.readParcelable(JsonObjectList.class.getClassLoader());
        actionMap = in.readParcelable(ActionMap.class.getClassLoader());
        vinModelList = in.createTypedArrayList(VinModelList.CREATOR);
        imeiNumber = in.readString();
        deviceModel = in.readString();
        osName = in.readString();
        appVersion = in.readString();
        mobileNumber = in.readString();
        customerId = in.readInt();
        userName = in.readString();
    }

    public static final Creator<ServiceDetailsData> CREATOR = new Creator<ServiceDetailsData>() {
        @Override
        public ServiceDetailsData createFromParcel(Parcel in) {
            return new ServiceDetailsData(in);
        }

        @Override
        public ServiceDetailsData[] newArray(int size) {
            return new ServiceDetailsData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(categoryList);
        parcel.writeTypedList(appoinmentAvailabilityList);
        parcel.writeTypedList(jsonObjectList);
        parcel.writeParcelable(actionMap, i);
        parcel.writeTypedList(vinModelList);
        parcel.writeString(imeiNumber);
        parcel.writeString(deviceModel);
        parcel.writeString(osName);
        parcel.writeString(appVersion);
        parcel.writeString(mobileNumber);
        parcel.writeInt(customerId);
        parcel.writeString(userName);
    }

    public static class AppoinmentAvailabilityList implements Parcelable{
        @Expose
        @SerializedName("avlblTime")
        public String avlblTime;

        protected AppoinmentAvailabilityList(Parcel in) {
            avlblTime = in.readString();
        }

        public static final Creator<AppoinmentAvailabilityList> CREATOR = new Creator<AppoinmentAvailabilityList>() {
            @Override
            public AppoinmentAvailabilityList createFromParcel(Parcel in) {
                return new AppoinmentAvailabilityList(in);
            }

            @Override
            public AppoinmentAvailabilityList[] newArray(int size) {
                return new AppoinmentAvailabilityList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(avlblTime);
        }
    }

    public static class JsonObjectList implements Parcelable{
        @Expose
        @SerializedName("SourceType")
        public String SourceType;

        protected JsonObjectList(Parcel in) {
            SourceType = in.readString();

        }

        public static final Creator<JsonObjectList> CREATOR = new Creator<JsonObjectList>() {
            @Override
            public JsonObjectList createFromParcel(Parcel in) {
                return new JsonObjectList(in);
            }

            @Override
            public JsonObjectList[] newArray(int size) {
                return new JsonObjectList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(SourceType);

        }
    }

    public static class ActionMap implements Parcelable{
        @Expose
        @SerializedName("RE_SCHEDULE")
        public String RE_SCHEDULE;
        @Expose
        @SerializedName("NEW_APPOINTMENT")
        public String NEW_APPOINTMENT;
        @Expose
        @SerializedName("CANCEL")
        public String CANCEL;

        protected ActionMap(Parcel in) {
            RE_SCHEDULE = in.readString();
            NEW_APPOINTMENT = in.readString();
            CANCEL = in.readString();
        }

        public static final Creator<ActionMap> CREATOR = new Creator<ActionMap>() {
            @Override
            public ActionMap createFromParcel(Parcel in) {
                return new ActionMap(in);
            }

            @Override
            public ActionMap[] newArray(int size) {
                return new ActionMap[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(RE_SCHEDULE);
            parcel.writeString(NEW_APPOINTMENT);
            parcel.writeString(CANCEL);
        }
    }

    public static class VinModelList implements Parcelable{
        @Expose
        @SerializedName("chassisNo")
        public String chassisNo;
        @Expose
        @SerializedName("registrationNo")
        public String registrationNo;

        protected VinModelList(Parcel in) {
            chassisNo = in.readString();
            registrationNo = in.readString();
        }

        public static final Creator<VinModelList> CREATOR = new Creator<VinModelList>() {
            @Override
            public VinModelList createFromParcel(Parcel in) {
                return new VinModelList(in);
            }

            @Override
            public VinModelList[] newArray(int size) {
                return new VinModelList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(chassisNo);
            parcel.writeString(registrationNo);
        }
    }

    public static class CategoryList implements Parcelable{
            @Expose
        @SerializedName("serviceCategoryId")
        public String serviceCategoryId;
        @Expose
        @SerializedName("categoryDesc")
        public String categoryDesc;

        protected CategoryList(Parcel in) {
            serviceCategoryId = in.readString();
            categoryDesc = in.readString();
        }

        public static final Creator<CategoryList> CREATOR = new Creator<CategoryList>() {
            @Override
            public CategoryList createFromParcel(Parcel in) {
                return new CategoryList(in);
            }

            @Override
            public CategoryList[] newArray(int size) {
                return new CategoryList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(serviceCategoryId);
            parcel.writeString(categoryDesc);
        }
    }
}